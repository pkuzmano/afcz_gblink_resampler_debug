# GBLink/Resampler debug project for the AFCZ with 8-SFP RTM (25.10.2019.)

## Firmware
This project is meant to provide a complete test of the GBLink IP. In addition, input DSP channels are driven by the Resampler IP, which is also tested in this way.
The idea is to also implement the phase-locked loop for regulating the number of data/buckets per turn on the SIS card, and the migrate it to this project once fully operational.

## Software
Memory maps of GBLink, Resamplers (4 in total, I/Q for CH1/CH2) and NCO emulators (2 in total, for CH1/CH2) can all be accessed via Zynq.
In the Beam Control project, this control/monitoring will be done via PCIe over uTCA.
Resamplers: configurable Tout (ratio inversed), number of buckets (~ftw), k (proportional constant for the phase-locked loop), step (for step response testing) and controls (recoveryEnable, loopEnable, stepLoad).
NCO emulators: configurable frev/sync period and offset (for misaligning CH1/CH2 frevs)

## System clock

create_clock -period 8.000 -name gth_sysclk [get_ports gth_sysclkp_i]
set_clock_groups -asynchronous -group [get_clocks gth_sysclk -include_generated_clocks]

set_property PACKAGE_PIN H23 [get_ports gth_sysclkp_i]
set_property IOSTANDARD LVDS [get_ports gth_sysclkp_i]

## Refclk

create_clock -period 8.000 -name gth_q228_refclk0 [get_ports gth_q228_refclk0p_i]
set_clock_groups -asynchronous -group [get_clocks gth_q228_refclk0 -include_generated_clocks]

set_property PACKAGE_PIN U11 [get_ports gth_q228_refclk0n_i]
set_property PACKAGE_PIN U12 [get_ports gth_q228_refclk0p_i]

## Status LEDs

#set_property PACKAGE_PIN AW14 [get_ports ]
#set_property IOSTANDARD LVCMOS18 [get_ports ]

#set_property PACKAGE_PIN AW11 [get_ports ]
#set_property IOSTANDARD LVCMOS18 [get_ports ]

## Transceiver pins, Q228
###set_property PACKAGE_PIN M6 [get_ports gth_q228_txp_o[0]]
###set_property PACKAGE_PIN M5 [get_ports gth_q228_txn_o[0]]
## TX
#set_property PACKAGE_PIN L8 [get_ports gth_q228_txp_o[1]]
#set_property PACKAGE_PIN L7 [get_ports gth_q228_txn_o[1]]
#set_property PACKAGE_PIN K6 [get_ports gth_q228_txp_o[2]]
#set_property PACKAGE_PIN K5 [get_ports gth_q228_txn_o[2]]
#set_property PACKAGE_PIN J8 [get_ports gth_q228_txp_o[3]]
#set_property PACKAGE_PIN J7 [get_ports gth_q228_txn_o[3]]

## RX
###set_property PACKAGE_PIN M2 [get_ports gth_q228_rxp_i[0]]
###set_property PACKAGE_PIN M1 [get_ports gth_q228_rxn_i[0]]
#set_property PACKAGE_PIN L4 [get_ports gth_q228_rxp_i[1]]
#set_property PACKAGE_PIN L3 [get_ports gth_q228_rxn_i[1]]
#set_property PACKAGE_PIN K2 [get_ports gth_q228_rxp_i[2]]
#set_property PACKAGE_PIN K1 [get_ports gth_q228_rxn_i[2]]
#set_property PACKAGE_PIN J4 [get_ports gth_q228_rxp_i[3]]
#set_property PACKAGE_PIN J3 [get_ports gth_q228_rxn_i[3]]

## Transceiver pins, Q227

## TX
#set_property PACKAGE_PIN T6 [get_ports gth_q227_txp_o[0]]
#set_property PACKAGE_PIN T5 [get_ports gth_q227_txn_o[0]]
#set_property PACKAGE_PIN R8 [get_ports gth_q227_txp_o[1]]
#set_property PACKAGE_PIN R7 [get_ports gth_q227_txn_o[1]]
#set_property PACKAGE_PIN P6 [get_ports gth_q227_txp_o[2]]
#set_property PACKAGE_PIN P5 [get_ports gth_q227_txn_o[2]]
#set_property PACKAGE_PIN N8 [get_ports gth_q227_txp_o[3]]
#set_property PACKAGE_PIN N7 [get_ports gth_q227_txn_o[3]]

## RX
#set_property PACKAGE_PIN T2 [get_ports gth_q227_rxp_i[0]]
#set_property PACKAGE_PIN T1 [get_ports gth_q227_rxn_i[0]]
#set_property PACKAGE_PIN R4 [get_ports gth_q227_rxp_i[1]]
#set_property PACKAGE_PIN R3 [get_ports gth_q227_rxn_i[1]]
#set_property PACKAGE_PIN P2 [get_ports gth_q227_rxp_i[2]]
#set_property PACKAGE_PIN P1 [get_ports gth_q227_rxn_i[2]]
#set_property PACKAGE_PIN N4 [get_ports gth_q227_rxp_i[3]]
#set_property PACKAGE_PIN N3 [get_ports gth_q227_rxn_i[3]]

##set_property C_CLK_INPUT_FREQ_HZ 78125000 [get_debug_cores dbg_hub]
#set_property C_CLK_INPUT_FREQ_HZ 156250000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets top_level_i/clk_wiz_1_clk_out1]
##connect_debug_port dbg_hub/clk [get_nets top_level_i/zynq_ultra_ps_e_0_pl_clk0]

#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]


#create_debug_core u_ila_0 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
#set_property C_DATA_DEPTH 8192 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
#set_property port_width 1 [get_debug_ports u_ila_0/clk]
#connect_debug_port u_ila_0/clk [get_nets [list top_level_i/clk_wiz_1/inst/clk_out1]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
#set_property port_width 16 [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[15]}]]
#create_debug_core u_ila_1 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_1]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1]
#set_property C_DATA_DEPTH 8192 [get_debug_cores u_ila_1]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_1]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_1]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1]
#set_property port_width 1 [get_debug_ports u_ila_1/clk]
#connect_debug_port u_ila_1/clk [get_nets [list top_level_i/clk_wiz_1/inst/clk_out2]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe0]
#set_property port_width 32 [get_debug_ports u_ila_1/probe0]
#connect_debug_port u_ila_1/probe0 [get_nets [list {top_level_i/GBLink_0/s_tx_dsp_ch1_data[0]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[1]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[2]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[3]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[4]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[5]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[6]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[7]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[8]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[9]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[10]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[11]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[12]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[13]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[14]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[15]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[16]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[17]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[18]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[19]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[20]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[21]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[22]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[23]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[24]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[25]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[26]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[27]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[28]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[29]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[30]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[31]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
#set_property port_width 2 [get_debug_ports u_ila_0/probe1]
#connect_debug_port u_ila_0/probe1 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tdata[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tdata[1]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
#set_property port_width 64 [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[35]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[36]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[37]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[38]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[39]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[40]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[41]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[42]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[43]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[44]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[45]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[46]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[47]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[48]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[49]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[50]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[51]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[52]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[53]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[54]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[55]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[56]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[57]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[58]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[59]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[60]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[61]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[62]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[63]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
#set_property port_width 1 [get_debug_ports u_ila_0/probe3]
#connect_debug_port u_ila_0/probe3 [get_nets [list top_level_i/GBLink_0/s_tx_dsp_ch1_frev]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
#set_property port_width 1 [get_debug_ports u_ila_0/probe4]
#connect_debug_port u_ila_0/probe4 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txControls_general_enable_del]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
#set_property port_width 1 [get_debug_ports u_ila_0/probe5]
#connect_debug_port u_ila_0/probe5 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txControls_general_reset_del]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
#set_property port_width 1 [get_debug_ports u_ila_0/probe6]
#connect_debug_port u_ila_0/probe6 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tvalid]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
#set_property port_width 1 [get_debug_ports u_ila_0/probe7]
#connect_debug_port u_ila_0/probe7 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tvalid]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
#set_property port_width 1 [get_debug_ports u_ila_0/probe8]
#connect_debug_port u_ila_0/probe8 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tready]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
#set_property port_width 1 [get_debug_ports u_ila_0/probe9]
#connect_debug_port u_ila_0/probe9 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tready]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe1]
#set_property port_width 64 [get_debug_ports u_ila_1/probe1]
#connect_debug_port u_ila_1/probe1 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[35]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[36]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[37]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[38]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[39]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[40]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[41]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[42]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[43]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[44]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[45]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[46]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[47]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[48]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[49]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[50]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[51]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[52]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[53]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[54]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[55]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[56]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[57]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[58]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[59]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[60]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[61]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[62]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[63]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe2]
#set_property port_width 16 [get_debug_ports u_ila_1/probe2]
#connect_debug_port u_ila_1/probe2 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_4[15]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe3]
#set_property port_width 4 [get_debug_ports u_ila_1/probe3]
#connect_debug_port u_ila_1/probe3 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[3]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe4]
#set_property port_width 3 [get_debug_ports u_ila_1/probe4]
#connect_debug_port u_ila_1/probe4 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[3]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe5]
#set_property port_width 36 [get_debug_ports u_ila_1/probe5]
#connect_debug_port u_ila_1/probe5 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[35]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe6]
#set_property port_width 36 [get_debug_ports u_ila_1/probe6]
#connect_debug_port u_ila_1/probe6 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[35]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe7]
#set_property port_width 1 [get_debug_ports u_ila_1/probe7]
#connect_debug_port u_ila_1/probe7 [get_nets [list top_level_i/GBLink_0/s_tx_dsp_ch1_valid]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe8]
#set_property port_width 1 [get_debug_ports u_ila_1/probe8]
#connect_debug_port u_ila_1/probe8 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/rd_en]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe9]
#set_property port_width 1 [get_debug_ports u_ila_1/probe9]
#connect_debug_port u_ila_1/probe9 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/srst]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe10]
#set_property port_width 1 [get_debug_ports u_ila_1/probe10]
#connect_debug_port u_ila_1/probe10 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/valid]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe11]
#set_property port_width 1 [get_debug_ports u_ila_1/probe11]
#connect_debug_port u_ila_1/probe11 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/wr_ack]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe12]
#set_property port_width 1 [get_debug_ports u_ila_1/probe12]
#connect_debug_port u_ila_1/probe12 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/wr_en]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe13]
#set_property port_width 1 [get_debug_ports u_ila_1/probe13]
#connect_debug_port u_ila_1/probe13 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_done_reg_n_0_[1]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe14]
#set_property port_width 1 [get_debug_ports u_ila_1/probe14]
#connect_debug_port u_ila_1/probe14 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_done_msw_ch1_reg_n_0]]
#set_property C_CLK_INPUT_FREQ_HZ 250000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets clk]

#create_debug_core u_ila_0 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
##set_property C_DATA_DEPTH 8192 [get_debug_cores u_ila_0]
#set_property C_DATA_DEPTH 32768 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]

#set_property port_width 1 [get_debug_ports u_ila_0/clk]
##connect_debug_port u_ila_0/clk [get_nets [list top_level_i/clk_wiz_1/inst/clk_out2]]
#connect_debug_port u_ila_0/clk [get_nets [list top_level_i/clk_wiz_1/clk_250]]

#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
#set_property port_width 16 [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/packets_sent_counter_reg__0[15]}]]

##create_debug_core u_ila_1 ila
##set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1]
##set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_1]
##set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1]
##set_property C_DATA_DEPTH 8192 [get_debug_cores u_ila_1]
##set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_1]
##set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1]
##set_property C_TRIGIN_EN false [get_debug_cores u_ila_1]
##set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1]
##set_property port_width 1 [get_debug_ports u_ila_1/clk]
##connect_debug_port u_ila_1/clk [get_nets [list top_level_i/clk_wiz_1/inst/clk_out2]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
#set_property port_width 32 [get_debug_ports u_ila_0/probe1]
#connect_debug_port u_ila_0/probe1 [get_nets [list {top_level_i/GBLink_0/s_tx_dsp_ch1_data[0]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[1]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[2]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[3]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[4]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[5]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[6]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[7]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[8]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[9]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[10]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[11]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[12]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[13]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[14]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[15]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[16]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[17]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[18]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[19]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[20]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[21]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[22]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[23]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[24]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[25]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[26]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[27]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[28]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[29]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[30]} {top_level_i/GBLink_0/s_tx_dsp_ch1_data[31]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
#set_property port_width 2 [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tdata[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tdata[1]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
#set_property port_width 64 [get_debug_ports u_ila_0/probe3]
#connect_debug_port u_ila_0/probe3 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[35]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[36]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[37]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[38]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[39]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[40]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[41]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[42]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[43]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[44]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[45]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[46]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[47]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[48]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[49]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[50]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[51]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[52]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[53]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[54]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[55]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[56]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[57]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[58]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[59]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[60]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[61]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[62]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tdata[63]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
#set_property port_width 1 [get_debug_ports u_ila_0/probe4]
#connect_debug_port u_ila_0/probe4 [get_nets [list top_level_i/GBLink_0/s_tx_dsp_ch1_frev]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
#set_property port_width 1 [get_debug_ports u_ila_0/probe5]
#connect_debug_port u_ila_0/probe5 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txControls_general_enable_del]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
#set_property port_width 1 [get_debug_ports u_ila_0/probe6]
#connect_debug_port u_ila_0/probe6 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txControls_general_reset_del]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
#set_property port_width 1 [get_debug_ports u_ila_0/probe7]
#connect_debug_port u_ila_0/probe7 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tvalid]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
#set_property port_width 1 [get_debug_ports u_ila_0/probe8]
#connect_debug_port u_ila_0/probe8 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tvalid]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
#set_property port_width 1 [get_debug_ports u_ila_0/probe9]
#connect_debug_port u_ila_0/probe9 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_data_tready]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
#set_property port_width 1 [get_debug_ports u_ila_0/probe10]
#connect_debug_port u_ila_0/probe10 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/m_axis_user_k_tready]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
#set_property port_width 64 [get_debug_ports u_ila_0/probe11]
#connect_debug_port u_ila_0/probe11 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[35]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[36]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[37]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[38]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[39]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[40]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[41]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[42]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[43]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[44]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[45]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[46]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[47]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[48]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[49]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[50]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[51]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[52]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[53]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[54]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[55]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[56]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[57]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[58]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[59]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[60]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[61]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[62]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/tx_data_buffer[63]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
#set_property port_width 16 [get_debug_ports u_ila_0/probe12]
#connect_debug_port u_ila_0/probe12 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_counter_reg[1]_2[15]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
#set_property port_width 4 [get_debug_ports u_ila_0/probe13]
#connect_debug_port u_ila_0/probe13 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fsm_state[3]}]]

##create_debug_port u_ila_0 probe
##set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
##set_property port_width 3 [get_debug_ports u_ila_0/probe14]
##connect_debug_port u_ila_0/probe14 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[3]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
#set_property port_width 2 [get_debug_ports u_ila_0/probe14]
#connect_debug_port u_ila_0/probe14 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/gblink_tx_fifo_ch1_state[1]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
#set_property port_width 36 [get_debug_ports u_ila_0/probe15]
#connect_debug_port u_ila_0/probe15 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/dout[35]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
#set_property port_width 36 [get_debug_ports u_ila_0/probe16]
#connect_debug_port u_ila_0/probe16 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[15]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[16]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[17]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[18]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[19]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[20]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[21]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[22]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[23]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[24]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[25]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[26]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[27]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[28]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[29]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[30]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[31]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[32]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[33]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[34]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/din[35]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe17]
#set_property port_width 1 [get_debug_ports u_ila_0/probe17]
#connect_debug_port u_ila_0/probe17 [get_nets [list top_level_i/GBLink_0/s_tx_dsp_ch1_valid]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe18]
#set_property port_width 1 [get_debug_ports u_ila_0/probe18]
#connect_debug_port u_ila_0/probe18 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/rd_en]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe19]
#set_property port_width 1 [get_debug_ports u_ila_0/probe19]
#connect_debug_port u_ila_0/probe19 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/srst]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe20]
#set_property port_width 1 [get_debug_ports u_ila_0/probe20]
#connect_debug_port u_ila_0/probe20 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/valid]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe21]
#set_property port_width 1 [get_debug_ports u_ila_0/probe21]
#connect_debug_port u_ila_0/probe21 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/wr_ack]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe22]
#set_property port_width 1 [get_debug_ports u_ila_0/probe22]
#connect_debug_port u_ila_0/probe22 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/wr_en]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe23]
#set_property port_width 1 [get_debug_ports u_ila_0/probe23]
#connect_debug_port u_ila_0/probe23 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_done_reg_n_0_[1]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe24]
#set_property port_width 1 [get_debug_ports u_ila_0/probe24]
#connect_debug_port u_ila_0/probe24 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/data_read_done_msw_ch1_reg_n_0]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe25]
#set_property port_width 1 [get_debug_ports u_ila_0/probe25]
#connect_debug_port u_ila_0/probe25 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/wr_rst_busy]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe26]
#set_property port_width 1 [get_debug_ports u_ila_0/probe26]
#connect_debug_port u_ila_0/probe26 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/rd_rst_busy]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe27]
#set_property port_width 16 [get_debug_ports u_ila_0/probe27]
#connect_debug_port u_ila_0/probe27 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumData[15]}]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe28]
#set_property port_width 1 [get_debug_ports u_ila_0/probe28]
#connect_debug_port u_ila_0/probe28 [get_nets [list top_level_i/GBLink_0/U0/gblink_tx_logic_inst/fifo_generator_gblink_tx_ch1_inst/empty]]

#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe29]
#set_property port_width 16 [get_debug_ports u_ila_0/probe29]
#connect_debug_port u_ila_0/probe29 [get_nets [list {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[0]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[1]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[2]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[3]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[4]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[5]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[6]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[7]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[8]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[9]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[10]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[11]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[12]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[13]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[14]} {top_level_i/GBLink_0/U0/gblink_tx_logic_inst/txStatus_fifoCh1NumDataTurn[15]}]]

#set_property C_CLK_INPUT_FREQ_HZ 250000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
##connect_debug_port dbg_hub/clk [get_nets top_level_i/clk_wiz_1_clk_out2]
#connect_debug_port dbg_hub/clk [get_nets top_level_i/clk_wiz_1/clk_250]

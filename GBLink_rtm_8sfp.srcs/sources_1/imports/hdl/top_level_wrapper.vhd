--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4.1 (win64) Build 2117270 Tue Jan 30 15:32:00 MST 2018
--Date        : Tue Apr 23 18:38:27 2019
--Host        : pccag4034 running 64-bit major release  (build 9200)
--Command     : generate_target top_level_wrapper.bd
--Design      : top_level_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top_level_wrapper is
  port (
    gth_sysclkp_i : in STD_LOGIC;
    gth_sysclkn_i : in STD_LOGIC;
    gth_q228_refclk0p_i : in STD_LOGIC;
    gth_q228_refclk0n_i : in STD_LOGIC
--    gth_q228_rxn_i : in STD_LOGIC_VECTOR ( 0 to 0 );
--    gth_q228_rxp_i : in STD_LOGIC_VECTOR ( 0 to 0 );
--    gth_q228_txn_o : out STD_LOGIC_VECTOR ( 0 to 0 );
--    gth_q228_txp_o : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
end top_level_wrapper;

architecture STRUCTURE of top_level_wrapper is
  component top_level is
  port (
    gth_sysclk_i : in STD_LOGIC
--    gth_refclk_i : in STD_LOGIC;
--    gth_rxn_i : in STD_LOGIC_VECTOR ( 0 to 0 );
--    gth_rxp_i : in STD_LOGIC_VECTOR ( 0 to 0 );
--    gth_txn_o : out STD_LOGIC_VECTOR ( 0 to 0 );
--    gth_txp_o : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level;

signal gth_sysclk_i : STD_LOGIC;
signal gth_refclk_i : STD_LOGIC;

begin

--
-- System clock IBUFGDS instantiation
--
IBUFGDS_gth_syslck_inst : IBUFGDS
generic map (
  DIFF_TERM => False
)
port map (
  I =>  gth_sysclkp_i,
  IB => gth_sysclkn_i,
  O =>  gth_sysclk_i
);

--
-- Refclk IBUFDS instantiation
--
IBUFDS_GTE4_gth_refclk_ints : IBUFDS_GTE4
port map (
  O => gth_refclk_i,
  ODIV2 => open,
  CEB => '0',
  I => gth_q228_refclk0p_i,
  IB => gth_q228_refclk0n_i
);

top_level_i: component top_level
     port map (
--      gth_refclk_i => gth_refclk_i,
      gth_sysclk_i => gth_sysclk_i
--      gth_rxn_i => gth_q228_rxn_i,
--      gth_rxp_i => gth_q228_rxp_i,
--      gth_txn_o => gth_q228_txn_o,
--      gth_txp_o => gth_q228_txp_o
    );
end STRUCTURE;

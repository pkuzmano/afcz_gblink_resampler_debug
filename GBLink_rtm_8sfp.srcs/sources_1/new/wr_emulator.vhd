----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.05.2019 15:47:50
-- Design Name: 
-- Module Name: wr_emulator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity wr_emulator is
	port (
		clk_250 : in std_logic;
		resetn_250 : in std_logic;
		sync_offset_i : std_logic_vector(15 downto 0); -- 1 sync pulse after specified delay (in clock cycles)
		sync : out std_logic
	);
end wr_emulator;

architecture Behavioral of wr_emulator is

	type sync_state_t is (S0, S1);

	signal sync_state		: sync_state_t := S0;
	signal sync_state_next	: sync_state_t;

	signal sync_int : std_logic;

	constant C_SYNC_OFFSET_WIDTH : integer := 16;

	signal sync_counter			: std_logic_vector(C_SYNC_OFFSET_WIDTH-1 downto 0);

begin

	process (sync_state, sync_int) begin
	  case sync_state is
	    when S0 =>
		  if (sync_int = '1') then
		    sync_state_next <= S1;
		  else
		    sync_state_next <= S0;
		  end if;
		when S1 =>
		  sync_state_next <= S1;
		when others =>
		  null;
	  end case;
	end process;

	process (clk_250) begin
	  if (rising_edge(clk_250)) then
	    if (resetn_250 = '0') then
		  sync_state <= S0;
		else
		  sync_state <= sync_state_next;
		end if;
	  end if;
	end process;


	-- Generation of "sync"

	process (clk_250) begin
	  if (rising_edge(clk_250)) then
	    if (resetn_250 = '0') then
		  sync_counter <= (others => '0');
		  sync_int <= '0';
		elsif (sync_state = S0) then
		  sync_counter <= std_logic_vector(unsigned(sync_counter) + 1);
		  sync_int <= '0';
		  if (sync_counter = sync_offset_i) then
		    sync_int <= '1';
		  end if;
		else
		  sync_counter <= (others => '0');
		  sync_int <= '0';
		end if;
	  end if;
	end process;

	sync <= sync_int;

end Behavioral;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.05.2019 15:47:50
-- Design Name: 
-- Module Name: tx_dsp_emulator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tx_dsp_emulator is
	port (
		clk_125 : in std_logic;
		resetn_125 : in std_logic;
		clk_250 : in std_logic;
		resetn_250 : in std_logic;
		frev_counter_max_i : in std_logic_vector(15 downto 0); -- 1 frev pulse every (frev_counter_max_i + 1) clock cycles (clk_250, 4ns)
		frev_offset_i : in std_logic_vector(15 downto 0);
		m_tx_dsp_data : out std_logic_vector(31 downto 0);
		m_tx_dsp_valid : out std_logic;
		m_tx_dsp_frev : out std_logic;
		m_tx_dsp_turn_number : out std_logic_vector(31 downto 0) -- 29 should suffice for coast cycles
	);
end tx_dsp_emulator;

architecture Behavioral of tx_dsp_emulator is

	type valid_state_t is (S0, S1, S2, S3);

	signal read_pointer : integer range 0 to 7; -- data: 1 to 8

	signal valid_state		: valid_state_t := S0;
	signal valid_state_next	: valid_state_t;

	constant C_FREV_COUNTER_WIDTH : integer := 16;

	signal frev_counter			: std_logic_vector(C_FREV_COUNTER_WIDTH-1 downto 0);

begin

	-- Generation of "m_tx_dsp_valid"

	process (valid_state) begin
	  case valid_state is
	    when S0 =>
	      valid_state_next <= S1;
		  m_tx_dsp_valid <= '1';
	    when S1 =>
	      valid_state_next <= S2;
		  m_tx_dsp_valid <= '0';
	    when S2 =>
	      valid_state_next <= S3;
		  m_tx_dsp_valid <= '1';
	    when S3 =>
	      valid_state_next <= S0;
		  m_tx_dsp_valid <= '1';
		when others =>
		  null;
	  end case;
	end process;

	process (clk_250) begin
	  if (rising_edge(clk_250)) then
	    if (resetn_250 = '0') then
		  valid_state <= S0;
		else
		  valid_state <= valid_state_next;
		end if;
	  end if;
	end process;

	-- Generation of "m_tx_dsp_data"

	process (clk_250) begin
	  if (rising_edge(clk_250)) then
	    if (resetn_250 = '0') then
		  read_pointer <= 0;
		elsif (read_pointer = 7) then
		  read_pointer <= 0;
		else
		  read_pointer <= read_pointer + 1;
		end if;
	  end if;
	end process;
	
	process (clk_250)
	  variable sig_one : integer := 1;
	begin
	  if (rising_edge(clk_250)) then
	    if (resetn_250 = '0') then
		  m_tx_dsp_data <= std_logic_vector(to_unsigned(sig_one, 32));
		else
		  m_tx_dsp_data <= std_logic_vector(to_unsigned(read_pointer, 32) + to_unsigned(sig_one, 32));
		end if;
	  end if;
	end process;

	-- Generation of "m_tx_dsp_frev"

	process (clk_125) begin
	  if (rising_edge(clk_125)) then
	    if (resetn_125 = '0') then
		  frev_counter <= (others => '0');
		  m_tx_dsp_frev <= '0';
		else
		  frev_counter <= std_logic_vector(unsigned(frev_counter) + 1);
		  m_tx_dsp_frev <= '0';
		  if (frev_counter = std_logic_vector(unsigned(frev_counter_max_i) + unsigned(frev_offset_i))) then
		    m_tx_dsp_frev <= '1';
		  end if;
		end if;
	  end if;
	end process;

	-- Generation of "m_tx_dsp_turn_number"

	m_tx_dsp_turn_number <= (others => '0');

end Behavioral;

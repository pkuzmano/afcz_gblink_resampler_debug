----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.05.2019 15:47:50
-- Design Name: 
-- Module Name: tx_nco_emulator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tx_nco_emulator is
	port (
		clk : in std_logic;
		resetn : in std_logic;
		m_tx_nco_id : out std_logic_vector(7 downto 0) -- check what range is necessary
	);
end tx_nco_emulator;

architecture Behavioral of tx_nco_emulator is

begin

	-- Generation of "m_tx_nco_id"

	m_tx_nco_id <= (others => '0');

end Behavioral;

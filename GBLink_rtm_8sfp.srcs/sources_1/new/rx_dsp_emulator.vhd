----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.05.2019 15:47:50
-- Design Name: 
-- Module Name: rx_dsp_emulator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rx_dsp_emulator is
	port (
		clk : in std_logic;
		resetn : in std_logic;
		m_rx_dsp_read : out std_logic;
		m_rx_dsp_valid : in std_logic;
		m_rx_dsp_data : in std_logic_vector(31 downto 0);
		m_rx_dsp_frev_flag : in std_logic;
		m_rx_dsp_full_error : in std_logic;
		m_rx_dsp_turn_error : in std_logic;
		m_rx_id : in std_logic_vector(7 downto 0); -- check what range is necessary
		m_rx_dsp_bucket_index : in std_logic_vector(15 downto 0)
	);
end rx_dsp_emulator;

architecture Behavioral of rx_dsp_emulator is

begin

	-- Generation of "m_rx_dsp_read"

	m_rx_dsp_read <= '1';

end Behavioral;

-- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: cern.ch:user:GBLink:1.0
-- IP Revision: 58

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY top_level_GBLink_0_0 IS
  PORT (
    aurora_reset_pb : OUT STD_LOGIC;
    aurora_pma_init : OUT STD_LOGIC;
    aurora_link_reset_out : IN STD_LOGIC;
    aurora_sys_reset_out : IN STD_LOGIC;
    aurora_gt_pll_lock : IN STD_LOGIC;
    aurora_lane_up : IN STD_LOGIC;
    aurora_channel_up : IN STD_LOGIC;
    aurora_hard_err : IN STD_LOGIC;
    aurora_soft_err : IN STD_LOGIC;
    user_clk_slower : IN STD_LOGIC;
    user_rst_slower : IN STD_LOGIC;
    user_clk_faster : IN STD_LOGIC;
    user_rst_faster : IN STD_LOGIC;
    s_tx_dsp_ch1_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_tx_dsp_ch1_valid : IN STD_LOGIC;
    s_tx_dsp_ch1_frev : IN STD_LOGIC;
    s_tx_dsp_ch1_turn_number : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_tx_dsp_ch2_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_tx_dsp_ch2_valid : IN STD_LOGIC;
    s_tx_dsp_ch2_frev : IN STD_LOGIC;
    s_tx_dsp_ch2_turn_number : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_tx_nco_id : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    sync_tx : IN STD_LOGIC;
    sync_rx : IN STD_LOGIC;
    s_rx_dsp_ch1_read : IN STD_LOGIC;
    s_rx_dsp_ch1_valid : OUT STD_LOGIC;
    s_rx_dsp_ch1_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_rx_dsp_ch1_frev_flag : OUT STD_LOGIC;
    s_rx_dsp_ch1_bucket_index : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_rx_dsp_ch1_full_error : OUT STD_LOGIC;
    s_rx_dsp_ch1_turn_error : OUT STD_LOGIC;
    s_rx_ch1_id : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_rx_dsp_ch2_read : IN STD_LOGIC;
    s_rx_dsp_ch2_valid : OUT STD_LOGIC;
    s_rx_dsp_ch2_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_rx_dsp_ch2_frev_flag : OUT STD_LOGIC;
    s_rx_dsp_ch2_bucket_index : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_rx_dsp_ch2_full_error : OUT STD_LOGIC;
    s_rx_dsp_ch2_turn_error : OUT STD_LOGIC;
    s_rx_ch2_id : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_aclk : IN STD_LOGIC;
    s_axi_aresetn : IN STD_LOGIC;
    s_axi_awaddr : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awvalid : IN STD_LOGIC;
    s_axi_awready : OUT STD_LOGIC;
    s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_wvalid : IN STD_LOGIC;
    s_axi_wready : OUT STD_LOGIC;
    s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bvalid : OUT STD_LOGIC;
    s_axi_bready : IN STD_LOGIC;
    s_axi_araddr : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arvalid : IN STD_LOGIC;
    s_axi_arready : OUT STD_LOGIC;
    s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_rvalid : OUT STD_LOGIC;
    s_axi_rready : IN STD_LOGIC;
    m_axis_data_aclk : IN STD_LOGIC;
    m_axis_data_aresetn : IN STD_LOGIC;
    m_axis_data_tvalid : OUT STD_LOGIC;
    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_data_tstrb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_data_tlast : OUT STD_LOGIC;
    m_axis_data_tready : IN STD_LOGIC;
    m_axis_user_k_aclk : IN STD_LOGIC;
    m_axis_user_k_aresetn : IN STD_LOGIC;
    m_axis_user_k_tvalid : OUT STD_LOGIC;
    m_axis_user_k_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_user_k_tstrb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_user_k_tlast : OUT STD_LOGIC;
    m_axis_user_k_tready : IN STD_LOGIC;
    s_axis_data_aclk : IN STD_LOGIC;
    s_axis_data_aresetn : IN STD_LOGIC;
    s_axis_data_tready : OUT STD_LOGIC;
    s_axis_data_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_data_tstrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_data_tlast : IN STD_LOGIC;
    s_axis_data_tvalid : IN STD_LOGIC;
    s_axis_user_k_aclk : IN STD_LOGIC;
    s_axis_user_k_aresetn : IN STD_LOGIC;
    s_axis_user_k_tready : OUT STD_LOGIC;
    s_axis_user_k_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_user_k_tstrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_user_k_tlast : IN STD_LOGIC;
    s_axis_user_k_tvalid : IN STD_LOGIC
  );
END top_level_GBLink_0_0;

ARCHITECTURE top_level_GBLink_0_0_arch OF top_level_GBLink_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF top_level_GBLink_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT GBLink_v1_0 IS
    GENERIC (
      C_S_AXI_DATA_WIDTH : INTEGER; -- Width of S_AXI data bus
      C_S_AXI_ADDR_WIDTH : INTEGER; -- Width of S_AXI address bus
      C_M_AXIS_USER_K_TDATA_WIDTH : INTEGER; -- Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
      C_M_AXIS_USER_K_START_COUNT : INTEGER; -- Start count is the number of clock cycles the master will wait before initiating/issuing any transaction.
      C_S_AXIS_DATA_TDATA_WIDTH : INTEGER; -- AXI4Stream sink: Data Width
      C_S_AXIS_USER_K_TDATA_WIDTH : INTEGER; -- AXI4Stream sink: Data Width
      C_M_AXIS_DATA_TDATA_WIDTH : INTEGER; -- Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
      C_M_AXIS_DATA_START_COUNT : INTEGER -- Start count is the number of clock cycles the master will wait before initiating/issuing any transaction.
    );
    PORT (
      aurora_reset_pb : OUT STD_LOGIC;
      aurora_pma_init : OUT STD_LOGIC;
      aurora_link_reset_out : IN STD_LOGIC;
      aurora_sys_reset_out : IN STD_LOGIC;
      aurora_gt_pll_lock : IN STD_LOGIC;
      aurora_lane_up : IN STD_LOGIC;
      aurora_channel_up : IN STD_LOGIC;
      aurora_hard_err : IN STD_LOGIC;
      aurora_soft_err : IN STD_LOGIC;
      user_clk_slower : IN STD_LOGIC;
      user_rst_slower : IN STD_LOGIC;
      user_clk_faster : IN STD_LOGIC;
      user_rst_faster : IN STD_LOGIC;
      s_tx_dsp_ch1_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_tx_dsp_ch1_valid : IN STD_LOGIC;
      s_tx_dsp_ch1_frev : IN STD_LOGIC;
      s_tx_dsp_ch1_turn_number : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_tx_dsp_ch2_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_tx_dsp_ch2_valid : IN STD_LOGIC;
      s_tx_dsp_ch2_frev : IN STD_LOGIC;
      s_tx_dsp_ch2_turn_number : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_tx_nco_id : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      sync_tx : IN STD_LOGIC;
      sync_rx : IN STD_LOGIC;
      s_rx_dsp_ch1_read : IN STD_LOGIC;
      s_rx_dsp_ch1_valid : OUT STD_LOGIC;
      s_rx_dsp_ch1_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_rx_dsp_ch1_frev_flag : OUT STD_LOGIC;
      s_rx_dsp_ch1_bucket_index : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_rx_dsp_ch1_full_error : OUT STD_LOGIC;
      s_rx_dsp_ch1_turn_error : OUT STD_LOGIC;
      s_rx_ch1_id : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_rx_dsp_ch2_read : IN STD_LOGIC;
      s_rx_dsp_ch2_valid : OUT STD_LOGIC;
      s_rx_dsp_ch2_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_rx_dsp_ch2_frev_flag : OUT STD_LOGIC;
      s_rx_dsp_ch2_bucket_index : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_rx_dsp_ch2_full_error : OUT STD_LOGIC;
      s_rx_dsp_ch2_turn_error : OUT STD_LOGIC;
      s_rx_ch2_id : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axi_aclk : IN STD_LOGIC;
      s_axi_aresetn : IN STD_LOGIC;
      s_axi_awaddr : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s_axi_awvalid : IN STD_LOGIC;
      s_axi_awready : OUT STD_LOGIC;
      s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s_axi_wvalid : IN STD_LOGIC;
      s_axi_wready : OUT STD_LOGIC;
      s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_bvalid : OUT STD_LOGIC;
      s_axi_bready : IN STD_LOGIC;
      s_axi_araddr : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s_axi_arvalid : IN STD_LOGIC;
      s_axi_arready : OUT STD_LOGIC;
      s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_rvalid : OUT STD_LOGIC;
      s_axi_rready : IN STD_LOGIC;
      m_axis_data_aclk : IN STD_LOGIC;
      m_axis_data_aresetn : IN STD_LOGIC;
      m_axis_data_tvalid : OUT STD_LOGIC;
      m_axis_data_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_data_tstrb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_data_tlast : OUT STD_LOGIC;
      m_axis_data_tready : IN STD_LOGIC;
      m_axis_user_k_aclk : IN STD_LOGIC;
      m_axis_user_k_aresetn : IN STD_LOGIC;
      m_axis_user_k_tvalid : OUT STD_LOGIC;
      m_axis_user_k_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_user_k_tstrb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_user_k_tlast : OUT STD_LOGIC;
      m_axis_user_k_tready : IN STD_LOGIC;
      s_axis_data_aclk : IN STD_LOGIC;
      s_axis_data_aresetn : IN STD_LOGIC;
      s_axis_data_tready : OUT STD_LOGIC;
      s_axis_data_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_data_tstrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_data_tlast : IN STD_LOGIC;
      s_axis_data_tvalid : IN STD_LOGIC;
      s_axis_user_k_aclk : IN STD_LOGIC;
      s_axis_user_k_aresetn : IN STD_LOGIC;
      s_axis_user_k_tready : OUT STD_LOGIC;
      s_axis_user_k_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_user_k_tstrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_user_k_tlast : IN STD_LOGIC;
      s_axis_user_k_tvalid : IN STD_LOGIC
    );
  END COMPONENT GBLink_v1_0;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF top_level_GBLink_0_0_arch: ARCHITECTURE IS "GBLink_v1_0,Vivado 2017.4.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF top_level_GBLink_0_0_arch : ARCHITECTURE IS "top_level_GBLink_0_0,GBLink_v1_0,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_user_k_tvalid: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_USER_K TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_user_k_tlast: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_USER_K TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_user_k_tstrb: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_USER_K TSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_user_k_tdata: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_USER_K TDATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_user_k_tready: SIGNAL IS "XIL_INTERFACENAME S_AXIS_USER_K, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_user_k_tready: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_USER_K TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_user_k_aresetn: SIGNAL IS "XIL_INTERFACENAME S_AXIS_USER_K_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_user_k_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 S_AXIS_USER_K_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_user_k_aclk: SIGNAL IS "XIL_INTERFACENAME S_AXIS_USER_K_CLK, ASSOCIATED_BUSIF S_AXIS_USER_K, ASSOCIATED_RESET s_axis_user_k_aresetn, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_user_k_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 S_AXIS_USER_K_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_data_tvalid: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_DATA TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_data_tlast: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_DATA TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_data_tstrb: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_DATA TSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_data_tdata: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_DATA TDATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_data_tready: SIGNAL IS "XIL_INTERFACENAME S_AXIS_DATA, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_data_tready: SIGNAL IS "xilinx.com:interface:axis:1.0 S_AXIS_DATA TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_data_aresetn: SIGNAL IS "XIL_INTERFACENAME S_AXIS_DATA_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_data_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 S_AXIS_DATA_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_data_aclk: SIGNAL IS "XIL_INTERFACENAME S_AXIS_DATA_CLK, ASSOCIATED_BUSIF S_AXIS_DATA, ASSOCIATED_RESET s_axis_data_aresetn, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_data_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 S_AXIS_DATA_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_user_k_tready: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_USER_K TREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_user_k_tlast: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_USER_K TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_user_k_tstrb: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_USER_K TSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_user_k_tdata: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_USER_K TDATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_user_k_tvalid: SIGNAL IS "XIL_INTERFACENAME M_AXIS_USER_K, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_user_k_tvalid: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_USER_K TVALID";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_user_k_aresetn: SIGNAL IS "XIL_INTERFACENAME M_AXIS_USER_K_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_user_k_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 M_AXIS_USER_K_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_user_k_aclk: SIGNAL IS "XIL_INTERFACENAME M_AXIS_USER_K_CLK, ASSOCIATED_BUSIF M_AXIS_USER_K, ASSOCIATED_RESET m_axis_user_k_aresetn, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_user_k_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 M_AXIS_USER_K_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_data_tready: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_DATA TREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_data_tlast: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_DATA TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_data_tstrb: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_DATA TSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_data_tdata: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_DATA TDATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_data_tvalid: SIGNAL IS "XIL_INTERFACENAME M_AXIS_DATA, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_data_tvalid: SIGNAL IS "xilinx.com:interface:axis:1.0 M_AXIS_DATA TVALID";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_data_aresetn: SIGNAL IS "XIL_INTERFACENAME M_AXIS_DATA_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_data_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 M_AXIS_DATA_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_data_aclk: SIGNAL IS "XIL_INTERFACENAME M_AXIS_DATA_CLK, ASSOCIATED_BUSIF M_AXIS_DATA, ASSOCIATED_RESET m_axis_data_aresetn, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_data_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 M_AXIS_DATA_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWPROT";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_awaddr: SIGNAL IS "XIL_INTERFACENAME S_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 78125000, ID_WIDTH 0, ADDR_WIDTH 12, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN top_level_zynq_ultra_ps_e_0_0_pl_clk0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_aresetn: SIGNAL IS "XIL_INTERFACENAME S_AXI_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 S_AXI_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_aclk: SIGNAL IS "XIL_INTERFACENAME S_AXI_CLK, ASSOCIATED_BUSIF S_AXI, ASSOCIATED_RESET s_axi_aresetn, FREQ_HZ 78125000, PHASE 0.000, CLK_DOMAIN top_level_zynq_ultra_ps_e_0_0_pl_clk0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 S_AXI_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_ch2_id: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_id";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch2_turn_error: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_dsp_turn_error";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch2_full_error: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_dsp_full_error";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch2_bucket_index: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_dsp_bucket_index";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch2_frev_flag: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_dsp_frev_flag";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch2_data: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_dsp_data";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch2_valid: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_dsp_valid";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch2_read: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH2 rx_dsp_read";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_ch1_id: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_id";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch1_turn_error: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_dsp_turn_error";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch1_full_error: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_dsp_full_error";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch1_bucket_index: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_dsp_bucket_index";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch1_frev_flag: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_dsp_frev_flag";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch1_data: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_dsp_data";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch1_valid: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_dsp_valid";
  ATTRIBUTE X_INTERFACE_INFO OF s_rx_dsp_ch1_read: SIGNAL IS "cern.ch:user:RX_DSP:1.0 S_RX_DSP_CH1 rx_dsp_read";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_nco_id: SIGNAL IS "cern.ch:user:TX_NCO:1.0 S_TX_NCO tx_nco_id";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch2_turn_number: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH2 tx_dsp_turn_number";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch2_frev: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH2 tx_dsp_frev";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch2_valid: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH2 tx_dsp_valid";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch2_data: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH2 tx_dsp_data";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch1_turn_number: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH1 tx_dsp_turn_number";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch1_frev: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH1 tx_dsp_frev";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch1_valid: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH1 tx_dsp_valid";
  ATTRIBUTE X_INTERFACE_INFO OF s_tx_dsp_ch1_data: SIGNAL IS "cern.ch:user:TX_DSP:1.0 S_TX_DSP_CH1 tx_dsp_data";
  ATTRIBUTE X_INTERFACE_PARAMETER OF user_rst_faster: SIGNAL IS "XIL_INTERFACENAME user_rst_faster, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF user_rst_faster: SIGNAL IS "xilinx.com:signal:reset:1.0 user_rst_faster RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF user_clk_faster: SIGNAL IS "XIL_INTERFACENAME user_clk_faster, ASSOCIATED_RESET user_rst_faster, FREQ_HZ 250000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125";
  ATTRIBUTE X_INTERFACE_INFO OF user_clk_faster: SIGNAL IS "xilinx.com:signal:clock:1.0 user_clk_faster CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF user_rst_slower: SIGNAL IS "XIL_INTERFACENAME user_rst_slower, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF user_rst_slower: SIGNAL IS "xilinx.com:signal:reset:1.0 user_rst_slower RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF user_clk_slower: SIGNAL IS "XIL_INTERFACENAME user_clk_slower, ASSOCIATED_RESET user_rst_slower, FREQ_HZ 125000000, PHASE 0.0, CLK_DOMAIN top_level_clk_wiz_1_0_clk_125";
  ATTRIBUTE X_INTERFACE_INFO OF user_clk_slower: SIGNAL IS "xilinx.com:signal:clock:1.0 user_clk_slower CLK";
BEGIN
  U0 : GBLink_v1_0
    GENERIC MAP (
      C_S_AXI_DATA_WIDTH => 32,
      C_S_AXI_ADDR_WIDTH => 12,
      C_M_AXIS_USER_K_TDATA_WIDTH => 64,
      C_M_AXIS_USER_K_START_COUNT => 1024,
      C_S_AXIS_DATA_TDATA_WIDTH => 64,
      C_S_AXIS_USER_K_TDATA_WIDTH => 64,
      C_M_AXIS_DATA_TDATA_WIDTH => 64,
      C_M_AXIS_DATA_START_COUNT => 256
    )
    PORT MAP (
      aurora_reset_pb => aurora_reset_pb,
      aurora_pma_init => aurora_pma_init,
      aurora_link_reset_out => aurora_link_reset_out,
      aurora_sys_reset_out => aurora_sys_reset_out,
      aurora_gt_pll_lock => aurora_gt_pll_lock,
      aurora_lane_up => aurora_lane_up,
      aurora_channel_up => aurora_channel_up,
      aurora_hard_err => aurora_hard_err,
      aurora_soft_err => aurora_soft_err,
      user_clk_slower => user_clk_slower,
      user_rst_slower => user_rst_slower,
      user_clk_faster => user_clk_faster,
      user_rst_faster => user_rst_faster,
      s_tx_dsp_ch1_data => s_tx_dsp_ch1_data,
      s_tx_dsp_ch1_valid => s_tx_dsp_ch1_valid,
      s_tx_dsp_ch1_frev => s_tx_dsp_ch1_frev,
      s_tx_dsp_ch1_turn_number => s_tx_dsp_ch1_turn_number,
      s_tx_dsp_ch2_data => s_tx_dsp_ch2_data,
      s_tx_dsp_ch2_valid => s_tx_dsp_ch2_valid,
      s_tx_dsp_ch2_frev => s_tx_dsp_ch2_frev,
      s_tx_dsp_ch2_turn_number => s_tx_dsp_ch2_turn_number,
      s_tx_nco_id => s_tx_nco_id,
      sync_tx => sync_tx,
      sync_rx => sync_rx,
      s_rx_dsp_ch1_read => s_rx_dsp_ch1_read,
      s_rx_dsp_ch1_valid => s_rx_dsp_ch1_valid,
      s_rx_dsp_ch1_data => s_rx_dsp_ch1_data,
      s_rx_dsp_ch1_frev_flag => s_rx_dsp_ch1_frev_flag,
      s_rx_dsp_ch1_bucket_index => s_rx_dsp_ch1_bucket_index,
      s_rx_dsp_ch1_full_error => s_rx_dsp_ch1_full_error,
      s_rx_dsp_ch1_turn_error => s_rx_dsp_ch1_turn_error,
      s_rx_ch1_id => s_rx_ch1_id,
      s_rx_dsp_ch2_read => s_rx_dsp_ch2_read,
      s_rx_dsp_ch2_valid => s_rx_dsp_ch2_valid,
      s_rx_dsp_ch2_data => s_rx_dsp_ch2_data,
      s_rx_dsp_ch2_frev_flag => s_rx_dsp_ch2_frev_flag,
      s_rx_dsp_ch2_bucket_index => s_rx_dsp_ch2_bucket_index,
      s_rx_dsp_ch2_full_error => s_rx_dsp_ch2_full_error,
      s_rx_dsp_ch2_turn_error => s_rx_dsp_ch2_turn_error,
      s_rx_ch2_id => s_rx_ch2_id,
      s_axi_aclk => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_awaddr => s_axi_awaddr,
      s_axi_awprot => s_axi_awprot,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_awready => s_axi_awready,
      s_axi_wdata => s_axi_wdata,
      s_axi_wstrb => s_axi_wstrb,
      s_axi_wvalid => s_axi_wvalid,
      s_axi_wready => s_axi_wready,
      s_axi_bresp => s_axi_bresp,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_bready => s_axi_bready,
      s_axi_araddr => s_axi_araddr,
      s_axi_arprot => s_axi_arprot,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_arready => s_axi_arready,
      s_axi_rdata => s_axi_rdata,
      s_axi_rresp => s_axi_rresp,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_rready => s_axi_rready,
      m_axis_data_aclk => m_axis_data_aclk,
      m_axis_data_aresetn => m_axis_data_aresetn,
      m_axis_data_tvalid => m_axis_data_tvalid,
      m_axis_data_tdata => m_axis_data_tdata,
      m_axis_data_tstrb => m_axis_data_tstrb,
      m_axis_data_tlast => m_axis_data_tlast,
      m_axis_data_tready => m_axis_data_tready,
      m_axis_user_k_aclk => m_axis_user_k_aclk,
      m_axis_user_k_aresetn => m_axis_user_k_aresetn,
      m_axis_user_k_tvalid => m_axis_user_k_tvalid,
      m_axis_user_k_tdata => m_axis_user_k_tdata,
      m_axis_user_k_tstrb => m_axis_user_k_tstrb,
      m_axis_user_k_tlast => m_axis_user_k_tlast,
      m_axis_user_k_tready => m_axis_user_k_tready,
      s_axis_data_aclk => s_axis_data_aclk,
      s_axis_data_aresetn => s_axis_data_aresetn,
      s_axis_data_tready => s_axis_data_tready,
      s_axis_data_tdata => s_axis_data_tdata,
      s_axis_data_tstrb => s_axis_data_tstrb,
      s_axis_data_tlast => s_axis_data_tlast,
      s_axis_data_tvalid => s_axis_data_tvalid,
      s_axis_user_k_aclk => s_axis_user_k_aclk,
      s_axis_user_k_aresetn => s_axis_user_k_aresetn,
      s_axis_user_k_tready => s_axis_user_k_tready,
      s_axis_user_k_tdata => s_axis_user_k_tdata,
      s_axis_user_k_tstrb => s_axis_user_k_tstrb,
      s_axis_user_k_tlast => s_axis_user_k_tlast,
      s_axis_user_k_tvalid => s_axis_user_k_tvalid
    );
END top_level_GBLink_0_0_arch;

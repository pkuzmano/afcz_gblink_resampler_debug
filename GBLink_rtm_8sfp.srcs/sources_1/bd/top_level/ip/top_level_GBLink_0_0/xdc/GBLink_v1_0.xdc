create_clock -period 8.000 -name m_axis_data_aclk -waveform {0.000 4.000} [get_ports m_axis_data_aclk]
create_clock -period 8.000 -name m_axis_user_k_aclk -waveform {0.000 4.000} [get_ports m_axis_user_k_aclk]
create_clock -period 8.000 -name s_axi_aclk -waveform {0.000 4.000} [get_ports s_axi_aclk]
create_clock -period 4.000 -name user_clk_faster -waveform {0.000 2.000} [get_ports user_clk_faster]
create_clock -period 8.000 -name user_clk_slower -waveform {0.000 4.000} [get_ports user_clk_slower]

#set_false_path -through [get_nets gblink_tx_logic_inst/txControls*]
#set_false_path -through [get_nets gblink_rx_logic_inst/rxControls*]

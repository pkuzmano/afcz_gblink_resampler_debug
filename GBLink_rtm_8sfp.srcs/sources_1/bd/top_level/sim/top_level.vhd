--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4.1 (win64) Build 2117270 Tue Jan 30 15:32:00 MST 2018
--Date        : Fri Oct 25 14:12:28 2019
--Host        : pccag4034 running 64-bit major release  (build 9200)
--Command     : generate_target top_level.bd
--Design      : top_level
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity m00_couplers_imp_K308NP is
  port (
    M_ACLK : in STD_LOGIC;
    M_ARESETN : in STD_LOGIC;
    M_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_ACLK : in STD_LOGIC;
    S_ARESETN : in STD_LOGIC;
    S_AXI_araddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awaddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end m00_couplers_imp_K308NP;

architecture STRUCTURE of m00_couplers_imp_K308NP is
  signal m00_couplers_to_m00_couplers_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m00_couplers_to_m00_couplers_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m00_couplers_to_m00_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m00_couplers_to_m00_couplers_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m00_couplers_to_m00_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_m00_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_m00_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_m00_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_m00_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m00_couplers_to_m00_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  M_AXI_araddr(39 downto 0) <= m00_couplers_to_m00_couplers_ARADDR(39 downto 0);
  M_AXI_arprot(2 downto 0) <= m00_couplers_to_m00_couplers_ARPROT(2 downto 0);
  M_AXI_arvalid(0) <= m00_couplers_to_m00_couplers_ARVALID(0);
  M_AXI_awaddr(39 downto 0) <= m00_couplers_to_m00_couplers_AWADDR(39 downto 0);
  M_AXI_awprot(2 downto 0) <= m00_couplers_to_m00_couplers_AWPROT(2 downto 0);
  M_AXI_awvalid(0) <= m00_couplers_to_m00_couplers_AWVALID(0);
  M_AXI_bready(0) <= m00_couplers_to_m00_couplers_BREADY(0);
  M_AXI_rready(0) <= m00_couplers_to_m00_couplers_RREADY(0);
  M_AXI_wdata(31 downto 0) <= m00_couplers_to_m00_couplers_WDATA(31 downto 0);
  M_AXI_wstrb(3 downto 0) <= m00_couplers_to_m00_couplers_WSTRB(3 downto 0);
  M_AXI_wvalid(0) <= m00_couplers_to_m00_couplers_WVALID(0);
  S_AXI_arready(0) <= m00_couplers_to_m00_couplers_ARREADY(0);
  S_AXI_awready(0) <= m00_couplers_to_m00_couplers_AWREADY(0);
  S_AXI_bresp(1 downto 0) <= m00_couplers_to_m00_couplers_BRESP(1 downto 0);
  S_AXI_bvalid(0) <= m00_couplers_to_m00_couplers_BVALID(0);
  S_AXI_rdata(31 downto 0) <= m00_couplers_to_m00_couplers_RDATA(31 downto 0);
  S_AXI_rresp(1 downto 0) <= m00_couplers_to_m00_couplers_RRESP(1 downto 0);
  S_AXI_rvalid(0) <= m00_couplers_to_m00_couplers_RVALID(0);
  S_AXI_wready(0) <= m00_couplers_to_m00_couplers_WREADY(0);
  m00_couplers_to_m00_couplers_ARADDR(39 downto 0) <= S_AXI_araddr(39 downto 0);
  m00_couplers_to_m00_couplers_ARPROT(2 downto 0) <= S_AXI_arprot(2 downto 0);
  m00_couplers_to_m00_couplers_ARREADY(0) <= M_AXI_arready(0);
  m00_couplers_to_m00_couplers_ARVALID(0) <= S_AXI_arvalid(0);
  m00_couplers_to_m00_couplers_AWADDR(39 downto 0) <= S_AXI_awaddr(39 downto 0);
  m00_couplers_to_m00_couplers_AWPROT(2 downto 0) <= S_AXI_awprot(2 downto 0);
  m00_couplers_to_m00_couplers_AWREADY(0) <= M_AXI_awready(0);
  m00_couplers_to_m00_couplers_AWVALID(0) <= S_AXI_awvalid(0);
  m00_couplers_to_m00_couplers_BREADY(0) <= S_AXI_bready(0);
  m00_couplers_to_m00_couplers_BRESP(1 downto 0) <= M_AXI_bresp(1 downto 0);
  m00_couplers_to_m00_couplers_BVALID(0) <= M_AXI_bvalid(0);
  m00_couplers_to_m00_couplers_RDATA(31 downto 0) <= M_AXI_rdata(31 downto 0);
  m00_couplers_to_m00_couplers_RREADY(0) <= S_AXI_rready(0);
  m00_couplers_to_m00_couplers_RRESP(1 downto 0) <= M_AXI_rresp(1 downto 0);
  m00_couplers_to_m00_couplers_RVALID(0) <= M_AXI_rvalid(0);
  m00_couplers_to_m00_couplers_WDATA(31 downto 0) <= S_AXI_wdata(31 downto 0);
  m00_couplers_to_m00_couplers_WREADY(0) <= M_AXI_wready(0);
  m00_couplers_to_m00_couplers_WSTRB(3 downto 0) <= S_AXI_wstrb(3 downto 0);
  m00_couplers_to_m00_couplers_WVALID(0) <= S_AXI_wvalid(0);
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity m01_couplers_imp_189BYX9 is
  port (
    M_ACLK : in STD_LOGIC;
    M_ARESETN : in STD_LOGIC;
    M_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_ACLK : in STD_LOGIC;
    S_ARESETN : in STD_LOGIC;
    S_AXI_araddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awaddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end m01_couplers_imp_189BYX9;

architecture STRUCTURE of m01_couplers_imp_189BYX9 is
  signal m01_couplers_to_m01_couplers_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m01_couplers_to_m01_couplers_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m01_couplers_to_m01_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m01_couplers_to_m01_couplers_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m01_couplers_to_m01_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_m01_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_m01_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_m01_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_m01_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m01_couplers_to_m01_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  M_AXI_araddr(39 downto 0) <= m01_couplers_to_m01_couplers_ARADDR(39 downto 0);
  M_AXI_arprot(2 downto 0) <= m01_couplers_to_m01_couplers_ARPROT(2 downto 0);
  M_AXI_arvalid(0) <= m01_couplers_to_m01_couplers_ARVALID(0);
  M_AXI_awaddr(39 downto 0) <= m01_couplers_to_m01_couplers_AWADDR(39 downto 0);
  M_AXI_awprot(2 downto 0) <= m01_couplers_to_m01_couplers_AWPROT(2 downto 0);
  M_AXI_awvalid(0) <= m01_couplers_to_m01_couplers_AWVALID(0);
  M_AXI_bready(0) <= m01_couplers_to_m01_couplers_BREADY(0);
  M_AXI_rready(0) <= m01_couplers_to_m01_couplers_RREADY(0);
  M_AXI_wdata(31 downto 0) <= m01_couplers_to_m01_couplers_WDATA(31 downto 0);
  M_AXI_wstrb(3 downto 0) <= m01_couplers_to_m01_couplers_WSTRB(3 downto 0);
  M_AXI_wvalid(0) <= m01_couplers_to_m01_couplers_WVALID(0);
  S_AXI_arready(0) <= m01_couplers_to_m01_couplers_ARREADY(0);
  S_AXI_awready(0) <= m01_couplers_to_m01_couplers_AWREADY(0);
  S_AXI_bresp(1 downto 0) <= m01_couplers_to_m01_couplers_BRESP(1 downto 0);
  S_AXI_bvalid(0) <= m01_couplers_to_m01_couplers_BVALID(0);
  S_AXI_rdata(31 downto 0) <= m01_couplers_to_m01_couplers_RDATA(31 downto 0);
  S_AXI_rresp(1 downto 0) <= m01_couplers_to_m01_couplers_RRESP(1 downto 0);
  S_AXI_rvalid(0) <= m01_couplers_to_m01_couplers_RVALID(0);
  S_AXI_wready(0) <= m01_couplers_to_m01_couplers_WREADY(0);
  m01_couplers_to_m01_couplers_ARADDR(39 downto 0) <= S_AXI_araddr(39 downto 0);
  m01_couplers_to_m01_couplers_ARPROT(2 downto 0) <= S_AXI_arprot(2 downto 0);
  m01_couplers_to_m01_couplers_ARREADY(0) <= M_AXI_arready(0);
  m01_couplers_to_m01_couplers_ARVALID(0) <= S_AXI_arvalid(0);
  m01_couplers_to_m01_couplers_AWADDR(39 downto 0) <= S_AXI_awaddr(39 downto 0);
  m01_couplers_to_m01_couplers_AWPROT(2 downto 0) <= S_AXI_awprot(2 downto 0);
  m01_couplers_to_m01_couplers_AWREADY(0) <= M_AXI_awready(0);
  m01_couplers_to_m01_couplers_AWVALID(0) <= S_AXI_awvalid(0);
  m01_couplers_to_m01_couplers_BREADY(0) <= S_AXI_bready(0);
  m01_couplers_to_m01_couplers_BRESP(1 downto 0) <= M_AXI_bresp(1 downto 0);
  m01_couplers_to_m01_couplers_BVALID(0) <= M_AXI_bvalid(0);
  m01_couplers_to_m01_couplers_RDATA(31 downto 0) <= M_AXI_rdata(31 downto 0);
  m01_couplers_to_m01_couplers_RREADY(0) <= S_AXI_rready(0);
  m01_couplers_to_m01_couplers_RRESP(1 downto 0) <= M_AXI_rresp(1 downto 0);
  m01_couplers_to_m01_couplers_RVALID(0) <= M_AXI_rvalid(0);
  m01_couplers_to_m01_couplers_WDATA(31 downto 0) <= S_AXI_wdata(31 downto 0);
  m01_couplers_to_m01_couplers_WREADY(0) <= M_AXI_wready(0);
  m01_couplers_to_m01_couplers_WSTRB(3 downto 0) <= S_AXI_wstrb(3 downto 0);
  m01_couplers_to_m01_couplers_WVALID(0) <= S_AXI_wvalid(0);
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity s00_couplers_imp_1LSKJNT is
  port (
    M_ACLK : in STD_LOGIC;
    M_ARESETN : in STD_LOGIC;
    M_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_arready : in STD_LOGIC;
    M_AXI_arvalid : out STD_LOGIC;
    M_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_awready : in STD_LOGIC;
    M_AXI_awvalid : out STD_LOGIC;
    M_AXI_bready : out STD_LOGIC;
    M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_bvalid : in STD_LOGIC;
    M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_rready : out STD_LOGIC;
    M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_rvalid : in STD_LOGIC;
    M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_wready : in STD_LOGIC;
    M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_wvalid : out STD_LOGIC;
    S_ACLK : in STD_LOGIC;
    S_ARESETN : in STD_LOGIC;
    S_AXI_araddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_arid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_arready : out STD_LOGIC;
    S_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_aruser : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_arvalid : in STD_LOGIC;
    S_AXI_awaddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_awid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_awready : out STD_LOGIC;
    S_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_awuser : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_awvalid : in STD_LOGIC;
    S_AXI_bid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_bready : in STD_LOGIC;
    S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : out STD_LOGIC;
    S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S_AXI_rlast : out STD_LOGIC;
    S_AXI_rready : in STD_LOGIC;
    S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : out STD_LOGIC;
    S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wlast : in STD_LOGIC;
    S_AXI_wready : out STD_LOGIC;
    S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : in STD_LOGIC
  );
end s00_couplers_imp_1LSKJNT;

architecture STRUCTURE of s00_couplers_imp_1LSKJNT is
  component top_level_auto_pc_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    m_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awvalid : out STD_LOGIC;
    m_axi_awready : in STD_LOGIC;
    m_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_wvalid : out STD_LOGIC;
    m_axi_wready : in STD_LOGIC;
    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bvalid : in STD_LOGIC;
    m_axi_bready : out STD_LOGIC;
    m_axi_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    m_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arvalid : out STD_LOGIC;
    m_axi_arready : in STD_LOGIC;
    m_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rvalid : in STD_LOGIC;
    m_axi_rready : out STD_LOGIC
  );
  end component top_level_auto_pc_0;
  signal S_ACLK_1 : STD_LOGIC;
  signal S_ARESETN_1 : STD_LOGIC;
  signal auto_pc_to_s00_couplers_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal auto_pc_to_s00_couplers_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal auto_pc_to_s00_couplers_ARREADY : STD_LOGIC;
  signal auto_pc_to_s00_couplers_ARVALID : STD_LOGIC;
  signal auto_pc_to_s00_couplers_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal auto_pc_to_s00_couplers_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal auto_pc_to_s00_couplers_AWREADY : STD_LOGIC;
  signal auto_pc_to_s00_couplers_AWVALID : STD_LOGIC;
  signal auto_pc_to_s00_couplers_BREADY : STD_LOGIC;
  signal auto_pc_to_s00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal auto_pc_to_s00_couplers_BVALID : STD_LOGIC;
  signal auto_pc_to_s00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal auto_pc_to_s00_couplers_RREADY : STD_LOGIC;
  signal auto_pc_to_s00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal auto_pc_to_s00_couplers_RVALID : STD_LOGIC;
  signal auto_pc_to_s00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal auto_pc_to_s00_couplers_WREADY : STD_LOGIC;
  signal auto_pc_to_s00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal auto_pc_to_s00_couplers_WVALID : STD_LOGIC;
  signal s00_couplers_to_auto_pc_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal s00_couplers_to_auto_pc_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_auto_pc_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_auto_pc_ARID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s00_couplers_to_auto_pc_ARLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal s00_couplers_to_auto_pc_ARLOCK : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_auto_pc_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_auto_pc_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_auto_pc_ARREADY : STD_LOGIC;
  signal s00_couplers_to_auto_pc_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_auto_pc_ARVALID : STD_LOGIC;
  signal s00_couplers_to_auto_pc_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal s00_couplers_to_auto_pc_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_auto_pc_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_auto_pc_AWID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s00_couplers_to_auto_pc_AWLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal s00_couplers_to_auto_pc_AWLOCK : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_auto_pc_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_auto_pc_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_auto_pc_AWREADY : STD_LOGIC;
  signal s00_couplers_to_auto_pc_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_auto_pc_AWVALID : STD_LOGIC;
  signal s00_couplers_to_auto_pc_BID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s00_couplers_to_auto_pc_BREADY : STD_LOGIC;
  signal s00_couplers_to_auto_pc_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_auto_pc_BVALID : STD_LOGIC;
  signal s00_couplers_to_auto_pc_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_auto_pc_RID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s00_couplers_to_auto_pc_RLAST : STD_LOGIC;
  signal s00_couplers_to_auto_pc_RREADY : STD_LOGIC;
  signal s00_couplers_to_auto_pc_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_auto_pc_RVALID : STD_LOGIC;
  signal s00_couplers_to_auto_pc_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_auto_pc_WLAST : STD_LOGIC;
  signal s00_couplers_to_auto_pc_WREADY : STD_LOGIC;
  signal s00_couplers_to_auto_pc_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_auto_pc_WVALID : STD_LOGIC;
begin
  M_AXI_araddr(39 downto 0) <= auto_pc_to_s00_couplers_ARADDR(39 downto 0);
  M_AXI_arprot(2 downto 0) <= auto_pc_to_s00_couplers_ARPROT(2 downto 0);
  M_AXI_arvalid <= auto_pc_to_s00_couplers_ARVALID;
  M_AXI_awaddr(39 downto 0) <= auto_pc_to_s00_couplers_AWADDR(39 downto 0);
  M_AXI_awprot(2 downto 0) <= auto_pc_to_s00_couplers_AWPROT(2 downto 0);
  M_AXI_awvalid <= auto_pc_to_s00_couplers_AWVALID;
  M_AXI_bready <= auto_pc_to_s00_couplers_BREADY;
  M_AXI_rready <= auto_pc_to_s00_couplers_RREADY;
  M_AXI_wdata(31 downto 0) <= auto_pc_to_s00_couplers_WDATA(31 downto 0);
  M_AXI_wstrb(3 downto 0) <= auto_pc_to_s00_couplers_WSTRB(3 downto 0);
  M_AXI_wvalid <= auto_pc_to_s00_couplers_WVALID;
  S_ACLK_1 <= S_ACLK;
  S_ARESETN_1 <= S_ARESETN;
  S_AXI_arready <= s00_couplers_to_auto_pc_ARREADY;
  S_AXI_awready <= s00_couplers_to_auto_pc_AWREADY;
  S_AXI_bid(15 downto 0) <= s00_couplers_to_auto_pc_BID(15 downto 0);
  S_AXI_bresp(1 downto 0) <= s00_couplers_to_auto_pc_BRESP(1 downto 0);
  S_AXI_bvalid <= s00_couplers_to_auto_pc_BVALID;
  S_AXI_rdata(31 downto 0) <= s00_couplers_to_auto_pc_RDATA(31 downto 0);
  S_AXI_rid(15 downto 0) <= s00_couplers_to_auto_pc_RID(15 downto 0);
  S_AXI_rlast <= s00_couplers_to_auto_pc_RLAST;
  S_AXI_rresp(1 downto 0) <= s00_couplers_to_auto_pc_RRESP(1 downto 0);
  S_AXI_rvalid <= s00_couplers_to_auto_pc_RVALID;
  S_AXI_wready <= s00_couplers_to_auto_pc_WREADY;
  auto_pc_to_s00_couplers_ARREADY <= M_AXI_arready;
  auto_pc_to_s00_couplers_AWREADY <= M_AXI_awready;
  auto_pc_to_s00_couplers_BRESP(1 downto 0) <= M_AXI_bresp(1 downto 0);
  auto_pc_to_s00_couplers_BVALID <= M_AXI_bvalid;
  auto_pc_to_s00_couplers_RDATA(31 downto 0) <= M_AXI_rdata(31 downto 0);
  auto_pc_to_s00_couplers_RRESP(1 downto 0) <= M_AXI_rresp(1 downto 0);
  auto_pc_to_s00_couplers_RVALID <= M_AXI_rvalid;
  auto_pc_to_s00_couplers_WREADY <= M_AXI_wready;
  s00_couplers_to_auto_pc_ARADDR(39 downto 0) <= S_AXI_araddr(39 downto 0);
  s00_couplers_to_auto_pc_ARBURST(1 downto 0) <= S_AXI_arburst(1 downto 0);
  s00_couplers_to_auto_pc_ARCACHE(3 downto 0) <= S_AXI_arcache(3 downto 0);
  s00_couplers_to_auto_pc_ARID(15 downto 0) <= S_AXI_arid(15 downto 0);
  s00_couplers_to_auto_pc_ARLEN(7 downto 0) <= S_AXI_arlen(7 downto 0);
  s00_couplers_to_auto_pc_ARLOCK(0) <= S_AXI_arlock(0);
  s00_couplers_to_auto_pc_ARPROT(2 downto 0) <= S_AXI_arprot(2 downto 0);
  s00_couplers_to_auto_pc_ARQOS(3 downto 0) <= S_AXI_arqos(3 downto 0);
  s00_couplers_to_auto_pc_ARSIZE(2 downto 0) <= S_AXI_arsize(2 downto 0);
  s00_couplers_to_auto_pc_ARVALID <= S_AXI_arvalid;
  s00_couplers_to_auto_pc_AWADDR(39 downto 0) <= S_AXI_awaddr(39 downto 0);
  s00_couplers_to_auto_pc_AWBURST(1 downto 0) <= S_AXI_awburst(1 downto 0);
  s00_couplers_to_auto_pc_AWCACHE(3 downto 0) <= S_AXI_awcache(3 downto 0);
  s00_couplers_to_auto_pc_AWID(15 downto 0) <= S_AXI_awid(15 downto 0);
  s00_couplers_to_auto_pc_AWLEN(7 downto 0) <= S_AXI_awlen(7 downto 0);
  s00_couplers_to_auto_pc_AWLOCK(0) <= S_AXI_awlock(0);
  s00_couplers_to_auto_pc_AWPROT(2 downto 0) <= S_AXI_awprot(2 downto 0);
  s00_couplers_to_auto_pc_AWQOS(3 downto 0) <= S_AXI_awqos(3 downto 0);
  s00_couplers_to_auto_pc_AWSIZE(2 downto 0) <= S_AXI_awsize(2 downto 0);
  s00_couplers_to_auto_pc_AWVALID <= S_AXI_awvalid;
  s00_couplers_to_auto_pc_BREADY <= S_AXI_bready;
  s00_couplers_to_auto_pc_RREADY <= S_AXI_rready;
  s00_couplers_to_auto_pc_WDATA(31 downto 0) <= S_AXI_wdata(31 downto 0);
  s00_couplers_to_auto_pc_WLAST <= S_AXI_wlast;
  s00_couplers_to_auto_pc_WSTRB(3 downto 0) <= S_AXI_wstrb(3 downto 0);
  s00_couplers_to_auto_pc_WVALID <= S_AXI_wvalid;
auto_pc: component top_level_auto_pc_0
     port map (
      aclk => S_ACLK_1,
      aresetn => S_ARESETN_1,
      m_axi_araddr(39 downto 0) => auto_pc_to_s00_couplers_ARADDR(39 downto 0),
      m_axi_arprot(2 downto 0) => auto_pc_to_s00_couplers_ARPROT(2 downto 0),
      m_axi_arready => auto_pc_to_s00_couplers_ARREADY,
      m_axi_arvalid => auto_pc_to_s00_couplers_ARVALID,
      m_axi_awaddr(39 downto 0) => auto_pc_to_s00_couplers_AWADDR(39 downto 0),
      m_axi_awprot(2 downto 0) => auto_pc_to_s00_couplers_AWPROT(2 downto 0),
      m_axi_awready => auto_pc_to_s00_couplers_AWREADY,
      m_axi_awvalid => auto_pc_to_s00_couplers_AWVALID,
      m_axi_bready => auto_pc_to_s00_couplers_BREADY,
      m_axi_bresp(1 downto 0) => auto_pc_to_s00_couplers_BRESP(1 downto 0),
      m_axi_bvalid => auto_pc_to_s00_couplers_BVALID,
      m_axi_rdata(31 downto 0) => auto_pc_to_s00_couplers_RDATA(31 downto 0),
      m_axi_rready => auto_pc_to_s00_couplers_RREADY,
      m_axi_rresp(1 downto 0) => auto_pc_to_s00_couplers_RRESP(1 downto 0),
      m_axi_rvalid => auto_pc_to_s00_couplers_RVALID,
      m_axi_wdata(31 downto 0) => auto_pc_to_s00_couplers_WDATA(31 downto 0),
      m_axi_wready => auto_pc_to_s00_couplers_WREADY,
      m_axi_wstrb(3 downto 0) => auto_pc_to_s00_couplers_WSTRB(3 downto 0),
      m_axi_wvalid => auto_pc_to_s00_couplers_WVALID,
      s_axi_araddr(39 downto 0) => s00_couplers_to_auto_pc_ARADDR(39 downto 0),
      s_axi_arburst(1 downto 0) => s00_couplers_to_auto_pc_ARBURST(1 downto 0),
      s_axi_arcache(3 downto 0) => s00_couplers_to_auto_pc_ARCACHE(3 downto 0),
      s_axi_arid(15 downto 0) => s00_couplers_to_auto_pc_ARID(15 downto 0),
      s_axi_arlen(7 downto 0) => s00_couplers_to_auto_pc_ARLEN(7 downto 0),
      s_axi_arlock(0) => s00_couplers_to_auto_pc_ARLOCK(0),
      s_axi_arprot(2 downto 0) => s00_couplers_to_auto_pc_ARPROT(2 downto 0),
      s_axi_arqos(3 downto 0) => s00_couplers_to_auto_pc_ARQOS(3 downto 0),
      s_axi_arready => s00_couplers_to_auto_pc_ARREADY,
      s_axi_arregion(3 downto 0) => B"0000",
      s_axi_arsize(2 downto 0) => s00_couplers_to_auto_pc_ARSIZE(2 downto 0),
      s_axi_arvalid => s00_couplers_to_auto_pc_ARVALID,
      s_axi_awaddr(39 downto 0) => s00_couplers_to_auto_pc_AWADDR(39 downto 0),
      s_axi_awburst(1 downto 0) => s00_couplers_to_auto_pc_AWBURST(1 downto 0),
      s_axi_awcache(3 downto 0) => s00_couplers_to_auto_pc_AWCACHE(3 downto 0),
      s_axi_awid(15 downto 0) => s00_couplers_to_auto_pc_AWID(15 downto 0),
      s_axi_awlen(7 downto 0) => s00_couplers_to_auto_pc_AWLEN(7 downto 0),
      s_axi_awlock(0) => s00_couplers_to_auto_pc_AWLOCK(0),
      s_axi_awprot(2 downto 0) => s00_couplers_to_auto_pc_AWPROT(2 downto 0),
      s_axi_awqos(3 downto 0) => s00_couplers_to_auto_pc_AWQOS(3 downto 0),
      s_axi_awready => s00_couplers_to_auto_pc_AWREADY,
      s_axi_awregion(3 downto 0) => B"0000",
      s_axi_awsize(2 downto 0) => s00_couplers_to_auto_pc_AWSIZE(2 downto 0),
      s_axi_awvalid => s00_couplers_to_auto_pc_AWVALID,
      s_axi_bid(15 downto 0) => s00_couplers_to_auto_pc_BID(15 downto 0),
      s_axi_bready => s00_couplers_to_auto_pc_BREADY,
      s_axi_bresp(1 downto 0) => s00_couplers_to_auto_pc_BRESP(1 downto 0),
      s_axi_bvalid => s00_couplers_to_auto_pc_BVALID,
      s_axi_rdata(31 downto 0) => s00_couplers_to_auto_pc_RDATA(31 downto 0),
      s_axi_rid(15 downto 0) => s00_couplers_to_auto_pc_RID(15 downto 0),
      s_axi_rlast => s00_couplers_to_auto_pc_RLAST,
      s_axi_rready => s00_couplers_to_auto_pc_RREADY,
      s_axi_rresp(1 downto 0) => s00_couplers_to_auto_pc_RRESP(1 downto 0),
      s_axi_rvalid => s00_couplers_to_auto_pc_RVALID,
      s_axi_wdata(31 downto 0) => s00_couplers_to_auto_pc_WDATA(31 downto 0),
      s_axi_wlast => s00_couplers_to_auto_pc_WLAST,
      s_axi_wready => s00_couplers_to_auto_pc_WREADY,
      s_axi_wstrb(3 downto 0) => s00_couplers_to_auto_pc_WSTRB(3 downto 0),
      s_axi_wvalid => s00_couplers_to_auto_pc_WVALID
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top_level_ps8_0_axi_periph_0 is
  port (
    ACLK : in STD_LOGIC;
    ARESETN : in STD_LOGIC;
    M00_ACLK : in STD_LOGIC;
    M00_ARESETN : in STD_LOGIC;
    M00_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M00_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M00_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_ACLK : in STD_LOGIC;
    M01_ARESETN : in STD_LOGIC;
    M01_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M01_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M01_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M01_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M01_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M01_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M01_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M01_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S00_ACLK : in STD_LOGIC;
    S00_ARESETN : in STD_LOGIC;
    S00_AXI_araddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S00_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_arid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S00_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S00_AXI_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_arready : out STD_LOGIC;
    S00_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_aruser : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S00_AXI_arvalid : in STD_LOGIC;
    S00_AXI_awaddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    S00_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_awid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S00_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S00_AXI_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_awready : out STD_LOGIC;
    S00_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_awuser : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S00_AXI_awvalid : in STD_LOGIC;
    S00_AXI_bid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S00_AXI_bready : in STD_LOGIC;
    S00_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_bvalid : out STD_LOGIC;
    S00_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_rid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    S00_AXI_rlast : out STD_LOGIC;
    S00_AXI_rready : in STD_LOGIC;
    S00_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_rvalid : out STD_LOGIC;
    S00_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_wlast : in STD_LOGIC;
    S00_AXI_wready : out STD_LOGIC;
    S00_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_wvalid : in STD_LOGIC
  );
end top_level_ps8_0_axi_periph_0;

architecture STRUCTURE of top_level_ps8_0_axi_periph_0 is
  component top_level_xbar_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 79 downto 0 );
    m_axi_awprot : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_awvalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awready : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_wdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_wvalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_wready : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bresp : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_bvalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bready : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_araddr : out STD_LOGIC_VECTOR ( 79 downto 0 );
    m_axi_arprot : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_arvalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arready : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_rvalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rready : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  end component top_level_xbar_0;
  signal M00_ACLK_1 : STD_LOGIC;
  signal M00_ARESETN_1 : STD_LOGIC;
  signal M01_ACLK_1 : STD_LOGIC;
  signal M01_ARESETN_1 : STD_LOGIC;
  signal S00_ACLK_1 : STD_LOGIC;
  signal S00_ARESETN_1 : STD_LOGIC;
  signal m00_couplers_to_ps8_0_axi_periph_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_ps8_0_axi_periph_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m00_couplers_to_ps8_0_axi_periph_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_ps8_0_axi_periph_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m01_couplers_to_ps8_0_axi_periph_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ps8_0_axi_periph_ACLK_net : STD_LOGIC;
  signal ps8_0_axi_periph_ARESETN_net : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARLOCK : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARREADY : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARUSER : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_ARVALID : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWLOCK : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWREADY : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWUSER : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_AWVALID : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_BID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_BREADY : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_BVALID : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_RID : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_RLAST : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_RREADY : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_RVALID : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_WLAST : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_WREADY : STD_LOGIC;
  signal ps8_0_axi_periph_to_s00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ps8_0_axi_periph_to_s00_couplers_WVALID : STD_LOGIC;
  signal s00_couplers_to_xbar_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal s00_couplers_to_xbar_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_xbar_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_ARVALID : STD_LOGIC;
  signal s00_couplers_to_xbar_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal s00_couplers_to_xbar_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_xbar_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_AWVALID : STD_LOGIC;
  signal s00_couplers_to_xbar_BREADY : STD_LOGIC;
  signal s00_couplers_to_xbar_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_xbar_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_xbar_RREADY : STD_LOGIC;
  signal s00_couplers_to_xbar_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_xbar_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_xbar_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_xbar_WVALID : STD_LOGIC;
  signal xbar_to_m00_couplers_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal xbar_to_m00_couplers_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal xbar_to_m00_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal xbar_to_m00_couplers_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal xbar_to_m00_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m00_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m00_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m00_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m00_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal xbar_to_m00_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_ARADDR : STD_LOGIC_VECTOR ( 79 downto 40 );
  signal xbar_to_m01_couplers_ARPROT : STD_LOGIC_VECTOR ( 5 downto 3 );
  signal xbar_to_m01_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_ARVALID : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_AWADDR : STD_LOGIC_VECTOR ( 79 downto 40 );
  signal xbar_to_m01_couplers_AWPROT : STD_LOGIC_VECTOR ( 5 downto 3 );
  signal xbar_to_m01_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_AWVALID : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_BREADY : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m01_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m01_couplers_RREADY : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m01_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_WDATA : STD_LOGIC_VECTOR ( 63 downto 32 );
  signal xbar_to_m01_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_WSTRB : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal xbar_to_m01_couplers_WVALID : STD_LOGIC_VECTOR ( 1 to 1 );
begin
  M00_ACLK_1 <= M00_ACLK;
  M00_ARESETN_1 <= M00_ARESETN;
  M00_AXI_araddr(39 downto 0) <= m00_couplers_to_ps8_0_axi_periph_ARADDR(39 downto 0);
  M00_AXI_arprot(2 downto 0) <= m00_couplers_to_ps8_0_axi_periph_ARPROT(2 downto 0);
  M00_AXI_arvalid(0) <= m00_couplers_to_ps8_0_axi_periph_ARVALID(0);
  M00_AXI_awaddr(39 downto 0) <= m00_couplers_to_ps8_0_axi_periph_AWADDR(39 downto 0);
  M00_AXI_awprot(2 downto 0) <= m00_couplers_to_ps8_0_axi_periph_AWPROT(2 downto 0);
  M00_AXI_awvalid(0) <= m00_couplers_to_ps8_0_axi_periph_AWVALID(0);
  M00_AXI_bready(0) <= m00_couplers_to_ps8_0_axi_periph_BREADY(0);
  M00_AXI_rready(0) <= m00_couplers_to_ps8_0_axi_periph_RREADY(0);
  M00_AXI_wdata(31 downto 0) <= m00_couplers_to_ps8_0_axi_periph_WDATA(31 downto 0);
  M00_AXI_wstrb(3 downto 0) <= m00_couplers_to_ps8_0_axi_periph_WSTRB(3 downto 0);
  M00_AXI_wvalid(0) <= m00_couplers_to_ps8_0_axi_periph_WVALID(0);
  M01_ACLK_1 <= M01_ACLK;
  M01_ARESETN_1 <= M01_ARESETN;
  M01_AXI_araddr(39 downto 0) <= m01_couplers_to_ps8_0_axi_periph_ARADDR(39 downto 0);
  M01_AXI_arprot(2 downto 0) <= m01_couplers_to_ps8_0_axi_periph_ARPROT(2 downto 0);
  M01_AXI_arvalid(0) <= m01_couplers_to_ps8_0_axi_periph_ARVALID(0);
  M01_AXI_awaddr(39 downto 0) <= m01_couplers_to_ps8_0_axi_periph_AWADDR(39 downto 0);
  M01_AXI_awprot(2 downto 0) <= m01_couplers_to_ps8_0_axi_periph_AWPROT(2 downto 0);
  M01_AXI_awvalid(0) <= m01_couplers_to_ps8_0_axi_periph_AWVALID(0);
  M01_AXI_bready(0) <= m01_couplers_to_ps8_0_axi_periph_BREADY(0);
  M01_AXI_rready(0) <= m01_couplers_to_ps8_0_axi_periph_RREADY(0);
  M01_AXI_wdata(31 downto 0) <= m01_couplers_to_ps8_0_axi_periph_WDATA(31 downto 0);
  M01_AXI_wstrb(3 downto 0) <= m01_couplers_to_ps8_0_axi_periph_WSTRB(3 downto 0);
  M01_AXI_wvalid(0) <= m01_couplers_to_ps8_0_axi_periph_WVALID(0);
  S00_ACLK_1 <= S00_ACLK;
  S00_ARESETN_1 <= S00_ARESETN;
  S00_AXI_arready <= ps8_0_axi_periph_to_s00_couplers_ARREADY;
  S00_AXI_awready <= ps8_0_axi_periph_to_s00_couplers_AWREADY;
  S00_AXI_bid(15 downto 0) <= ps8_0_axi_periph_to_s00_couplers_BID(15 downto 0);
  S00_AXI_bresp(1 downto 0) <= ps8_0_axi_periph_to_s00_couplers_BRESP(1 downto 0);
  S00_AXI_bvalid <= ps8_0_axi_periph_to_s00_couplers_BVALID;
  S00_AXI_rdata(31 downto 0) <= ps8_0_axi_periph_to_s00_couplers_RDATA(31 downto 0);
  S00_AXI_rid(15 downto 0) <= ps8_0_axi_periph_to_s00_couplers_RID(15 downto 0);
  S00_AXI_rlast <= ps8_0_axi_periph_to_s00_couplers_RLAST;
  S00_AXI_rresp(1 downto 0) <= ps8_0_axi_periph_to_s00_couplers_RRESP(1 downto 0);
  S00_AXI_rvalid <= ps8_0_axi_periph_to_s00_couplers_RVALID;
  S00_AXI_wready <= ps8_0_axi_periph_to_s00_couplers_WREADY;
  m00_couplers_to_ps8_0_axi_periph_ARREADY(0) <= M00_AXI_arready(0);
  m00_couplers_to_ps8_0_axi_periph_AWREADY(0) <= M00_AXI_awready(0);
  m00_couplers_to_ps8_0_axi_periph_BRESP(1 downto 0) <= M00_AXI_bresp(1 downto 0);
  m00_couplers_to_ps8_0_axi_periph_BVALID(0) <= M00_AXI_bvalid(0);
  m00_couplers_to_ps8_0_axi_periph_RDATA(31 downto 0) <= M00_AXI_rdata(31 downto 0);
  m00_couplers_to_ps8_0_axi_periph_RRESP(1 downto 0) <= M00_AXI_rresp(1 downto 0);
  m00_couplers_to_ps8_0_axi_periph_RVALID(0) <= M00_AXI_rvalid(0);
  m00_couplers_to_ps8_0_axi_periph_WREADY(0) <= M00_AXI_wready(0);
  m01_couplers_to_ps8_0_axi_periph_ARREADY(0) <= M01_AXI_arready(0);
  m01_couplers_to_ps8_0_axi_periph_AWREADY(0) <= M01_AXI_awready(0);
  m01_couplers_to_ps8_0_axi_periph_BRESP(1 downto 0) <= M01_AXI_bresp(1 downto 0);
  m01_couplers_to_ps8_0_axi_periph_BVALID(0) <= M01_AXI_bvalid(0);
  m01_couplers_to_ps8_0_axi_periph_RDATA(31 downto 0) <= M01_AXI_rdata(31 downto 0);
  m01_couplers_to_ps8_0_axi_periph_RRESP(1 downto 0) <= M01_AXI_rresp(1 downto 0);
  m01_couplers_to_ps8_0_axi_periph_RVALID(0) <= M01_AXI_rvalid(0);
  m01_couplers_to_ps8_0_axi_periph_WREADY(0) <= M01_AXI_wready(0);
  ps8_0_axi_periph_ACLK_net <= ACLK;
  ps8_0_axi_periph_ARESETN_net <= ARESETN;
  ps8_0_axi_periph_to_s00_couplers_ARADDR(39 downto 0) <= S00_AXI_araddr(39 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARBURST(1 downto 0) <= S00_AXI_arburst(1 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARCACHE(3 downto 0) <= S00_AXI_arcache(3 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARID(15 downto 0) <= S00_AXI_arid(15 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARLEN(7 downto 0) <= S00_AXI_arlen(7 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARLOCK(0) <= S00_AXI_arlock(0);
  ps8_0_axi_periph_to_s00_couplers_ARPROT(2 downto 0) <= S00_AXI_arprot(2 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARQOS(3 downto 0) <= S00_AXI_arqos(3 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARSIZE(2 downto 0) <= S00_AXI_arsize(2 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARUSER(15 downto 0) <= S00_AXI_aruser(15 downto 0);
  ps8_0_axi_periph_to_s00_couplers_ARVALID <= S00_AXI_arvalid;
  ps8_0_axi_periph_to_s00_couplers_AWADDR(39 downto 0) <= S00_AXI_awaddr(39 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWBURST(1 downto 0) <= S00_AXI_awburst(1 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWCACHE(3 downto 0) <= S00_AXI_awcache(3 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWID(15 downto 0) <= S00_AXI_awid(15 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWLEN(7 downto 0) <= S00_AXI_awlen(7 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWLOCK(0) <= S00_AXI_awlock(0);
  ps8_0_axi_periph_to_s00_couplers_AWPROT(2 downto 0) <= S00_AXI_awprot(2 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWQOS(3 downto 0) <= S00_AXI_awqos(3 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWSIZE(2 downto 0) <= S00_AXI_awsize(2 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWUSER(15 downto 0) <= S00_AXI_awuser(15 downto 0);
  ps8_0_axi_periph_to_s00_couplers_AWVALID <= S00_AXI_awvalid;
  ps8_0_axi_periph_to_s00_couplers_BREADY <= S00_AXI_bready;
  ps8_0_axi_periph_to_s00_couplers_RREADY <= S00_AXI_rready;
  ps8_0_axi_periph_to_s00_couplers_WDATA(31 downto 0) <= S00_AXI_wdata(31 downto 0);
  ps8_0_axi_periph_to_s00_couplers_WLAST <= S00_AXI_wlast;
  ps8_0_axi_periph_to_s00_couplers_WSTRB(3 downto 0) <= S00_AXI_wstrb(3 downto 0);
  ps8_0_axi_periph_to_s00_couplers_WVALID <= S00_AXI_wvalid;
m00_couplers: entity work.m00_couplers_imp_K308NP
     port map (
      M_ACLK => M00_ACLK_1,
      M_ARESETN => M00_ARESETN_1,
      M_AXI_araddr(39 downto 0) => m00_couplers_to_ps8_0_axi_periph_ARADDR(39 downto 0),
      M_AXI_arprot(2 downto 0) => m00_couplers_to_ps8_0_axi_periph_ARPROT(2 downto 0),
      M_AXI_arready(0) => m00_couplers_to_ps8_0_axi_periph_ARREADY(0),
      M_AXI_arvalid(0) => m00_couplers_to_ps8_0_axi_periph_ARVALID(0),
      M_AXI_awaddr(39 downto 0) => m00_couplers_to_ps8_0_axi_periph_AWADDR(39 downto 0),
      M_AXI_awprot(2 downto 0) => m00_couplers_to_ps8_0_axi_periph_AWPROT(2 downto 0),
      M_AXI_awready(0) => m00_couplers_to_ps8_0_axi_periph_AWREADY(0),
      M_AXI_awvalid(0) => m00_couplers_to_ps8_0_axi_periph_AWVALID(0),
      M_AXI_bready(0) => m00_couplers_to_ps8_0_axi_periph_BREADY(0),
      M_AXI_bresp(1 downto 0) => m00_couplers_to_ps8_0_axi_periph_BRESP(1 downto 0),
      M_AXI_bvalid(0) => m00_couplers_to_ps8_0_axi_periph_BVALID(0),
      M_AXI_rdata(31 downto 0) => m00_couplers_to_ps8_0_axi_periph_RDATA(31 downto 0),
      M_AXI_rready(0) => m00_couplers_to_ps8_0_axi_periph_RREADY(0),
      M_AXI_rresp(1 downto 0) => m00_couplers_to_ps8_0_axi_periph_RRESP(1 downto 0),
      M_AXI_rvalid(0) => m00_couplers_to_ps8_0_axi_periph_RVALID(0),
      M_AXI_wdata(31 downto 0) => m00_couplers_to_ps8_0_axi_periph_WDATA(31 downto 0),
      M_AXI_wready(0) => m00_couplers_to_ps8_0_axi_periph_WREADY(0),
      M_AXI_wstrb(3 downto 0) => m00_couplers_to_ps8_0_axi_periph_WSTRB(3 downto 0),
      M_AXI_wvalid(0) => m00_couplers_to_ps8_0_axi_periph_WVALID(0),
      S_ACLK => ps8_0_axi_periph_ACLK_net,
      S_ARESETN => ps8_0_axi_periph_ARESETN_net,
      S_AXI_araddr(39 downto 0) => xbar_to_m00_couplers_ARADDR(39 downto 0),
      S_AXI_arprot(2 downto 0) => xbar_to_m00_couplers_ARPROT(2 downto 0),
      S_AXI_arready(0) => xbar_to_m00_couplers_ARREADY(0),
      S_AXI_arvalid(0) => xbar_to_m00_couplers_ARVALID(0),
      S_AXI_awaddr(39 downto 0) => xbar_to_m00_couplers_AWADDR(39 downto 0),
      S_AXI_awprot(2 downto 0) => xbar_to_m00_couplers_AWPROT(2 downto 0),
      S_AXI_awready(0) => xbar_to_m00_couplers_AWREADY(0),
      S_AXI_awvalid(0) => xbar_to_m00_couplers_AWVALID(0),
      S_AXI_bready(0) => xbar_to_m00_couplers_BREADY(0),
      S_AXI_bresp(1 downto 0) => xbar_to_m00_couplers_BRESP(1 downto 0),
      S_AXI_bvalid(0) => xbar_to_m00_couplers_BVALID(0),
      S_AXI_rdata(31 downto 0) => xbar_to_m00_couplers_RDATA(31 downto 0),
      S_AXI_rready(0) => xbar_to_m00_couplers_RREADY(0),
      S_AXI_rresp(1 downto 0) => xbar_to_m00_couplers_RRESP(1 downto 0),
      S_AXI_rvalid(0) => xbar_to_m00_couplers_RVALID(0),
      S_AXI_wdata(31 downto 0) => xbar_to_m00_couplers_WDATA(31 downto 0),
      S_AXI_wready(0) => xbar_to_m00_couplers_WREADY(0),
      S_AXI_wstrb(3 downto 0) => xbar_to_m00_couplers_WSTRB(3 downto 0),
      S_AXI_wvalid(0) => xbar_to_m00_couplers_WVALID(0)
    );
m01_couplers: entity work.m01_couplers_imp_189BYX9
     port map (
      M_ACLK => M01_ACLK_1,
      M_ARESETN => M01_ARESETN_1,
      M_AXI_araddr(39 downto 0) => m01_couplers_to_ps8_0_axi_periph_ARADDR(39 downto 0),
      M_AXI_arprot(2 downto 0) => m01_couplers_to_ps8_0_axi_periph_ARPROT(2 downto 0),
      M_AXI_arready(0) => m01_couplers_to_ps8_0_axi_periph_ARREADY(0),
      M_AXI_arvalid(0) => m01_couplers_to_ps8_0_axi_periph_ARVALID(0),
      M_AXI_awaddr(39 downto 0) => m01_couplers_to_ps8_0_axi_periph_AWADDR(39 downto 0),
      M_AXI_awprot(2 downto 0) => m01_couplers_to_ps8_0_axi_periph_AWPROT(2 downto 0),
      M_AXI_awready(0) => m01_couplers_to_ps8_0_axi_periph_AWREADY(0),
      M_AXI_awvalid(0) => m01_couplers_to_ps8_0_axi_periph_AWVALID(0),
      M_AXI_bready(0) => m01_couplers_to_ps8_0_axi_periph_BREADY(0),
      M_AXI_bresp(1 downto 0) => m01_couplers_to_ps8_0_axi_periph_BRESP(1 downto 0),
      M_AXI_bvalid(0) => m01_couplers_to_ps8_0_axi_periph_BVALID(0),
      M_AXI_rdata(31 downto 0) => m01_couplers_to_ps8_0_axi_periph_RDATA(31 downto 0),
      M_AXI_rready(0) => m01_couplers_to_ps8_0_axi_periph_RREADY(0),
      M_AXI_rresp(1 downto 0) => m01_couplers_to_ps8_0_axi_periph_RRESP(1 downto 0),
      M_AXI_rvalid(0) => m01_couplers_to_ps8_0_axi_periph_RVALID(0),
      M_AXI_wdata(31 downto 0) => m01_couplers_to_ps8_0_axi_periph_WDATA(31 downto 0),
      M_AXI_wready(0) => m01_couplers_to_ps8_0_axi_periph_WREADY(0),
      M_AXI_wstrb(3 downto 0) => m01_couplers_to_ps8_0_axi_periph_WSTRB(3 downto 0),
      M_AXI_wvalid(0) => m01_couplers_to_ps8_0_axi_periph_WVALID(0),
      S_ACLK => ps8_0_axi_periph_ACLK_net,
      S_ARESETN => ps8_0_axi_periph_ARESETN_net,
      S_AXI_araddr(39 downto 0) => xbar_to_m01_couplers_ARADDR(79 downto 40),
      S_AXI_arprot(2 downto 0) => xbar_to_m01_couplers_ARPROT(5 downto 3),
      S_AXI_arready(0) => xbar_to_m01_couplers_ARREADY(0),
      S_AXI_arvalid(0) => xbar_to_m01_couplers_ARVALID(1),
      S_AXI_awaddr(39 downto 0) => xbar_to_m01_couplers_AWADDR(79 downto 40),
      S_AXI_awprot(2 downto 0) => xbar_to_m01_couplers_AWPROT(5 downto 3),
      S_AXI_awready(0) => xbar_to_m01_couplers_AWREADY(0),
      S_AXI_awvalid(0) => xbar_to_m01_couplers_AWVALID(1),
      S_AXI_bready(0) => xbar_to_m01_couplers_BREADY(1),
      S_AXI_bresp(1 downto 0) => xbar_to_m01_couplers_BRESP(1 downto 0),
      S_AXI_bvalid(0) => xbar_to_m01_couplers_BVALID(0),
      S_AXI_rdata(31 downto 0) => xbar_to_m01_couplers_RDATA(31 downto 0),
      S_AXI_rready(0) => xbar_to_m01_couplers_RREADY(1),
      S_AXI_rresp(1 downto 0) => xbar_to_m01_couplers_RRESP(1 downto 0),
      S_AXI_rvalid(0) => xbar_to_m01_couplers_RVALID(0),
      S_AXI_wdata(31 downto 0) => xbar_to_m01_couplers_WDATA(63 downto 32),
      S_AXI_wready(0) => xbar_to_m01_couplers_WREADY(0),
      S_AXI_wstrb(3 downto 0) => xbar_to_m01_couplers_WSTRB(7 downto 4),
      S_AXI_wvalid(0) => xbar_to_m01_couplers_WVALID(1)
    );
s00_couplers: entity work.s00_couplers_imp_1LSKJNT
     port map (
      M_ACLK => ps8_0_axi_periph_ACLK_net,
      M_ARESETN => ps8_0_axi_periph_ARESETN_net,
      M_AXI_araddr(39 downto 0) => s00_couplers_to_xbar_ARADDR(39 downto 0),
      M_AXI_arprot(2 downto 0) => s00_couplers_to_xbar_ARPROT(2 downto 0),
      M_AXI_arready => s00_couplers_to_xbar_ARREADY(0),
      M_AXI_arvalid => s00_couplers_to_xbar_ARVALID,
      M_AXI_awaddr(39 downto 0) => s00_couplers_to_xbar_AWADDR(39 downto 0),
      M_AXI_awprot(2 downto 0) => s00_couplers_to_xbar_AWPROT(2 downto 0),
      M_AXI_awready => s00_couplers_to_xbar_AWREADY(0),
      M_AXI_awvalid => s00_couplers_to_xbar_AWVALID,
      M_AXI_bready => s00_couplers_to_xbar_BREADY,
      M_AXI_bresp(1 downto 0) => s00_couplers_to_xbar_BRESP(1 downto 0),
      M_AXI_bvalid => s00_couplers_to_xbar_BVALID(0),
      M_AXI_rdata(31 downto 0) => s00_couplers_to_xbar_RDATA(31 downto 0),
      M_AXI_rready => s00_couplers_to_xbar_RREADY,
      M_AXI_rresp(1 downto 0) => s00_couplers_to_xbar_RRESP(1 downto 0),
      M_AXI_rvalid => s00_couplers_to_xbar_RVALID(0),
      M_AXI_wdata(31 downto 0) => s00_couplers_to_xbar_WDATA(31 downto 0),
      M_AXI_wready => s00_couplers_to_xbar_WREADY(0),
      M_AXI_wstrb(3 downto 0) => s00_couplers_to_xbar_WSTRB(3 downto 0),
      M_AXI_wvalid => s00_couplers_to_xbar_WVALID,
      S_ACLK => S00_ACLK_1,
      S_ARESETN => S00_ARESETN_1,
      S_AXI_araddr(39 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARADDR(39 downto 0),
      S_AXI_arburst(1 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARBURST(1 downto 0),
      S_AXI_arcache(3 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARCACHE(3 downto 0),
      S_AXI_arid(15 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARID(15 downto 0),
      S_AXI_arlen(7 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARLEN(7 downto 0),
      S_AXI_arlock(0) => ps8_0_axi_periph_to_s00_couplers_ARLOCK(0),
      S_AXI_arprot(2 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARPROT(2 downto 0),
      S_AXI_arqos(3 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARQOS(3 downto 0),
      S_AXI_arready => ps8_0_axi_periph_to_s00_couplers_ARREADY,
      S_AXI_arsize(2 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARSIZE(2 downto 0),
      S_AXI_aruser(15 downto 0) => ps8_0_axi_periph_to_s00_couplers_ARUSER(15 downto 0),
      S_AXI_arvalid => ps8_0_axi_periph_to_s00_couplers_ARVALID,
      S_AXI_awaddr(39 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWADDR(39 downto 0),
      S_AXI_awburst(1 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWBURST(1 downto 0),
      S_AXI_awcache(3 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWCACHE(3 downto 0),
      S_AXI_awid(15 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWID(15 downto 0),
      S_AXI_awlen(7 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWLEN(7 downto 0),
      S_AXI_awlock(0) => ps8_0_axi_periph_to_s00_couplers_AWLOCK(0),
      S_AXI_awprot(2 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWPROT(2 downto 0),
      S_AXI_awqos(3 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWQOS(3 downto 0),
      S_AXI_awready => ps8_0_axi_periph_to_s00_couplers_AWREADY,
      S_AXI_awsize(2 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWSIZE(2 downto 0),
      S_AXI_awuser(15 downto 0) => ps8_0_axi_periph_to_s00_couplers_AWUSER(15 downto 0),
      S_AXI_awvalid => ps8_0_axi_periph_to_s00_couplers_AWVALID,
      S_AXI_bid(15 downto 0) => ps8_0_axi_periph_to_s00_couplers_BID(15 downto 0),
      S_AXI_bready => ps8_0_axi_periph_to_s00_couplers_BREADY,
      S_AXI_bresp(1 downto 0) => ps8_0_axi_periph_to_s00_couplers_BRESP(1 downto 0),
      S_AXI_bvalid => ps8_0_axi_periph_to_s00_couplers_BVALID,
      S_AXI_rdata(31 downto 0) => ps8_0_axi_periph_to_s00_couplers_RDATA(31 downto 0),
      S_AXI_rid(15 downto 0) => ps8_0_axi_periph_to_s00_couplers_RID(15 downto 0),
      S_AXI_rlast => ps8_0_axi_periph_to_s00_couplers_RLAST,
      S_AXI_rready => ps8_0_axi_periph_to_s00_couplers_RREADY,
      S_AXI_rresp(1 downto 0) => ps8_0_axi_periph_to_s00_couplers_RRESP(1 downto 0),
      S_AXI_rvalid => ps8_0_axi_periph_to_s00_couplers_RVALID,
      S_AXI_wdata(31 downto 0) => ps8_0_axi_periph_to_s00_couplers_WDATA(31 downto 0),
      S_AXI_wlast => ps8_0_axi_periph_to_s00_couplers_WLAST,
      S_AXI_wready => ps8_0_axi_periph_to_s00_couplers_WREADY,
      S_AXI_wstrb(3 downto 0) => ps8_0_axi_periph_to_s00_couplers_WSTRB(3 downto 0),
      S_AXI_wvalid => ps8_0_axi_periph_to_s00_couplers_WVALID
    );
xbar: component top_level_xbar_0
     port map (
      aclk => ps8_0_axi_periph_ACLK_net,
      aresetn => ps8_0_axi_periph_ARESETN_net,
      m_axi_araddr(79 downto 40) => xbar_to_m01_couplers_ARADDR(79 downto 40),
      m_axi_araddr(39 downto 0) => xbar_to_m00_couplers_ARADDR(39 downto 0),
      m_axi_arprot(5 downto 3) => xbar_to_m01_couplers_ARPROT(5 downto 3),
      m_axi_arprot(2 downto 0) => xbar_to_m00_couplers_ARPROT(2 downto 0),
      m_axi_arready(1) => xbar_to_m01_couplers_ARREADY(0),
      m_axi_arready(0) => xbar_to_m00_couplers_ARREADY(0),
      m_axi_arvalid(1) => xbar_to_m01_couplers_ARVALID(1),
      m_axi_arvalid(0) => xbar_to_m00_couplers_ARVALID(0),
      m_axi_awaddr(79 downto 40) => xbar_to_m01_couplers_AWADDR(79 downto 40),
      m_axi_awaddr(39 downto 0) => xbar_to_m00_couplers_AWADDR(39 downto 0),
      m_axi_awprot(5 downto 3) => xbar_to_m01_couplers_AWPROT(5 downto 3),
      m_axi_awprot(2 downto 0) => xbar_to_m00_couplers_AWPROT(2 downto 0),
      m_axi_awready(1) => xbar_to_m01_couplers_AWREADY(0),
      m_axi_awready(0) => xbar_to_m00_couplers_AWREADY(0),
      m_axi_awvalid(1) => xbar_to_m01_couplers_AWVALID(1),
      m_axi_awvalid(0) => xbar_to_m00_couplers_AWVALID(0),
      m_axi_bready(1) => xbar_to_m01_couplers_BREADY(1),
      m_axi_bready(0) => xbar_to_m00_couplers_BREADY(0),
      m_axi_bresp(3 downto 2) => xbar_to_m01_couplers_BRESP(1 downto 0),
      m_axi_bresp(1 downto 0) => xbar_to_m00_couplers_BRESP(1 downto 0),
      m_axi_bvalid(1) => xbar_to_m01_couplers_BVALID(0),
      m_axi_bvalid(0) => xbar_to_m00_couplers_BVALID(0),
      m_axi_rdata(63 downto 32) => xbar_to_m01_couplers_RDATA(31 downto 0),
      m_axi_rdata(31 downto 0) => xbar_to_m00_couplers_RDATA(31 downto 0),
      m_axi_rready(1) => xbar_to_m01_couplers_RREADY(1),
      m_axi_rready(0) => xbar_to_m00_couplers_RREADY(0),
      m_axi_rresp(3 downto 2) => xbar_to_m01_couplers_RRESP(1 downto 0),
      m_axi_rresp(1 downto 0) => xbar_to_m00_couplers_RRESP(1 downto 0),
      m_axi_rvalid(1) => xbar_to_m01_couplers_RVALID(0),
      m_axi_rvalid(0) => xbar_to_m00_couplers_RVALID(0),
      m_axi_wdata(63 downto 32) => xbar_to_m01_couplers_WDATA(63 downto 32),
      m_axi_wdata(31 downto 0) => xbar_to_m00_couplers_WDATA(31 downto 0),
      m_axi_wready(1) => xbar_to_m01_couplers_WREADY(0),
      m_axi_wready(0) => xbar_to_m00_couplers_WREADY(0),
      m_axi_wstrb(7 downto 4) => xbar_to_m01_couplers_WSTRB(7 downto 4),
      m_axi_wstrb(3 downto 0) => xbar_to_m00_couplers_WSTRB(3 downto 0),
      m_axi_wvalid(1) => xbar_to_m01_couplers_WVALID(1),
      m_axi_wvalid(0) => xbar_to_m00_couplers_WVALID(0),
      s_axi_araddr(39 downto 0) => s00_couplers_to_xbar_ARADDR(39 downto 0),
      s_axi_arprot(2 downto 0) => s00_couplers_to_xbar_ARPROT(2 downto 0),
      s_axi_arready(0) => s00_couplers_to_xbar_ARREADY(0),
      s_axi_arvalid(0) => s00_couplers_to_xbar_ARVALID,
      s_axi_awaddr(39 downto 0) => s00_couplers_to_xbar_AWADDR(39 downto 0),
      s_axi_awprot(2 downto 0) => s00_couplers_to_xbar_AWPROT(2 downto 0),
      s_axi_awready(0) => s00_couplers_to_xbar_AWREADY(0),
      s_axi_awvalid(0) => s00_couplers_to_xbar_AWVALID,
      s_axi_bready(0) => s00_couplers_to_xbar_BREADY,
      s_axi_bresp(1 downto 0) => s00_couplers_to_xbar_BRESP(1 downto 0),
      s_axi_bvalid(0) => s00_couplers_to_xbar_BVALID(0),
      s_axi_rdata(31 downto 0) => s00_couplers_to_xbar_RDATA(31 downto 0),
      s_axi_rready(0) => s00_couplers_to_xbar_RREADY,
      s_axi_rresp(1 downto 0) => s00_couplers_to_xbar_RRESP(1 downto 0),
      s_axi_rvalid(0) => s00_couplers_to_xbar_RVALID(0),
      s_axi_wdata(31 downto 0) => s00_couplers_to_xbar_WDATA(31 downto 0),
      s_axi_wready(0) => s00_couplers_to_xbar_WREADY(0),
      s_axi_wstrb(3 downto 0) => s00_couplers_to_xbar_WSTRB(3 downto 0),
      s_axi_wvalid(0) => s00_couplers_to_xbar_WVALID
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top_level is
  port (
    gth_sysclk_i : in STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of top_level : entity is "top_level,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=top_level,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=45,numReposBlks=41,numNonXlnxBlks=5,numHierBlks=4,maxHierDepth=0,numSysgenBlks=4,numHlsBlks=0,numHdlrefBlks=12,numPkgbdBlks=0,bdsource=USER,da_axi4_cnt=2,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of top_level : entity is "top_level.hwdef";
end top_level;

architecture STRUCTURE of top_level is
  component top_level_clk_wiz_1_0 is
  port (
    reset : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    clk_125 : out STD_LOGIC;
    clk_250 : out STD_LOGIC;
    locked : out STD_LOGIC
  );
  end component top_level_clk_wiz_1_0;
  component top_level_proc_sys_reset_0_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_proc_sys_reset_0_0;
  component top_level_proc_sys_reset_1_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_proc_sys_reset_1_0;
  component top_level_zynq_ultra_ps_e_0_0 is
  port (
    maxihpm0_fpd_aclk : in STD_LOGIC;
    maxigp0_awid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    maxigp0_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    maxigp0_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    maxigp0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    maxigp0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    maxigp0_awlock : out STD_LOGIC;
    maxigp0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    maxigp0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    maxigp0_awvalid : out STD_LOGIC;
    maxigp0_awuser : out STD_LOGIC_VECTOR ( 15 downto 0 );
    maxigp0_awready : in STD_LOGIC;
    maxigp0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    maxigp0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    maxigp0_wlast : out STD_LOGIC;
    maxigp0_wvalid : out STD_LOGIC;
    maxigp0_wready : in STD_LOGIC;
    maxigp0_bid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    maxigp0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    maxigp0_bvalid : in STD_LOGIC;
    maxigp0_bready : out STD_LOGIC;
    maxigp0_arid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    maxigp0_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    maxigp0_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    maxigp0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    maxigp0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    maxigp0_arlock : out STD_LOGIC;
    maxigp0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    maxigp0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    maxigp0_arvalid : out STD_LOGIC;
    maxigp0_aruser : out STD_LOGIC_VECTOR ( 15 downto 0 );
    maxigp0_arready : in STD_LOGIC;
    maxigp0_rid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    maxigp0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    maxigp0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    maxigp0_rlast : in STD_LOGIC;
    maxigp0_rvalid : in STD_LOGIC;
    maxigp0_rready : out STD_LOGIC;
    maxigp0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    maxigp0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    emio_enet0_tsu_inc_ctrl : in STD_LOGIC_VECTOR ( 1 downto 0 );
    emio_enet0_tsu_timer_cmp_val : out STD_LOGIC;
    emio_uart1_txd : out STD_LOGIC;
    emio_uart1_rxd : in STD_LOGIC;
    pl_ps_irq0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    pl_resetn0 : out STD_LOGIC;
    pl_clk0 : out STD_LOGIC
  );
  end component top_level_zynq_ultra_ps_e_0_0;
  component top_level_proc_sys_reset_0_1 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_proc_sys_reset_0_1;
  component top_level_tx_nco_emulator_0_0 is
  port (
    clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    m_tx_nco_id : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  end component top_level_tx_nco_emulator_0_0;
  component top_level_rx_dsp_emulator_0_0 is
  port (
    clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    m_rx_dsp_read : out STD_LOGIC;
    m_rx_dsp_valid : in STD_LOGIC;
    m_rx_dsp_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_rx_dsp_frev_flag : in STD_LOGIC;
    m_rx_dsp_full_error : in STD_LOGIC;
    m_rx_dsp_turn_error : in STD_LOGIC;
    m_rx_id : in STD_LOGIC_VECTOR ( 7 downto 0 );
    m_rx_dsp_bucket_index : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_rx_dsp_emulator_0_0;
  component top_level_rx_dsp_emulator_1_0 is
  port (
    clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    m_rx_dsp_read : out STD_LOGIC;
    m_rx_dsp_valid : in STD_LOGIC;
    m_rx_dsp_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_rx_dsp_frev_flag : in STD_LOGIC;
    m_rx_dsp_full_error : in STD_LOGIC;
    m_rx_dsp_turn_error : in STD_LOGIC;
    m_rx_id : in STD_LOGIC_VECTOR ( 7 downto 0 );
    m_rx_dsp_bucket_index : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_rx_dsp_emulator_1_0;
  component top_level_resampler_0_0 is
  port (
    data_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    new_x0 : in STD_LOGIC;
    rst : in STD_LOGIC;
    tout_down : in STD_LOGIC_VECTOR ( 28 downto 0 );
    tout_up : in STD_LOGIC_VECTOR ( 28 downto 0 );
    clk : in STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    valid : out STD_LOGIC;
    gateway_outi : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gateway_outi1 : out STD_LOGIC_VECTOR ( 16 downto 0 );
    gateway_outi2 : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  end component top_level_resampler_0_0;
  component top_level_resampler_0_i_0 is
  port (
    data_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    new_x0 : in STD_LOGIC;
    rst : in STD_LOGIC;
    tout_down : in STD_LOGIC_VECTOR ( 28 downto 0 );
    tout_up : in STD_LOGIC_VECTOR ( 28 downto 0 );
    clk : in STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    valid : out STD_LOGIC;
    gateway_outi : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gateway_outi1 : out STD_LOGIC_VECTOR ( 16 downto 0 );
    gateway_outi2 : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  end component top_level_resampler_0_i_0;
  component top_level_xlslice_0_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_xlslice_0_0;
  component top_level_xlslice_0_i_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_xlslice_0_i_0;
  component top_level_util_vector_logic_0_0 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_util_vector_logic_0_0;
  component top_level_xlconcat_0_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    In1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component top_level_xlconcat_0_0;
  component top_level_util_vector_logic_0_1 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_util_vector_logic_0_1;
  component top_level_GBLink_0_0 is
  port (
    aurora_reset_pb : out STD_LOGIC;
    aurora_pma_init : out STD_LOGIC;
    aurora_link_reset_out : in STD_LOGIC;
    aurora_sys_reset_out : in STD_LOGIC;
    aurora_gt_pll_lock : in STD_LOGIC;
    aurora_lane_up : in STD_LOGIC;
    aurora_channel_up : in STD_LOGIC;
    aurora_hard_err : in STD_LOGIC;
    aurora_soft_err : in STD_LOGIC;
    user_clk_slower : in STD_LOGIC;
    user_rst_slower : in STD_LOGIC;
    user_clk_faster : in STD_LOGIC;
    user_rst_faster : in STD_LOGIC;
    s_tx_dsp_ch1_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_tx_dsp_ch1_valid : in STD_LOGIC;
    s_tx_dsp_ch1_frev : in STD_LOGIC;
    s_tx_dsp_ch1_turn_number : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_tx_dsp_ch2_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_tx_dsp_ch2_valid : in STD_LOGIC;
    s_tx_dsp_ch2_frev : in STD_LOGIC;
    s_tx_dsp_ch2_turn_number : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_tx_nco_id : in STD_LOGIC_VECTOR ( 7 downto 0 );
    sync_tx : in STD_LOGIC;
    sync_rx : in STD_LOGIC;
    s_rx_dsp_ch1_read : in STD_LOGIC;
    s_rx_dsp_ch1_valid : out STD_LOGIC;
    s_rx_dsp_ch1_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_rx_dsp_ch1_frev_flag : out STD_LOGIC;
    s_rx_dsp_ch1_bucket_index : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_rx_dsp_ch1_full_error : out STD_LOGIC;
    s_rx_dsp_ch1_turn_error : out STD_LOGIC;
    s_rx_ch1_id : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s_rx_dsp_ch2_read : in STD_LOGIC;
    s_rx_dsp_ch2_valid : out STD_LOGIC;
    s_rx_dsp_ch2_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_rx_dsp_ch2_frev_flag : out STD_LOGIC;
    s_rx_dsp_ch2_bucket_index : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_rx_dsp_ch2_full_error : out STD_LOGIC;
    s_rx_dsp_ch2_turn_error : out STD_LOGIC;
    s_rx_ch2_id : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    m_axis_data_aclk : in STD_LOGIC;
    m_axis_data_aresetn : in STD_LOGIC;
    m_axis_data_tvalid : out STD_LOGIC;
    m_axis_data_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_data_tstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_data_tlast : out STD_LOGIC;
    m_axis_data_tready : in STD_LOGIC;
    m_axis_user_k_aclk : in STD_LOGIC;
    m_axis_user_k_aresetn : in STD_LOGIC;
    m_axis_user_k_tvalid : out STD_LOGIC;
    m_axis_user_k_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_user_k_tstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_user_k_tlast : out STD_LOGIC;
    m_axis_user_k_tready : in STD_LOGIC;
    s_axis_data_aclk : in STD_LOGIC;
    s_axis_data_aresetn : in STD_LOGIC;
    s_axis_data_tready : out STD_LOGIC;
    s_axis_data_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_data_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_data_tlast : in STD_LOGIC;
    s_axis_data_tvalid : in STD_LOGIC;
    s_axis_user_k_aclk : in STD_LOGIC;
    s_axis_user_k_aresetn : in STD_LOGIC;
    s_axis_user_k_tready : out STD_LOGIC;
    s_axis_user_k_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_user_k_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_user_k_tlast : in STD_LOGIC;
    s_axis_user_k_tvalid : in STD_LOGIC
  );
  end component top_level_GBLink_0_0;
  component top_level_rst_250_0 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_rst_250_0;
  component top_level_xlslice_0_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  end component top_level_xlslice_0_1;
  component top_level_wr_emulator_0_0 is
  port (
    clk_250 : in STD_LOGIC;
    resetn_250 : in STD_LOGIC;
    sync_offset_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    sync : out STD_LOGIC
  );
  end component top_level_wr_emulator_0_0;
  component top_level_xlconcat_1_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    In1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component top_level_xlconcat_1_0;
  component top_level_xlconcat_tout_rsp_0_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    In1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component top_level_xlconcat_tout_rsp_0_0;
  component top_level_resampler_0_i_1 is
  port (
    data_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    new_x0 : in STD_LOGIC;
    rst : in STD_LOGIC;
    tout_down : in STD_LOGIC_VECTOR ( 28 downto 0 );
    tout_up : in STD_LOGIC_VECTOR ( 28 downto 0 );
    clk : in STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    valid : out STD_LOGIC;
    gateway_outi : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gateway_outi1 : out STD_LOGIC_VECTOR ( 16 downto 0 );
    gateway_outi2 : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  end component top_level_resampler_0_i_1;
  component top_level_resampler_0_q_0 is
  port (
    data_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    new_x0 : in STD_LOGIC;
    rst : in STD_LOGIC;
    tout_down : in STD_LOGIC_VECTOR ( 28 downto 0 );
    tout_up : in STD_LOGIC_VECTOR ( 28 downto 0 );
    clk : in STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    valid : out STD_LOGIC;
    gateway_outi : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gateway_outi1 : out STD_LOGIC_VECTOR ( 16 downto 0 );
    gateway_outi2 : out STD_LOGIC_VECTOR ( 16 downto 0 )
  );
  end component top_level_resampler_0_q_0;
  component top_level_xlslice_0_i_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_xlslice_0_i_1;
  component top_level_xlslice_0_q_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_xlslice_0_q_0;
  component top_level_xlconcat_0_1 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    In1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component top_level_xlconcat_0_1;
  component top_level_util_vector_logic_0_2 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_util_vector_logic_0_2;
  component top_level_tx_dsp_emulator_0_0 is
  port (
    clk_125 : in STD_LOGIC;
    resetn_125 : in STD_LOGIC;
    clk_250 : in STD_LOGIC;
    resetn_250 : in STD_LOGIC;
    frev_counter_max_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    frev_offset_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m_tx_dsp_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_tx_dsp_valid : out STD_LOGIC;
    m_tx_dsp_frev : out STD_LOGIC;
    m_tx_dsp_turn_number : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component top_level_tx_dsp_emulator_0_0;
  component top_level_tx_dsp_emulator_0_2 is
  port (
    clk_125 : in STD_LOGIC;
    resetn_125 : in STD_LOGIC;
    clk_250 : in STD_LOGIC;
    resetn_250 : in STD_LOGIC;
    frev_counter_max_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    frev_offset_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m_tx_dsp_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_tx_dsp_valid : out STD_LOGIC;
    m_tx_dsp_frev : out STD_LOGIC;
    m_tx_dsp_turn_number : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component top_level_tx_dsp_emulator_0_2;
  component top_level_axi4lite_cheburashka_bridge_0_0 is
  port (
    ACLK : in STD_LOGIC;
    ARESETN : in STD_LOGIC;
    ARVALID : in STD_LOGIC;
    AWVALID : in STD_LOGIC;
    BREADY : in STD_LOGIC;
    RREADY : in STD_LOGIC;
    WLAST : in STD_LOGIC;
    WVALID : in STD_LOGIC;
    ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ARREADY : out STD_LOGIC;
    AWREADY : out STD_LOGIC;
    BVALID : out STD_LOGIC;
    RLAST : out STD_LOGIC;
    RVALID : out STD_LOGIC;
    WREADY : out STD_LOGIC;
    BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ch_addr_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ch_rd_data_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ch_wr_data_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ch_rd_mem_o : out STD_LOGIC;
    ch_wr_mem_o : out STD_LOGIC;
    ch_rd_done_i : in STD_LOGIC;
    ch_wr_done_i : in STD_LOGIC
  );
  end component top_level_axi4lite_cheburashka_bridge_0_0;
  component top_level_util_vector_logic_2_1 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_util_vector_logic_2_1;
  component top_level_RegCtrl_afczGBLinkResamplerDebug_0_0 is
  port (
    Clk : in STD_LOGIC;
    Rst : in STD_LOGIC;
    VMEAddr : in STD_LOGIC_VECTOR ( 11 downto 2 );
    VMERdData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    VMEWrData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    VMERdMem : in STD_LOGIC;
    VMEWrMem : in STD_LOGIC;
    VMERdDone : out STD_LOGIC;
    VMEWrDone : out STD_LOGIC;
    VMERdError : out STD_LOGIC;
    VMEWrError : out STD_LOGIC;
    resamplerCh1_Sel : out STD_LOGIC;
    resamplerCh1_Addr : out STD_LOGIC_VECTOR ( 9 downto 2 );
    resamplerCh1_RdData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    resamplerCh1_WrData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    resamplerCh1_RdMem : out STD_LOGIC;
    resamplerCh1_WrMem : out STD_LOGIC;
    resamplerCh1_RdDone : in STD_LOGIC;
    resamplerCh1_WrDone : in STD_LOGIC;
    resamplerCh1_RdError : in STD_LOGIC;
    resamplerCh1_WrError : in STD_LOGIC;
    ncoEmulatorCh1_Sel : out STD_LOGIC;
    ncoEmulatorCh1_Addr : out STD_LOGIC_VECTOR ( 9 downto 2 );
    ncoEmulatorCh1_RdData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ncoEmulatorCh1_WrData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ncoEmulatorCh1_RdMem : out STD_LOGIC;
    ncoEmulatorCh1_WrMem : out STD_LOGIC;
    ncoEmulatorCh1_RdDone : in STD_LOGIC;
    ncoEmulatorCh1_WrDone : in STD_LOGIC;
    ncoEmulatorCh1_RdError : in STD_LOGIC;
    ncoEmulatorCh1_WrError : in STD_LOGIC;
    resamplerCh2_Sel : out STD_LOGIC;
    resamplerCh2_Addr : out STD_LOGIC_VECTOR ( 9 downto 2 );
    resamplerCh2_RdData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    resamplerCh2_WrData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    resamplerCh2_RdMem : out STD_LOGIC;
    resamplerCh2_WrMem : out STD_LOGIC;
    resamplerCh2_RdDone : in STD_LOGIC;
    resamplerCh2_WrDone : in STD_LOGIC;
    resamplerCh2_RdError : in STD_LOGIC;
    resamplerCh2_WrError : in STD_LOGIC;
    ncoEmulatorCh2_Sel : out STD_LOGIC;
    ncoEmulatorCh2_Addr : out STD_LOGIC_VECTOR ( 9 downto 2 );
    ncoEmulatorCh2_RdData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ncoEmulatorCh2_WrData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ncoEmulatorCh2_RdMem : out STD_LOGIC;
    ncoEmulatorCh2_WrMem : out STD_LOGIC;
    ncoEmulatorCh2_RdDone : in STD_LOGIC;
    ncoEmulatorCh2_WrDone : in STD_LOGIC;
    ncoEmulatorCh2_RdError : in STD_LOGIC;
    ncoEmulatorCh2_WrError : in STD_LOGIC
  );
  end component top_level_RegCtrl_afczGBLinkResamplerDebug_0_0;
  component top_level_RegCtrl_ncoEmulator_0_0 is
  port (
    Clk : in STD_LOGIC;
    Rst : in STD_LOGIC;
    VMEAddr : in STD_LOGIC_VECTOR ( 9 downto 2 );
    VMERdData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    VMEWrData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    VMERdMem : in STD_LOGIC;
    VMEWrMem : in STD_LOGIC;
    VMERdDone : out STD_LOGIC;
    VMEWrDone : out STD_LOGIC;
    VMERdError : out STD_LOGIC;
    VMEWrError : out STD_LOGIC;
    frevCounterMax : out STD_LOGIC_VECTOR ( 15 downto 0 );
    frevOffset : out STD_LOGIC_VECTOR ( 15 downto 0 );
    syncOffset : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_RegCtrl_ncoEmulator_0_0;
  component top_level_RegCtrl_ncoEmulator_0_1 is
  port (
    Clk : in STD_LOGIC;
    Rst : in STD_LOGIC;
    VMEAddr : in STD_LOGIC_VECTOR ( 9 downto 2 );
    VMERdData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    VMEWrData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    VMERdMem : in STD_LOGIC;
    VMEWrMem : in STD_LOGIC;
    VMERdDone : out STD_LOGIC;
    VMEWrDone : out STD_LOGIC;
    VMERdError : out STD_LOGIC;
    VMEWrError : out STD_LOGIC;
    frevCounterMax : out STD_LOGIC_VECTOR ( 15 downto 0 );
    frevOffset : out STD_LOGIC_VECTOR ( 15 downto 0 );
    syncOffset : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_RegCtrl_ncoEmulator_0_1;
  component top_level_RegCtrl_resampler_0_0 is
  port (
    Clk : in STD_LOGIC;
    Rst : in STD_LOGIC;
    VMEAddr : in STD_LOGIC_VECTOR ( 9 downto 2 );
    VMERdData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    VMEWrData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    VMERdMem : in STD_LOGIC;
    VMEWrMem : in STD_LOGIC;
    VMERdDone : out STD_LOGIC;
    VMEWrDone : out STD_LOGIC;
    VMERdError : out STD_LOGIC;
    VMEWrError : out STD_LOGIC;
    toutUpHigh16 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    toutUpLow16 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    numData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    numDataTurn : in STD_LOGIC_VECTOR ( 31 downto 0 );
    control_loopEnable : out STD_LOGIC;
    numBuckets : out STD_LOGIC_VECTOR ( 15 downto 0 );
    k : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_RegCtrl_resampler_0_0;
  component top_level_RegCtrl_resampler_0_1 is
  port (
    Clk : in STD_LOGIC;
    Rst : in STD_LOGIC;
    VMEAddr : in STD_LOGIC_VECTOR ( 9 downto 2 );
    VMERdData : out STD_LOGIC_VECTOR ( 31 downto 0 );
    VMEWrData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    VMERdMem : in STD_LOGIC;
    VMEWrMem : in STD_LOGIC;
    VMERdDone : out STD_LOGIC;
    VMEWrDone : out STD_LOGIC;
    VMERdError : out STD_LOGIC;
    VMEWrError : out STD_LOGIC;
    toutUpHigh16 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    toutUpLow16 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    numData : in STD_LOGIC_VECTOR ( 31 downto 0 );
    numDataTurn : in STD_LOGIC_VECTOR ( 31 downto 0 );
    control_loopEnable : out STD_LOGIC;
    numBuckets : out STD_LOGIC_VECTOR ( 15 downto 0 );
    k : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component top_level_RegCtrl_resampler_0_1;
  component top_level_rst_250_1 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_rst_250_1;
  component top_level_xlconstant_0_0 is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component top_level_xlconstant_0_0;
  component top_level_system_ila_0_0 is
  port (
    clk : in STD_LOGIC;
    SLOT_0_AXI_awid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SLOT_0_AXI_awaddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    SLOT_0_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    SLOT_0_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_0_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_0_AXI_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    SLOT_0_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_0_AXI_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXI_awuser : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SLOT_0_AXI_awvalid : in STD_LOGIC;
    SLOT_0_AXI_awready : in STD_LOGIC;
    SLOT_0_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_0_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXI_wlast : in STD_LOGIC;
    SLOT_0_AXI_wvalid : in STD_LOGIC;
    SLOT_0_AXI_wready : in STD_LOGIC;
    SLOT_0_AXI_bid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SLOT_0_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_0_AXI_bvalid : in STD_LOGIC;
    SLOT_0_AXI_bready : in STD_LOGIC;
    SLOT_0_AXI_arid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SLOT_0_AXI_araddr : in STD_LOGIC_VECTOR ( 39 downto 0 );
    SLOT_0_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    SLOT_0_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_0_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_0_AXI_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    SLOT_0_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_0_AXI_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXI_aruser : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SLOT_0_AXI_arvalid : in STD_LOGIC;
    SLOT_0_AXI_arready : in STD_LOGIC;
    SLOT_0_AXI_rid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SLOT_0_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_0_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_0_AXI_rlast : in STD_LOGIC;
    SLOT_0_AXI_rvalid : in STD_LOGIC;
    SLOT_0_AXI_rready : in STD_LOGIC;
    SLOT_1_AXI_awaddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    SLOT_1_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_1_AXI_awvalid : in STD_LOGIC;
    SLOT_1_AXI_awready : in STD_LOGIC;
    SLOT_1_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_1_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_1_AXI_wvalid : in STD_LOGIC;
    SLOT_1_AXI_wready : in STD_LOGIC;
    SLOT_1_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_1_AXI_bvalid : in STD_LOGIC;
    SLOT_1_AXI_bready : in STD_LOGIC;
    SLOT_1_AXI_araddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    SLOT_1_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_1_AXI_arvalid : in STD_LOGIC;
    SLOT_1_AXI_arready : in STD_LOGIC;
    SLOT_1_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_1_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_1_AXI_rvalid : in STD_LOGIC;
    SLOT_1_AXI_rready : in STD_LOGIC;
    SLOT_2_AXI_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_2_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_2_AXI_awvalid : in STD_LOGIC;
    SLOT_2_AXI_awready : in STD_LOGIC;
    SLOT_2_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_2_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_2_AXI_wvalid : in STD_LOGIC;
    SLOT_2_AXI_wready : in STD_LOGIC;
    SLOT_2_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_2_AXI_bvalid : in STD_LOGIC;
    SLOT_2_AXI_bready : in STD_LOGIC;
    SLOT_2_AXI_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_2_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    SLOT_2_AXI_arvalid : in STD_LOGIC;
    SLOT_2_AXI_arready : in STD_LOGIC;
    SLOT_2_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_2_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    SLOT_2_AXI_rvalid : in STD_LOGIC;
    SLOT_2_AXI_rready : in STD_LOGIC;
    resetn : in STD_LOGIC
  );
  end component top_level_system_ila_0_0;
  signal GBLink_0_M_AXIS_DATA_TDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal GBLink_0_M_AXIS_DATA_TLAST : STD_LOGIC;
  signal GBLink_0_M_AXIS_DATA_TREADY : STD_LOGIC;
  signal GBLink_0_M_AXIS_DATA_TSTRB : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal GBLink_0_M_AXIS_DATA_TVALID : STD_LOGIC;
  signal GBLink_0_M_AXIS_USER_K_TDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal GBLink_0_M_AXIS_USER_K_TLAST : STD_LOGIC;
  signal GBLink_0_M_AXIS_USER_K_TREADY : STD_LOGIC;
  signal GBLink_0_M_AXIS_USER_K_TSTRB : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal GBLink_0_M_AXIS_USER_K_TVALID : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_VMERdData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_VMERdDone : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_VMEWrDone : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_Addr : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_RdMem : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_WrData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_WrMem : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_Addr : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_RdMem : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_WrData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_WrMem : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_Addr : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_RdMem : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_WrData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_WrMem : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_Addr : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_RdMem : STD_LOGIC;
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_WrData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_WrMem : STD_LOGIC;
  signal RegCtrl_ncoEmulator_0_VMERdData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_ncoEmulator_0_VMERdDone : STD_LOGIC;
  signal RegCtrl_ncoEmulator_0_VMERdError : STD_LOGIC;
  signal RegCtrl_ncoEmulator_0_VMEWrDone : STD_LOGIC;
  signal RegCtrl_ncoEmulator_0_VMEWrError : STD_LOGIC;
  signal RegCtrl_ncoEmulator_0_frevCounterMax : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_ncoEmulator_0_frevOffset : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_ncoEmulator_0_syncOffset : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_ncoEmulator_1_VMERdData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_ncoEmulator_1_VMERdDone : STD_LOGIC;
  signal RegCtrl_ncoEmulator_1_VMERdError : STD_LOGIC;
  signal RegCtrl_ncoEmulator_1_VMEWrDone : STD_LOGIC;
  signal RegCtrl_ncoEmulator_1_VMEWrError : STD_LOGIC;
  signal RegCtrl_ncoEmulator_1_frevCounterMax : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_ncoEmulator_1_frevOffset : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_resampler_0_VMERdData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_resampler_0_VMERdDone : STD_LOGIC;
  signal RegCtrl_resampler_0_VMERdError : STD_LOGIC;
  signal RegCtrl_resampler_0_VMEWrDone : STD_LOGIC;
  signal RegCtrl_resampler_0_VMEWrError : STD_LOGIC;
  signal RegCtrl_resampler_0_toutUpHigh16 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_resampler_0_toutUpLow16 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_resampler_1_VMERdData : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RegCtrl_resampler_1_VMERdDone : STD_LOGIC;
  signal RegCtrl_resampler_1_VMERdError : STD_LOGIC;
  signal RegCtrl_resampler_1_VMEWrDone : STD_LOGIC;
  signal RegCtrl_resampler_1_VMEWrError : STD_LOGIC;
  signal RegCtrl_resampler_1_toutUpHigh16 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal RegCtrl_resampler_1_toutUpLow16 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi4lite_cheburashka_bridge_0_ch_addr_o : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi4lite_cheburashka_bridge_0_ch_rd_mem_o : STD_LOGIC;
  signal axi4lite_cheburashka_bridge_0_ch_wr_data_o : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi4lite_cheburashka_bridge_0_ch_wr_mem_o : STD_LOGIC;
  signal clk_in1_0_1 : STD_LOGIC;
  signal clk_wiz_1_clk_125 : STD_LOGIC;
  signal clk_wiz_1_clk_250 : STD_LOGIC;
  signal proc_sys_reset_0_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal proc_sys_reset_1_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal proc_sys_reset_2_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal proc_sys_reset_zynq_interconnect_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ps8_0_axi_periph_M00_AXI_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  attribute CONN_BUS_INFO : string;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_ARADDR : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARADDR";
  attribute DEBUG : string;
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_ARADDR : signal is "true";
  attribute MARK_DEBUG : boolean;
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_ARADDR : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_ARPROT : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARPROT";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_ARPROT : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_ARPROT : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_ARREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_ARREADY : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARREADY";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_ARREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_ARREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_ARVALID : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARVALID";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_ARVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_ARVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_AWADDR : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWADDR";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_AWADDR : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_AWADDR : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_AWPROT : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWPROT";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_AWPROT : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_AWPROT : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_AWREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_AWREADY : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWREADY";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_AWREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_AWREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_AWVALID : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWVALID";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_AWVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_AWVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_BREADY : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE BREADY";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_BREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_BREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_BRESP : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE BRESP";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_BRESP : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_BRESP : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_BVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_BVALID : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE BVALID";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_BVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_BVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_RDATA : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RDATA";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_RDATA : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_RDATA : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_RREADY : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RREADY";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_RREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_RREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_RRESP : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RRESP";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_RRESP : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_RRESP : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_RVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_RVALID : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RVALID";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_RVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_RVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_WDATA : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WDATA";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_WDATA : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_WDATA : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_WREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_WREADY : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WREADY";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_WREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_WREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_WSTRB : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WSTRB";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_WSTRB : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_WSTRB : signal is std.standard.true;
  signal ps8_0_axi_periph_M00_AXI_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M00_AXI_WVALID : signal is "ps8_0_axi_periph_M00_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WVALID";
  attribute DEBUG of ps8_0_axi_periph_M00_AXI_WVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M00_AXI_WVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_ARADDR : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARADDR";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_ARADDR : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_ARADDR : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_ARPROT : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARPROT";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_ARPROT : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_ARPROT : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_ARREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_ARREADY : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARREADY";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_ARREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_ARREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_ARVALID : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE ARVALID";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_ARVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_ARVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_AWADDR : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWADDR";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_AWADDR : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_AWADDR : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_AWPROT : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWPROT";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_AWPROT : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_AWPROT : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_AWREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_AWREADY : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWREADY";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_AWREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_AWREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_AWVALID : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE AWVALID";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_AWVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_AWVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_BREADY : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE BREADY";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_BREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_BREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_BRESP : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE BRESP";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_BRESP : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_BRESP : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_BVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_BVALID : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE BVALID";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_BVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_BVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_RDATA : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RDATA";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_RDATA : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_RDATA : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_RREADY : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RREADY";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_RREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_RREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_RRESP : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RRESP";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_RRESP : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_RRESP : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_RVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_RVALID : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE RVALID";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_RVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_RVALID : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_WDATA : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WDATA";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_WDATA : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_WDATA : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_WREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_WREADY : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WREADY";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_WREADY : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_WREADY : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_WSTRB : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WSTRB";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_WSTRB : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_WSTRB : signal is std.standard.true;
  signal ps8_0_axi_periph_M01_AXI_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CONN_BUS_INFO of ps8_0_axi_periph_M01_AXI_WVALID : signal is "ps8_0_axi_periph_M01_AXI xilinx.com:interface:aximm:1.0 AXI4LITE WVALID";
  attribute DEBUG of ps8_0_axi_periph_M01_AXI_WVALID : signal is "true";
  attribute MARK_DEBUG of ps8_0_axi_periph_M01_AXI_WVALID : signal is std.standard.true;
  signal resampler_0_i_data_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal resampler_0_i_valid : STD_LOGIC;
  signal resampler_0_q_data_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal resampler_0_q_valid : STD_LOGIC;
  signal resampler_1_i_data_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal resampler_1_i_valid : STD_LOGIC;
  signal resampler_1_q_data_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal resampler_1_q_valid : STD_LOGIC;
  signal rst_250_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rst_pl_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_dsp_emulator_0_m_rx_dsp_bucket_index : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rx_dsp_emulator_0_m_rx_dsp_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_dsp_emulator_0_m_rx_dsp_frev_flag : STD_LOGIC;
  signal rx_dsp_emulator_0_m_rx_dsp_full_error : STD_LOGIC;
  signal rx_dsp_emulator_0_m_rx_dsp_read : STD_LOGIC;
  signal rx_dsp_emulator_0_m_rx_dsp_turn_error : STD_LOGIC;
  signal rx_dsp_emulator_0_m_rx_dsp_valid : STD_LOGIC;
  signal rx_dsp_emulator_0_m_rx_id : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_dsp_emulator_1_m_rx_dsp_bucket_index : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rx_dsp_emulator_1_m_rx_dsp_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_dsp_emulator_1_m_rx_dsp_frev_flag : STD_LOGIC;
  signal rx_dsp_emulator_1_m_rx_dsp_full_error : STD_LOGIC;
  signal rx_dsp_emulator_1_m_rx_dsp_read : STD_LOGIC;
  signal rx_dsp_emulator_1_m_rx_dsp_turn_error : STD_LOGIC;
  signal rx_dsp_emulator_1_m_rx_dsp_valid : STD_LOGIC;
  signal rx_dsp_emulator_1_m_rx_id : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tx_dsp_emulator_0_m_tx_dsp_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal tx_dsp_emulator_0_m_tx_dsp_frev : STD_LOGIC;
  signal tx_dsp_emulator_1_m_tx_dsp_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal tx_dsp_emulator_1_m_tx_dsp_frev : STD_LOGIC;
  signal tx_nco_emulator_0_m_tx_nco_id : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal util_vector_logic_0_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_1_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_clk_125_new_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wr_emulator_0_sync : STD_LOGIC;
  signal xlconcat_0_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xlconcat_1_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xlconcat_tout_rsp_0_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xlconcat_tout_rsp_1_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xlconstant_0_dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xlslice_0_i_Dout : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal xlslice_0_q_Dout : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal xlslice_1_i_Dout : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal xlslice_1_q_Dout : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal xlslice_cheburashka_Dout : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARADDR : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARADDR";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARADDR : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARADDR : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARBURST : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARBURST";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARBURST : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARBURST : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARCACHE : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARCACHE";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARCACHE : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARCACHE : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARID : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLEN : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARLEN";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLEN : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLEN : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLOCK : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLOCK : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARLOCK";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLOCK : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLOCK : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARPROT : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARPROT";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARPROT : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARPROT : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARQOS : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARQOS";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARQOS : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARQOS : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARREADY : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARREADY";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARREADY : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARREADY : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARSIZE : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARSIZE";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARSIZE : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARSIZE : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARUSER : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARUSER : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARUSER";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARUSER : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARUSER : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARVALID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 ARVALID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARVALID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARVALID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWADDR : STD_LOGIC_VECTOR ( 39 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWADDR : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWADDR";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWADDR : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWADDR : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWBURST : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWBURST";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWBURST : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWBURST : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWCACHE : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWCACHE";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWCACHE : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWCACHE : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWID : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLEN : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWLEN";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLEN : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLEN : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLOCK : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLOCK : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWLOCK";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLOCK : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLOCK : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWPROT : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWPROT";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWPROT : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWPROT : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWQOS : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWQOS";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWQOS : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWQOS : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWREADY : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWREADY";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWREADY : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWREADY : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWSIZE : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWSIZE";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWSIZE : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWSIZE : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWUSER : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWUSER : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWUSER";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWUSER : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWUSER : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWVALID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 AWVALID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWVALID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWVALID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BID : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 BID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BREADY : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 BREADY";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BREADY : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BREADY : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BRESP : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 BRESP";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BRESP : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BRESP : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BVALID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 BVALID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BVALID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BVALID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RDATA : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 RDATA";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RDATA : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RDATA : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RID : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 RID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RLAST : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RLAST : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 RLAST";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RLAST : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RLAST : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RREADY : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 RREADY";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RREADY : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RREADY : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RRESP : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 RRESP";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RRESP : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RRESP : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RVALID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 RVALID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RVALID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RVALID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WDATA : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 WDATA";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WDATA : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WDATA : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WLAST : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WLAST : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 WLAST";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WLAST : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WLAST : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WREADY : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WREADY : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 WREADY";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WREADY : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WREADY : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WSTRB : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 WSTRB";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WSTRB : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WSTRB : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WVALID : STD_LOGIC;
  attribute CONN_BUS_INFO of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WVALID : signal is "zynq_ultra_ps_e_0_M_AXI_HPM0_FPD xilinx.com:interface:aximm:1.0 AXI4 WVALID";
  attribute DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WVALID : signal is "true";
  attribute MARK_DEBUG of zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WVALID : signal is std.standard.true;
  signal zynq_ultra_ps_e_0_pl_clk0 : STD_LOGIC;
  signal zynq_ultra_ps_e_0_pl_resetn0 : STD_LOGIC;
  signal NLW_GBLink_0_aurora_pma_init_UNCONNECTED : STD_LOGIC;
  signal NLW_GBLink_0_aurora_reset_pb_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_afczGBLinkResamplerDebug_0_VMERdError_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_afczGBLinkResamplerDebug_0_VMEWrError_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh1_Sel_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_Sel_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_Sel_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_Sel_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_ncoEmulator_1_syncOffset_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_RegCtrl_resampler_0_control_loopEnable_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_resampler_0_k_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_RegCtrl_resampler_0_numBuckets_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_RegCtrl_resampler_1_control_loopEnable_UNCONNECTED : STD_LOGIC;
  signal NLW_RegCtrl_resampler_1_k_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_RegCtrl_resampler_1_numBuckets_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_axi4lite_cheburashka_bridge_0_RLAST_UNCONNECTED : STD_LOGIC;
  signal NLW_clk_wiz_1_locked_UNCONNECTED : STD_LOGIC;
  signal NLW_proc_sys_reset_125_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_proc_sys_reset_125_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_125_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_125_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_250_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_proc_sys_reset_250_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_250_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_250_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_zynq_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_proc_sys_reset_zynq_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_zynq_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_resampler_0_i_gateway_outi_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_resampler_0_i_gateway_outi1_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_resampler_0_i_gateway_outi2_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_resampler_0_q_gateway_outi_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_resampler_0_q_gateway_outi1_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_resampler_0_q_gateway_outi2_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_resampler_1_i_gateway_outi_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_resampler_1_i_gateway_outi1_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_resampler_1_i_gateway_outi2_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_resampler_1_q_gateway_outi_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_resampler_1_q_gateway_outi1_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_resampler_1_q_gateway_outi2_UNCONNECTED : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal NLW_rst_125_Res_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_tx_dsp_emulator_0_m_tx_dsp_valid_UNCONNECTED : STD_LOGIC;
  signal NLW_tx_dsp_emulator_0_m_tx_dsp_turn_number_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_tx_dsp_emulator_1_m_tx_dsp_valid_UNCONNECTED : STD_LOGIC;
  signal NLW_tx_dsp_emulator_1_m_tx_dsp_turn_number_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_zynq_ultra_ps_e_0_emio_enet0_tsu_timer_cmp_val_UNCONNECTED : STD_LOGIC;
  signal NLW_zynq_ultra_ps_e_0_emio_uart1_txd_UNCONNECTED : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of gth_sysclk_i : signal is "xilinx.com:signal:clock:1.0 CLK.GTH_SYSCLK_I CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of gth_sysclk_i : signal is "XIL_INTERFACENAME CLK.GTH_SYSCLK_I, CLK_DOMAIN top_level_gth_sysclk_i, FREQ_HZ 125000000, PHASE 0.000";
begin
  clk_in1_0_1 <= gth_sysclk_i;
GBLink_0: component top_level_GBLink_0_0
     port map (
      aurora_channel_up => '0',
      aurora_gt_pll_lock => '0',
      aurora_hard_err => '0',
      aurora_lane_up => '0',
      aurora_link_reset_out => '0',
      aurora_pma_init => NLW_GBLink_0_aurora_pma_init_UNCONNECTED,
      aurora_reset_pb => NLW_GBLink_0_aurora_reset_pb_UNCONNECTED,
      aurora_soft_err => '0',
      aurora_sys_reset_out => '0',
      m_axis_data_aclk => clk_wiz_1_clk_125,
      m_axis_data_aresetn => proc_sys_reset_1_peripheral_aresetn(0),
      m_axis_data_tdata(63 downto 0) => GBLink_0_M_AXIS_DATA_TDATA(63 downto 0),
      m_axis_data_tlast => GBLink_0_M_AXIS_DATA_TLAST,
      m_axis_data_tready => GBLink_0_M_AXIS_DATA_TREADY,
      m_axis_data_tstrb(7 downto 0) => GBLink_0_M_AXIS_DATA_TSTRB(7 downto 0),
      m_axis_data_tvalid => GBLink_0_M_AXIS_DATA_TVALID,
      m_axis_user_k_aclk => clk_wiz_1_clk_125,
      m_axis_user_k_aresetn => proc_sys_reset_1_peripheral_aresetn(0),
      m_axis_user_k_tdata(63 downto 0) => GBLink_0_M_AXIS_USER_K_TDATA(63 downto 0),
      m_axis_user_k_tlast => GBLink_0_M_AXIS_USER_K_TLAST,
      m_axis_user_k_tready => GBLink_0_M_AXIS_USER_K_TREADY,
      m_axis_user_k_tstrb(7 downto 0) => GBLink_0_M_AXIS_USER_K_TSTRB(7 downto 0),
      m_axis_user_k_tvalid => GBLink_0_M_AXIS_USER_K_TVALID,
      s_axi_aclk => zynq_ultra_ps_e_0_pl_clk0,
      s_axi_araddr(11 downto 0) => ps8_0_axi_periph_M00_AXI_ARADDR(11 downto 0),
      s_axi_aresetn => proc_sys_reset_0_peripheral_aresetn(0),
      s_axi_arprot(2 downto 0) => ps8_0_axi_periph_M00_AXI_ARPROT(2 downto 0),
      s_axi_arready => ps8_0_axi_periph_M00_AXI_ARREADY,
      s_axi_arvalid => ps8_0_axi_periph_M00_AXI_ARVALID(0),
      s_axi_awaddr(11 downto 0) => ps8_0_axi_periph_M00_AXI_AWADDR(11 downto 0),
      s_axi_awprot(2 downto 0) => ps8_0_axi_periph_M00_AXI_AWPROT(2 downto 0),
      s_axi_awready => ps8_0_axi_periph_M00_AXI_AWREADY,
      s_axi_awvalid => ps8_0_axi_periph_M00_AXI_AWVALID(0),
      s_axi_bready => ps8_0_axi_periph_M00_AXI_BREADY(0),
      s_axi_bresp(1 downto 0) => ps8_0_axi_periph_M00_AXI_BRESP(1 downto 0),
      s_axi_bvalid => ps8_0_axi_periph_M00_AXI_BVALID,
      s_axi_rdata(31 downto 0) => ps8_0_axi_periph_M00_AXI_RDATA(31 downto 0),
      s_axi_rready => ps8_0_axi_periph_M00_AXI_RREADY(0),
      s_axi_rresp(1 downto 0) => ps8_0_axi_periph_M00_AXI_RRESP(1 downto 0),
      s_axi_rvalid => ps8_0_axi_periph_M00_AXI_RVALID,
      s_axi_wdata(31 downto 0) => ps8_0_axi_periph_M00_AXI_WDATA(31 downto 0),
      s_axi_wready => ps8_0_axi_periph_M00_AXI_WREADY,
      s_axi_wstrb(3 downto 0) => ps8_0_axi_periph_M00_AXI_WSTRB(3 downto 0),
      s_axi_wvalid => ps8_0_axi_periph_M00_AXI_WVALID(0),
      s_axis_data_aclk => clk_wiz_1_clk_125,
      s_axis_data_aresetn => proc_sys_reset_1_peripheral_aresetn(0),
      s_axis_data_tdata(63 downto 0) => GBLink_0_M_AXIS_DATA_TDATA(63 downto 0),
      s_axis_data_tlast => GBLink_0_M_AXIS_DATA_TLAST,
      s_axis_data_tready => GBLink_0_M_AXIS_DATA_TREADY,
      s_axis_data_tstrb(7 downto 0) => GBLink_0_M_AXIS_DATA_TSTRB(7 downto 0),
      s_axis_data_tvalid => GBLink_0_M_AXIS_DATA_TVALID,
      s_axis_user_k_aclk => clk_wiz_1_clk_125,
      s_axis_user_k_aresetn => proc_sys_reset_1_peripheral_aresetn(0),
      s_axis_user_k_tdata(63 downto 0) => GBLink_0_M_AXIS_USER_K_TDATA(63 downto 0),
      s_axis_user_k_tlast => GBLink_0_M_AXIS_USER_K_TLAST,
      s_axis_user_k_tready => GBLink_0_M_AXIS_USER_K_TREADY,
      s_axis_user_k_tstrb(7 downto 0) => GBLink_0_M_AXIS_USER_K_TSTRB(7 downto 0),
      s_axis_user_k_tvalid => GBLink_0_M_AXIS_USER_K_TVALID,
      s_rx_ch1_id(7 downto 0) => rx_dsp_emulator_0_m_rx_id(7 downto 0),
      s_rx_ch2_id(7 downto 0) => rx_dsp_emulator_1_m_rx_id(7 downto 0),
      s_rx_dsp_ch1_bucket_index(15 downto 0) => rx_dsp_emulator_0_m_rx_dsp_bucket_index(15 downto 0),
      s_rx_dsp_ch1_data(31 downto 0) => rx_dsp_emulator_0_m_rx_dsp_data(31 downto 0),
      s_rx_dsp_ch1_frev_flag => rx_dsp_emulator_0_m_rx_dsp_frev_flag,
      s_rx_dsp_ch1_full_error => rx_dsp_emulator_0_m_rx_dsp_full_error,
      s_rx_dsp_ch1_read => rx_dsp_emulator_0_m_rx_dsp_read,
      s_rx_dsp_ch1_turn_error => rx_dsp_emulator_0_m_rx_dsp_turn_error,
      s_rx_dsp_ch1_valid => rx_dsp_emulator_0_m_rx_dsp_valid,
      s_rx_dsp_ch2_bucket_index(15 downto 0) => rx_dsp_emulator_1_m_rx_dsp_bucket_index(15 downto 0),
      s_rx_dsp_ch2_data(31 downto 0) => rx_dsp_emulator_1_m_rx_dsp_data(31 downto 0),
      s_rx_dsp_ch2_frev_flag => rx_dsp_emulator_1_m_rx_dsp_frev_flag,
      s_rx_dsp_ch2_full_error => rx_dsp_emulator_1_m_rx_dsp_full_error,
      s_rx_dsp_ch2_read => rx_dsp_emulator_1_m_rx_dsp_read,
      s_rx_dsp_ch2_turn_error => rx_dsp_emulator_1_m_rx_dsp_turn_error,
      s_rx_dsp_ch2_valid => rx_dsp_emulator_1_m_rx_dsp_valid,
      s_tx_dsp_ch1_data(31 downto 0) => xlconcat_0_dout(31 downto 0),
      s_tx_dsp_ch1_frev => tx_dsp_emulator_0_m_tx_dsp_frev,
      s_tx_dsp_ch1_turn_number(31 downto 0) => B"00000000000000000000000000000000",
      s_tx_dsp_ch1_valid => util_vector_logic_0_Res(0),
      s_tx_dsp_ch2_data(31 downto 0) => xlconcat_1_dout(31 downto 0),
      s_tx_dsp_ch2_frev => tx_dsp_emulator_1_m_tx_dsp_frev,
      s_tx_dsp_ch2_turn_number(31 downto 0) => B"00000000000000000000000000000000",
      s_tx_dsp_ch2_valid => util_vector_logic_1_Res(0),
      s_tx_nco_id(7 downto 0) => tx_nco_emulator_0_m_tx_nco_id(7 downto 0),
      sync_rx => wr_emulator_0_sync,
      sync_tx => wr_emulator_0_sync,
      user_clk_faster => clk_wiz_1_clk_250,
      user_clk_slower => clk_wiz_1_clk_125,
      user_rst_faster => proc_sys_reset_2_peripheral_aresetn(0),
      user_rst_slower => proc_sys_reset_1_peripheral_aresetn(0)
    );
RegCtrl_afczGBLinkResamplerDebug_0: component top_level_RegCtrl_afczGBLinkResamplerDebug_0_0
     port map (
      Clk => zynq_ultra_ps_e_0_pl_clk0,
      Rst => rst_pl_Res(0),
      VMEAddr(11 downto 2) => xlslice_cheburashka_Dout(9 downto 0),
      VMERdData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_VMERdData(31 downto 0),
      VMERdDone => RegCtrl_afczGBLinkResamplerDebug_0_VMERdDone,
      VMERdError => NLW_RegCtrl_afczGBLinkResamplerDebug_0_VMERdError_UNCONNECTED,
      VMERdMem => axi4lite_cheburashka_bridge_0_ch_rd_mem_o,
      VMEWrData(31 downto 0) => axi4lite_cheburashka_bridge_0_ch_wr_data_o(31 downto 0),
      VMEWrDone => RegCtrl_afczGBLinkResamplerDebug_0_VMEWrDone,
      VMEWrError => NLW_RegCtrl_afczGBLinkResamplerDebug_0_VMEWrError_UNCONNECTED,
      VMEWrMem => axi4lite_cheburashka_bridge_0_ch_wr_mem_o,
      ncoEmulatorCh1_Addr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_Addr(9 downto 2),
      ncoEmulatorCh1_RdData(31 downto 0) => RegCtrl_ncoEmulator_0_VMERdData(31 downto 0),
      ncoEmulatorCh1_RdDone => RegCtrl_ncoEmulator_0_VMERdDone,
      ncoEmulatorCh1_RdError => RegCtrl_ncoEmulator_0_VMERdError,
      ncoEmulatorCh1_RdMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_RdMem,
      ncoEmulatorCh1_Sel => NLW_RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh1_Sel_UNCONNECTED,
      ncoEmulatorCh1_WrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_WrData(31 downto 0),
      ncoEmulatorCh1_WrDone => RegCtrl_ncoEmulator_0_VMEWrDone,
      ncoEmulatorCh1_WrError => RegCtrl_ncoEmulator_0_VMEWrError,
      ncoEmulatorCh1_WrMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_WrMem,
      ncoEmulatorCh2_Addr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_Addr(9 downto 2),
      ncoEmulatorCh2_RdData(31 downto 0) => RegCtrl_ncoEmulator_1_VMERdData(31 downto 0),
      ncoEmulatorCh2_RdDone => RegCtrl_ncoEmulator_1_VMERdDone,
      ncoEmulatorCh2_RdError => RegCtrl_ncoEmulator_1_VMERdError,
      ncoEmulatorCh2_RdMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_RdMem,
      ncoEmulatorCh2_Sel => NLW_RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_Sel_UNCONNECTED,
      ncoEmulatorCh2_WrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_WrData(31 downto 0),
      ncoEmulatorCh2_WrDone => RegCtrl_ncoEmulator_1_VMEWrDone,
      ncoEmulatorCh2_WrError => RegCtrl_ncoEmulator_1_VMEWrError,
      ncoEmulatorCh2_WrMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_WrMem,
      resamplerCh1_Addr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_Addr(9 downto 2),
      resamplerCh1_RdData(31 downto 0) => RegCtrl_resampler_0_VMERdData(31 downto 0),
      resamplerCh1_RdDone => RegCtrl_resampler_0_VMERdDone,
      resamplerCh1_RdError => RegCtrl_resampler_0_VMERdError,
      resamplerCh1_RdMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_RdMem,
      resamplerCh1_Sel => NLW_RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_Sel_UNCONNECTED,
      resamplerCh1_WrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_WrData(31 downto 0),
      resamplerCh1_WrDone => RegCtrl_resampler_0_VMEWrDone,
      resamplerCh1_WrError => RegCtrl_resampler_0_VMEWrError,
      resamplerCh1_WrMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_WrMem,
      resamplerCh2_Addr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_Addr(9 downto 2),
      resamplerCh2_RdData(31 downto 0) => RegCtrl_resampler_1_VMERdData(31 downto 0),
      resamplerCh2_RdDone => RegCtrl_resampler_1_VMERdDone,
      resamplerCh2_RdError => RegCtrl_resampler_1_VMERdError,
      resamplerCh2_RdMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_RdMem,
      resamplerCh2_Sel => NLW_RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_Sel_UNCONNECTED,
      resamplerCh2_WrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_WrData(31 downto 0),
      resamplerCh2_WrDone => RegCtrl_resampler_1_VMEWrDone,
      resamplerCh2_WrError => RegCtrl_resampler_1_VMEWrError,
      resamplerCh2_WrMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_WrMem
    );
RegCtrl_ncoEmulator_0: component top_level_RegCtrl_ncoEmulator_0_0
     port map (
      Clk => zynq_ultra_ps_e_0_pl_clk0,
      Rst => rst_pl_Res(0),
      VMEAddr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_Addr(9 downto 2),
      VMERdData(31 downto 0) => RegCtrl_ncoEmulator_0_VMERdData(31 downto 0),
      VMERdDone => RegCtrl_ncoEmulator_0_VMERdDone,
      VMERdError => RegCtrl_ncoEmulator_0_VMERdError,
      VMERdMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_RdMem,
      VMEWrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_WrData(31 downto 0),
      VMEWrDone => RegCtrl_ncoEmulator_0_VMEWrDone,
      VMEWrError => RegCtrl_ncoEmulator_0_VMEWrError,
      VMEWrMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulator_WrMem,
      frevCounterMax(15 downto 0) => RegCtrl_ncoEmulator_0_frevCounterMax(15 downto 0),
      frevOffset(15 downto 0) => RegCtrl_ncoEmulator_0_frevOffset(15 downto 0),
      syncOffset(15 downto 0) => RegCtrl_ncoEmulator_0_syncOffset(15 downto 0)
    );
RegCtrl_ncoEmulator_1: component top_level_RegCtrl_ncoEmulator_0_1
     port map (
      Clk => zynq_ultra_ps_e_0_pl_clk0,
      Rst => rst_pl_Res(0),
      VMEAddr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_Addr(9 downto 2),
      VMERdData(31 downto 0) => RegCtrl_ncoEmulator_1_VMERdData(31 downto 0),
      VMERdDone => RegCtrl_ncoEmulator_1_VMERdDone,
      VMERdError => RegCtrl_ncoEmulator_1_VMERdError,
      VMERdMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_RdMem,
      VMEWrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_WrData(31 downto 0),
      VMEWrDone => RegCtrl_ncoEmulator_1_VMEWrDone,
      VMEWrError => RegCtrl_ncoEmulator_1_VMEWrError,
      VMEWrMem => RegCtrl_afczGBLinkResamplerDebug_0_ncoEmulatorCh2_WrMem,
      frevCounterMax(15 downto 0) => RegCtrl_ncoEmulator_1_frevCounterMax(15 downto 0),
      frevOffset(15 downto 0) => RegCtrl_ncoEmulator_1_frevOffset(15 downto 0),
      syncOffset(15 downto 0) => NLW_RegCtrl_ncoEmulator_1_syncOffset_UNCONNECTED(15 downto 0)
    );
RegCtrl_resampler_0: component top_level_RegCtrl_resampler_0_0
     port map (
      Clk => zynq_ultra_ps_e_0_pl_clk0,
      Rst => rst_pl_Res(0),
      VMEAddr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_Addr(9 downto 2),
      VMERdData(31 downto 0) => RegCtrl_resampler_0_VMERdData(31 downto 0),
      VMERdDone => RegCtrl_resampler_0_VMERdDone,
      VMERdError => RegCtrl_resampler_0_VMERdError,
      VMERdMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_RdMem,
      VMEWrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_WrData(31 downto 0),
      VMEWrDone => RegCtrl_resampler_0_VMEWrDone,
      VMEWrError => RegCtrl_resampler_0_VMEWrError,
      VMEWrMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh1_WrMem,
      control_loopEnable => NLW_RegCtrl_resampler_0_control_loopEnable_UNCONNECTED,
      k(15 downto 0) => NLW_RegCtrl_resampler_0_k_UNCONNECTED(15 downto 0),
      numBuckets(15 downto 0) => NLW_RegCtrl_resampler_0_numBuckets_UNCONNECTED(15 downto 0),
      numData(31 downto 0) => B"00000000000000000000000000000000",
      numDataTurn(31 downto 0) => B"00000000000000000000000000000000",
      toutUpHigh16(15 downto 0) => RegCtrl_resampler_0_toutUpHigh16(15 downto 0),
      toutUpLow16(15 downto 0) => RegCtrl_resampler_0_toutUpLow16(15 downto 0)
    );
RegCtrl_resampler_1: component top_level_RegCtrl_resampler_0_1
     port map (
      Clk => zynq_ultra_ps_e_0_pl_clk0,
      Rst => rst_pl_Res(0),
      VMEAddr(9 downto 2) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_Addr(9 downto 2),
      VMERdData(31 downto 0) => RegCtrl_resampler_1_VMERdData(31 downto 0),
      VMERdDone => RegCtrl_resampler_1_VMERdDone,
      VMERdError => RegCtrl_resampler_1_VMERdError,
      VMERdMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_RdMem,
      VMEWrData(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_WrData(31 downto 0),
      VMEWrDone => RegCtrl_resampler_1_VMEWrDone,
      VMEWrError => RegCtrl_resampler_1_VMEWrError,
      VMEWrMem => RegCtrl_afczGBLinkResamplerDebug_0_resamplerCh2_WrMem,
      control_loopEnable => NLW_RegCtrl_resampler_1_control_loopEnable_UNCONNECTED,
      k(15 downto 0) => NLW_RegCtrl_resampler_1_k_UNCONNECTED(15 downto 0),
      numBuckets(15 downto 0) => NLW_RegCtrl_resampler_1_numBuckets_UNCONNECTED(15 downto 0),
      numData(31 downto 0) => B"00000000000000000000000000000000",
      numDataTurn(31 downto 0) => B"00000000000000000000000000000000",
      toutUpHigh16(15 downto 0) => RegCtrl_resampler_1_toutUpHigh16(15 downto 0),
      toutUpLow16(15 downto 0) => RegCtrl_resampler_1_toutUpLow16(15 downto 0)
    );
axi4lite_cheburashka_bridge_0: component top_level_axi4lite_cheburashka_bridge_0_0
     port map (
      ACLK => zynq_ultra_ps_e_0_pl_clk0,
      ARADDR(31 downto 0) => ps8_0_axi_periph_M01_AXI_ARADDR(31 downto 0),
      ARESETN => proc_sys_reset_0_peripheral_aresetn(0),
      ARREADY => ps8_0_axi_periph_M01_AXI_ARREADY,
      ARVALID => ps8_0_axi_periph_M01_AXI_ARVALID(0),
      AWADDR(31 downto 0) => ps8_0_axi_periph_M01_AXI_AWADDR(31 downto 0),
      AWREADY => ps8_0_axi_periph_M01_AXI_AWREADY,
      AWVALID => ps8_0_axi_periph_M01_AXI_AWVALID(0),
      BREADY => ps8_0_axi_periph_M01_AXI_BREADY(0),
      BRESP(1 downto 0) => ps8_0_axi_periph_M01_AXI_BRESP(1 downto 0),
      BVALID => ps8_0_axi_periph_M01_AXI_BVALID,
      RDATA(31 downto 0) => ps8_0_axi_periph_M01_AXI_RDATA(31 downto 0),
      RLAST => NLW_axi4lite_cheburashka_bridge_0_RLAST_UNCONNECTED,
      RREADY => ps8_0_axi_periph_M01_AXI_RREADY(0),
      RRESP(1 downto 0) => ps8_0_axi_periph_M01_AXI_RRESP(1 downto 0),
      RVALID => ps8_0_axi_periph_M01_AXI_RVALID,
      WDATA(31 downto 0) => ps8_0_axi_periph_M01_AXI_WDATA(31 downto 0),
      WLAST => xlconstant_0_dout(0),
      WREADY => ps8_0_axi_periph_M01_AXI_WREADY,
      WSTRB(3 downto 0) => ps8_0_axi_periph_M01_AXI_WSTRB(3 downto 0),
      WVALID => ps8_0_axi_periph_M01_AXI_WVALID(0),
      ch_addr_o(31 downto 0) => axi4lite_cheburashka_bridge_0_ch_addr_o(31 downto 0),
      ch_rd_data_i(31 downto 0) => RegCtrl_afczGBLinkResamplerDebug_0_VMERdData(31 downto 0),
      ch_rd_done_i => RegCtrl_afczGBLinkResamplerDebug_0_VMERdDone,
      ch_rd_mem_o => axi4lite_cheburashka_bridge_0_ch_rd_mem_o,
      ch_wr_data_o(31 downto 0) => axi4lite_cheburashka_bridge_0_ch_wr_data_o(31 downto 0),
      ch_wr_done_i => RegCtrl_afczGBLinkResamplerDebug_0_VMEWrDone,
      ch_wr_mem_o => axi4lite_cheburashka_bridge_0_ch_wr_mem_o
    );
clk_wiz_1: component top_level_clk_wiz_1_0
     port map (
      clk_125 => clk_wiz_1_clk_125,
      clk_250 => clk_wiz_1_clk_250,
      clk_in1 => clk_in1_0_1,
      locked => NLW_clk_wiz_1_locked_UNCONNECTED,
      reset => '0'
    );
proc_sys_reset_125: component top_level_proc_sys_reset_1_0
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_proc_sys_reset_125_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => zynq_ultra_ps_e_0_pl_resetn0,
      interconnect_aresetn(0) => NLW_proc_sys_reset_125_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_proc_sys_reset_125_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => proc_sys_reset_1_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_proc_sys_reset_125_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => clk_wiz_1_clk_125
    );
proc_sys_reset_250: component top_level_proc_sys_reset_0_1
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_proc_sys_reset_250_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => zynq_ultra_ps_e_0_pl_resetn0,
      interconnect_aresetn(0) => NLW_proc_sys_reset_250_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_proc_sys_reset_250_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => proc_sys_reset_2_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_proc_sys_reset_250_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => clk_wiz_1_clk_250
    );
proc_sys_reset_zynq: component top_level_proc_sys_reset_0_0
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_proc_sys_reset_zynq_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => zynq_ultra_ps_e_0_pl_resetn0,
      interconnect_aresetn(0) => proc_sys_reset_zynq_interconnect_aresetn(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_proc_sys_reset_zynq_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => proc_sys_reset_0_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_proc_sys_reset_zynq_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => zynq_ultra_ps_e_0_pl_clk0
    );
ps8_0_axi_periph: entity work.top_level_ps8_0_axi_periph_0
     port map (
      ACLK => zynq_ultra_ps_e_0_pl_clk0,
      ARESETN => proc_sys_reset_zynq_interconnect_aresetn(0),
      M00_ACLK => zynq_ultra_ps_e_0_pl_clk0,
      M00_ARESETN => proc_sys_reset_0_peripheral_aresetn(0),
      M00_AXI_araddr(39 downto 0) => ps8_0_axi_periph_M00_AXI_ARADDR(39 downto 0),
      M00_AXI_arprot(2 downto 0) => ps8_0_axi_periph_M00_AXI_ARPROT(2 downto 0),
      M00_AXI_arready(0) => ps8_0_axi_periph_M00_AXI_ARREADY,
      M00_AXI_arvalid(0) => ps8_0_axi_periph_M00_AXI_ARVALID(0),
      M00_AXI_awaddr(39 downto 0) => ps8_0_axi_periph_M00_AXI_AWADDR(39 downto 0),
      M00_AXI_awprot(2 downto 0) => ps8_0_axi_periph_M00_AXI_AWPROT(2 downto 0),
      M00_AXI_awready(0) => ps8_0_axi_periph_M00_AXI_AWREADY,
      M00_AXI_awvalid(0) => ps8_0_axi_periph_M00_AXI_AWVALID(0),
      M00_AXI_bready(0) => ps8_0_axi_periph_M00_AXI_BREADY(0),
      M00_AXI_bresp(1 downto 0) => ps8_0_axi_periph_M00_AXI_BRESP(1 downto 0),
      M00_AXI_bvalid(0) => ps8_0_axi_periph_M00_AXI_BVALID,
      M00_AXI_rdata(31 downto 0) => ps8_0_axi_periph_M00_AXI_RDATA(31 downto 0),
      M00_AXI_rready(0) => ps8_0_axi_periph_M00_AXI_RREADY(0),
      M00_AXI_rresp(1 downto 0) => ps8_0_axi_periph_M00_AXI_RRESP(1 downto 0),
      M00_AXI_rvalid(0) => ps8_0_axi_periph_M00_AXI_RVALID,
      M00_AXI_wdata(31 downto 0) => ps8_0_axi_periph_M00_AXI_WDATA(31 downto 0),
      M00_AXI_wready(0) => ps8_0_axi_periph_M00_AXI_WREADY,
      M00_AXI_wstrb(3 downto 0) => ps8_0_axi_periph_M00_AXI_WSTRB(3 downto 0),
      M00_AXI_wvalid(0) => ps8_0_axi_periph_M00_AXI_WVALID(0),
      M01_ACLK => zynq_ultra_ps_e_0_pl_clk0,
      M01_ARESETN => proc_sys_reset_0_peripheral_aresetn(0),
      M01_AXI_araddr(39 downto 0) => ps8_0_axi_periph_M01_AXI_ARADDR(39 downto 0),
      M01_AXI_arprot(2 downto 0) => ps8_0_axi_periph_M01_AXI_ARPROT(2 downto 0),
      M01_AXI_arready(0) => ps8_0_axi_periph_M01_AXI_ARREADY,
      M01_AXI_arvalid(0) => ps8_0_axi_periph_M01_AXI_ARVALID(0),
      M01_AXI_awaddr(39 downto 0) => ps8_0_axi_periph_M01_AXI_AWADDR(39 downto 0),
      M01_AXI_awprot(2 downto 0) => ps8_0_axi_periph_M01_AXI_AWPROT(2 downto 0),
      M01_AXI_awready(0) => ps8_0_axi_periph_M01_AXI_AWREADY,
      M01_AXI_awvalid(0) => ps8_0_axi_periph_M01_AXI_AWVALID(0),
      M01_AXI_bready(0) => ps8_0_axi_periph_M01_AXI_BREADY(0),
      M01_AXI_bresp(1 downto 0) => ps8_0_axi_periph_M01_AXI_BRESP(1 downto 0),
      M01_AXI_bvalid(0) => ps8_0_axi_periph_M01_AXI_BVALID,
      M01_AXI_rdata(31 downto 0) => ps8_0_axi_periph_M01_AXI_RDATA(31 downto 0),
      M01_AXI_rready(0) => ps8_0_axi_periph_M01_AXI_RREADY(0),
      M01_AXI_rresp(1 downto 0) => ps8_0_axi_periph_M01_AXI_RRESP(1 downto 0),
      M01_AXI_rvalid(0) => ps8_0_axi_periph_M01_AXI_RVALID,
      M01_AXI_wdata(31 downto 0) => ps8_0_axi_periph_M01_AXI_WDATA(31 downto 0),
      M01_AXI_wready(0) => ps8_0_axi_periph_M01_AXI_WREADY,
      M01_AXI_wstrb(3 downto 0) => ps8_0_axi_periph_M01_AXI_WSTRB(3 downto 0),
      M01_AXI_wvalid(0) => ps8_0_axi_periph_M01_AXI_WVALID(0),
      S00_ACLK => zynq_ultra_ps_e_0_pl_clk0,
      S00_ARESETN => proc_sys_reset_0_peripheral_aresetn(0),
      S00_AXI_araddr(39 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARADDR(39 downto 0),
      S00_AXI_arburst(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARBURST(1 downto 0),
      S00_AXI_arcache(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARCACHE(3 downto 0),
      S00_AXI_arid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARID(15 downto 0),
      S00_AXI_arlen(7 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLEN(7 downto 0),
      S00_AXI_arlock(0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLOCK,
      S00_AXI_arprot(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARPROT(2 downto 0),
      S00_AXI_arqos(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARQOS(3 downto 0),
      S00_AXI_arready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARREADY,
      S00_AXI_arsize(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARSIZE(2 downto 0),
      S00_AXI_aruser(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARUSER(15 downto 0),
      S00_AXI_arvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARVALID,
      S00_AXI_awaddr(39 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWADDR(39 downto 0),
      S00_AXI_awburst(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWBURST(1 downto 0),
      S00_AXI_awcache(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWCACHE(3 downto 0),
      S00_AXI_awid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWID(15 downto 0),
      S00_AXI_awlen(7 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLEN(7 downto 0),
      S00_AXI_awlock(0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLOCK,
      S00_AXI_awprot(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWPROT(2 downto 0),
      S00_AXI_awqos(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWQOS(3 downto 0),
      S00_AXI_awready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWREADY,
      S00_AXI_awsize(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWSIZE(2 downto 0),
      S00_AXI_awuser(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWUSER(15 downto 0),
      S00_AXI_awvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWVALID,
      S00_AXI_bid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BID(15 downto 0),
      S00_AXI_bready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BREADY,
      S00_AXI_bresp(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BRESP(1 downto 0),
      S00_AXI_bvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BVALID,
      S00_AXI_rdata(31 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RDATA(31 downto 0),
      S00_AXI_rid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RID(15 downto 0),
      S00_AXI_rlast => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RLAST,
      S00_AXI_rready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RREADY,
      S00_AXI_rresp(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RRESP(1 downto 0),
      S00_AXI_rvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RVALID,
      S00_AXI_wdata(31 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WDATA(31 downto 0),
      S00_AXI_wlast => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WLAST,
      S00_AXI_wready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WREADY,
      S00_AXI_wstrb(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WSTRB(3 downto 0),
      S00_AXI_wvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WVALID
    );
resampler_0_i: component top_level_resampler_0_0
     port map (
      clk => clk_wiz_1_clk_250,
      data_in(15 downto 0) => xlslice_0_i_Dout(15 downto 0),
      data_out(15 downto 0) => resampler_0_i_data_out(15 downto 0),
      gateway_outi(15 downto 0) => NLW_resampler_0_i_gateway_outi_UNCONNECTED(15 downto 0),
      gateway_outi1(16 downto 0) => NLW_resampler_0_i_gateway_outi1_UNCONNECTED(16 downto 0),
      gateway_outi2(16 downto 0) => NLW_resampler_0_i_gateway_outi2_UNCONNECTED(16 downto 0),
      new_x0 => util_vector_logic_clk_125_new_Res(0),
      rst => rst_250_Res(0),
      tout_down(28 downto 0) => B"00000000000000000000000000000",
      tout_up(28 downto 0) => xlconcat_tout_rsp_0_dout(28 downto 0),
      valid => resampler_0_i_valid
    );
resampler_0_q: component top_level_resampler_0_i_0
     port map (
      clk => clk_wiz_1_clk_250,
      data_in(15 downto 0) => xlslice_0_q_Dout(15 downto 0),
      data_out(15 downto 0) => resampler_0_q_data_out(15 downto 0),
      gateway_outi(15 downto 0) => NLW_resampler_0_q_gateway_outi_UNCONNECTED(15 downto 0),
      gateway_outi1(16 downto 0) => NLW_resampler_0_q_gateway_outi1_UNCONNECTED(16 downto 0),
      gateway_outi2(16 downto 0) => NLW_resampler_0_q_gateway_outi2_UNCONNECTED(16 downto 0),
      new_x0 => util_vector_logic_clk_125_new_Res(0),
      rst => rst_250_Res(0),
      tout_down(28 downto 0) => B"00000000000000000000000000000",
      tout_up(28 downto 0) => xlconcat_tout_rsp_0_dout(28 downto 0),
      valid => resampler_0_q_valid
    );
resampler_1_i: component top_level_resampler_0_i_1
     port map (
      clk => clk_wiz_1_clk_250,
      data_in(15 downto 0) => xlslice_1_i_Dout(15 downto 0),
      data_out(15 downto 0) => resampler_1_i_data_out(15 downto 0),
      gateway_outi(15 downto 0) => NLW_resampler_1_i_gateway_outi_UNCONNECTED(15 downto 0),
      gateway_outi1(16 downto 0) => NLW_resampler_1_i_gateway_outi1_UNCONNECTED(16 downto 0),
      gateway_outi2(16 downto 0) => NLW_resampler_1_i_gateway_outi2_UNCONNECTED(16 downto 0),
      new_x0 => util_vector_logic_clk_125_new_Res(0),
      rst => rst_250_Res(0),
      tout_down(28 downto 0) => B"00000000000000000000000000000",
      tout_up(28 downto 0) => xlconcat_tout_rsp_1_dout(28 downto 0),
      valid => resampler_1_i_valid
    );
resampler_1_q: component top_level_resampler_0_q_0
     port map (
      clk => clk_wiz_1_clk_250,
      data_in(15 downto 0) => xlslice_1_q_Dout(15 downto 0),
      data_out(15 downto 0) => resampler_1_q_data_out(15 downto 0),
      gateway_outi(15 downto 0) => NLW_resampler_1_q_gateway_outi_UNCONNECTED(15 downto 0),
      gateway_outi1(16 downto 0) => NLW_resampler_1_q_gateway_outi1_UNCONNECTED(16 downto 0),
      gateway_outi2(16 downto 0) => NLW_resampler_1_q_gateway_outi2_UNCONNECTED(16 downto 0),
      new_x0 => util_vector_logic_clk_125_new_Res(0),
      rst => rst_250_Res(0),
      tout_down(28 downto 0) => B"00000000000000000000000000000",
      tout_up(28 downto 0) => xlconcat_tout_rsp_1_dout(28 downto 0),
      valid => resampler_1_q_valid
    );
rst_125: component top_level_rst_250_0
     port map (
      Op1(0) => proc_sys_reset_1_peripheral_aresetn(0),
      Res(0) => NLW_rst_125_Res_UNCONNECTED(0)
    );
rst_250: component top_level_util_vector_logic_0_0
     port map (
      Op1(0) => proc_sys_reset_2_peripheral_aresetn(0),
      Res(0) => rst_250_Res(0)
    );
rst_pl: component top_level_rst_250_1
     port map (
      Op1(0) => proc_sys_reset_0_peripheral_aresetn(0),
      Res(0) => rst_pl_Res(0)
    );
rx_dsp_emulator_0: component top_level_rx_dsp_emulator_0_0
     port map (
      clk => clk_wiz_1_clk_250,
      m_rx_dsp_bucket_index(15 downto 0) => rx_dsp_emulator_0_m_rx_dsp_bucket_index(15 downto 0),
      m_rx_dsp_data(31 downto 0) => rx_dsp_emulator_0_m_rx_dsp_data(31 downto 0),
      m_rx_dsp_frev_flag => rx_dsp_emulator_0_m_rx_dsp_frev_flag,
      m_rx_dsp_full_error => rx_dsp_emulator_0_m_rx_dsp_full_error,
      m_rx_dsp_read => rx_dsp_emulator_0_m_rx_dsp_read,
      m_rx_dsp_turn_error => rx_dsp_emulator_0_m_rx_dsp_turn_error,
      m_rx_dsp_valid => rx_dsp_emulator_0_m_rx_dsp_valid,
      m_rx_id(7 downto 0) => rx_dsp_emulator_0_m_rx_id(7 downto 0),
      resetn => proc_sys_reset_2_peripheral_aresetn(0)
    );
rx_dsp_emulator_1: component top_level_rx_dsp_emulator_1_0
     port map (
      clk => clk_wiz_1_clk_250,
      m_rx_dsp_bucket_index(15 downto 0) => rx_dsp_emulator_1_m_rx_dsp_bucket_index(15 downto 0),
      m_rx_dsp_data(31 downto 0) => rx_dsp_emulator_1_m_rx_dsp_data(31 downto 0),
      m_rx_dsp_frev_flag => rx_dsp_emulator_1_m_rx_dsp_frev_flag,
      m_rx_dsp_full_error => rx_dsp_emulator_1_m_rx_dsp_full_error,
      m_rx_dsp_read => rx_dsp_emulator_1_m_rx_dsp_read,
      m_rx_dsp_turn_error => rx_dsp_emulator_1_m_rx_dsp_turn_error,
      m_rx_dsp_valid => rx_dsp_emulator_1_m_rx_dsp_valid,
      m_rx_id(7 downto 0) => rx_dsp_emulator_1_m_rx_id(7 downto 0),
      resetn => proc_sys_reset_2_peripheral_aresetn(0)
    );
system_ila_0: component top_level_system_ila_0_0
     port map (
      SLOT_0_AXI_araddr(39 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARADDR(39 downto 0),
      SLOT_0_AXI_arburst(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARBURST(1 downto 0),
      SLOT_0_AXI_arcache(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARCACHE(3 downto 0),
      SLOT_0_AXI_arid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARID(15 downto 0),
      SLOT_0_AXI_arlen(7 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLEN(7 downto 0),
      SLOT_0_AXI_arlock(0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLOCK,
      SLOT_0_AXI_arprot(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARPROT(2 downto 0),
      SLOT_0_AXI_arqos(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARQOS(3 downto 0),
      SLOT_0_AXI_arready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARREADY,
      SLOT_0_AXI_arregion(3 downto 0) => B"0000",
      SLOT_0_AXI_arsize(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARSIZE(2 downto 0),
      SLOT_0_AXI_aruser(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARUSER(15 downto 0),
      SLOT_0_AXI_arvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARVALID,
      SLOT_0_AXI_awaddr(39 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWADDR(39 downto 0),
      SLOT_0_AXI_awburst(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWBURST(1 downto 0),
      SLOT_0_AXI_awcache(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWCACHE(3 downto 0),
      SLOT_0_AXI_awid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWID(15 downto 0),
      SLOT_0_AXI_awlen(7 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLEN(7 downto 0),
      SLOT_0_AXI_awlock(0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLOCK,
      SLOT_0_AXI_awprot(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWPROT(2 downto 0),
      SLOT_0_AXI_awqos(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWQOS(3 downto 0),
      SLOT_0_AXI_awready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWREADY,
      SLOT_0_AXI_awregion(3 downto 0) => B"0000",
      SLOT_0_AXI_awsize(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWSIZE(2 downto 0),
      SLOT_0_AXI_awuser(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWUSER(15 downto 0),
      SLOT_0_AXI_awvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWVALID,
      SLOT_0_AXI_bid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BID(15 downto 0),
      SLOT_0_AXI_bready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BREADY,
      SLOT_0_AXI_bresp(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BRESP(1 downto 0),
      SLOT_0_AXI_bvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BVALID,
      SLOT_0_AXI_rdata(31 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RDATA(31 downto 0),
      SLOT_0_AXI_rid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RID(15 downto 0),
      SLOT_0_AXI_rlast => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RLAST,
      SLOT_0_AXI_rready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RREADY,
      SLOT_0_AXI_rresp(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RRESP(1 downto 0),
      SLOT_0_AXI_rvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RVALID,
      SLOT_0_AXI_wdata(31 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WDATA(31 downto 0),
      SLOT_0_AXI_wlast => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WLAST,
      SLOT_0_AXI_wready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WREADY,
      SLOT_0_AXI_wstrb(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WSTRB(3 downto 0),
      SLOT_0_AXI_wvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WVALID,
      SLOT_1_AXI_araddr(11 downto 0) => ps8_0_axi_periph_M00_AXI_ARADDR(11 downto 0),
      SLOT_1_AXI_arprot(2 downto 0) => ps8_0_axi_periph_M00_AXI_ARPROT(2 downto 0),
      SLOT_1_AXI_arready => ps8_0_axi_periph_M00_AXI_ARREADY,
      SLOT_1_AXI_arvalid => ps8_0_axi_periph_M00_AXI_ARVALID(0),
      SLOT_1_AXI_awaddr(11 downto 0) => ps8_0_axi_periph_M00_AXI_AWADDR(11 downto 0),
      SLOT_1_AXI_awprot(2 downto 0) => ps8_0_axi_periph_M00_AXI_AWPROT(2 downto 0),
      SLOT_1_AXI_awready => ps8_0_axi_periph_M00_AXI_AWREADY,
      SLOT_1_AXI_awvalid => ps8_0_axi_periph_M00_AXI_AWVALID(0),
      SLOT_1_AXI_bready => ps8_0_axi_periph_M00_AXI_BREADY(0),
      SLOT_1_AXI_bresp(1 downto 0) => ps8_0_axi_periph_M00_AXI_BRESP(1 downto 0),
      SLOT_1_AXI_bvalid => ps8_0_axi_periph_M00_AXI_BVALID,
      SLOT_1_AXI_rdata(31 downto 0) => ps8_0_axi_periph_M00_AXI_RDATA(31 downto 0),
      SLOT_1_AXI_rready => ps8_0_axi_periph_M00_AXI_RREADY(0),
      SLOT_1_AXI_rresp(1 downto 0) => ps8_0_axi_periph_M00_AXI_RRESP(1 downto 0),
      SLOT_1_AXI_rvalid => ps8_0_axi_periph_M00_AXI_RVALID,
      SLOT_1_AXI_wdata(31 downto 0) => ps8_0_axi_periph_M00_AXI_WDATA(31 downto 0),
      SLOT_1_AXI_wready => ps8_0_axi_periph_M00_AXI_WREADY,
      SLOT_1_AXI_wstrb(3 downto 0) => ps8_0_axi_periph_M00_AXI_WSTRB(3 downto 0),
      SLOT_1_AXI_wvalid => ps8_0_axi_periph_M00_AXI_WVALID(0),
      SLOT_2_AXI_araddr(31 downto 0) => ps8_0_axi_periph_M01_AXI_ARADDR(31 downto 0),
      SLOT_2_AXI_arprot(2 downto 0) => ps8_0_axi_periph_M01_AXI_ARPROT(2 downto 0),
      SLOT_2_AXI_arready => ps8_0_axi_periph_M01_AXI_ARREADY,
      SLOT_2_AXI_arvalid => ps8_0_axi_periph_M01_AXI_ARVALID(0),
      SLOT_2_AXI_awaddr(31 downto 0) => ps8_0_axi_periph_M01_AXI_AWADDR(31 downto 0),
      SLOT_2_AXI_awprot(2 downto 0) => ps8_0_axi_periph_M01_AXI_AWPROT(2 downto 0),
      SLOT_2_AXI_awready => ps8_0_axi_periph_M01_AXI_AWREADY,
      SLOT_2_AXI_awvalid => ps8_0_axi_periph_M01_AXI_AWVALID(0),
      SLOT_2_AXI_bready => ps8_0_axi_periph_M01_AXI_BREADY(0),
      SLOT_2_AXI_bresp(1 downto 0) => ps8_0_axi_periph_M01_AXI_BRESP(1 downto 0),
      SLOT_2_AXI_bvalid => ps8_0_axi_periph_M01_AXI_BVALID,
      SLOT_2_AXI_rdata(31 downto 0) => ps8_0_axi_periph_M01_AXI_RDATA(31 downto 0),
      SLOT_2_AXI_rready => ps8_0_axi_periph_M01_AXI_RREADY(0),
      SLOT_2_AXI_rresp(1 downto 0) => ps8_0_axi_periph_M01_AXI_RRESP(1 downto 0),
      SLOT_2_AXI_rvalid => ps8_0_axi_periph_M01_AXI_RVALID,
      SLOT_2_AXI_wdata(31 downto 0) => ps8_0_axi_periph_M01_AXI_WDATA(31 downto 0),
      SLOT_2_AXI_wready => ps8_0_axi_periph_M01_AXI_WREADY,
      SLOT_2_AXI_wstrb(3 downto 0) => ps8_0_axi_periph_M01_AXI_WSTRB(3 downto 0),
      SLOT_2_AXI_wvalid => ps8_0_axi_periph_M01_AXI_WVALID(0),
      clk => zynq_ultra_ps_e_0_pl_clk0,
      resetn => proc_sys_reset_0_peripheral_aresetn(0)
    );
tx_dsp_emulator_0: component top_level_tx_dsp_emulator_0_0
     port map (
      clk_125 => clk_wiz_1_clk_125,
      clk_250 => clk_wiz_1_clk_250,
      frev_counter_max_i(15 downto 0) => RegCtrl_ncoEmulator_0_frevCounterMax(15 downto 0),
      frev_offset_i(15 downto 0) => RegCtrl_ncoEmulator_0_frevOffset(15 downto 0),
      m_tx_dsp_data(31 downto 0) => tx_dsp_emulator_0_m_tx_dsp_data(31 downto 0),
      m_tx_dsp_frev => tx_dsp_emulator_0_m_tx_dsp_frev,
      m_tx_dsp_turn_number(31 downto 0) => NLW_tx_dsp_emulator_0_m_tx_dsp_turn_number_UNCONNECTED(31 downto 0),
      m_tx_dsp_valid => NLW_tx_dsp_emulator_0_m_tx_dsp_valid_UNCONNECTED,
      resetn_125 => proc_sys_reset_1_peripheral_aresetn(0),
      resetn_250 => proc_sys_reset_2_peripheral_aresetn(0)
    );
tx_dsp_emulator_1: component top_level_tx_dsp_emulator_0_2
     port map (
      clk_125 => clk_wiz_1_clk_125,
      clk_250 => clk_wiz_1_clk_250,
      frev_counter_max_i(15 downto 0) => RegCtrl_ncoEmulator_1_frevCounterMax(15 downto 0),
      frev_offset_i(15 downto 0) => RegCtrl_ncoEmulator_1_frevOffset(15 downto 0),
      m_tx_dsp_data(31 downto 0) => tx_dsp_emulator_1_m_tx_dsp_data(31 downto 0),
      m_tx_dsp_frev => tx_dsp_emulator_1_m_tx_dsp_frev,
      m_tx_dsp_turn_number(31 downto 0) => NLW_tx_dsp_emulator_1_m_tx_dsp_turn_number_UNCONNECTED(31 downto 0),
      m_tx_dsp_valid => NLW_tx_dsp_emulator_1_m_tx_dsp_valid_UNCONNECTED,
      resetn_125 => proc_sys_reset_1_peripheral_aresetn(0),
      resetn_250 => proc_sys_reset_2_peripheral_aresetn(0)
    );
tx_nco_emulator_0: component top_level_tx_nco_emulator_0_0
     port map (
      clk => clk_wiz_1_clk_250,
      m_tx_nco_id(7 downto 0) => tx_nco_emulator_0_m_tx_nco_id(7 downto 0),
      resetn => proc_sys_reset_2_peripheral_aresetn(0)
    );
util_vector_logic_0: component top_level_util_vector_logic_0_1
     port map (
      Op1(0) => resampler_0_i_valid,
      Op2(0) => resampler_0_q_valid,
      Res(0) => util_vector_logic_0_Res(0)
    );
util_vector_logic_1: component top_level_util_vector_logic_0_2
     port map (
      Op1(0) => resampler_1_i_valid,
      Op2(0) => resampler_1_q_valid,
      Res(0) => util_vector_logic_1_Res(0)
    );
util_vector_logic_clk_125_new: component top_level_util_vector_logic_2_1
     port map (
      Op1(0) => clk_wiz_1_clk_125,
      Op2(0) => '0',
      Res(0) => util_vector_logic_clk_125_new_Res(0)
    );
wr_emulator_0: component top_level_wr_emulator_0_0
     port map (
      clk_250 => clk_wiz_1_clk_250,
      resetn_250 => proc_sys_reset_2_peripheral_aresetn(0),
      sync => wr_emulator_0_sync,
      sync_offset_i(15 downto 0) => RegCtrl_ncoEmulator_0_syncOffset(15 downto 0)
    );
xlconcat_0: component top_level_xlconcat_0_0
     port map (
      In0(15 downto 0) => resampler_0_q_data_out(15 downto 0),
      In1(15 downto 0) => resampler_0_i_data_out(15 downto 0),
      dout(31 downto 0) => xlconcat_0_dout(31 downto 0)
    );
xlconcat_1: component top_level_xlconcat_0_1
     port map (
      In0(15 downto 0) => resampler_1_q_data_out(15 downto 0),
      In1(15 downto 0) => resampler_1_i_data_out(15 downto 0),
      dout(31 downto 0) => xlconcat_1_dout(31 downto 0)
    );
xlconcat_tout_rsp_0: component top_level_xlconcat_1_0
     port map (
      In0(15 downto 0) => RegCtrl_resampler_0_toutUpLow16(15 downto 0),
      In1(15 downto 0) => RegCtrl_resampler_0_toutUpHigh16(15 downto 0),
      dout(31 downto 0) => xlconcat_tout_rsp_0_dout(31 downto 0)
    );
xlconcat_tout_rsp_1: component top_level_xlconcat_tout_rsp_0_0
     port map (
      In0(15 downto 0) => RegCtrl_resampler_1_toutUpLow16(15 downto 0),
      In1(15 downto 0) => RegCtrl_resampler_1_toutUpHigh16(15 downto 0),
      dout(31 downto 0) => xlconcat_tout_rsp_1_dout(31 downto 0)
    );
xlconstant_0: component top_level_xlconstant_0_0
     port map (
      dout(0) => xlconstant_0_dout(0)
    );
xlslice_0_i: component top_level_xlslice_0_0
     port map (
      Din(31 downto 0) => tx_dsp_emulator_0_m_tx_dsp_data(31 downto 0),
      Dout(15 downto 0) => xlslice_0_i_Dout(15 downto 0)
    );
xlslice_0_q: component top_level_xlslice_0_i_0
     port map (
      Din(31 downto 0) => tx_dsp_emulator_0_m_tx_dsp_data(31 downto 0),
      Dout(15 downto 0) => xlslice_0_q_Dout(15 downto 0)
    );
xlslice_1_i: component top_level_xlslice_0_i_1
     port map (
      Din(31 downto 0) => tx_dsp_emulator_1_m_tx_dsp_data(31 downto 0),
      Dout(15 downto 0) => xlslice_1_i_Dout(15 downto 0)
    );
xlslice_1_q: component top_level_xlslice_0_q_0
     port map (
      Din(31 downto 0) => tx_dsp_emulator_1_m_tx_dsp_data(31 downto 0),
      Dout(15 downto 0) => xlslice_1_q_Dout(15 downto 0)
    );
xlslice_cheburashka: component top_level_xlslice_0_1
     port map (
      Din(31 downto 0) => axi4lite_cheburashka_bridge_0_ch_addr_o(31 downto 0),
      Dout(9 downto 0) => xlslice_cheburashka_Dout(9 downto 0)
    );
zynq_ultra_ps_e_0: component top_level_zynq_ultra_ps_e_0_0
     port map (
      emio_enet0_tsu_inc_ctrl(1 downto 0) => B"00",
      emio_enet0_tsu_timer_cmp_val => NLW_zynq_ultra_ps_e_0_emio_enet0_tsu_timer_cmp_val_UNCONNECTED,
      emio_uart1_rxd => '0',
      emio_uart1_txd => NLW_zynq_ultra_ps_e_0_emio_uart1_txd_UNCONNECTED,
      maxigp0_araddr(39 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARADDR(39 downto 0),
      maxigp0_arburst(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARBURST(1 downto 0),
      maxigp0_arcache(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARCACHE(3 downto 0),
      maxigp0_arid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARID(15 downto 0),
      maxigp0_arlen(7 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLEN(7 downto 0),
      maxigp0_arlock => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARLOCK,
      maxigp0_arprot(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARPROT(2 downto 0),
      maxigp0_arqos(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARQOS(3 downto 0),
      maxigp0_arready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARREADY,
      maxigp0_arsize(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARSIZE(2 downto 0),
      maxigp0_aruser(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARUSER(15 downto 0),
      maxigp0_arvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_ARVALID,
      maxigp0_awaddr(39 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWADDR(39 downto 0),
      maxigp0_awburst(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWBURST(1 downto 0),
      maxigp0_awcache(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWCACHE(3 downto 0),
      maxigp0_awid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWID(15 downto 0),
      maxigp0_awlen(7 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLEN(7 downto 0),
      maxigp0_awlock => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWLOCK,
      maxigp0_awprot(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWPROT(2 downto 0),
      maxigp0_awqos(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWQOS(3 downto 0),
      maxigp0_awready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWREADY,
      maxigp0_awsize(2 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWSIZE(2 downto 0),
      maxigp0_awuser(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWUSER(15 downto 0),
      maxigp0_awvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_AWVALID,
      maxigp0_bid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BID(15 downto 0),
      maxigp0_bready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BREADY,
      maxigp0_bresp(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BRESP(1 downto 0),
      maxigp0_bvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_BVALID,
      maxigp0_rdata(31 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RDATA(31 downto 0),
      maxigp0_rid(15 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RID(15 downto 0),
      maxigp0_rlast => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RLAST,
      maxigp0_rready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RREADY,
      maxigp0_rresp(1 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RRESP(1 downto 0),
      maxigp0_rvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_RVALID,
      maxigp0_wdata(31 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WDATA(31 downto 0),
      maxigp0_wlast => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WLAST,
      maxigp0_wready => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WREADY,
      maxigp0_wstrb(3 downto 0) => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WSTRB(3 downto 0),
      maxigp0_wvalid => zynq_ultra_ps_e_0_M_AXI_HPM0_FPD_WVALID,
      maxihpm0_fpd_aclk => zynq_ultra_ps_e_0_pl_clk0,
      pl_clk0 => zynq_ultra_ps_e_0_pl_clk0,
      pl_ps_irq0(0) => '0',
      pl_resetn0 => zynq_ultra_ps_e_0_pl_resetn0
    );
end STRUCTURE;

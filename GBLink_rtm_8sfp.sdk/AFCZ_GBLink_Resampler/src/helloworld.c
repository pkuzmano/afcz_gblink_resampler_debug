/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include "GBLink.h"
#include "xil_io.h"
#include "sleep.h"

#define RESAMPLER_MEM_RANGE       (0x00000400) // 1kB
#define NCO_EMULATOR_MEM_RANGE    (0x00000400) // 1kB

#define RESAMPLER_CH1_BASEADDR    (XPAR_AXI4LITE_CHEBURASHKA_BRIDGE_0_BASEADDR)
#define NCO_EMULATOR_CH1_BASEADDR (RESAMPLER_CH1_BASEADDR + RESAMPLER_MEM_RANGE)
#define RESAMPLER_CH2_BASEADDR    (NCO_EMULATOR_CH1_BASEADDR + NCO_EMULATOR_MEM_RANGE)
#define NCO_EMULATOR_CH2_BASEADDR (RESAMPLER_CH2_BASEADDR + RESAMPLER_MEM_RANGE)


int main()
{
    init_platform();

    print("Hello World\n\r");

    xil_printf("Simple readout...\n\r");

    u32 reg;

    xil_printf("Resampler CH1...\n\r");

    u32 tout_ch1;
    tout_ch1 = 0x02800000; // 0.625
    //tout_ch1 = 0x04000000; // 1.0

    reg = Xil_In32(RESAMPLER_CH1_BASEADDR + 0*4);
    xil_printf("(get) Tout Higher 16 bits : 0x%04x\n\r", reg);
    Xil_Out32(RESAMPLER_CH1_BASEADDR + 0*4, 0xFFFF0000 + ((tout_ch1 & 0xFFFF0000) >> 16));
    reg = Xil_In32(RESAMPLER_CH1_BASEADDR + 0*4);
    xil_printf("(set) Tout Higher 16 bits : 0x%04x\n\r", reg);

    reg = Xil_In32(RESAMPLER_CH1_BASEADDR + 1*4);
    xil_printf("(get) Tout Lower 16 bits : 0x%04x\n\r", reg);
    Xil_Out32(RESAMPLER_CH1_BASEADDR + 1*4, 0xFFFF0000 + (tout_ch1 & 0x0000FFFF));
    reg = Xil_In32(RESAMPLER_CH1_BASEADDR + 1*4);
    xil_printf("(set) Tout Lower 16 bits : 0x%04x\n\r", reg);

    xil_printf("Resampler CH2...\n\r");

    u32 tout_ch2;
    //tout_ch2 = 0x02800000; // 0.625
    tout_ch2 = 0x04000000; // 1.0

    reg = Xil_In32(RESAMPLER_CH2_BASEADDR + 0*4);
    xil_printf("(get) Tout Higher 16 bits : 0x%04x\n\r", reg);
    Xil_Out32(RESAMPLER_CH2_BASEADDR + 0*4, 0xFFFF0000 + ((tout_ch2 & 0xFFFF0000) >> 16));
    reg = Xil_In32(RESAMPLER_CH2_BASEADDR + 0*4);
    xil_printf("(set) Tout Higher 16 bits : 0x%04x\n\r", reg);

    reg = Xil_In32(RESAMPLER_CH2_BASEADDR + 1*4);
    xil_printf("(get) Tout Lower 16 bits : 0x%04x\n\r", reg);
    Xil_Out32(RESAMPLER_CH2_BASEADDR + 1*4, 0xFFFF0000 + (tout_ch2 & 0x0000FFFF));
    reg = Xil_In32(RESAMPLER_CH2_BASEADDR + 1*4);
    xil_printf("(set) Tout Lower 16 bits : 0x%04x\n\r", reg);

    xil_printf("NCO Emulator CH1...\n\r");

    u32 frev_cnt_max_ch1 = 31; // 1 frev pulse every 32 clock periods (125MHz, 8ns)
    u32 frev_offset_ch1 = 30; // frev pulse offset (125MHz, 8ns)
    u32 sync_offset_ch1 = 200; // sync pulse offset (125MHz, 8ns)

    reg = Xil_In32(NCO_EMULATOR_CH1_BASEADDR + 0*4);
    xil_printf("(get) Frev Period : %d = %dns\n\r", reg + 1, (reg + 1)*8);
    Xil_Out32(NCO_EMULATOR_CH1_BASEADDR + 0*4, 0xFFFF0000 + frev_cnt_max_ch1);
    reg = Xil_In32(NCO_EMULATOR_CH1_BASEADDR + 0*4);
    xil_printf("(set) Frev Period : %d = %dns\n\r", reg + 1, (reg + 1)*8);

    reg = Xil_In32(NCO_EMULATOR_CH1_BASEADDR + 1*4);
    xil_printf("(get) Frev Offset : %d = %dns\n\r", reg, reg*8);
    Xil_Out32(NCO_EMULATOR_CH1_BASEADDR + 1*4, 0xFFFF0000 + frev_offset_ch1);
    reg = Xil_In32(NCO_EMULATOR_CH1_BASEADDR + 1*4);
    xil_printf("(set) Frev Offset : %d = %dns\n\r", reg, reg);

    reg = Xil_In32(NCO_EMULATOR_CH1_BASEADDR + 2*4);
    xil_printf("(get) Sync Offset : %d = %dns\n\r", reg, reg*8);
    Xil_Out32(NCO_EMULATOR_CH1_BASEADDR + 2*4, 0xFFFF0000 + sync_offset_ch1);
    reg = Xil_In32(NCO_EMULATOR_CH1_BASEADDR + 2*4);
    xil_printf("(set) Sync Offset : %d = %dns\n\r", reg, reg);

    xil_printf("NCO Emulator CH2...\n\r");

    u32 frev_cnt_max_ch2 = 31; // 1 frev pulse every 32 clock periods (125MHz, 8ns)
    u32 frev_offset_ch2 = 40; // frev pulse offset (125MHz, 8ns)

    reg = Xil_In32(NCO_EMULATOR_CH2_BASEADDR + 0*4);
    xil_printf("(get) Frev Period : %d = %dns\n\r", reg + 1, (reg + 1)*8);
    Xil_Out32(NCO_EMULATOR_CH2_BASEADDR + 0*4, 0xFFFF0000 + frev_cnt_max_ch2);
    reg = Xil_In32(NCO_EMULATOR_CH2_BASEADDR + 0*4);
    xil_printf("(set) Frev Period : %d = %dns\n\r", reg + 1, (reg + 1)*8);

    reg = Xil_In32(NCO_EMULATOR_CH2_BASEADDR + 1*4);
    xil_printf("(get) Frev Offset : %d = %dns\n\r", reg, reg*8);
    Xil_Out32(NCO_EMULATOR_CH2_BASEADDR + 1*4, 0xFFFF0000 + frev_offset_ch2);
    reg = Xil_In32(NCO_EMULATOR_CH2_BASEADDR + 1*4);
    xil_printf("(set) Frev Offset : %d = %dns\n\r", reg, reg);

    xil_printf("GBLink...\n\r");

    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0*4);
    xil_printf("Firmware ID     : 0x%08x\n\r", reg);

    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 1*4);
    xil_printf("Firmware version: 0x%08x\n\r", reg);

    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 2*4);
    xil_printf("Designer ID     : 0x%08x\n\r", reg);

    // TX controls area: 0x800
    xil_printf("\n\rTX controls\n\r\n\r");

    // Disabling TX logic
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 0*4, 0xFFFF0000);

    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 0*4);
    xil_printf("TX logic: %s\n\r", (reg & 0x00000001) ? "enabled" : "disabled");

    // numBuckets register
    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 1*4);
    xil_printf("Number of buckets : %d\n\r", reg);
    // modify # buckets
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 1*4, 0xFFFF0000 + 3075 /*4620*/);
    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 1*4);
    xil_printf("(set) Number of buckets : %d\n\r", reg);

    // interleaver register
    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 2*4);
    xil_printf("Interleaver enable: %s\n\r", (reg & 0x00000001) ? "1" : "0");
    xil_printf("Rate ratio, ch1   : %d\n\r", (reg & 0x00000030) >> 4);
    xil_printf("Rate ratio, ch2   : %d\n\r", (reg & 0x00000300) >> 8);
    // modify interleaver controls
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 2*4, 0xFFFF0000 + 0x00000000 + ((0) << 4) + ((0) << 8));
    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 2*4);
    xil_printf("(set) Interleaver enable : %s\n\r", (reg & 0x00000001) ? "1" : "0");
    xil_printf("(set) Rate ratio, ch1   : %d\n\r", (reg & 0x00000030) >> 4);
    xil_printf("(set) Rate ratio, ch2   : %d\n\r", (reg & 0x00000300) >> 8);

    // fixedLatency register
    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 3*4);
    xil_printf("Fixed-latency mode enable: %s\n\r", (reg & 0x00000000) ? "1" : "0");
    // modify fixed-latency mode controls
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 3*4, 0xFFFF0000 + 0x00000000);
    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 3*4);
    xil_printf("(set) Fixed-latency mode enable: %s\n\r", (reg & 0x00000001) ? "1" : "0");

    /*xil_printf("Issuing Aurora reset through Aurora control register...\n\r");
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 4*4, 0xFFFF0001);
    xil_printf("Aurora control  : wrote 0x%08x\n\r", 0xFFFF0001);

    // wait until "gt_pll_lock", "lane_up" and "channel_up" are asserted
    while (1) {
        reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 3*4);
        xil_printf("Aurora status     : 0x%08x\n\r", reg);
        if ((reg & 0x0000001C) == 0x0000001C)
        	break;
        sleep(1);
    }
    xil_printf("gt_pll_lock: OK\n\r");
    xil_printf("lane_up    : OK\n\r");
    xil_printf("channel_up : OK\n\r");

    xil_printf("Waiting for Aurora reset bit to auto-clear...\n\r");
    while (1) {
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 4*4); // Aurora control
        xil_printf("Aurora control  : 0x%08x\n\r", reg);
    	if (!(reg & 0x00000001))
    		break;
    	sleep(5);
    }

    xil_printf("Checking latched Aurora status bits...\n\r");
    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 3*4);
    xil_printf("gt_pll_lock_latched : %s\n\r", (reg & 0x00000400) ? "1" : "0");
    xil_printf("lane_up_latched     : %s\n\r", (reg & 0x00000800) ? "1" : "0");
    xil_printf("channel_up_latched  : %s\n\r", (reg & 0x00001000) ? "1" : "0");
    xil_printf("hard_err_latched    : %s\n\r", (reg & 0x00002000) ? "1" : "0");
    xil_printf("soft_err_latched    : %s\n\r", (reg & 0x00004000) ? "1" : "0");

    xil_printf("Issuing Aurora latched status clear through Aurora control register...\n\r");
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 4*4, 0xFFFF007C);
    xil_printf("Aurora control  : wrote 0x%08x\n\r", 0xFFFF007C);

    xil_printf("Waiting for Aurora latched status clear bits to auto-clear...\n\r");
    while (1) {
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 4*4); // Aurora control
        xil_printf("Aurora control  : 0x%08x\n\r", reg);
    	if (!(reg & 0x0000007C))
    		break;
    	sleep(5);
    }*/

    xil_printf("Issuing TX logic reset through TXControls/General register...\n\r");
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 0*4, 0xFFFF0002);

    xil_printf("Waiting for TXControls/General reset bit to auto-clear...\n\r");
    while (1) {
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 0*4); // TXControls/General
        xil_printf("TXControl/General  : 0x%08x\n\r", reg);
    	if (!(reg & 0x00000002))
    		break;
    	sleep(5);
    }

    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 0*4);
    xil_printf("TX logic: %s\n\r", (reg & 0x00000001) ? "enabled" : "disabled");

    // Enabling TX logic
    Xil_Out32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 0*4, 0xFFFF0001);

    reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x800 + 0*4);
    xil_printf("TX logic: %s\n\r", (reg & 0x00000001) ? "enabled" : "disabled");

    /*xil_printf("Checking latched Aurora status bits periodically ...\n\r");
    while (1) {
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 3*4);
    	xil_printf("gt_pll_lock_latched : %s\n\r", (reg & 0x00000400) ? "1" : "0");
    	xil_printf("lane_up_latched     : %s\n\r", (reg & 0x00000800) ? "1" : "0");
    	xil_printf("channel_up_latched  : %s\n\r", (reg & 0x00001000) ? "1" : "0");
    	xil_printf("hard_err_latched    : %s\n\r", (reg & 0x00002000) ? "1" : "0");
    	xil_printf("soft_err_latched    : %s\n\r", (reg & 0x00004000) ? "1" : "0");
    	if ((reg & 0x00007C00) != 0x00001C00) {
    		printf("Failure!\n\r");
    		break;
    	}
    	sleep(5);
    }*/

    /*xil_printf("Checking TX status registers periodically ...\n\r");
    // TX status area: 0x400
    xil_printf("\n\rTX status\n\r\n\r");
    while (1) {
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x400 + 0*4);
    	xil_printf("fifoCh1NumData     : %d\n\r", reg);
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x400 + 1*4);
    	xil_printf("fifoCh1NumDataTurn : %d\n\r", reg);
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x400 + 2*4);
    	xil_printf("fifoCh2NumData     : %d\n\r", reg);
    	reg = Xil_In32(XPAR_GBLINK_0_S_AXI_BASEADDR + 0x400 + 3*4);
    	xil_printf("fifoCh2NumDataTurn : %d\n\r", reg);
    	sleep(2);
    }*/

    cleanup_platform();
    return 0;
}

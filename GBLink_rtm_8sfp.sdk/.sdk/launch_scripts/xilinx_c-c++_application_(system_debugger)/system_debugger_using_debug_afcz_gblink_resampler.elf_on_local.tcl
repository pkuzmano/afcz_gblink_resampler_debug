#If required use the below command and launch symbol server from an external shell.
#symbol_server.bat -S -s tcp::1534
connect -path [list tcp::1534 tcp:pcbe13169.cern.ch:3121]
source C:/Xilinx/SDK/2017.4/scripts/sdk/util/zynqmp_utils.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-HS3 210299A887D3"} -index 1
loadhw -hw C:/PLDesign/AFCZ_GBlink_Resampler_debug_intercon/GBLink_rtm_8sfp.sdk/top_level_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x80000000 0xbfffffff} {0x400000000 0x5ffffffff} {0x1000000000 0x7fffffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-HS3 210299A887D3"} -index 1
source C:/PLDesign/AFCZ_GBlink_Resampler_debug_intercon/GBLink_rtm_8sfp.sdk/top_level_wrapper_hw_platform_0/psu_init.tcl
psu_init
after 1000
psu_ps_pl_isolation_removal
after 1000
psu_ps_pl_reset_config
catch {psu_protection}
targets -set -nocase -filter {name =~"*A53*0" && jtag_cable_name =~ "Digilent JTAG-HS3 210299A887D3"} -index 1
rst -processor
dow C:/PLDesign/AFCZ_GBlink_Resampler_debug_intercon/GBLink_rtm_8sfp.sdk/AFCZ_GBLink_Resampler/Debug/AFCZ_GBLink_Resampler.elf
configparams force-mem-access 0
bpadd -addr &main

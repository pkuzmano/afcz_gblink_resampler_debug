-------------------------------------------------------------------
-- System Generator version 2018.2 VHDL source file.
--
-- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

--$Header: /devl/xcs/repo/env/Jobs/sysgen/src/xbs/blocks/xlconvert/hdl/xlconvert.vhd,v 1.1 2004/11/22 00:17:30 rosty Exp $
---------------------------------------------------------------------
--
--  Filename      : xlconvert.vhd
--
--  Description   : VHDL description of a fixed point converter block that
--                  converts the input to a new output type.

--
---------------------------------------------------------------------


---------------------------------------------------------------------
--
--  Entity        : xlconvert
--
--  Architecture  : behavior
--
--  Description   : Top level VHDL description of fixed point conver block.
--
---------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity convert_func_call_resampler_xlconvert is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        result : out std_logic_vector (dout_width-1 downto 0));
end convert_func_call_resampler_xlconvert ;

architecture behavior of convert_func_call_resampler_xlconvert is
begin
    -- Convert to output type and do saturation arith.
    result <= convert_type(din, din_width, din_bin_pt, din_arith,
                           dout_width, dout_bin_pt, dout_arith,
                           quantization, overflow);
end behavior;


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity resampler_xlconvert  is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        en_width     : integer := 1;
        en_bin_pt    : integer := 0;
        en_arith     : integer := xlUnsigned;
        bool_conversion : integer :=0;           -- if one, convert ufix_1_0 to
                                                 -- bool
        latency      : integer := 0;             -- Ouput delay clk cycles
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        en  : in std_logic_vector (en_width-1 downto 0);
        ce  : in std_logic;
        clr : in std_logic;
        clk : in std_logic;
        dout : out std_logic_vector (dout_width-1 downto 0));

end resampler_xlconvert ;

architecture behavior of resampler_xlconvert  is

    component synth_reg
        generic (width       : integer;
                 latency     : integer);
        port (i       : in std_logic_vector(width-1 downto 0);
              ce      : in std_logic;
              clr     : in std_logic;
              clk     : in std_logic;
              o       : out std_logic_vector(width-1 downto 0));
    end component;

    component convert_func_call_resampler_xlconvert 
        generic (
            din_width    : integer := 16;            -- Width of input
            din_bin_pt   : integer := 4;             -- Binary point of input
            din_arith    : integer := xlUnsigned;    -- Type of arith of input
            dout_width   : integer := 8;             -- Width of output
            dout_bin_pt  : integer := 2;             -- Binary point of output
            dout_arith   : integer := xlUnsigned;    -- Type of arith of output
            quantization : integer := xlTruncate;    -- xlRound or xlTruncate
            overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
        port (
            din : in std_logic_vector (din_width-1 downto 0);
            result : out std_logic_vector (dout_width-1 downto 0));
    end component;


    -- synthesis translate_off
--    signal real_din, real_dout : real;    -- For debugging info ports
    -- synthesis translate_on
    signal result : std_logic_vector(dout_width-1 downto 0);
    signal internal_ce : std_logic;

begin

    -- Debugging info for internal full precision variables
    -- synthesis translate_off
--     real_din <= to_real(din, din_bin_pt, din_arith);
--     real_dout <= to_real(dout, dout_bin_pt, dout_arith);
    -- synthesis translate_on

    internal_ce <= ce and en(0);

    bool_conversion_generate : if (bool_conversion = 1)
    generate
      result <= din;
    end generate; --bool_conversion_generate

    std_conversion_generate : if (bool_conversion = 0)
    generate
      -- Workaround for XST bug
      convert : convert_func_call_resampler_xlconvert 
        generic map (
          din_width   => din_width,
          din_bin_pt  => din_bin_pt,
          din_arith   => din_arith,
          dout_width  => dout_width,
          dout_bin_pt => dout_bin_pt,
          dout_arith  => dout_arith,
          quantization => quantization,
          overflow     => overflow)
        port map (
          din => din,
          result => result);
    end generate; --std_conversion_generate

    latency_test : if (latency > 0) generate
        reg : synth_reg
            generic map (
              width => dout_width,
              latency => latency
            )
            port map (
              i => result,
              ce => internal_ce,
              clr => clr,
              clk => clk,
              o => dout
            );
    end generate;

    latency0 : if (latency = 0)
    generate
        dout <= result;
    end generate latency0;

end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_ee88b0f9a0 is
  port (
    d : in std_logic_vector((16 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((16 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_ee88b0f9a0;
architecture behavior of sysgen_delay_ee88b0f9a0
is
  signal d_1_22: std_logic_vector((16 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((16 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((16 - 1) downto 0) := "0000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((16 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "0000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  q <= op_mem_0_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_8fc3988b6c is
  port (
    d : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_8fc3988b6c;
architecture behavior of sysgen_delay_8fc3988b6c
is
  signal d_1_22: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic;
  signal op_mem_0_8_24: std_logic := '0';
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= '0';
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  q <= std_logic_to_vector(op_mem_0_8_24);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_552786d6d7 is
  port (
    d : in std_logic_vector((27 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_552786d6d7;
architecture behavior of sysgen_delay_552786d6d7
is
  signal d_1_22: std_logic_vector((27 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((27 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((27 - 1) downto 0) := "000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((27 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  q <= op_mem_0_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_b08e6841ee is
  port (
    d : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_b08e6841ee;
architecture behavior of sysgen_delay_b08e6841ee
is
  signal d_1_22: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic;
  signal op_mem_0_8_24: std_logic := '0';
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic;
  signal op_mem_1_8_24: std_logic := '0';
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic;
  signal op_mem_2_8_24: std_logic := '0';
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic;
  signal op_mem_2_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic;
  signal op_mem_1_join_10_5_rst: std_logic;
begin
  d_1_22 <= d(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= '0';
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= '0';
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= '0';
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_1_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  q <= std_logic_to_vector(op_mem_2_8_24);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_cced485432 is
  port (
    d : in std_logic_vector((44 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((44 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_cced485432;
architecture behavior of sysgen_delay_cced485432
is
  signal d_1_22: std_logic_vector((44 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_1_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  q <= op_mem_2_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_35fdb3d1fe is
  port (
    op : out std_logic_vector((52 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_35fdb3d1fe;
architecture behavior of sysgen_constant_35fdb3d1fe
is
begin
  op <= "0111110100110000001000010110111011101101100100001101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_06d43d52c3 is
  port (
    d : in std_logic_vector((52 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((52 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_06d43d52c3;
architecture behavior of sysgen_delay_06d43d52c3
is
  signal d_1_22: std_logic_vector((52 - 1) downto 0);
  signal en_1_25: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((52 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((52 - 1) downto 0) := "0000000000000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_8_24_en: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((52 - 1) downto 0);
  signal op_mem_0_join_10_5_en: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  en_1_25 <= en(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "0000000000000000000000000000000000000000000000000000";
      elsif ((ce = '1') and (op_mem_0_8_24_en = '1')) then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, en_1_25, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    elsif en_1_25 = '1' then
      op_mem_0_join_10_5_rst <= '0';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    if en_1_25 = '1' then
      op_mem_0_join_10_5_en <= '1';
    else 
      op_mem_0_join_10_5_en <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_0_8_24_en <= op_mem_0_join_10_5_en;
  q <= op_mem_0_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_ac46a2e4aa is
  port (
    d : in std_logic_vector((44 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((44 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_ac46a2e4aa;
architecture behavior of sysgen_delay_ac46a2e4aa
is
  signal d_1_22: std_logic_vector((44 - 1) downto 0);
  signal en_1_25: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_8_24_en: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_join_10_5_en: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  en_1_25 <= en(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000000000000000000";
      elsif ((ce = '1') and (op_mem_0_8_24_en = '1')) then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, en_1_25, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    elsif en_1_25 = '1' then
      op_mem_0_join_10_5_rst <= '0';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    if en_1_25 = '1' then
      op_mem_0_join_10_5_en <= '1';
    else 
      op_mem_0_join_10_5_en <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_0_8_24_en <= op_mem_0_join_10_5_en;
  q <= op_mem_0_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_abs_4665453b1c is
  port (
    a : in std_logic_vector((29 - 1) downto 0);
    op : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_abs_4665453b1c;
architecture behavior of sysgen_abs_4665453b1c
is
  signal a_16_25: signed((29 - 1) downto 0);
  type array_type_op_mem_48_20 is array (0 to (1 - 1)) of signed((29 - 1) downto 0);
  signal op_mem_48_20: array_type_op_mem_48_20 := (
    0 => "00000000000000000000000000000");
  signal op_mem_48_20_front_din: signed((29 - 1) downto 0);
  signal op_mem_48_20_back: signed((29 - 1) downto 0);
  signal op_mem_48_20_push_front_pop_back_en: std_logic;
  signal cast_34_28: signed((30 - 1) downto 0);
  signal internal_ip_34_13_neg: signed((30 - 1) downto 0);
  signal rel_31_8: boolean;
  signal internal_ip_join_31_5: signed((30 - 1) downto 0);
  signal internal_ip_join_28_1: signed((30 - 1) downto 0);
  signal cast_internal_ip_40_3_convert: signed((29 - 1) downto 0);
begin
  a_16_25 <= std_logic_vector_to_signed(a);
  op_mem_48_20_back <= op_mem_48_20(0);
  proc_op_mem_48_20: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_48_20_push_front_pop_back_en = '1')) then
        op_mem_48_20(0) <= op_mem_48_20_front_din;
      end if;
    end if;
  end process proc_op_mem_48_20;
  cast_34_28 <= s2s_cast(a_16_25, 26, 30, 26);
  internal_ip_34_13_neg <=  -cast_34_28;
  rel_31_8 <= a_16_25 >= std_logic_vector_to_signed("00000000000000000000000000000");
  proc_if_31_5: process (a_16_25, internal_ip_34_13_neg, rel_31_8)
  is
  begin
    if rel_31_8 then
      internal_ip_join_31_5 <= s2s_cast(a_16_25, 26, 30, 26);
    else 
      internal_ip_join_31_5 <= internal_ip_34_13_neg;
    end if;
  end process proc_if_31_5;
  proc_if_28_1: process (internal_ip_join_31_5)
  is
  begin
    if false then
      internal_ip_join_28_1 <= std_logic_vector_to_signed("000000000000000000000000000000");
    else 
      internal_ip_join_28_1 <= internal_ip_join_31_5;
    end if;
  end process proc_if_28_1;
  cast_internal_ip_40_3_convert <= s2s_cast(internal_ip_join_28_1, 26, 29, 26);
  op_mem_48_20_push_front_pop_back_en <= '0';
  op <= signed_to_std_logic_vector(cast_internal_ip_40_3_convert);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_concat_35df48c783 is
  port (
    in0 : in std_logic_vector((1 - 1) downto 0);
    in1 : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((2 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_concat_35df48c783;
architecture behavior of sysgen_concat_35df48c783
is
  signal in0_1_23: boolean;
  signal in1_1_27: boolean;
  signal y_2_1_concat: unsigned((2 - 1) downto 0);
begin
  in0_1_23 <= ((in0) = "1");
  in1_1_27 <= ((in1) = "1");
  y_2_1_concat <= std_logic_vector_to_unsigned(boolean_to_vector(in0_1_23) & boolean_to_vector(in1_1_27));
  y <= unsigned_to_std_logic_vector(y_2_1_concat);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_28d7591431 is
  port (
    op : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_28d7591431;
architecture behavior of sysgen_constant_28d7591431
is
begin
  op <= "00000000000000000000000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_e1a3721ebb is
  port (
    op : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_e1a3721ebb;
architecture behavior of sysgen_constant_e1a3721ebb
is
begin
  op <= "00010000000000000000000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_1f637969ae is
  port (
    op : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_1f637969ae;
architecture behavior of sysgen_constant_1f637969ae
is
begin
  op <= "11100000000000000000000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
--
--  Filename      : xlregister.vhd
--
--  Description   : VHDL description of an arbitrary wide register.
--                  Unlike the delay block, an initial value is
--                  specified and is considered valid at the start
--                  of simulation.  The register is only one word
--                  deep.
--
--  Mod. History  : Removed valid bit logic from wrapper.
--                : Changed VHDL to use a bit_vector generic for its
--
---------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity resampler_xlregister is

   generic (d_width          : integer := 5;          -- Width of d input
            init_value       : bit_vector := b"00");  -- Binary init value string

   port (d   : in std_logic_vector (d_width-1 downto 0);
         rst : in std_logic_vector(0 downto 0) := "0";
         en  : in std_logic_vector(0 downto 0) := "1";
         ce  : in std_logic;
         clk : in std_logic;
         q   : out std_logic_vector (d_width-1 downto 0));

end resampler_xlregister;

architecture behavior of resampler_xlregister is

   component synth_reg_w_init
      generic (width      : integer;
               init_index : integer;
               init_value : bit_vector;
               latency    : integer);
      port (i   : in std_logic_vector(width-1 downto 0);
            ce  : in std_logic;
            clr : in std_logic;
            clk : in std_logic;
            o   : out std_logic_vector(width-1 downto 0));
   end component; -- end synth_reg_w_init

   -- synthesis translate_off
   signal real_d, real_q           : real;    -- For debugging info ports
   -- synthesis translate_on
   signal internal_clr             : std_logic;
   signal internal_ce              : std_logic;

begin

   internal_clr <= rst(0) and ce;
   internal_ce  <= en(0) and ce;

   -- Synthesizable behavioral model
   synth_reg_inst : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d,
                ce  => internal_ce,
                clr => internal_clr,
                clk => clk,
                o   => q);

end architecture behavior;


library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_4518c31358 is
  port (
    d : in std_logic_vector((29 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_4518c31358;
architecture behavior of sysgen_delay_4518c31358
is
  signal d_1_22: std_logic_vector((29 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  q <= op_mem_0_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_inverter_3b83bb2925 is
  port (
    ip : in std_logic_vector((1 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_inverter_3b83bb2925;
architecture behavior of sysgen_inverter_3b83bb2925
is
  signal ip_1_26: boolean;
  type array_type_op_mem_22_20 is array (0 to (1 - 1)) of boolean;
  signal op_mem_22_20: array_type_op_mem_22_20 := (
    0 => false);
  signal op_mem_22_20_front_din: boolean;
  signal op_mem_22_20_back: boolean;
  signal op_mem_22_20_push_front_pop_back_en: std_logic;
  signal internal_ip_12_1_bitnot: boolean;
begin
  ip_1_26 <= ((ip) = "1");
  op_mem_22_20_back <= op_mem_22_20(0);
  proc_op_mem_22_20: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_22_20_push_front_pop_back_en = '1')) then
        op_mem_22_20(0) <= op_mem_22_20_front_din;
      end if;
    end if;
  end process proc_op_mem_22_20;
  internal_ip_12_1_bitnot <= ((not boolean_to_vector(ip_1_26)) = "1");
  op_mem_22_20_push_front_pop_back_en <= '0';
  op <= boolean_to_vector(internal_ip_12_1_bitnot);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_mux_bbd0f8a028 is
  port (
    sel : in std_logic_vector((2 - 1) downto 0);
    d0 : in std_logic_vector((29 - 1) downto 0);
    d1 : in std_logic_vector((29 - 1) downto 0);
    d2 : in std_logic_vector((29 - 1) downto 0);
    d3 : in std_logic_vector((29 - 1) downto 0);
    y : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_mux_bbd0f8a028;
architecture behavior of sysgen_mux_bbd0f8a028
is
  signal sel_1_20: std_logic_vector((2 - 1) downto 0);
  signal d0_1_24: std_logic_vector((29 - 1) downto 0);
  signal d1_1_27: std_logic_vector((29 - 1) downto 0);
  signal d2_1_30: std_logic_vector((29 - 1) downto 0);
  signal d3_1_33: std_logic_vector((29 - 1) downto 0);
  signal unregy_join_6_1: std_logic_vector((29 - 1) downto 0);
begin
  sel_1_20 <= sel;
  d0_1_24 <= d0;
  d1_1_27 <= d1;
  d2_1_30 <= d2;
  d3_1_33 <= d3;
  proc_switch_6_1: process (d0_1_24, d1_1_27, d2_1_30, d3_1_33, sel_1_20)
  is
  begin
    case sel_1_20 is 
      when "00" =>
        unregy_join_6_1 <= d0_1_24;
      when "01" =>
        unregy_join_6_1 <= d1_1_27;
      when "10" =>
        unregy_join_6_1 <= d2_1_30;
      when others =>
        unregy_join_6_1 <= d3_1_33;
    end case;
  end process proc_switch_6_1;
  y <= unregy_join_6_1;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_relational_d8d46a9b80 is
  port (
    a : in std_logic_vector((29 - 1) downto 0);
    b : in std_logic_vector((29 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_relational_d8d46a9b80;
architecture behavior of sysgen_relational_d8d46a9b80
is
  signal a_1_31: signed((29 - 1) downto 0);
  signal b_1_34: signed((29 - 1) downto 0);
  signal result_18_3_rel: boolean;
begin
  a_1_31 <= std_logic_vector_to_signed(a);
  b_1_34 <= std_logic_vector_to_signed(b);
  result_18_3_rel <= a_1_31 > b_1_34;
  op <= boolean_to_vector(result_18_3_rel);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity resampler_xldelay is
   generic(width        : integer := -1;
           latency      : integer := -1;
           reg_retiming : integer :=  0;
           reset        : integer :=  0);
   port(d       : in std_logic_vector (width-1 downto 0);
        ce      : in std_logic;
        clk     : in std_logic;
        en      : in std_logic;
        rst     : in std_logic;
        q       : out std_logic_vector (width-1 downto 0));

end resampler_xldelay;

architecture behavior of resampler_xldelay is
   component synth_reg
      generic (width       : integer;
               latency     : integer);
      port (i       : in std_logic_vector(width-1 downto 0);
            ce      : in std_logic;
            clr     : in std_logic;
            clk     : in std_logic;
            o       : out std_logic_vector(width-1 downto 0));
   end component; -- end component synth_reg

   component synth_reg_reg
      generic (width       : integer;
               latency     : integer);
      port (i       : in std_logic_vector(width-1 downto 0);
            ce      : in std_logic;
            clr     : in std_logic;
            clk     : in std_logic;
            o       : out std_logic_vector(width-1 downto 0));
   end component;

   signal internal_ce  : std_logic;

begin
   internal_ce  <= ce and en;

   srl_delay: if ((reg_retiming = 0) and (reset = 0)) or (latency < 1) generate
     synth_reg_srl_inst : synth_reg
       generic map (
         width   => width,
         latency => latency)
       port map (
         i   => d,
         ce  => internal_ce,
         clr => '0',
         clk => clk,
         o   => q);
   end generate srl_delay;

   reg_delay: if ((reg_retiming = 1) or (reset = 1)) and (latency >= 1) generate
     synth_reg_reg_inst : synth_reg_reg
       generic map (
         width   => width,
         latency => latency)
       port map (
         i   => d,
         ce  => internal_ce,
         clr => rst,
         clk => clk,
         o   => q);
   end generate reg_delay;
end architecture behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_b7f5764dfb is
  port (
    d : in std_logic_vector((44 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((44 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_b7f5764dfb;
architecture behavior of sysgen_delay_b7f5764dfb
is
  signal d_1_22: std_logic_vector((44 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_3_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_3_8_24_rst: std_logic;
  signal op_mem_4_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_4_8_24_rst: std_logic;
  signal op_mem_5_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_5_8_24_rst: std_logic;
  signal op_mem_6_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_6_8_24_rst: std_logic;
  signal op_mem_7_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_7_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_7_8_24_rst: std_logic;
  signal op_mem_8_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_8_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_8_8_24_rst: std_logic;
  signal op_mem_9_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_9_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_9_8_24_rst: std_logic;
  signal op_mem_10_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_10_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_10_8_24_rst: std_logic;
  signal op_mem_11_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_11_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_11_8_24_rst: std_logic;
  signal op_mem_12_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_12_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_12_8_24_rst: std_logic;
  signal op_mem_13_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_13_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_13_8_24_rst: std_logic;
  signal op_mem_14_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_14_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_14_8_24_rst: std_logic;
  signal op_mem_15_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_15_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_15_8_24_rst: std_logic;
  signal op_mem_16_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_16_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_16_8_24_rst: std_logic;
  signal op_mem_17_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_17_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_17_8_24_rst: std_logic;
  signal op_mem_18_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_18_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_18_8_24_rst: std_logic;
  signal op_mem_18_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_18_join_10_5_rst: std_logic;
  signal op_mem_15_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_15_join_10_5_rst: std_logic;
  signal op_mem_14_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_14_join_10_5_rst: std_logic;
  signal op_mem_13_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_13_join_10_5_rst: std_logic;
  signal op_mem_3_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_join_10_5_rst: std_logic;
  signal op_mem_17_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_17_join_10_5_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
  signal op_mem_6_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_join_10_5_rst: std_logic;
  signal op_mem_5_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_join_10_5_rst: std_logic;
  signal op_mem_7_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_7_join_10_5_rst: std_logic;
  signal op_mem_9_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_9_join_10_5_rst: std_logic;
  signal op_mem_11_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_11_join_10_5_rst: std_logic;
  signal op_mem_12_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_12_join_10_5_rst: std_logic;
  signal op_mem_8_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_8_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_4_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_join_10_5_rst: std_logic;
  signal op_mem_16_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_16_join_10_5_rst: std_logic;
  signal op_mem_10_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_10_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_op_mem_3_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_3_8_24_rst = '1')) then
        op_mem_3_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_3_8_24 <= op_mem_3_8_24_next;
      end if;
    end if;
  end process proc_op_mem_3_8_24;
  proc_op_mem_4_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_4_8_24_rst = '1')) then
        op_mem_4_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_4_8_24 <= op_mem_4_8_24_next;
      end if;
    end if;
  end process proc_op_mem_4_8_24;
  proc_op_mem_5_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_5_8_24_rst = '1')) then
        op_mem_5_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_5_8_24 <= op_mem_5_8_24_next;
      end if;
    end if;
  end process proc_op_mem_5_8_24;
  proc_op_mem_6_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_6_8_24_rst = '1')) then
        op_mem_6_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_6_8_24 <= op_mem_6_8_24_next;
      end if;
    end if;
  end process proc_op_mem_6_8_24;
  proc_op_mem_7_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_7_8_24_rst = '1')) then
        op_mem_7_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_7_8_24 <= op_mem_7_8_24_next;
      end if;
    end if;
  end process proc_op_mem_7_8_24;
  proc_op_mem_8_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_8_8_24_rst = '1')) then
        op_mem_8_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_8_8_24 <= op_mem_8_8_24_next;
      end if;
    end if;
  end process proc_op_mem_8_8_24;
  proc_op_mem_9_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_9_8_24_rst = '1')) then
        op_mem_9_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_9_8_24 <= op_mem_9_8_24_next;
      end if;
    end if;
  end process proc_op_mem_9_8_24;
  proc_op_mem_10_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_10_8_24_rst = '1')) then
        op_mem_10_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_10_8_24 <= op_mem_10_8_24_next;
      end if;
    end if;
  end process proc_op_mem_10_8_24;
  proc_op_mem_11_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_11_8_24_rst = '1')) then
        op_mem_11_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_11_8_24 <= op_mem_11_8_24_next;
      end if;
    end if;
  end process proc_op_mem_11_8_24;
  proc_op_mem_12_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_12_8_24_rst = '1')) then
        op_mem_12_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_12_8_24 <= op_mem_12_8_24_next;
      end if;
    end if;
  end process proc_op_mem_12_8_24;
  proc_op_mem_13_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_13_8_24_rst = '1')) then
        op_mem_13_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_13_8_24 <= op_mem_13_8_24_next;
      end if;
    end if;
  end process proc_op_mem_13_8_24;
  proc_op_mem_14_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_14_8_24_rst = '1')) then
        op_mem_14_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_14_8_24 <= op_mem_14_8_24_next;
      end if;
    end if;
  end process proc_op_mem_14_8_24;
  proc_op_mem_15_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_15_8_24_rst = '1')) then
        op_mem_15_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_15_8_24 <= op_mem_15_8_24_next;
      end if;
    end if;
  end process proc_op_mem_15_8_24;
  proc_op_mem_16_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_16_8_24_rst = '1')) then
        op_mem_16_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_16_8_24 <= op_mem_16_8_24_next;
      end if;
    end if;
  end process proc_op_mem_16_8_24;
  proc_op_mem_17_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_17_8_24_rst = '1')) then
        op_mem_17_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_17_8_24 <= op_mem_17_8_24_next;
      end if;
    end if;
  end process proc_op_mem_17_8_24;
  proc_op_mem_18_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_18_8_24_rst = '1')) then
        op_mem_18_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_18_8_24 <= op_mem_18_8_24_next;
      end if;
    end if;
  end process proc_op_mem_18_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_10_8_24, op_mem_11_8_24, op_mem_12_8_24, op_mem_13_8_24, op_mem_14_8_24, op_mem_15_8_24, op_mem_16_8_24, op_mem_17_8_24, op_mem_1_8_24, op_mem_2_8_24, op_mem_3_8_24, op_mem_4_8_24, op_mem_5_8_24, op_mem_6_8_24, op_mem_7_8_24, op_mem_8_8_24, op_mem_9_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_18_join_10_5_rst <= '1';
    else 
      op_mem_18_join_10_5_rst <= '0';
    end if;
    op_mem_18_join_10_5 <= op_mem_17_8_24;
    if rst_1_29 = '1' then
      op_mem_15_join_10_5_rst <= '1';
    else 
      op_mem_15_join_10_5_rst <= '0';
    end if;
    op_mem_15_join_10_5 <= op_mem_14_8_24;
    if rst_1_29 = '1' then
      op_mem_14_join_10_5_rst <= '1';
    else 
      op_mem_14_join_10_5_rst <= '0';
    end if;
    op_mem_14_join_10_5 <= op_mem_13_8_24;
    if rst_1_29 = '1' then
      op_mem_13_join_10_5_rst <= '1';
    else 
      op_mem_13_join_10_5_rst <= '0';
    end if;
    op_mem_13_join_10_5 <= op_mem_12_8_24;
    if rst_1_29 = '1' then
      op_mem_3_join_10_5_rst <= '1';
    else 
      op_mem_3_join_10_5_rst <= '0';
    end if;
    op_mem_3_join_10_5 <= op_mem_2_8_24;
    if rst_1_29 = '1' then
      op_mem_17_join_10_5_rst <= '1';
    else 
      op_mem_17_join_10_5_rst <= '0';
    end if;
    op_mem_17_join_10_5 <= op_mem_16_8_24;
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
    if rst_1_29 = '1' then
      op_mem_6_join_10_5_rst <= '1';
    else 
      op_mem_6_join_10_5_rst <= '0';
    end if;
    op_mem_6_join_10_5 <= op_mem_5_8_24;
    if rst_1_29 = '1' then
      op_mem_5_join_10_5_rst <= '1';
    else 
      op_mem_5_join_10_5_rst <= '0';
    end if;
    op_mem_5_join_10_5 <= op_mem_4_8_24;
    if rst_1_29 = '1' then
      op_mem_7_join_10_5_rst <= '1';
    else 
      op_mem_7_join_10_5_rst <= '0';
    end if;
    op_mem_7_join_10_5 <= op_mem_6_8_24;
    if rst_1_29 = '1' then
      op_mem_9_join_10_5_rst <= '1';
    else 
      op_mem_9_join_10_5_rst <= '0';
    end if;
    op_mem_9_join_10_5 <= op_mem_8_8_24;
    if rst_1_29 = '1' then
      op_mem_11_join_10_5_rst <= '1';
    else 
      op_mem_11_join_10_5_rst <= '0';
    end if;
    op_mem_11_join_10_5 <= op_mem_10_8_24;
    if rst_1_29 = '1' then
      op_mem_12_join_10_5_rst <= '1';
    else 
      op_mem_12_join_10_5_rst <= '0';
    end if;
    op_mem_12_join_10_5 <= op_mem_11_8_24;
    if rst_1_29 = '1' then
      op_mem_8_join_10_5_rst <= '1';
    else 
      op_mem_8_join_10_5_rst <= '0';
    end if;
    op_mem_8_join_10_5 <= op_mem_7_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_4_join_10_5_rst <= '1';
    else 
      op_mem_4_join_10_5_rst <= '0';
    end if;
    op_mem_4_join_10_5 <= op_mem_3_8_24;
    if rst_1_29 = '1' then
      op_mem_16_join_10_5_rst <= '1';
    else 
      op_mem_16_join_10_5_rst <= '0';
    end if;
    op_mem_16_join_10_5 <= op_mem_15_8_24;
    if rst_1_29 = '1' then
      op_mem_10_join_10_5_rst <= '1';
    else 
      op_mem_10_join_10_5_rst <= '0';
    end if;
    op_mem_10_join_10_5 <= op_mem_9_8_24;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  op_mem_3_8_24_next <= op_mem_2_8_24;
  op_mem_3_8_24_rst <= op_mem_3_join_10_5_rst;
  op_mem_4_8_24_next <= op_mem_3_8_24;
  op_mem_4_8_24_rst <= op_mem_4_join_10_5_rst;
  op_mem_5_8_24_next <= op_mem_4_8_24;
  op_mem_5_8_24_rst <= op_mem_5_join_10_5_rst;
  op_mem_6_8_24_next <= op_mem_5_8_24;
  op_mem_6_8_24_rst <= op_mem_6_join_10_5_rst;
  op_mem_7_8_24_next <= op_mem_6_8_24;
  op_mem_7_8_24_rst <= op_mem_7_join_10_5_rst;
  op_mem_8_8_24_next <= op_mem_7_8_24;
  op_mem_8_8_24_rst <= op_mem_8_join_10_5_rst;
  op_mem_9_8_24_next <= op_mem_8_8_24;
  op_mem_9_8_24_rst <= op_mem_9_join_10_5_rst;
  op_mem_10_8_24_next <= op_mem_9_8_24;
  op_mem_10_8_24_rst <= op_mem_10_join_10_5_rst;
  op_mem_11_8_24_next <= op_mem_10_8_24;
  op_mem_11_8_24_rst <= op_mem_11_join_10_5_rst;
  op_mem_12_8_24_next <= op_mem_11_8_24;
  op_mem_12_8_24_rst <= op_mem_12_join_10_5_rst;
  op_mem_13_8_24_next <= op_mem_12_8_24;
  op_mem_13_8_24_rst <= op_mem_13_join_10_5_rst;
  op_mem_14_8_24_next <= op_mem_13_8_24;
  op_mem_14_8_24_rst <= op_mem_14_join_10_5_rst;
  op_mem_15_8_24_next <= op_mem_14_8_24;
  op_mem_15_8_24_rst <= op_mem_15_join_10_5_rst;
  op_mem_16_8_24_next <= op_mem_15_8_24;
  op_mem_16_8_24_rst <= op_mem_16_join_10_5_rst;
  op_mem_17_8_24_next <= op_mem_16_8_24;
  op_mem_17_8_24_rst <= op_mem_17_join_10_5_rst;
  op_mem_18_8_24_next <= op_mem_17_8_24;
  op_mem_18_8_24_rst <= op_mem_18_join_10_5_rst;
  q <= op_mem_18_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_dfc16757ae is
  port (
    d : in std_logic_vector((44 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((44 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_dfc16757ae;
architecture behavior of sysgen_delay_dfc16757ae
is
  signal d_1_22: std_logic_vector((44 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_3_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_3_8_24_rst: std_logic;
  signal op_mem_4_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_4_8_24_rst: std_logic;
  signal op_mem_5_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_5_8_24_rst: std_logic;
  signal op_mem_6_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_6_8_24_rst: std_logic;
  signal op_mem_7_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_7_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_7_8_24_rst: std_logic;
  signal op_mem_8_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_8_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_8_8_24_rst: std_logic;
  signal op_mem_9_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_9_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_9_8_24_rst: std_logic;
  signal op_mem_10_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_10_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_10_8_24_rst: std_logic;
  signal op_mem_11_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_11_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_11_8_24_rst: std_logic;
  signal op_mem_12_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_12_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_12_8_24_rst: std_logic;
  signal op_mem_13_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_13_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_13_8_24_rst: std_logic;
  signal op_mem_14_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_14_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_14_8_24_rst: std_logic;
  signal op_mem_7_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_7_join_10_5_rst: std_logic;
  signal op_mem_14_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_14_join_10_5_rst: std_logic;
  signal op_mem_6_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_join_10_5_rst: std_logic;
  signal op_mem_10_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_10_join_10_5_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
  signal op_mem_8_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_8_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_9_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_9_join_10_5_rst: std_logic;
  signal op_mem_12_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_12_join_10_5_rst: std_logic;
  signal op_mem_11_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_11_join_10_5_rst: std_logic;
  signal op_mem_3_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_join_10_5_rst: std_logic;
  signal op_mem_4_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_join_10_5_rst: std_logic;
  signal op_mem_5_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
  signal op_mem_13_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_13_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_op_mem_3_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_3_8_24_rst = '1')) then
        op_mem_3_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_3_8_24 <= op_mem_3_8_24_next;
      end if;
    end if;
  end process proc_op_mem_3_8_24;
  proc_op_mem_4_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_4_8_24_rst = '1')) then
        op_mem_4_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_4_8_24 <= op_mem_4_8_24_next;
      end if;
    end if;
  end process proc_op_mem_4_8_24;
  proc_op_mem_5_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_5_8_24_rst = '1')) then
        op_mem_5_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_5_8_24 <= op_mem_5_8_24_next;
      end if;
    end if;
  end process proc_op_mem_5_8_24;
  proc_op_mem_6_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_6_8_24_rst = '1')) then
        op_mem_6_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_6_8_24 <= op_mem_6_8_24_next;
      end if;
    end if;
  end process proc_op_mem_6_8_24;
  proc_op_mem_7_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_7_8_24_rst = '1')) then
        op_mem_7_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_7_8_24 <= op_mem_7_8_24_next;
      end if;
    end if;
  end process proc_op_mem_7_8_24;
  proc_op_mem_8_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_8_8_24_rst = '1')) then
        op_mem_8_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_8_8_24 <= op_mem_8_8_24_next;
      end if;
    end if;
  end process proc_op_mem_8_8_24;
  proc_op_mem_9_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_9_8_24_rst = '1')) then
        op_mem_9_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_9_8_24 <= op_mem_9_8_24_next;
      end if;
    end if;
  end process proc_op_mem_9_8_24;
  proc_op_mem_10_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_10_8_24_rst = '1')) then
        op_mem_10_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_10_8_24 <= op_mem_10_8_24_next;
      end if;
    end if;
  end process proc_op_mem_10_8_24;
  proc_op_mem_11_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_11_8_24_rst = '1')) then
        op_mem_11_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_11_8_24 <= op_mem_11_8_24_next;
      end if;
    end if;
  end process proc_op_mem_11_8_24;
  proc_op_mem_12_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_12_8_24_rst = '1')) then
        op_mem_12_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_12_8_24 <= op_mem_12_8_24_next;
      end if;
    end if;
  end process proc_op_mem_12_8_24;
  proc_op_mem_13_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_13_8_24_rst = '1')) then
        op_mem_13_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_13_8_24 <= op_mem_13_8_24_next;
      end if;
    end if;
  end process proc_op_mem_13_8_24;
  proc_op_mem_14_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_14_8_24_rst = '1')) then
        op_mem_14_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_14_8_24 <= op_mem_14_8_24_next;
      end if;
    end if;
  end process proc_op_mem_14_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_10_8_24, op_mem_11_8_24, op_mem_12_8_24, op_mem_13_8_24, op_mem_1_8_24, op_mem_2_8_24, op_mem_3_8_24, op_mem_4_8_24, op_mem_5_8_24, op_mem_6_8_24, op_mem_7_8_24, op_mem_8_8_24, op_mem_9_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_7_join_10_5_rst <= '1';
    else 
      op_mem_7_join_10_5_rst <= '0';
    end if;
    op_mem_7_join_10_5 <= op_mem_6_8_24;
    if rst_1_29 = '1' then
      op_mem_14_join_10_5_rst <= '1';
    else 
      op_mem_14_join_10_5_rst <= '0';
    end if;
    op_mem_14_join_10_5 <= op_mem_13_8_24;
    if rst_1_29 = '1' then
      op_mem_6_join_10_5_rst <= '1';
    else 
      op_mem_6_join_10_5_rst <= '0';
    end if;
    op_mem_6_join_10_5 <= op_mem_5_8_24;
    if rst_1_29 = '1' then
      op_mem_10_join_10_5_rst <= '1';
    else 
      op_mem_10_join_10_5_rst <= '0';
    end if;
    op_mem_10_join_10_5 <= op_mem_9_8_24;
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
    if rst_1_29 = '1' then
      op_mem_8_join_10_5_rst <= '1';
    else 
      op_mem_8_join_10_5_rst <= '0';
    end if;
    op_mem_8_join_10_5 <= op_mem_7_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_9_join_10_5_rst <= '1';
    else 
      op_mem_9_join_10_5_rst <= '0';
    end if;
    op_mem_9_join_10_5 <= op_mem_8_8_24;
    if rst_1_29 = '1' then
      op_mem_12_join_10_5_rst <= '1';
    else 
      op_mem_12_join_10_5_rst <= '0';
    end if;
    op_mem_12_join_10_5 <= op_mem_11_8_24;
    if rst_1_29 = '1' then
      op_mem_11_join_10_5_rst <= '1';
    else 
      op_mem_11_join_10_5_rst <= '0';
    end if;
    op_mem_11_join_10_5 <= op_mem_10_8_24;
    if rst_1_29 = '1' then
      op_mem_3_join_10_5_rst <= '1';
    else 
      op_mem_3_join_10_5_rst <= '0';
    end if;
    op_mem_3_join_10_5 <= op_mem_2_8_24;
    if rst_1_29 = '1' then
      op_mem_4_join_10_5_rst <= '1';
    else 
      op_mem_4_join_10_5_rst <= '0';
    end if;
    op_mem_4_join_10_5 <= op_mem_3_8_24;
    if rst_1_29 = '1' then
      op_mem_5_join_10_5_rst <= '1';
    else 
      op_mem_5_join_10_5_rst <= '0';
    end if;
    op_mem_5_join_10_5 <= op_mem_4_8_24;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
    if rst_1_29 = '1' then
      op_mem_13_join_10_5_rst <= '1';
    else 
      op_mem_13_join_10_5_rst <= '0';
    end if;
    op_mem_13_join_10_5 <= op_mem_12_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  op_mem_3_8_24_next <= op_mem_2_8_24;
  op_mem_3_8_24_rst <= op_mem_3_join_10_5_rst;
  op_mem_4_8_24_next <= op_mem_3_8_24;
  op_mem_4_8_24_rst <= op_mem_4_join_10_5_rst;
  op_mem_5_8_24_next <= op_mem_4_8_24;
  op_mem_5_8_24_rst <= op_mem_5_join_10_5_rst;
  op_mem_6_8_24_next <= op_mem_5_8_24;
  op_mem_6_8_24_rst <= op_mem_6_join_10_5_rst;
  op_mem_7_8_24_next <= op_mem_6_8_24;
  op_mem_7_8_24_rst <= op_mem_7_join_10_5_rst;
  op_mem_8_8_24_next <= op_mem_7_8_24;
  op_mem_8_8_24_rst <= op_mem_8_join_10_5_rst;
  op_mem_9_8_24_next <= op_mem_8_8_24;
  op_mem_9_8_24_rst <= op_mem_9_join_10_5_rst;
  op_mem_10_8_24_next <= op_mem_9_8_24;
  op_mem_10_8_24_rst <= op_mem_10_join_10_5_rst;
  op_mem_11_8_24_next <= op_mem_10_8_24;
  op_mem_11_8_24_rst <= op_mem_11_join_10_5_rst;
  op_mem_12_8_24_next <= op_mem_11_8_24;
  op_mem_12_8_24_rst <= op_mem_12_join_10_5_rst;
  op_mem_13_8_24_next <= op_mem_12_8_24;
  op_mem_13_8_24_rst <= op_mem_13_join_10_5_rst;
  op_mem_14_8_24_next <= op_mem_13_8_24;
  op_mem_14_8_24_rst <= op_mem_14_join_10_5_rst;
  q <= op_mem_14_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_b67b4f5ede is
  port (
    d : in std_logic_vector((27 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_b67b4f5ede;
architecture behavior of sysgen_delay_b67b4f5ede
is
  signal d_1_22: std_logic_vector((27 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((27 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((27 - 1) downto 0) := "000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((27 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((27 - 1) downto 0) := "000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((27 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((27 - 1) downto 0) := "000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((27 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((27 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((27 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_1_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  q <= op_mem_2_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_1b78fad3aa is
  port (
    d : in std_logic_vector((44 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((44 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_1b78fad3aa;
architecture behavior of sysgen_delay_1b78fad3aa
is
  signal d_1_22: std_logic_vector((44 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_3_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_3_8_24_rst: std_logic;
  signal op_mem_4_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_4_8_24_rst: std_logic;
  signal op_mem_5_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_5_8_24_rst: std_logic;
  signal op_mem_6_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_6_8_24_rst: std_logic;
  signal op_mem_7_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_7_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_7_8_24_rst: std_logic;
  signal op_mem_8_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_8_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_8_8_24_rst: std_logic;
  signal op_mem_9_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_9_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_9_8_24_rst: std_logic;
  signal op_mem_10_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_10_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_10_8_24_rst: std_logic;
  signal op_mem_9_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_9_join_10_5_rst: std_logic;
  signal op_mem_4_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_join_10_5_rst: std_logic;
  signal op_mem_6_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_join_10_5_rst: std_logic;
  signal op_mem_5_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_join_10_5_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
  signal op_mem_7_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_7_join_10_5_rst: std_logic;
  signal op_mem_10_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_10_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
  signal op_mem_8_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_8_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_3_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_op_mem_3_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_3_8_24_rst = '1')) then
        op_mem_3_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_3_8_24 <= op_mem_3_8_24_next;
      end if;
    end if;
  end process proc_op_mem_3_8_24;
  proc_op_mem_4_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_4_8_24_rst = '1')) then
        op_mem_4_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_4_8_24 <= op_mem_4_8_24_next;
      end if;
    end if;
  end process proc_op_mem_4_8_24;
  proc_op_mem_5_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_5_8_24_rst = '1')) then
        op_mem_5_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_5_8_24 <= op_mem_5_8_24_next;
      end if;
    end if;
  end process proc_op_mem_5_8_24;
  proc_op_mem_6_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_6_8_24_rst = '1')) then
        op_mem_6_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_6_8_24 <= op_mem_6_8_24_next;
      end if;
    end if;
  end process proc_op_mem_6_8_24;
  proc_op_mem_7_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_7_8_24_rst = '1')) then
        op_mem_7_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_7_8_24 <= op_mem_7_8_24_next;
      end if;
    end if;
  end process proc_op_mem_7_8_24;
  proc_op_mem_8_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_8_8_24_rst = '1')) then
        op_mem_8_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_8_8_24 <= op_mem_8_8_24_next;
      end if;
    end if;
  end process proc_op_mem_8_8_24;
  proc_op_mem_9_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_9_8_24_rst = '1')) then
        op_mem_9_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_9_8_24 <= op_mem_9_8_24_next;
      end if;
    end if;
  end process proc_op_mem_9_8_24;
  proc_op_mem_10_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_10_8_24_rst = '1')) then
        op_mem_10_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_10_8_24 <= op_mem_10_8_24_next;
      end if;
    end if;
  end process proc_op_mem_10_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_1_8_24, op_mem_2_8_24, op_mem_3_8_24, op_mem_4_8_24, op_mem_5_8_24, op_mem_6_8_24, op_mem_7_8_24, op_mem_8_8_24, op_mem_9_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_9_join_10_5_rst <= '1';
    else 
      op_mem_9_join_10_5_rst <= '0';
    end if;
    op_mem_9_join_10_5 <= op_mem_8_8_24;
    if rst_1_29 = '1' then
      op_mem_4_join_10_5_rst <= '1';
    else 
      op_mem_4_join_10_5_rst <= '0';
    end if;
    op_mem_4_join_10_5 <= op_mem_3_8_24;
    if rst_1_29 = '1' then
      op_mem_6_join_10_5_rst <= '1';
    else 
      op_mem_6_join_10_5_rst <= '0';
    end if;
    op_mem_6_join_10_5 <= op_mem_5_8_24;
    if rst_1_29 = '1' then
      op_mem_5_join_10_5_rst <= '1';
    else 
      op_mem_5_join_10_5_rst <= '0';
    end if;
    op_mem_5_join_10_5 <= op_mem_4_8_24;
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
    if rst_1_29 = '1' then
      op_mem_7_join_10_5_rst <= '1';
    else 
      op_mem_7_join_10_5_rst <= '0';
    end if;
    op_mem_7_join_10_5 <= op_mem_6_8_24;
    if rst_1_29 = '1' then
      op_mem_10_join_10_5_rst <= '1';
    else 
      op_mem_10_join_10_5_rst <= '0';
    end if;
    op_mem_10_join_10_5 <= op_mem_9_8_24;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
    if rst_1_29 = '1' then
      op_mem_8_join_10_5_rst <= '1';
    else 
      op_mem_8_join_10_5_rst <= '0';
    end if;
    op_mem_8_join_10_5 <= op_mem_7_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_3_join_10_5_rst <= '1';
    else 
      op_mem_3_join_10_5_rst <= '0';
    end if;
    op_mem_3_join_10_5 <= op_mem_2_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  op_mem_3_8_24_next <= op_mem_2_8_24;
  op_mem_3_8_24_rst <= op_mem_3_join_10_5_rst;
  op_mem_4_8_24_next <= op_mem_3_8_24;
  op_mem_4_8_24_rst <= op_mem_4_join_10_5_rst;
  op_mem_5_8_24_next <= op_mem_4_8_24;
  op_mem_5_8_24_rst <= op_mem_5_join_10_5_rst;
  op_mem_6_8_24_next <= op_mem_5_8_24;
  op_mem_6_8_24_rst <= op_mem_6_join_10_5_rst;
  op_mem_7_8_24_next <= op_mem_6_8_24;
  op_mem_7_8_24_rst <= op_mem_7_join_10_5_rst;
  op_mem_8_8_24_next <= op_mem_7_8_24;
  op_mem_8_8_24_rst <= op_mem_8_join_10_5_rst;
  op_mem_9_8_24_next <= op_mem_8_8_24;
  op_mem_9_8_24_rst <= op_mem_9_join_10_5_rst;
  op_mem_10_8_24_next <= op_mem_9_8_24;
  op_mem_10_8_24_rst <= op_mem_10_join_10_5_rst;
  q <= op_mem_10_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_2d25fa5d59 is
  port (
    d : in std_logic_vector((44 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((44 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_2d25fa5d59;
architecture behavior of sysgen_delay_2d25fa5d59
is
  signal d_1_22: std_logic_vector((44 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_3_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_3_8_24_rst: std_logic;
  signal op_mem_4_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_4_8_24_rst: std_logic;
  signal op_mem_5_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_5_8_24_rst: std_logic;
  signal op_mem_6_8_24_next: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_8_24: std_logic_vector((44 - 1) downto 0) := "00000000000000000000000000000000000000000000";
  signal op_mem_6_8_24_rst: std_logic;
  signal op_mem_5_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_5_join_10_5_rst: std_logic;
  signal op_mem_3_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_3_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
  signal op_mem_4_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_4_join_10_5_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
  signal op_mem_6_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_6_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((44 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_op_mem_3_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_3_8_24_rst = '1')) then
        op_mem_3_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_3_8_24 <= op_mem_3_8_24_next;
      end if;
    end if;
  end process proc_op_mem_3_8_24;
  proc_op_mem_4_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_4_8_24_rst = '1')) then
        op_mem_4_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_4_8_24 <= op_mem_4_8_24_next;
      end if;
    end if;
  end process proc_op_mem_4_8_24;
  proc_op_mem_5_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_5_8_24_rst = '1')) then
        op_mem_5_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_5_8_24 <= op_mem_5_8_24_next;
      end if;
    end if;
  end process proc_op_mem_5_8_24;
  proc_op_mem_6_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_6_8_24_rst = '1')) then
        op_mem_6_8_24 <= "00000000000000000000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_6_8_24 <= op_mem_6_8_24_next;
      end if;
    end if;
  end process proc_op_mem_6_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_1_8_24, op_mem_2_8_24, op_mem_3_8_24, op_mem_4_8_24, op_mem_5_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_5_join_10_5_rst <= '1';
    else 
      op_mem_5_join_10_5_rst <= '0';
    end if;
    op_mem_5_join_10_5 <= op_mem_4_8_24;
    if rst_1_29 = '1' then
      op_mem_3_join_10_5_rst <= '1';
    else 
      op_mem_3_join_10_5_rst <= '0';
    end if;
    op_mem_3_join_10_5 <= op_mem_2_8_24;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
    if rst_1_29 = '1' then
      op_mem_4_join_10_5_rst <= '1';
    else 
      op_mem_4_join_10_5_rst <= '0';
    end if;
    op_mem_4_join_10_5 <= op_mem_3_8_24;
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
    if rst_1_29 = '1' then
      op_mem_6_join_10_5_rst <= '1';
    else 
      op_mem_6_join_10_5_rst <= '0';
    end if;
    op_mem_6_join_10_5 <= op_mem_5_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  op_mem_3_8_24_next <= op_mem_2_8_24;
  op_mem_3_8_24_rst <= op_mem_3_join_10_5_rst;
  op_mem_4_8_24_next <= op_mem_3_8_24;
  op_mem_4_8_24_rst <= op_mem_4_join_10_5_rst;
  op_mem_5_8_24_next <= op_mem_4_8_24;
  op_mem_5_8_24_rst <= op_mem_5_join_10_5_rst;
  op_mem_6_8_24_next <= op_mem_5_8_24;
  op_mem_6_8_24_rst <= op_mem_6_join_10_5_rst;
  q <= op_mem_6_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_3809e7e7a5 is
  port (
    op : out std_logic_vector((4 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_3809e7e7a5;
architecture behavior of sysgen_constant_3809e7e7a5
is
begin
  op <= "0100";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_703c620e80 is
  port (
    input_port : in std_logic_vector((17 - 1) downto 0);
    output_port : out std_logic_vector((17 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_703c620e80;
architecture behavior of sysgen_reinterpret_703c620e80
is
  signal input_port_1_40: signed((17 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_signed(input_port);
  output_port <= signed_to_std_logic_vector(input_port_1_40);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_fd0b169856 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_fd0b169856;
architecture behavior of sysgen_constant_fd0b169856
is
begin
  op <= "000000000000000000000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_e4f08cd372 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_e4f08cd372;
architecture behavior of sysgen_constant_e4f08cd372
is
begin
  op <= "000010000010111000000000101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_1c20f10c72 is
  port (
    op : out std_logic_vector((17 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_1c20f10c72;
architecture behavior of sysgen_constant_1c20f10c72
is
begin
  op <= "00000000000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_3d0eda5bb9 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_3d0eda5bb9;
architecture behavior of sysgen_constant_3d0eda5bb9
is
begin
  op <= "000000000000000010100000011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_15df27d5ad is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_15df27d5ad;
architecture behavior of sysgen_constant_15df27d5ad
is
begin
  op <= "111111111111110000001011101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_1b7694dd33 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_1b7694dd33;
architecture behavior of sysgen_constant_1b7694dd33
is
begin
  op <= "000000000000111101001010111";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_5ed8fe4930 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_5ed8fe4930;
architecture behavior of sysgen_constant_5ed8fe4930
is
begin
  op <= "111111111101000010111000010";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_e45f78a27c is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_e45f78a27c;
architecture behavior of sysgen_constant_e45f78a27c
is
begin
  op <= "000000001000011010011010000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_9c8dfdbba6 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_9c8dfdbba6;
architecture behavior of sysgen_constant_9c8dfdbba6
is
begin
  op <= "111111100110001100000110000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_70aa9de4a3 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_70aa9de4a3;
architecture behavior of sysgen_constant_70aa9de4a3
is
begin
  op <= "000001111011011010010110111";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_2e837937c8 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_2e837937c8;
architecture behavior of sysgen_constant_2e837937c8
is
begin
  op <= "111100110000011000111011111";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_b28213583e is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_b28213583e;
architecture behavior of sysgen_constant_b28213583e
is
begin
  op <= "111111111111110101101101001";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_46f38e8fa5 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_46f38e8fa5;
architecture behavior of sysgen_constant_46f38e8fa5
is
begin
  op <= "000000000000111001011110111";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_27c7013ab2 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_27c7013ab2;
architecture behavior of sysgen_constant_27c7013ab2
is
begin
  op <= "111111111101000000000110101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_de70bacc42 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_de70bacc42;
architecture behavior of sysgen_constant_de70bacc42
is
begin
  op <= "000000000111101111111101001";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_1f7dbeddc3 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_1f7dbeddc3;
architecture behavior of sysgen_constant_1f7dbeddc3
is
begin
  op <= "111111101110001111011010000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_35f261146a is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_35f261146a;
architecture behavior of sysgen_constant_35f261146a
is
begin
  op <= "000000101001100000011001001";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_805f03b38e is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_805f03b38e;
architecture behavior of sysgen_constant_805f03b38e
is
begin
  op <= "111110101111111011010111000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_a454c59521 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_a454c59521;
architecture behavior of sysgen_constant_a454c59521
is
begin
  op <= "000001100101101010100100011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_9fd5968d9b is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_9fd5968d9b;
architecture behavior of sysgen_constant_9fd5968d9b
is
begin
  op <= "000000000000000101010110011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_b575f8b494 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_b575f8b494;
architecture behavior of sysgen_constant_b575f8b494
is
begin
  op <= "111111111111010110100010010";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_1feba3d02c is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_1feba3d02c;
architecture behavior of sysgen_constant_1feba3d02c
is
begin
  op <= "000000000010101101001011011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_246104e666 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_246104e666;
architecture behavior of sysgen_constant_246104e666
is
begin
  op <= "111111110111110010101011011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_504d725421 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_504d725421;
architecture behavior of sysgen_constant_504d725421
is
begin
  op <= "000000010100100011111101000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_dad3dd0c31 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_dad3dd0c31;
architecture behavior of sysgen_constant_dad3dd0c31
is
begin
  op <= "111111010000110100001110111";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_eb2f31b720 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_eb2f31b720;
architecture behavior of sysgen_constant_eb2f31b720
is
begin
  op <= "000001111000101110010001011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_d9348b126b is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_d9348b126b;
architecture behavior of sysgen_constant_d9348b126b
is
begin
  op <= "000000000000011110111110110";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_8be66ec32e is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_8be66ec32e;
architecture behavior of sysgen_constant_8be66ec32e
is
begin
  op <= "111111111110011111101001111";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_4f2b0c986b is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_4f2b0c986b;
architecture behavior of sysgen_constant_4f2b0c986b
is
begin
  op <= "000000000001110101010011001";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_45a7d6f74a is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_45a7d6f74a;
architecture behavior of sysgen_constant_45a7d6f74a
is
begin
  op <= "000000000010001010110111100";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_4a2055cf9f is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_4a2055cf9f;
architecture behavior of sysgen_constant_4a2055cf9f
is
begin
  op <= "111111101111001111001000011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_a5322a5e1d is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_a5322a5e1d;
architecture behavior of sysgen_constant_a5322a5e1d
is
begin
  op <= "000000110001010111101110100";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_933c0ddd93 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_933c0ddd93;
architecture behavior of sysgen_constant_933c0ddd93
is
begin
  op <= "111111000011101100011011011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_10f6429a43 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_10f6429a43;
architecture behavior of sysgen_constant_10f6429a43
is
begin
  op <= "111111111101101010100110011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_6089bb8102 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_6089bb8102;
architecture behavior of sysgen_constant_6089bb8102
is
begin
  op <= "000000001001010101100101101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_66f5b97168 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_66f5b97168;
architecture behavior of sysgen_constant_66f5b97168
is
begin
  op <= "111111101010011011001111011";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_3a459fcf31 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_3a459fcf31;
architecture behavior of sysgen_constant_3a459fcf31
is
begin
  op <= "000000100010111000101100100";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_8bf26556ab is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_8bf26556ab;
architecture behavior of sysgen_constant_8bf26556ab
is
begin
  op <= "111111010111010011011110001";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_19d465b51c is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_19d465b51c;
architecture behavior of sysgen_constant_19d465b51c
is
begin
  op <= "000000100000111111100011010";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_1d1a433e47 is
  port (
    op : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_1d1a433e47;
architecture behavior of sysgen_constant_1d1a433e47
is
begin
  op <= "111111101111011010110110101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_f0117058c6 is
  port (
    d : in std_logic_vector((29 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_f0117058c6;
architecture behavior of sysgen_delay_f0117058c6
is
  signal d_1_22: std_logic_vector((29 - 1) downto 0);
  signal en_1_25: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_8_24_en: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_join_10_5_en: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  en_1_25 <= en(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000";
      elsif ((ce = '1') and (op_mem_0_8_24_en = '1')) then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, en_1_25, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    elsif en_1_25 = '1' then
      op_mem_0_join_10_5_rst <= '0';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    if en_1_25 = '1' then
      op_mem_0_join_10_5_en <= '1';
    else 
      op_mem_0_join_10_5_en <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_0_8_24_en <= op_mem_0_join_10_5_en;
  q <= op_mem_0_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_387dc88455 is
  port (
    d : in std_logic_vector((29 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_387dc88455;
architecture behavior of sysgen_delay_387dc88455
is
  signal d_1_22: std_logic_vector((29 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_3_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_3_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_3_8_24_rst: std_logic;
  signal op_mem_4_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_4_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_4_8_24_rst: std_logic;
  signal op_mem_5_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_5_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_5_8_24_rst: std_logic;
  signal op_mem_6_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_6_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_6_8_24_rst: std_logic;
  signal op_mem_7_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_7_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_7_8_24_rst: std_logic;
  signal op_mem_8_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_8_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_8_8_24_rst: std_logic;
  signal op_mem_9_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_9_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_9_8_24_rst: std_logic;
  signal op_mem_10_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_10_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_10_8_24_rst: std_logic;
  signal op_mem_11_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_11_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_11_8_24_rst: std_logic;
  signal op_mem_12_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_12_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_12_8_24_rst: std_logic;
  signal op_mem_13_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_13_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_13_8_24_rst: std_logic;
  signal op_mem_14_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_14_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_14_8_24_rst: std_logic;
  signal op_mem_15_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_15_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_15_8_24_rst: std_logic;
  signal op_mem_16_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_16_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_16_8_24_rst: std_logic;
  signal op_mem_17_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_17_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_17_8_24_rst: std_logic;
  signal op_mem_18_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_18_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_18_8_24_rst: std_logic;
  signal op_mem_19_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_19_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_19_8_24_rst: std_logic;
  signal op_mem_15_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_15_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_10_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_10_join_10_5_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
  signal op_mem_13_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_13_join_10_5_rst: std_logic;
  signal op_mem_6_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_6_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
  signal op_mem_16_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_16_join_10_5_rst: std_logic;
  signal op_mem_7_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_7_join_10_5_rst: std_logic;
  signal op_mem_12_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_12_join_10_5_rst: std_logic;
  signal op_mem_9_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_9_join_10_5_rst: std_logic;
  signal op_mem_18_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_18_join_10_5_rst: std_logic;
  signal op_mem_5_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_5_join_10_5_rst: std_logic;
  signal op_mem_4_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_4_join_10_5_rst: std_logic;
  signal op_mem_11_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_11_join_10_5_rst: std_logic;
  signal op_mem_19_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_19_join_10_5_rst: std_logic;
  signal op_mem_3_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_3_join_10_5_rst: std_logic;
  signal op_mem_17_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_17_join_10_5_rst: std_logic;
  signal op_mem_8_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_8_join_10_5_rst: std_logic;
  signal op_mem_14_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_14_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_op_mem_3_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_3_8_24_rst = '1')) then
        op_mem_3_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_3_8_24 <= op_mem_3_8_24_next;
      end if;
    end if;
  end process proc_op_mem_3_8_24;
  proc_op_mem_4_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_4_8_24_rst = '1')) then
        op_mem_4_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_4_8_24 <= op_mem_4_8_24_next;
      end if;
    end if;
  end process proc_op_mem_4_8_24;
  proc_op_mem_5_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_5_8_24_rst = '1')) then
        op_mem_5_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_5_8_24 <= op_mem_5_8_24_next;
      end if;
    end if;
  end process proc_op_mem_5_8_24;
  proc_op_mem_6_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_6_8_24_rst = '1')) then
        op_mem_6_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_6_8_24 <= op_mem_6_8_24_next;
      end if;
    end if;
  end process proc_op_mem_6_8_24;
  proc_op_mem_7_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_7_8_24_rst = '1')) then
        op_mem_7_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_7_8_24 <= op_mem_7_8_24_next;
      end if;
    end if;
  end process proc_op_mem_7_8_24;
  proc_op_mem_8_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_8_8_24_rst = '1')) then
        op_mem_8_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_8_8_24 <= op_mem_8_8_24_next;
      end if;
    end if;
  end process proc_op_mem_8_8_24;
  proc_op_mem_9_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_9_8_24_rst = '1')) then
        op_mem_9_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_9_8_24 <= op_mem_9_8_24_next;
      end if;
    end if;
  end process proc_op_mem_9_8_24;
  proc_op_mem_10_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_10_8_24_rst = '1')) then
        op_mem_10_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_10_8_24 <= op_mem_10_8_24_next;
      end if;
    end if;
  end process proc_op_mem_10_8_24;
  proc_op_mem_11_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_11_8_24_rst = '1')) then
        op_mem_11_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_11_8_24 <= op_mem_11_8_24_next;
      end if;
    end if;
  end process proc_op_mem_11_8_24;
  proc_op_mem_12_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_12_8_24_rst = '1')) then
        op_mem_12_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_12_8_24 <= op_mem_12_8_24_next;
      end if;
    end if;
  end process proc_op_mem_12_8_24;
  proc_op_mem_13_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_13_8_24_rst = '1')) then
        op_mem_13_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_13_8_24 <= op_mem_13_8_24_next;
      end if;
    end if;
  end process proc_op_mem_13_8_24;
  proc_op_mem_14_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_14_8_24_rst = '1')) then
        op_mem_14_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_14_8_24 <= op_mem_14_8_24_next;
      end if;
    end if;
  end process proc_op_mem_14_8_24;
  proc_op_mem_15_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_15_8_24_rst = '1')) then
        op_mem_15_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_15_8_24 <= op_mem_15_8_24_next;
      end if;
    end if;
  end process proc_op_mem_15_8_24;
  proc_op_mem_16_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_16_8_24_rst = '1')) then
        op_mem_16_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_16_8_24 <= op_mem_16_8_24_next;
      end if;
    end if;
  end process proc_op_mem_16_8_24;
  proc_op_mem_17_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_17_8_24_rst = '1')) then
        op_mem_17_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_17_8_24 <= op_mem_17_8_24_next;
      end if;
    end if;
  end process proc_op_mem_17_8_24;
  proc_op_mem_18_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_18_8_24_rst = '1')) then
        op_mem_18_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_18_8_24 <= op_mem_18_8_24_next;
      end if;
    end if;
  end process proc_op_mem_18_8_24;
  proc_op_mem_19_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_19_8_24_rst = '1')) then
        op_mem_19_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_19_8_24 <= op_mem_19_8_24_next;
      end if;
    end if;
  end process proc_op_mem_19_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_10_8_24, op_mem_11_8_24, op_mem_12_8_24, op_mem_13_8_24, op_mem_14_8_24, op_mem_15_8_24, op_mem_16_8_24, op_mem_17_8_24, op_mem_18_8_24, op_mem_1_8_24, op_mem_2_8_24, op_mem_3_8_24, op_mem_4_8_24, op_mem_5_8_24, op_mem_6_8_24, op_mem_7_8_24, op_mem_8_8_24, op_mem_9_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_15_join_10_5_rst <= '1';
    else 
      op_mem_15_join_10_5_rst <= '0';
    end if;
    op_mem_15_join_10_5 <= op_mem_14_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_10_join_10_5_rst <= '1';
    else 
      op_mem_10_join_10_5_rst <= '0';
    end if;
    op_mem_10_join_10_5 <= op_mem_9_8_24;
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
    if rst_1_29 = '1' then
      op_mem_13_join_10_5_rst <= '1';
    else 
      op_mem_13_join_10_5_rst <= '0';
    end if;
    op_mem_13_join_10_5 <= op_mem_12_8_24;
    if rst_1_29 = '1' then
      op_mem_6_join_10_5_rst <= '1';
    else 
      op_mem_6_join_10_5_rst <= '0';
    end if;
    op_mem_6_join_10_5 <= op_mem_5_8_24;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
    if rst_1_29 = '1' then
      op_mem_16_join_10_5_rst <= '1';
    else 
      op_mem_16_join_10_5_rst <= '0';
    end if;
    op_mem_16_join_10_5 <= op_mem_15_8_24;
    if rst_1_29 = '1' then
      op_mem_7_join_10_5_rst <= '1';
    else 
      op_mem_7_join_10_5_rst <= '0';
    end if;
    op_mem_7_join_10_5 <= op_mem_6_8_24;
    if rst_1_29 = '1' then
      op_mem_12_join_10_5_rst <= '1';
    else 
      op_mem_12_join_10_5_rst <= '0';
    end if;
    op_mem_12_join_10_5 <= op_mem_11_8_24;
    if rst_1_29 = '1' then
      op_mem_9_join_10_5_rst <= '1';
    else 
      op_mem_9_join_10_5_rst <= '0';
    end if;
    op_mem_9_join_10_5 <= op_mem_8_8_24;
    if rst_1_29 = '1' then
      op_mem_18_join_10_5_rst <= '1';
    else 
      op_mem_18_join_10_5_rst <= '0';
    end if;
    op_mem_18_join_10_5 <= op_mem_17_8_24;
    if rst_1_29 = '1' then
      op_mem_5_join_10_5_rst <= '1';
    else 
      op_mem_5_join_10_5_rst <= '0';
    end if;
    op_mem_5_join_10_5 <= op_mem_4_8_24;
    if rst_1_29 = '1' then
      op_mem_4_join_10_5_rst <= '1';
    else 
      op_mem_4_join_10_5_rst <= '0';
    end if;
    op_mem_4_join_10_5 <= op_mem_3_8_24;
    if rst_1_29 = '1' then
      op_mem_11_join_10_5_rst <= '1';
    else 
      op_mem_11_join_10_5_rst <= '0';
    end if;
    op_mem_11_join_10_5 <= op_mem_10_8_24;
    if rst_1_29 = '1' then
      op_mem_19_join_10_5_rst <= '1';
    else 
      op_mem_19_join_10_5_rst <= '0';
    end if;
    op_mem_19_join_10_5 <= op_mem_18_8_24;
    if rst_1_29 = '1' then
      op_mem_3_join_10_5_rst <= '1';
    else 
      op_mem_3_join_10_5_rst <= '0';
    end if;
    op_mem_3_join_10_5 <= op_mem_2_8_24;
    if rst_1_29 = '1' then
      op_mem_17_join_10_5_rst <= '1';
    else 
      op_mem_17_join_10_5_rst <= '0';
    end if;
    op_mem_17_join_10_5 <= op_mem_16_8_24;
    if rst_1_29 = '1' then
      op_mem_8_join_10_5_rst <= '1';
    else 
      op_mem_8_join_10_5_rst <= '0';
    end if;
    op_mem_8_join_10_5 <= op_mem_7_8_24;
    if rst_1_29 = '1' then
      op_mem_14_join_10_5_rst <= '1';
    else 
      op_mem_14_join_10_5_rst <= '0';
    end if;
    op_mem_14_join_10_5 <= op_mem_13_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  op_mem_3_8_24_next <= op_mem_2_8_24;
  op_mem_3_8_24_rst <= op_mem_3_join_10_5_rst;
  op_mem_4_8_24_next <= op_mem_3_8_24;
  op_mem_4_8_24_rst <= op_mem_4_join_10_5_rst;
  op_mem_5_8_24_next <= op_mem_4_8_24;
  op_mem_5_8_24_rst <= op_mem_5_join_10_5_rst;
  op_mem_6_8_24_next <= op_mem_5_8_24;
  op_mem_6_8_24_rst <= op_mem_6_join_10_5_rst;
  op_mem_7_8_24_next <= op_mem_6_8_24;
  op_mem_7_8_24_rst <= op_mem_7_join_10_5_rst;
  op_mem_8_8_24_next <= op_mem_7_8_24;
  op_mem_8_8_24_rst <= op_mem_8_join_10_5_rst;
  op_mem_9_8_24_next <= op_mem_8_8_24;
  op_mem_9_8_24_rst <= op_mem_9_join_10_5_rst;
  op_mem_10_8_24_next <= op_mem_9_8_24;
  op_mem_10_8_24_rst <= op_mem_10_join_10_5_rst;
  op_mem_11_8_24_next <= op_mem_10_8_24;
  op_mem_11_8_24_rst <= op_mem_11_join_10_5_rst;
  op_mem_12_8_24_next <= op_mem_11_8_24;
  op_mem_12_8_24_rst <= op_mem_12_join_10_5_rst;
  op_mem_13_8_24_next <= op_mem_12_8_24;
  op_mem_13_8_24_rst <= op_mem_13_join_10_5_rst;
  op_mem_14_8_24_next <= op_mem_13_8_24;
  op_mem_14_8_24_rst <= op_mem_14_join_10_5_rst;
  op_mem_15_8_24_next <= op_mem_14_8_24;
  op_mem_15_8_24_rst <= op_mem_15_join_10_5_rst;
  op_mem_16_8_24_next <= op_mem_15_8_24;
  op_mem_16_8_24_rst <= op_mem_16_join_10_5_rst;
  op_mem_17_8_24_next <= op_mem_16_8_24;
  op_mem_17_8_24_rst <= op_mem_17_join_10_5_rst;
  op_mem_18_8_24_next <= op_mem_17_8_24;
  op_mem_18_8_24_rst <= op_mem_18_join_10_5_rst;
  op_mem_19_8_24_next <= op_mem_18_8_24;
  op_mem_19_8_24_rst <= op_mem_19_join_10_5_rst;
  q <= op_mem_19_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_4c7e345fc7 is
  port (
    d : in std_logic_vector((29 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_4c7e345fc7;
architecture behavior of sysgen_delay_4c7e345fc7
is
  signal d_1_22: std_logic_vector((29 - 1) downto 0);
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_1_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_1_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_1_8_24_rst: std_logic;
  signal op_mem_2_8_24_next: std_logic_vector((29 - 1) downto 0);
  signal op_mem_2_8_24: std_logic_vector((29 - 1) downto 0) := "00000000000000000000000000000";
  signal op_mem_2_8_24_rst: std_logic;
  signal op_mem_2_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_2_join_10_5_rst: std_logic;
  signal op_mem_0_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_0_join_10_5_rst: std_logic;
  signal op_mem_1_join_10_5: std_logic_vector((29 - 1) downto 0);
  signal op_mem_1_join_10_5_rst: std_logic;
begin
  d_1_22 <= d;
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_op_mem_1_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_1_8_24_rst = '1')) then
        op_mem_1_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_1_8_24 <= op_mem_1_8_24_next;
      end if;
    end if;
  end process proc_op_mem_1_8_24;
  proc_op_mem_2_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_2_8_24_rst = '1')) then
        op_mem_2_8_24 <= "00000000000000000000000000000";
      elsif (ce = '1') then 
        op_mem_2_8_24 <= op_mem_2_8_24_next;
      end if;
    end if;
  end process proc_op_mem_2_8_24;
  proc_if_10_5: process (d_1_22, op_mem_0_8_24, op_mem_1_8_24, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_2_join_10_5_rst <= '1';
    else 
      op_mem_2_join_10_5_rst <= '0';
    end if;
    op_mem_2_join_10_5 <= op_mem_1_8_24;
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
    if rst_1_29 = '1' then
      op_mem_1_join_10_5_rst <= '1';
    else 
      op_mem_1_join_10_5_rst <= '0';
    end if;
    op_mem_1_join_10_5 <= op_mem_0_8_24;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_1_8_24_next <= op_mem_0_8_24;
  op_mem_1_8_24_rst <= op_mem_1_join_10_5_rst;
  op_mem_2_8_24_next <= op_mem_1_8_24;
  op_mem_2_8_24_rst <= op_mem_2_join_10_5_rst;
  q <= op_mem_2_8_24;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_d92945d066 is
  port (
    op : out std_logic_vector((8 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_d92945d066;
architecture behavior of sysgen_constant_d92945d066
is
begin
  op <= "00000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_logical_9b9a61fa28 is
  port (
    d0 : in std_logic_vector((1 - 1) downto 0);
    d1 : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_logical_9b9a61fa28;
architecture behavior of sysgen_logical_9b9a61fa28
is
  signal d0_1_24: std_logic;
  signal d1_1_27: std_logic;
  signal fully_2_1_bit: std_logic;
  signal unregy_3_1_convert: std_logic_vector((1 - 1) downto 0);
begin
  d0_1_24 <= d0(0);
  d1_1_27 <= d1(0);
  fully_2_1_bit <= d0_1_24 and d1_1_27;
  unregy_3_1_convert <= cast(std_logic_to_vector(fully_2_1_bit), 0, 1, 0, xlUnsigned);
  y <= unregy_3_1_convert;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_relational_6710662b9f is
  port (
    a : in std_logic_vector((8 - 1) downto 0);
    b : in std_logic_vector((8 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_relational_6710662b9f;
architecture behavior of sysgen_relational_6710662b9f
is
  signal a_1_31: unsigned((8 - 1) downto 0);
  signal b_1_34: unsigned((8 - 1) downto 0);
  signal result_18_3_rel: boolean;
begin
  a_1_31 <= std_logic_vector_to_unsigned(a);
  b_1_34 <= std_logic_vector_to_unsigned(b);
  result_18_3_rel <= a_1_31 > b_1_34;
  op <= boolean_to_vector(result_18_3_rel);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_concat_4105021c84 is
  port (
    in0 : in std_logic_vector((1 - 1) downto 0);
    in1 : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((2 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_concat_4105021c84;
architecture behavior of sysgen_concat_4105021c84
is
  signal in0_1_23: unsigned((1 - 1) downto 0);
  signal in1_1_27: unsigned((1 - 1) downto 0);
  signal y_2_1_concat: unsigned((2 - 1) downto 0);
begin
  in0_1_23 <= std_logic_vector_to_unsigned(in0);
  in1_1_27 <= std_logic_vector_to_unsigned(in1);
  y_2_1_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(in0_1_23) & unsigned_to_std_logic_vector(in1_1_27));
  y <= unsigned_to_std_logic_vector(y_2_1_concat);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_concat_6f677e0681 is
  port (
    in0 : in std_logic_vector((2 - 1) downto 0);
    in1 : in std_logic_vector((27 - 1) downto 0);
    y : out std_logic_vector((29 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_concat_6f677e0681;
architecture behavior of sysgen_concat_6f677e0681
is
  signal in0_1_23: unsigned((2 - 1) downto 0);
  signal in1_1_27: unsigned((27 - 1) downto 0);
  signal y_2_1_concat: unsigned((29 - 1) downto 0);
begin
  in0_1_23 <= std_logic_vector_to_unsigned(in0);
  in1_1_27 <= std_logic_vector_to_unsigned(in1);
  y_2_1_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(in0_1_23) & unsigned_to_std_logic_vector(in1_1_27));
  y <= unsigned_to_std_logic_vector(y_2_1_concat);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_3e60f2b48c is
  port (
    input_port : in std_logic_vector((1 - 1) downto 0);
    output_port : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_3e60f2b48c;
architecture behavior of sysgen_reinterpret_3e60f2b48c
is
  signal input_port_1_40: boolean;
  signal output_port_7_5_convert: unsigned((1 - 1) downto 0);
begin
  input_port_1_40 <= ((input_port) = "1");
  output_port_7_5_convert <= u2u_cast(std_logic_vector_to_unsigned(boolean_to_vector(input_port_1_40)), 0, 1, 0);
  output_port <= unsigned_to_std_logic_vector(output_port_7_5_convert);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_712fcaed43 is
  port (
    input_port : in std_logic_vector((27 - 1) downto 0);
    output_port : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_712fcaed43;
architecture behavior of sysgen_reinterpret_712fcaed43
is
  signal input_port_1_40: signed((27 - 1) downto 0);
  signal output_port_5_5_force: unsigned((27 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_signed(input_port);
  output_port_5_5_force <= signed_to_unsigned(input_port_1_40);
  output_port <= unsigned_to_std_logic_vector(output_port_5_5_force);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_959796c013 is
  port (
    input_port : in std_logic_vector((27 - 1) downto 0);
    output_port : out std_logic_vector((27 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_959796c013;
architecture behavior of sysgen_reinterpret_959796c013
is
  signal input_port_1_40: unsigned((27 - 1) downto 0);
  signal output_port_5_5_force: signed((27 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port_5_5_force <= unsigned_to_signed(input_port_1_40);
  output_port <= signed_to_std_logic_vector(output_port_5_5_force);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
--
--  Filename      : xlslice.vhd
--
--  Description   : VHDL description of a block that sets the output to a
--                  specified range of the input bits. The output is always
--                  set to an unsigned type with it's binary point at zero.
--
---------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity resampler_xlslice is
    generic (
        new_msb      : integer := 9;           -- position of new msb
        new_lsb      : integer := 1;           -- position of new lsb
        x_width      : integer := 16;          -- Width of x input
        y_width      : integer := 8);          -- Width of y output
    port (
        x : in std_logic_vector (x_width-1 downto 0);
        y : out std_logic_vector (y_width-1 downto 0));
end resampler_xlslice;

architecture behavior of resampler_xlslice is
begin
    y <= x(new_msb downto new_lsb);
end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_efc83d4a08 is
  port (
    op : out std_logic_vector((4 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_efc83d4a08;
architecture behavior of sysgen_constant_efc83d4a08
is
begin
  op <= "1101";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_58ce3fbb94 is
  port (
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_58ce3fbb94;
architecture behavior of sysgen_constant_58ce3fbb94
is
begin
  op <= "1";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_logical_9da2671832 is
  port (
    d0 : in std_logic_vector((1 - 1) downto 0);
    d1 : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_logical_9da2671832;
architecture behavior of sysgen_logical_9da2671832
is
  signal d0_1_24: std_logic;
  signal d1_1_27: std_logic;
  signal fully_2_1_bit: std_logic;
  signal unregy_3_1_convert: std_logic_vector((1 - 1) downto 0);
begin
  d0_1_24 <= d0(0);
  d1_1_27 <= d1(0);
  fully_2_1_bit <= d0_1_24 or d1_1_27;
  unregy_3_1_convert <= cast(std_logic_to_vector(fully_2_1_bit), 0, 1, 0, xlUnsigned);
  y <= unregy_3_1_convert;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_relational_8c3b1d85bf is
  port (
    a : in std_logic_vector((4 - 1) downto 0);
    b : in std_logic_vector((4 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_relational_8c3b1d85bf;
architecture behavior of sysgen_relational_8c3b1d85bf
is
  signal a_1_31: unsigned((4 - 1) downto 0);
  signal b_1_34: unsigned((4 - 1) downto 0);
  signal result_18_3_rel: boolean;
begin
  a_1_31 <= std_logic_vector_to_unsigned(a);
  b_1_34 <= std_logic_vector_to_unsigned(b);
  result_18_3_rel <= a_1_31 > b_1_34;
  op <= boolean_to_vector(result_18_3_rel);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_delay_c31cbaf7cc is
  port (
    d : in std_logic_vector((1 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_delay_c31cbaf7cc;
architecture behavior of sysgen_delay_c31cbaf7cc
is
  signal d_1_22: std_logic;
  signal en_1_25: std_logic;
  signal rst_1_29: std_logic;
  signal op_mem_0_8_24_next: std_logic;
  signal op_mem_0_8_24: std_logic := '0';
  signal op_mem_0_8_24_rst: std_logic;
  signal op_mem_0_8_24_en: std_logic;
  signal op_mem_0_join_10_5: std_logic;
  signal op_mem_0_join_10_5_en: std_logic;
  signal op_mem_0_join_10_5_rst: std_logic;
begin
  d_1_22 <= d(0);
  en_1_25 <= en(0);
  rst_1_29 <= rst(0);
  proc_op_mem_0_8_24: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_0_8_24_rst = '1')) then
        op_mem_0_8_24 <= '0';
      elsif ((ce = '1') and (op_mem_0_8_24_en = '1')) then 
        op_mem_0_8_24 <= op_mem_0_8_24_next;
      end if;
    end if;
  end process proc_op_mem_0_8_24;
  proc_if_10_5: process (d_1_22, en_1_25, rst_1_29)
  is
  begin
    if rst_1_29 = '1' then
      op_mem_0_join_10_5_rst <= '1';
    elsif en_1_25 = '1' then
      op_mem_0_join_10_5_rst <= '0';
    else 
      op_mem_0_join_10_5_rst <= '0';
    end if;
    if en_1_25 = '1' then
      op_mem_0_join_10_5_en <= '1';
    else 
      op_mem_0_join_10_5_en <= '0';
    end if;
    op_mem_0_join_10_5 <= d_1_22;
  end process proc_if_10_5;
  op_mem_0_8_24_next <= d_1_22;
  op_mem_0_8_24_rst <= op_mem_0_join_10_5_rst;
  op_mem_0_8_24_en <= op_mem_0_join_10_5_en;
  q <= std_logic_to_vector(op_mem_0_8_24);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

-------------------------------------------------------------------
 -- System Generator VHDL source file.
 --
 -- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
 -- text/file contains proprietary, confidential information of Xilinx,
 -- Inc., is distributed under license from Xilinx, Inc., and may be used,
 -- copied and/or disclosed only pursuant to the terms of a valid license
 -- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
 -- this text/file solely for design, simulation, implementation and
 -- creation of design files limited to Xilinx devices or technologies.
 -- Use with non-Xilinx devices or technologies is expressly prohibited
 -- and immediately terminates your license unless covered by a separate
 -- agreement.
 --
 -- Xilinx is providing this design, code, or information "as is" solely
 -- for use in developing programs and solutions for Xilinx devices.  By
 -- providing this design, code, or information as one possible
 -- implementation of this feature, application or standard, Xilinx is
 -- making no representation that this implementation is free from any
 -- claims of infringement.  You are responsible for obtaining any rights
 -- you may require for your implementation.  Xilinx expressly disclaims
 -- any warranty whatsoever with respect to the adequacy of the
 -- implementation, including but not limited to warranties of
 -- merchantability or fitness for a particular purpose.
 --
 -- Xilinx products are not intended for use in life support appliances,
 -- devices, or systems.  Use in such applications is expressly prohibited.
 --
 -- Any modifications that are made to the source code are done at the user's
 -- sole risk and will be unsupported.
 --
 -- This copyright and support notice must be retained as part of this
 -- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
 -- reserved.
 -------------------------------------------------------------------
 library IEEE;
 use IEEE.std_logic_1164.all;
 use IEEE.std_logic_arith.all;

entity resampler_xladdsub is 
   generic (
     core_name0: string := "";
     a_width: integer := 16;
     a_bin_pt: integer := 4;
     a_arith: integer := xlUnsigned;
     c_in_width: integer := 16;
     c_in_bin_pt: integer := 4;
     c_in_arith: integer := xlUnsigned;
     c_out_width: integer := 16;
     c_out_bin_pt: integer := 4;
     c_out_arith: integer := xlUnsigned;
     b_width: integer := 8;
     b_bin_pt: integer := 2;
     b_arith: integer := xlUnsigned;
     s_width: integer := 17;
     s_bin_pt: integer := 4;
     s_arith: integer := xlUnsigned;
     rst_width: integer := 1;
     rst_bin_pt: integer := 0;
     rst_arith: integer := xlUnsigned;
     en_width: integer := 1;
     en_bin_pt: integer := 0;
     en_arith: integer := xlUnsigned;
     full_s_width: integer := 17;
     full_s_arith: integer := xlUnsigned;
     mode: integer := xlAddMode;
     extra_registers: integer := 0;
     latency: integer := 0;
     quantization: integer := xlTruncate;
     overflow: integer := xlWrap;
     c_latency: integer := 0;
     c_output_width: integer := 17;
     c_has_c_in : integer := 0;
     c_has_c_out : integer := 0
   );
   port (
     a: in std_logic_vector(a_width - 1 downto 0);
     b: in std_logic_vector(b_width - 1 downto 0);
     c_in : in std_logic_vector (0 downto 0) := "0";
     ce: in std_logic;
     clr: in std_logic := '0';
     clk: in std_logic;
     rst: in std_logic_vector(rst_width - 1 downto 0) := "0";
     en: in std_logic_vector(en_width - 1 downto 0) := "1";
     c_out : out std_logic_vector (0 downto 0);
     s: out std_logic_vector(s_width - 1 downto 0)
   );
 end resampler_xladdsub;
 
 architecture behavior of resampler_xladdsub is 
 component synth_reg
 generic (
 width: integer := 16;
 latency: integer := 5
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;
 
 function format_input(inp: std_logic_vector; old_width, delta, new_arith,
 new_width: integer)
 return std_logic_vector
 is
 variable vec: std_logic_vector(old_width-1 downto 0);
 variable padded_inp: std_logic_vector((old_width + delta)-1 downto 0);
 variable result: std_logic_vector(new_width-1 downto 0);
 begin
 vec := inp;
 if (delta > 0) then
 padded_inp := pad_LSB(vec, old_width+delta);
 result := extend_MSB(padded_inp, new_width, new_arith);
 else
 result := extend_MSB(vec, new_width, new_arith);
 end if;
 return result;
 end;
 
 constant full_s_bin_pt: integer := fractional_bits(a_bin_pt, b_bin_pt);
 constant full_a_width: integer := full_s_width;
 constant full_b_width: integer := full_s_width;
 
 signal full_a: std_logic_vector(full_a_width - 1 downto 0);
 signal full_b: std_logic_vector(full_b_width - 1 downto 0);
 signal core_s: std_logic_vector(full_s_width - 1 downto 0);
 signal conv_s: std_logic_vector(s_width - 1 downto 0);
 signal temp_cout : std_logic;
 signal internal_clr: std_logic;
 signal internal_ce: std_logic;
 signal extra_reg_ce: std_logic;
 signal override: std_logic;
 signal logic1: std_logic_vector(0 downto 0);


 component resampler_c_addsub_v12_0_i0
    port ( 
    a: in std_logic_vector(30 - 1 downto 0);
    s: out std_logic_vector(c_output_width - 1 downto 0);
    b: in std_logic_vector(30 - 1 downto 0) 
 		  ); 
 end component;

 component resampler_c_addsub_v12_0_i1
    port ( 
    a: in std_logic_vector(53 - 1 downto 0);
    clk: in std_logic:= '0';
    ce: in std_logic:= '0';
    s: out std_logic_vector(c_output_width - 1 downto 0);
    b: in std_logic_vector(53 - 1 downto 0) 
 		  ); 
 end component;

 component resampler_c_addsub_v12_0_i2
    port ( 
    a: in std_logic_vector(18 - 1 downto 0);
    clk: in std_logic:= '0';
    ce: in std_logic:= '0';
    s: out std_logic_vector(c_output_width - 1 downto 0);
    b: in std_logic_vector(18 - 1 downto 0) 
 		  ); 
 end component;

 component resampler_c_addsub_v12_0_i3
    port ( 
    a: in std_logic_vector(45 - 1 downto 0);
    clk: in std_logic:= '0';
    ce: in std_logic:= '0';
    s: out std_logic_vector(c_output_width - 1 downto 0);
    b: in std_logic_vector(45 - 1 downto 0) 
 		  ); 
 end component;

 component resampler_c_addsub_v12_0_i4
    port ( 
    a: in std_logic_vector(18 - 1 downto 0);
    clk: in std_logic:= '0';
    ce: in std_logic:= '0';
    s: out std_logic_vector(c_output_width - 1 downto 0);
    b: in std_logic_vector(18 - 1 downto 0) 
 		  ); 
 end component;

begin
 internal_clr <= (clr or (rst(0))) and ce;
 internal_ce <= ce and en(0);
 logic1(0) <= '1';
 addsub_process: process (a, b, core_s)
 begin
 full_a <= format_input (a, a_width, b_bin_pt - a_bin_pt, a_arith,
 full_a_width);
 full_b <= format_input (b, b_width, a_bin_pt - b_bin_pt, b_arith,
 full_b_width);
 conv_s <= convert_type (core_s, full_s_width, full_s_bin_pt, full_s_arith,
 s_width, s_bin_pt, s_arith, quantization, overflow);
 end process addsub_process;


 comp0: if ((core_name0 = "resampler_c_addsub_v12_0_i0")) generate 
  core_instance0:resampler_c_addsub_v12_0_i0
   port map ( 
         a => full_a,
         s => core_s,
         b => full_b
  ); 
   end generate;

 comp1: if ((core_name0 = "resampler_c_addsub_v12_0_i1")) generate 
  core_instance1:resampler_c_addsub_v12_0_i1
   port map ( 
         a => full_a,
         clk => clk,
         ce => internal_ce,
         s => core_s,
         b => full_b
  ); 
   end generate;

 comp2: if ((core_name0 = "resampler_c_addsub_v12_0_i2")) generate 
  core_instance2:resampler_c_addsub_v12_0_i2
   port map ( 
         a => full_a,
         clk => clk,
         ce => internal_ce,
         s => core_s,
         b => full_b
  ); 
   end generate;

 comp3: if ((core_name0 = "resampler_c_addsub_v12_0_i3")) generate 
  core_instance3:resampler_c_addsub_v12_0_i3
   port map ( 
         a => full_a,
         clk => clk,
         ce => internal_ce,
         s => core_s,
         b => full_b
  ); 
   end generate;

 comp4: if ((core_name0 = "resampler_c_addsub_v12_0_i4")) generate 
  core_instance4:resampler_c_addsub_v12_0_i4
   port map ( 
         a => full_a,
         clk => clk,
         ce => internal_ce,
         s => core_s,
         b => full_b
  ); 
   end generate;

latency_test: if (extra_registers > 0) generate
 override_test: if (c_latency > 1) generate
 override_pipe: synth_reg
 generic map (
 width => 1,
 latency => c_latency
 )
 port map (
 i => logic1,
 ce => internal_ce,
 clr => internal_clr,
 clk => clk,
 o(0) => override);
 extra_reg_ce <= ce and en(0) and override;
 end generate override_test;
 no_override: if ((c_latency = 0) or (c_latency = 1)) generate
 extra_reg_ce <= ce and en(0);
 end generate no_override;
 extra_reg: synth_reg
 generic map (
 width => s_width,
 latency => extra_registers
 )
 port map (
 i => conv_s,
 ce => extra_reg_ce,
 clr => internal_clr,
 clk => clk,
 o => s
 );
 cout_test: if (c_has_c_out = 1) generate
 c_out_extra_reg: synth_reg
 generic map (
 width => 1,
 latency => extra_registers
 )
 port map (
 i(0) => temp_cout,
 ce => extra_reg_ce,
 clr => internal_clr,
 clk => clk,
 o => c_out
 );
 end generate cout_test;
 end generate;
 
 latency_s: if ((latency = 0) or (extra_registers = 0)) generate
 s <= conv_s;
 end generate latency_s;
 latency0: if (((latency = 0) or (extra_registers = 0)) and
 (c_has_c_out = 1)) generate
 c_out(0) <= temp_cout;
 end generate latency0;
 tie_dangling_cout: if (c_has_c_out = 0) generate
 c_out <= "0";
 end generate tie_dangling_cout;
 end architecture behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
 --
 --  Filename      : xlcounter_rst.vhd
 --
 --  Created       : 1/31/01
 --  Modified      :
 --
 --  Description   : VHDL wrapper for a counter. This wrapper
 --                  uses the Binary Counter CoreGenerator core.
 --
 ---------------------------------------------------------------------
 
 
 ---------------------------------------------------------------------
 --
 --  Entity        : xlcounter
 --
 --  Architecture  : behavior
 --
 --  Description   : Top level VHDL description of a counter.
 --
 ---------------------------------------------------------------------
 
 library IEEE;
 use IEEE.std_logic_1164.all;

entity resampler_xlcounter_free is 
   generic (
     core_name0: string := "";
     op_width: integer := 5;
     op_arith: integer := xlSigned
   );
   port (
     ce: in std_logic;
     clr: in std_logic;
     clk: in std_logic;
     op: out std_logic_vector(op_width - 1 downto 0);
     up: in std_logic_vector(0 downto 0) := (others => '0');
     load: in std_logic_vector(0 downto 0) := (others => '0');
     din: in std_logic_vector(op_width - 1 downto 0) := (others => '0');
     en: in std_logic_vector(0 downto 0);
     rst: in std_logic_vector(0 downto 0)
   );
 end resampler_xlcounter_free;
 
 architecture behavior of resampler_xlcounter_free is


 component resampler_c_counter_binary_v12_0_i0
    port ( 
      clk: in std_logic;
      ce: in std_logic;
      SINIT: in std_logic;
      q: out std_logic_vector(op_width - 1 downto 0) 
 		  ); 
 end component;

-- synthesis translate_off
   constant zeroVec: std_logic_vector(op_width - 1 downto 0) := (others => '0');
   constant oneVec: std_logic_vector(op_width - 1 downto 0) := (others => '1');
   constant zeroStr: string(1 to op_width) :=
     std_logic_vector_to_bin_string(zeroVec);
   constant oneStr: string(1 to op_width) :=
     std_logic_vector_to_bin_string(oneVec);
 -- synthesis translate_on
 
   signal core_sinit: std_logic;
   signal core_ce: std_logic;
   signal op_net: std_logic_vector(op_width - 1 downto 0);
 begin
   core_ce <= ce and en(0);
   core_sinit <= (clr or rst(0)) and ce;
   op <= op_net;


 comp0: if ((core_name0 = "resampler_c_counter_binary_v12_0_i0")) generate 
  core_instance0:resampler_c_counter_binary_v12_0_i0
   port map ( 
        clk => clk,
        ce => core_ce,
        SINIT => core_sinit,
        q => op_net
  ); 
   end generate;

end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
 --
 --  Entity        : xlfifogen
 --
 --  Architecture  : behavior
 --
 --  Description   : Top level VHDL description of a fifo block
 --
 ---------------------------------------------------------------------
 
 library IEEE;
 use IEEE.std_logic_1164.all;
 use IEEE.std_logic_arith.all;
 use ieee.std_logic_unsigned.all;

entity resampler_xlfifogen_u is 
   generic (
     core_name0: string := "";
     data_width: integer := -1;
     data_count_width: integer := -1;
     percent_full_width: integer := -1;
     has_ae : integer := 0;
     has_af : integer := 0;
     extra_registers: integer := 0;
     has_rst : boolean := false
   );
   port (
     din: in std_logic_vector(data_width - 1 downto 0);
     we: in std_logic;
     we_ce: in std_logic;
     re: in std_logic;
     re_ce: in std_logic;
     rst: in std_logic;
     en: in std_logic;
     ce: in std_logic;
     clk: in std_logic;
     empty: out std_logic;
     full: out std_logic;
     percent_full: out std_logic_vector(percent_full_width - 1 downto 0);
     dcount: out std_logic_vector(data_count_width - 1 downto 0);
     ae: out std_logic;
     af: out std_logic;
     dout: out std_logic_vector(data_width - 1 downto 0)
   );
 end resampler_xlfifogen_u;
 
 architecture behavior of resampler_xlfifogen_u is
 component synth_reg
 generic (
 width: integer := 16;
 latency: integer := 5
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;
 


 component resampler_fifo_generator_i0
    port ( 
      clk: in std_logic;
      din: in std_logic_vector(data_width - 1 downto 0);
      wr_en: in std_logic;
      rd_en: in std_logic;
      dout: out std_logic_vector(data_width - 1 downto 0);
      full: out std_logic;
      empty: out std_logic;
      data_count: out std_logic_vector(data_count_width - 1 downto 0) 
 		  ); 
 end component;

 component resampler_fifo_generator_i1
    port ( 
      clk: in std_logic;
      din: in std_logic_vector(data_width - 1 downto 0);
      wr_en: in std_logic;
      rd_en: in std_logic;
      dout: out std_logic_vector(data_width - 1 downto 0);
      full: out std_logic;
      empty: out std_logic 
 		  ); 
 end component;

 component resampler_fifo_generator_i2
    port ( 
      clk: in std_logic;
      din: in std_logic_vector(data_width - 1 downto 0);
      wr_en: in std_logic;
      rd_en: in std_logic;
      dout: out std_logic_vector(data_width - 1 downto 0);
      full: out std_logic;
      empty: out std_logic;
      data_count: out std_logic_vector(data_count_width - 1 downto 0) 
 		  ); 
 end component;

  signal rd_en: std_logic;
   signal wr_en: std_logic;
   signal srst: std_logic;
   signal core_full: std_logic;
   signal core_dcount: std_logic_vector(data_count_width - 1 downto 0);
   signal srst_vec: std_logic_vector(0 downto 0);
   signal dout_net: std_logic_vector(data_width - 1 downto 0);
   signal count: integer; 
   signal empty_net: std_logic; 
   signal ae_net: std_logic; 
   signal af_net: std_logic; 
 
 begin
 

 comp0: if ((core_name0 = "resampler_fifo_generator_i0")) generate 
  core_instance0:resampler_fifo_generator_i0
   port map ( 
        clk => clk,
        din => din,
        wr_en => wr_en,
        rd_en => rd_en,
        full => core_full,
        dout => dout_net,
        empty => empty_net
,
        data_count => core_dcount
  ); 
   end generate;

 comp1: if ((core_name0 = "resampler_fifo_generator_i1")) generate 
  core_instance1:resampler_fifo_generator_i1
   port map ( 
        clk => clk,
        din => din,
        wr_en => wr_en,
        rd_en => rd_en,
        full => core_full,
        dout => dout_net,
        empty => empty_net

  ); 
   end generate;

 comp2: if ((core_name0 = "resampler_fifo_generator_i2")) generate 
  core_instance2:resampler_fifo_generator_i2
   port map ( 
        clk => clk,
        din => din,
        wr_en => wr_en,
        rd_en => rd_en,
        full => core_full,
        dout => dout_net,
        empty => empty_net
,
        data_count => core_dcount
  ); 
   end generate;

-- Process to remap data count from 0000->1111 when fifo is full.
   modify_count: process(core_full, core_dcount) is
   begin
     if core_full = '1' then
       percent_full <= (others => '1');
     else
       percent_full <= core_dcount(data_count_width-1 downto data_count_width-percent_full_width);
     end if;
   end process modify_count;
   
   
   --Zero ae/af if these signals are not specified on the core
   terminate_core_ae: if has_ae /= 1 generate
   begin
     ae <= '0';
   end generate terminate_core_ae;
   terminate_core_af: if has_af /= 1 generate
   begin
     af <= '0';
   end generate terminate_core_af;
latency_gt_0: if (extra_registers > 0) generate
   reg: synth_reg
     generic map (
       width => 1,
       latency => extra_registers
     )
     port map (
       i => std_logic_to_vector(rst),
       ce => ce,
       clr => '0',
       clk => clk,
       o => srst_vec
     );
     srst <= srst_vec(0);
 end generate;
 
 latency_eq_0: if (extra_registers = 0) generate
   srst <= rst and ce;
 end generate;
 
    process (dout_net, empty_net, core_full, core_dcount, ae_net, af_net, re, we, en, re_ce, we_ce) is 
    begin 
        dout <= dout_net; 
        empty <= empty_net; 
        full <= core_full; 
        dcount <= core_dcount;
        ae <= ae_net;
        af <= af_net;
        rd_en <= re and en and re_ce;
        wr_en <= we and en and we_ce;
    end process; 
 end  behavior;
 
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

-------------------------------------------------------------------
 -- System Generator VHDL source file.
 --
 -- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
 -- text/file contains proprietary, confidential information of Xilinx,
 -- Inc., is distributed under license from Xilinx, Inc., and may be used,
 -- copied and/or disclosed only pursuant to the terms of a valid license
 -- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
 -- this text/file solely for design, simulation, implementation and
 -- creation of design files limited to Xilinx devices or technologies.
 -- Use with non-Xilinx devices or technologies is expressly prohibited
 -- and immediately terminates your license unless covered by a separate
 -- agreement.
 --
 -- Xilinx is providing this design, code, or information "as is" solely
 -- for use in developing programs and solutions for Xilinx devices.  By
 -- providing this design, code, or information as one possible
 -- implementation of this feature, application or standard, Xilinx is
 -- making no representation that this implementation is free from any
 -- claims of infringement.  You are responsible for obtaining any rights
 -- you may require for your implementation.  Xilinx expressly disclaims
 -- any warranty whatsoever with respect to the adequacy of the
 -- implementation, including but not limited to warranties of
 -- merchantability or fitness for a particular purpose.
 --
 -- Xilinx products are not intended for use in life support appliances,
 -- devices, or systems.  Use in such applications is expressly prohibited.
 --
 -- Any modifications that are made to the source code are done at the user's
 -- sole risk and will be unsupported.
 --
 -- This copyright and support notice must be retained as part of this
 -- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
 -- reserved.
 -------------------------------------------------------------------
 library IEEE;
 use IEEE.std_logic_1164.all;
 use IEEE.std_logic_arith.all;

entity resampler_xlmult is 
   generic (
     core_name0: string := "";
     a_width: integer := 4;
     a_bin_pt: integer := 2;
     a_arith: integer := xlSigned;
     b_width: integer := 4;
     b_bin_pt: integer := 1;
     b_arith: integer := xlSigned;
     p_width: integer := 8;
     p_bin_pt: integer := 2;
     p_arith: integer := xlSigned;
     rst_width: integer := 1;
     rst_bin_pt: integer := 0;
     rst_arith: integer := xlUnsigned;
     en_width: integer := 1;
     en_bin_pt: integer := 0;
     en_arith: integer := xlUnsigned;
     quantization: integer := xlTruncate;
     overflow: integer := xlWrap;
     extra_registers: integer := 0;
     c_a_width: integer := 7;
     c_b_width: integer := 7;
     c_type: integer := 0;
     c_a_type: integer := 0;
     c_b_type: integer := 0;
     c_pipelined: integer := 1;
     c_baat: integer := 4;
     multsign: integer := xlSigned;
     c_output_width: integer := 16
   );
   port (
     a: in std_logic_vector(a_width - 1 downto 0);
     b: in std_logic_vector(b_width - 1 downto 0);
     ce: in std_logic;
     clr: in std_logic;
     clk: in std_logic;
     core_ce: in std_logic := '0';
     core_clr: in std_logic := '0';
     core_clk: in std_logic := '0';
     rst: in std_logic_vector(rst_width - 1 downto 0);
     en: in std_logic_vector(en_width - 1 downto 0);
     p: out std_logic_vector(p_width - 1 downto 0)
   );
 end  resampler_xlmult;
 
 architecture behavior of resampler_xlmult is
 component synth_reg
 generic (
 width: integer := 16;
 latency: integer := 5
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;


 component resampler_mult_gen_v12_0_i0
    port ( 
      b: in std_logic_vector(c_b_width - 1 downto 0);
      p: out std_logic_vector(c_output_width - 1 downto 0);
      clk: in std_logic;
      ce: in std_logic;
      sclr: in std_logic;
      a: in std_logic_vector(c_a_width - 1 downto 0) 
 		  ); 
 end component;

 component resampler_mult_gen_v12_0_i1
    port ( 
      b: in std_logic_vector(c_b_width - 1 downto 0);
      p: out std_logic_vector(c_output_width - 1 downto 0);
      clk: in std_logic;
      ce: in std_logic;
      sclr: in std_logic;
      a: in std_logic_vector(c_a_width - 1 downto 0) 
 		  ); 
 end component;

 component resampler_mult_gen_v12_0_i2
    port ( 
      b: in std_logic_vector(c_b_width - 1 downto 0);
      p: out std_logic_vector(c_output_width - 1 downto 0);
      a: in std_logic_vector(c_a_width - 1 downto 0) 
 		  ); 
 end component;

 component resampler_mult_gen_v12_0_i3
    port ( 
      b: in std_logic_vector(c_b_width - 1 downto 0);
      p: out std_logic_vector(c_output_width - 1 downto 0);
      clk: in std_logic;
      ce: in std_logic;
      sclr: in std_logic;
      a: in std_logic_vector(c_a_width - 1 downto 0) 
 		  ); 
 end component;

signal tmp_a: std_logic_vector(c_a_width - 1 downto 0);
 signal conv_a: std_logic_vector(c_a_width - 1 downto 0);
 signal tmp_b: std_logic_vector(c_b_width - 1 downto 0);
 signal conv_b: std_logic_vector(c_b_width - 1 downto 0);
 signal tmp_p: std_logic_vector(c_output_width - 1 downto 0);
 signal conv_p: std_logic_vector(p_width - 1 downto 0);
 -- synthesis translate_off
 signal real_a, real_b, real_p: real;
 -- synthesis translate_on
 signal rfd: std_logic;
 signal rdy: std_logic;
 signal nd: std_logic;
 signal internal_ce: std_logic;
 signal internal_clr: std_logic;
 signal internal_core_ce: std_logic;
 begin
 -- synthesis translate_off
 -- synthesis translate_on
 internal_ce <= ce and en(0);
 internal_core_ce <= core_ce and en(0);
 internal_clr <= (clr or rst(0)) and ce;
 nd <= internal_ce;
 input_process: process (a,b)
 begin
 tmp_a <= zero_ext(a, c_a_width);
 tmp_b <= zero_ext(b, c_b_width);
 end process;
 output_process: process (tmp_p)
 begin
 conv_p <= convert_type(tmp_p, c_output_width, a_bin_pt+b_bin_pt, multsign,
 p_width, p_bin_pt, p_arith, quantization, overflow);
 end process;


 comp0: if ((core_name0 = "resampler_mult_gen_v12_0_i0")) generate 
  core_instance0:resampler_mult_gen_v12_0_i0
   port map ( 
        a => tmp_a,
        clk => clk,
        ce => internal_ce,
        sclr => internal_clr,
        p => tmp_p,
        b => tmp_b
  ); 
   end generate;

 comp1: if ((core_name0 = "resampler_mult_gen_v12_0_i1")) generate 
  core_instance1:resampler_mult_gen_v12_0_i1
   port map ( 
        a => tmp_a,
        clk => clk,
        ce => internal_ce,
        sclr => internal_clr,
        p => tmp_p,
        b => tmp_b
  ); 
   end generate;

 comp2: if ((core_name0 = "resampler_mult_gen_v12_0_i2")) generate 
  core_instance2:resampler_mult_gen_v12_0_i2
   port map ( 
        a => tmp_a,
        p => tmp_p,
        b => tmp_b
  ); 
   end generate;

 comp3: if ((core_name0 = "resampler_mult_gen_v12_0_i3")) generate 
  core_instance3:resampler_mult_gen_v12_0_i3
   port map ( 
        a => tmp_a,
        clk => clk,
        ce => internal_ce,
        sclr => internal_clr,
        p => tmp_p,
        b => tmp_b
  ); 
   end generate;

latency_gt_0: if (extra_registers > 0) generate
 reg: synth_reg
 generic map (
 width => p_width,
 latency => extra_registers
 )
 port map (
 i => conv_p,
 ce => internal_ce,
 clr => internal_clr,
 clk => clk,
 o => p
 );
 end generate;
 latency_eq_0: if (extra_registers = 0) generate
 p <= conv_p;
 end generate;
 end architecture behavior;


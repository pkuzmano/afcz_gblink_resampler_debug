-- Generated from Simulink block Resampler/RSPUP_I/Dly_Comput_Great_IC_FunctionalModel_NoDelay
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_dly_comput_great_ic_functionalmodel_nodelay is
  port (
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    toutn : in std_logic_vector( 29-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dly : out std_logic_vector( 27-1 downto 0 );
    op : out std_logic_vector( 1-1 downto 0 )
  );
end resampler_dly_comput_great_ic_functionalmodel_nodelay;
architecture structural of resampler_dly_comput_great_ic_functionalmodel_nodelay is 
  signal outputreg0_q_net : std_logic_vector( 27-1 downto 0 );
  signal dlyreg_q_net : std_logic_vector( 29-1 downto 0 );
  signal outputreg1_q_net : std_logic_vector( 1-1 downto 0 );
  signal new_net : std_logic_vector( 1-1 downto 0 );
  signal tout_up_net : std_logic_vector( 29-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal absolute_op_net : std_logic_vector( 29-1 downto 0 );
  signal addsub2_s_net : std_logic_vector( 29-1 downto 0 );
  signal cstm1_op_net : std_logic_vector( 29-1 downto 0 );
  signal inputreg1_q_net : std_logic_vector( 29-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 29-1 downto 0 );
  signal opmux1_y_net : std_logic_vector( 29-1 downto 0 );
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal cst0_op_net : std_logic_vector( 29-1 downto 0 );
  signal concat_y_net : std_logic_vector( 2-1 downto 0 );
  signal validreg_q_net : std_logic_vector( 1-1 downto 0 );
  signal inputreg0_q_net : std_logic_vector( 1-1 downto 0 );
  signal cst0p5_op_net : std_logic_vector( 29-1 downto 0 );
  signal dlycast_dout_net : std_logic_vector( 27-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  dly <= outputreg0_q_net;
  op <= outputreg1_q_net;
  new_net <= new_x0;
  tout_up_net <= toutn;
  rst_net <= rst;
  clk_net <= clk_1;
  ce_net <= ce_1;
  absolute : entity xil_defaultlib.sysgen_abs_4665453b1c 
  port map (
    clr => '0',
    a => addsub2_s_net,
    clk => clk_net,
    ce => ce_net,
    op => absolute_op_net
  );
  addsub1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 26,
    a_width => 29,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 29,
    c_has_c_out => 0,
    c_latency => 0,
    c_output_width => 30,
    core_name0 => "resampler_c_addsub_v12_0_i0",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 30,
    latency => 0,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 26,
    s_width => 29
  )
  port map (
    clr => '0',
    en => "1",
    a => cstm1_op_net,
    b => inputreg1_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  addsub2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 26,
    a_width => 29,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 29,
    c_has_c_out => 0,
    c_latency => 0,
    c_output_width => 30,
    core_name0 => "resampler_c_addsub_v12_0_i0",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 30,
    latency => 0,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 26,
    s_width => 29
  )
  port map (
    clr => '0',
    en => "1",
    a => dlyreg_q_net,
    b => opmux1_y_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub2_s_net
  );
  concat : entity xil_defaultlib.sysgen_concat_35df48c783 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => inputreg0_q_net,
    in1 => validreg_q_net,
    y => concat_y_net
  );
  cst0 : entity xil_defaultlib.sysgen_constant_28d7591431 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => cst0_op_net
  );
  cst0p5 : entity xil_defaultlib.sysgen_constant_e1a3721ebb 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => cst0p5_op_net
  );
  cstm1 : entity xil_defaultlib.sysgen_constant_1f637969ae 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => cstm1_op_net
  );
  dlycast : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 26,
    din_width => 29,
    dout_arith => 2,
    dout_bin_pt => 26,
    dout_width => 27,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => dlyreg_q_net,
    clk => clk_net,
    ce => ce_net,
    dout => dlycast_dout_net
  );
  dlyreg : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 29,
    init_value => b"00100000000000000000000000000"
  )
  port map (
    en => "1",
    d => addsub2_s_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => dlyreg_q_net
  );
  inputreg0 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => new_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => inputreg0_q_net
  );
  inputreg1 : entity xil_defaultlib.sysgen_delay_4518c31358 
  port map (
    clr => '0',
    d => tout_up_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => inputreg1_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_3b83bb2925 
  port map (
    clr => '0',
    ip => relational1_op_net,
    clk => clk_net,
    ce => ce_net,
    op => inverter_op_net
  );
  opmux1 : entity xil_defaultlib.sysgen_mux_bbd0f8a028 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    sel => concat_y_net,
    d0 => cst0_op_net,
    d1 => inputreg1_q_net,
    d2 => cstm1_op_net,
    d3 => addsub1_s_net,
    y => opmux1_y_net
  );
  outputreg0 : entity xil_defaultlib.sysgen_delay_552786d6d7 
  port map (
    clr => '0',
    d => dlycast_dout_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => outputreg0_q_net
  );
  outputreg1 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => validreg_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => outputreg1_q_net
  );
  relational1 : entity xil_defaultlib.sysgen_relational_d8d46a9b80 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => absolute_op_net,
    b => cst0p5_op_net,
    op => relational1_op_net
  );
  validreg : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    d => inverter_op_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => validreg_q_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/Horner
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_horner is
  port (
    fir0 : in std_logic_vector( 44-1 downto 0 );
    fir1 : in std_logic_vector( 44-1 downto 0 );
    fir2 : in std_logic_vector( 44-1 downto 0 );
    fir3 : in std_logic_vector( 44-1 downto 0 );
    fir4 : in std_logic_vector( 44-1 downto 0 );
    fir5 : in std_logic_vector( 44-1 downto 0 );
    dly : in std_logic_vector( 27-1 downto 0 );
    valid : in std_logic_vector( 1-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 );
    validout : out std_logic_vector( 1-1 downto 0 )
  );
end resampler_horner;
architecture structural of resampler_horner is 
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal fifo1_dout_net : std_logic_vector( 44-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal fifo2_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo5_dout_net : std_logic_vector( 44-1 downto 0 );
  signal delaypipesl1_q_net : std_logic_vector( 27-1 downto 0 );
  signal fifo_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo4_dout_net : std_logic_vector( 44-1 downto 0 );
  signal delayvalid9_q_net : std_logic_vector( 1-1 downto 0 );
  signal fifo3_dout_net : std_logic_vector( 44-1 downto 0 );
  signal delaypipesl2_q_net : std_logic_vector( 1-1 downto 0 );
  signal addtree3_s_net : std_logic_vector( 44-1 downto 0 );
  signal delayfir01_q_net : std_logic_vector( 44-1 downto 0 );
  signal mult2_p_net : std_logic_vector( 52-1 downto 0 );
  signal addtree2_s_net : std_logic_vector( 44-1 downto 0 );
  signal ce_net : std_logic;
  signal delayfir41_q_net : std_logic_vector( 44-1 downto 0 );
  signal mult4_p_net : std_logic_vector( 52-1 downto 0 );
  signal addtree1_s_net : std_logic_vector( 44-1 downto 0 );
  signal delayfir31_q_net : std_logic_vector( 44-1 downto 0 );
  signal addtree4_s_net : std_logic_vector( 44-1 downto 0 );
  signal delayfir11_q_net : std_logic_vector( 44-1 downto 0 );
  signal delayfir21_q_net : std_logic_vector( 44-1 downto 0 );
  signal mult5_p_net : std_logic_vector( 52-1 downto 0 );
  signal mult3_p_net : std_logic_vector( 52-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 52-1 downto 0 );
  signal delayfir22_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayfir20_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayfir30_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayfir32_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayfir40_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayfir42_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayfir12_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayvalid1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayfir50_q_net : std_logic_vector( 27-1 downto 0 );
  signal delayvalid2_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayvalid5_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayvalid0_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayvalid3_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayvalid6_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayvalid4_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayvalid7_q_net : std_logic_vector( 1-1 downto 0 );
  signal delayvalid8_q_net : std_logic_vector( 1-1 downto 0 );
begin
  dataout <= addtree0_s_net;
  validout <= delayvalid9_q_net;
  fifo_dout_net <= fir0;
  fifo1_dout_net <= fir1;
  fifo2_dout_net <= fir2;
  fifo3_dout_net <= fir3;
  fifo4_dout_net <= fir4;
  fifo5_dout_net <= fir5;
  delaypipesl1_q_net <= dly;
  delaypipesl2_q_net <= valid;
  rst_net <= rst;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addtree0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 51,
    b_width => 52,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 53,
    core_name0 => "resampler_c_addsub_v12_0_i1",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 53,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    en => "1",
    a => delayfir01_q_net,
    b => mult1_p_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree0_s_net
  );
  addtree1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 51,
    b_width => 52,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 53,
    core_name0 => "resampler_c_addsub_v12_0_i1",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 53,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    en => "1",
    a => delayfir11_q_net,
    b => mult2_p_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree1_s_net
  );
  addtree2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 51,
    b_width => 52,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 53,
    core_name0 => "resampler_c_addsub_v12_0_i1",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 53,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    en => "1",
    a => delayfir21_q_net,
    b => mult3_p_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree2_s_net
  );
  addtree3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 51,
    b_width => 52,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 53,
    core_name0 => "resampler_c_addsub_v12_0_i1",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 53,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    en => "1",
    a => delayfir31_q_net,
    b => mult4_p_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree3_s_net
  );
  addtree4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 51,
    b_width => 52,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 53,
    core_name0 => "resampler_c_addsub_v12_0_i1",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 53,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    en => "1",
    a => delayfir41_q_net,
    b => mult5_p_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree4_s_net
  );
  delayfir01 : entity xil_defaultlib.sysgen_delay_b7f5764dfb 
  port map (
    clr => '0',
    d => fifo_dout_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir01_q_net
  );
  delayfir11 : entity xil_defaultlib.sysgen_delay_dfc16757ae 
  port map (
    clr => '0',
    d => fifo1_dout_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir11_q_net
  );
  delayfir12 : entity xil_defaultlib.sysgen_delay_552786d6d7 
  port map (
    clr => '0',
    d => delayfir20_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir12_q_net
  );
  delayfir20 : entity xil_defaultlib.sysgen_delay_b67b4f5ede 
  port map (
    clr => '0',
    d => delayfir22_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir20_q_net
  );
  delayfir21 : entity xil_defaultlib.sysgen_delay_1b78fad3aa 
  port map (
    clr => '0',
    d => fifo2_dout_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir21_q_net
  );
  delayfir22 : entity xil_defaultlib.sysgen_delay_552786d6d7 
  port map (
    clr => '0',
    d => delayfir30_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir22_q_net
  );
  delayfir30 : entity xil_defaultlib.sysgen_delay_b67b4f5ede 
  port map (
    clr => '0',
    d => delayfir32_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir30_q_net
  );
  delayfir31 : entity xil_defaultlib.sysgen_delay_2d25fa5d59 
  port map (
    clr => '0',
    d => fifo3_dout_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir31_q_net
  );
  delayfir32 : entity xil_defaultlib.sysgen_delay_552786d6d7 
  port map (
    clr => '0',
    d => delayfir40_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir32_q_net
  );
  delayfir40 : entity xil_defaultlib.sysgen_delay_b67b4f5ede 
  port map (
    clr => '0',
    d => delayfir42_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir40_q_net
  );
  delayfir41 : entity xil_defaultlib.sysgen_delay_cced485432 
  port map (
    clr => '0',
    d => fifo4_dout_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir41_q_net
  );
  delayfir42 : entity xil_defaultlib.sysgen_delay_552786d6d7 
  port map (
    clr => '0',
    d => delayfir50_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir42_q_net
  );
  delayfir50 : entity xil_defaultlib.sysgen_delay_b67b4f5ede 
  port map (
    clr => '0',
    d => delaypipesl1_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayfir50_q_net
  );
  delayvalid0 : entity xil_defaultlib.sysgen_delay_b08e6841ee 
  port map (
    clr => '0',
    d => delaypipesl2_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid0_q_net
  );
  delayvalid1 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delayvalid0_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid1_q_net
  );
  delayvalid2 : entity xil_defaultlib.sysgen_delay_b08e6841ee 
  port map (
    clr => '0',
    d => delayvalid1_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid2_q_net
  );
  delayvalid3 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delayvalid2_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid3_q_net
  );
  delayvalid4 : entity xil_defaultlib.sysgen_delay_b08e6841ee 
  port map (
    clr => '0',
    d => delayvalid3_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid4_q_net
  );
  delayvalid5 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delayvalid4_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid5_q_net
  );
  delayvalid6 : entity xil_defaultlib.sysgen_delay_b08e6841ee 
  port map (
    clr => '0',
    d => delayvalid5_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid6_q_net
  );
  delayvalid7 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delayvalid6_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid7_q_net
  );
  delayvalid8 : entity xil_defaultlib.sysgen_delay_b08e6841ee 
  port map (
    clr => '0',
    d => delayvalid7_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid8_q_net
  );
  delayvalid9 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delayvalid8_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delayvalid9_q_net
  );
  mult1 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 44,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 44,
    c_output_width => 71,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i1",
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 51,
    p_width => 52,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => addtree1_s_net,
    b => delayfir12_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult1_p_net
  );
  mult2 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 44,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 44,
    c_output_width => 71,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i1",
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 51,
    p_width => 52,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => addtree2_s_net,
    b => delayfir22_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult2_p_net
  );
  mult3 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 44,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 44,
    c_output_width => 71,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i1",
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 51,
    p_width => 52,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => addtree3_s_net,
    b => delayfir32_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult3_p_net
  );
  mult4 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 44,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 44,
    c_output_width => 71,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i1",
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 51,
    p_width => 52,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => addtree4_s_net,
    b => delayfir42_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult4_p_net
  );
  mult5 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 44,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 44,
    c_output_width => 71,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i1",
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 51,
    p_width => 52,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => fifo5_dout_net,
    b => delaypipesl1_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult5_p_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/SynchMem
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_synchmem is
  port (
    firin0 : in std_logic_vector( 44-1 downto 0 );
    firin1 : in std_logic_vector( 44-1 downto 0 );
    firin2 : in std_logic_vector( 44-1 downto 0 );
    firin3 : in std_logic_vector( 44-1 downto 0 );
    firin4 : in std_logic_vector( 44-1 downto 0 );
    firin5 : in std_logic_vector( 44-1 downto 0 );
    push : in std_logic_vector( 1-1 downto 0 );
    pop : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    firout0 : out std_logic_vector( 44-1 downto 0 );
    firout1 : out std_logic_vector( 44-1 downto 0 );
    firout2 : out std_logic_vector( 44-1 downto 0 );
    firout3 : out std_logic_vector( 44-1 downto 0 );
    firout4 : out std_logic_vector( 44-1 downto 0 );
    firout5 : out std_logic_vector( 44-1 downto 0 );
    num : out std_logic_vector( 8-1 downto 0 )
  );
end resampler_synchmem;
architecture structural of resampler_synchmem is 
  signal fifo_empty_net : std_logic;
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal fifo4_dout_net : std_logic_vector( 44-1 downto 0 );
  signal dlycast2_dout_net : std_logic_vector( 44-1 downto 0 );
  signal dlycast5_dout_net : std_logic_vector( 44-1 downto 0 );
  signal dlycast_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo1_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal dlycast1_dout_net : std_logic_vector( 44-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal dlycast3_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo3_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo1_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo2_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo_full_net : std_logic;
  signal fifo5_dout_net : std_logic_vector( 44-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal fifo_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal dlycast4_dout_net : std_logic_vector( 44-1 downto 0 );
  signal fifo1_full_net : std_logic;
  signal fifo3_empty_net : std_logic;
  signal fifo3_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal fifo2_empty_net : std_logic;
  signal fifo2_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal fifo4_empty_net : std_logic;
  signal fifo2_full_net : std_logic;
  signal fifo4_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal fifo1_empty_net : std_logic;
  signal fifo3_full_net : std_logic;
  signal fifo4_full_net : std_logic;
  signal fifo5_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal fifo5_full_net : std_logic;
  signal fifo5_empty_net : std_logic;
begin
  firout0 <= fifo_dout_net;
  firout1 <= fifo1_dout_net;
  firout2 <= fifo2_dout_net;
  firout3 <= fifo3_dout_net;
  firout4 <= fifo4_dout_net;
  firout5 <= fifo5_dout_net;
  num <= fifo1_dcount_net;
  dlycast_dout_net <= firin0;
  dlycast1_dout_net <= firin1;
  dlycast2_dout_net <= firin2;
  dlycast3_dout_net <= firin3;
  dlycast4_dout_net <= firin4;
  dlycast5_dout_net <= firin5;
  logical1_y_net <= push;
  logical2_y_net <= pop;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fifo : entity xil_defaultlib.resampler_xlfifogen_u 
  generic map (
    core_name0 => "resampler_fifo_generator_i0",
    data_count_width => 8,
    data_width => 44,
    extra_registers => 1,
    has_ae => 0,
    has_af => 0,
    has_rst => false,
    percent_full_width => 1
  )
  port map (
    en => '1',
    rst => '1',
    din => dlycast_dout_net,
    we => logical1_y_net(0),
    re => logical2_y_net(0),
    clk => clk_net,
    ce => ce_net,
    we_ce => ce_net,
    re_ce => ce_net,
    dout => fifo_dout_net,
    empty => fifo_empty_net,
    full => fifo_full_net,
    dcount => fifo_dcount_net
  );
  fifo1 : entity xil_defaultlib.resampler_xlfifogen_u 
  generic map (
    core_name0 => "resampler_fifo_generator_i0",
    data_count_width => 8,
    data_width => 44,
    extra_registers => 1,
    has_ae => 0,
    has_af => 0,
    has_rst => false,
    percent_full_width => 1
  )
  port map (
    en => '1',
    rst => '1',
    din => dlycast1_dout_net,
    we => logical1_y_net(0),
    re => logical2_y_net(0),
    clk => clk_net,
    ce => ce_net,
    we_ce => ce_net,
    re_ce => ce_net,
    dout => fifo1_dout_net,
    empty => fifo1_empty_net,
    full => fifo1_full_net,
    dcount => fifo1_dcount_net
  );
  fifo2 : entity xil_defaultlib.resampler_xlfifogen_u 
  generic map (
    core_name0 => "resampler_fifo_generator_i0",
    data_count_width => 8,
    data_width => 44,
    extra_registers => 1,
    has_ae => 0,
    has_af => 0,
    has_rst => false,
    percent_full_width => 1
  )
  port map (
    en => '1',
    rst => '1',
    din => dlycast2_dout_net,
    we => logical1_y_net(0),
    re => logical2_y_net(0),
    clk => clk_net,
    ce => ce_net,
    we_ce => ce_net,
    re_ce => ce_net,
    dout => fifo2_dout_net,
    empty => fifo2_empty_net,
    full => fifo2_full_net,
    dcount => fifo2_dcount_net
  );
  fifo3 : entity xil_defaultlib.resampler_xlfifogen_u 
  generic map (
    core_name0 => "resampler_fifo_generator_i0",
    data_count_width => 8,
    data_width => 44,
    extra_registers => 1,
    has_ae => 0,
    has_af => 0,
    has_rst => false,
    percent_full_width => 1
  )
  port map (
    en => '1',
    rst => '1',
    din => dlycast3_dout_net,
    we => logical1_y_net(0),
    re => logical2_y_net(0),
    clk => clk_net,
    ce => ce_net,
    we_ce => ce_net,
    re_ce => ce_net,
    dout => fifo3_dout_net,
    empty => fifo3_empty_net,
    full => fifo3_full_net,
    dcount => fifo3_dcount_net
  );
  fifo4 : entity xil_defaultlib.resampler_xlfifogen_u 
  generic map (
    core_name0 => "resampler_fifo_generator_i0",
    data_count_width => 8,
    data_width => 44,
    extra_registers => 1,
    has_ae => 0,
    has_af => 0,
    has_rst => false,
    percent_full_width => 1
  )
  port map (
    en => '1',
    rst => '1',
    din => dlycast4_dout_net,
    we => logical1_y_net(0),
    re => logical2_y_net(0),
    clk => clk_net,
    ce => ce_net,
    we_ce => ce_net,
    re_ce => ce_net,
    dout => fifo4_dout_net,
    empty => fifo4_empty_net,
    full => fifo4_full_net,
    dcount => fifo4_dcount_net
  );
  fifo5 : entity xil_defaultlib.resampler_xlfifogen_u 
  generic map (
    core_name0 => "resampler_fifo_generator_i0",
    data_count_width => 8,
    data_width => 44,
    extra_registers => 1,
    has_ae => 0,
    has_af => 0,
    has_rst => false,
    percent_full_width => 1
  )
  port map (
    en => '1',
    rst => '1',
    din => dlycast5_dout_net,
    we => logical1_y_net(0),
    re => logical2_y_net(0),
    clk => clk_net,
    ce => ce_net,
    we_ce => ce_net,
    re_ce => ce_net,
    dout => fifo5_dout_net,
    empty => fifo5_empty_net,
    full => fifo5_full_net,
    dcount => fifo5_dcount_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even0_FP/FIR_Even0_Coeff
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even0_coeff is
  port (
    coeff0 : out std_logic_vector( 27-1 downto 0 );
    coeff1 : out std_logic_vector( 27-1 downto 0 );
    coeff2 : out std_logic_vector( 27-1 downto 0 );
    coeff3 : out std_logic_vector( 27-1 downto 0 );
    coeff4 : out std_logic_vector( 27-1 downto 0 );
    coeff5 : out std_logic_vector( 27-1 downto 0 );
    coeff6 : out std_logic_vector( 27-1 downto 0 );
    coeff7 : out std_logic_vector( 27-1 downto 0 )
  );
end resampler_fir_even0_coeff;
architecture structural of resampler_fir_even0_coeff is 
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
begin
  coeff0 <= constant0_op_net;
  coeff1 <= constant1_op_net;
  coeff2 <= constant2_op_net;
  coeff3 <= constant3_op_net;
  coeff4 <= constant4_op_net;
  coeff5 <= constant5_op_net;
  coeff6 <= constant6_op_net;
  coeff7 <= constant7_op_net;
  constant0 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant2 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant2_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  constant4 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant4_op_net
  );
  constant5 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant5_op_net
  );
  constant6 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant6_op_net
  );
  constant7 : entity xil_defaultlib.sysgen_constant_e4f08cd372 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even0_FP/FIR_Even_Core_FullPipe
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even_core_fullpipe is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    data : in std_logic_vector( 17-1 downto 0 );
    coeff0 : in std_logic_vector( 27-1 downto 0 );
    coeff1 : in std_logic_vector( 27-1 downto 0 );
    coeff2 : in std_logic_vector( 27-1 downto 0 );
    coeff3 : in std_logic_vector( 27-1 downto 0 );
    coeff4 : in std_logic_vector( 27-1 downto 0 );
    coeff5 : in std_logic_vector( 27-1 downto 0 );
    coeff6 : in std_logic_vector( 27-1 downto 0 );
    coeff7 : in std_logic_vector( 27-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_even_core_fullpipe;
architecture structural of resampler_fir_even_core_fullpipe is 
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal tap1_q_net : std_logic_vector( 17-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal tap4_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub3_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 17-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal tap2_q_net : std_logic_vector( 17-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal addsub2_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap11_q_net : std_logic_vector( 17-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal addsub0_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap13_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap12_q_net : std_logic_vector( 17-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal tap14_q_net : std_logic_vector( 17-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal addtree2_s_net : std_logic_vector( 44-1 downto 0 );
  signal tap9_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub6_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap3_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap6_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap8_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub7_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap5_q_net : std_logic_vector( 17-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub4_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap10_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap7_q_net : std_logic_vector( 17-1 downto 0 );
  signal addtree1_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult2_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub5_s_net : std_logic_vector( 17-1 downto 0 );
  signal constant_op_net : std_logic_vector( 17-1 downto 0 );
  signal mult3_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree3_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult6_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult7_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult4_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree4_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree5_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult5_p_net : std_logic_vector( 44-1 downto 0 );
begin
  out_x0 <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= data;
  constant0_op_net <= coeff0;
  constant1_op_net <= coeff1;
  constant2_op_net <= coeff2;
  constant3_op_net <= coeff3;
  constant4_op_net <= coeff4;
  constant5_op_net <= coeff5;
  constant6_op_net <= coeff6;
  constant7_op_net <= coeff7;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addsub0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => reinterpret_output_port_net,
    b => tap14_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub0_s_net
  );
  addsub1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap1_q_net,
    b => tap13_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  addsub2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap4_q_net,
    b => tap12_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub2_s_net
  );
  addsub3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap2_q_net,
    b => tap11_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub3_s_net
  );
  addsub4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap3_q_net,
    b => tap10_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub4_s_net
  );
  addsub5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap5_q_net,
    b => tap9_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub5_s_net
  );
  addsub6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap6_q_net,
    b => tap8_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub6_s_net
  );
  addsub7 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap7_q_net,
    b => constant_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub7_s_net
  );
  addtree0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult0_p_net,
    b => mult1_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree0_s_net
  );
  addtree1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult2_p_net,
    b => mult3_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree1_s_net
  );
  addtree2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult4_p_net,
    b => mult5_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree2_s_net
  );
  addtree3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult6_p_net,
    b => mult7_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree3_s_net
  );
  addtree4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree0_s_net,
    b => addtree1_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree4_s_net
  );
  addtree5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree2_s_net,
    b => addtree3_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree5_s_net
  );
  addtree6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree4_s_net,
    b => addtree5_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree6_s_net
  );
  constant_x0 : entity xil_defaultlib.sysgen_constant_1c20f10c72 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  mult0 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub0_s_net,
    b => constant0_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult0_p_net
  );
  mult1 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub1_s_net,
    b => constant1_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult1_p_net
  );
  mult2 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub2_s_net,
    b => constant2_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult2_p_net
  );
  mult3 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub3_s_net,
    b => constant3_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult3_p_net
  );
  mult4 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub4_s_net,
    b => constant4_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult4_p_net
  );
  mult5 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub5_s_net,
    b => constant5_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult5_p_net
  );
  mult6 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub6_s_net,
    b => constant6_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult6_p_net
  );
  mult7 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub7_s_net,
    b => constant7_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult7_p_net
  );
  tap1 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => reinterpret_output_port_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap1_q_net
  );
  tap10 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap9_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap10_q_net
  );
  tap11 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap10_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap11_q_net
  );
  tap12 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap11_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap12_q_net
  );
  tap13 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap12_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap13_q_net
  );
  tap14 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap13_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap14_q_net
  );
  tap2 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap4_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap2_q_net
  );
  tap3 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap2_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap3_q_net
  );
  tap4 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap1_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap4_q_net
  );
  tap5 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap3_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap5_q_net
  );
  tap6 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap5_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap6_q_net
  );
  tap7 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap6_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap7_q_net
  );
  tap8 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap7_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap8_q_net
  );
  tap9 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap8_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap9_q_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even0_FP
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even0_fp is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    datain : in std_logic_vector( 17-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_even0_fp;
architecture structural of resampler_fir_even0_fp is 
  signal clk_net : std_logic;
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal ce_net : std_logic;
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
begin
  dataout <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= datain;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fir_even0_coeff : entity xil_defaultlib.resampler_fir_even0_coeff 
  port map (
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net
  );
  fir_even_core_fullpipe : entity xil_defaultlib.resampler_fir_even_core_fullpipe 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    data => reinterpret_output_port_net,
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out_x0 => addtree6_s_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even2_FP/FIR_Even2_Coeff
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even2_coeff is
  port (
    coeff0 : out std_logic_vector( 27-1 downto 0 );
    coeff1 : out std_logic_vector( 27-1 downto 0 );
    coeff2 : out std_logic_vector( 27-1 downto 0 );
    coeff3 : out std_logic_vector( 27-1 downto 0 );
    coeff4 : out std_logic_vector( 27-1 downto 0 );
    coeff5 : out std_logic_vector( 27-1 downto 0 );
    coeff6 : out std_logic_vector( 27-1 downto 0 );
    coeff7 : out std_logic_vector( 27-1 downto 0 )
  );
end resampler_fir_even2_coeff;
architecture structural of resampler_fir_even2_coeff is 
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
begin
  coeff0 <= constant0_op_net;
  coeff1 <= constant1_op_net;
  coeff2 <= constant2_op_net;
  coeff3 <= constant3_op_net;
  coeff4 <= constant4_op_net;
  coeff5 <= constant5_op_net;
  coeff6 <= constant6_op_net;
  coeff7 <= constant7_op_net;
  constant0 : entity xil_defaultlib.sysgen_constant_3d0eda5bb9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_15df27d5ad 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant2 : entity xil_defaultlib.sysgen_constant_1b7694dd33 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant2_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_5ed8fe4930 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  constant4 : entity xil_defaultlib.sysgen_constant_e45f78a27c 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant4_op_net
  );
  constant5 : entity xil_defaultlib.sysgen_constant_9c8dfdbba6 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant5_op_net
  );
  constant6 : entity xil_defaultlib.sysgen_constant_70aa9de4a3 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant6_op_net
  );
  constant7 : entity xil_defaultlib.sysgen_constant_2e837937c8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even2_FP/FIR_Even_Core_FullPipe
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even_core_fullpipe_x0 is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    data : in std_logic_vector( 17-1 downto 0 );
    coeff0 : in std_logic_vector( 27-1 downto 0 );
    coeff1 : in std_logic_vector( 27-1 downto 0 );
    coeff2 : in std_logic_vector( 27-1 downto 0 );
    coeff3 : in std_logic_vector( 27-1 downto 0 );
    coeff4 : in std_logic_vector( 27-1 downto 0 );
    coeff5 : in std_logic_vector( 27-1 downto 0 );
    coeff6 : in std_logic_vector( 27-1 downto 0 );
    coeff7 : in std_logic_vector( 27-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_even_core_fullpipe_x0;
architecture structural of resampler_fir_even_core_fullpipe_x0 is 
  signal tap6_q_net : std_logic_vector( 17-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult2_p_net : std_logic_vector( 44-1 downto 0 );
  signal tap8_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub7_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap5_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub6_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap7_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap3_q_net : std_logic_vector( 17-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree1_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult3_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub4_s_net : std_logic_vector( 17-1 downto 0 );
  signal constant_op_net : std_logic_vector( 17-1 downto 0 );
  signal addsub5_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap10_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap9_q_net : std_logic_vector( 17-1 downto 0 );
  signal addtree5_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult4_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree3_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree4_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult7_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult5_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult6_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree2_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal addsub2_s_net : std_logic_vector( 17-1 downto 0 );
  signal ce_net : std_logic;
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal addsub0_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 17-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal tap14_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap12_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap4_q_net : std_logic_vector( 17-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal addsub3_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap2_q_net : std_logic_vector( 17-1 downto 0 );
  signal clk_net : std_logic;
  signal tap11_q_net : std_logic_vector( 17-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal tap1_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap13_q_net : std_logic_vector( 17-1 downto 0 );
begin
  out_x0 <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= data;
  constant0_op_net <= coeff0;
  constant1_op_net <= coeff1;
  constant2_op_net <= coeff2;
  constant3_op_net <= coeff3;
  constant4_op_net <= coeff4;
  constant5_op_net <= coeff5;
  constant6_op_net <= coeff6;
  constant7_op_net <= coeff7;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addsub0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => reinterpret_output_port_net,
    b => tap14_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub0_s_net
  );
  addsub1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap1_q_net,
    b => tap13_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  addsub2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap4_q_net,
    b => tap12_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub2_s_net
  );
  addsub3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap2_q_net,
    b => tap11_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub3_s_net
  );
  addsub4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap3_q_net,
    b => tap10_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub4_s_net
  );
  addsub5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap5_q_net,
    b => tap9_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub5_s_net
  );
  addsub6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap6_q_net,
    b => tap8_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub6_s_net
  );
  addsub7 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap7_q_net,
    b => constant_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub7_s_net
  );
  addtree0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult0_p_net,
    b => mult1_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree0_s_net
  );
  addtree1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult2_p_net,
    b => mult3_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree1_s_net
  );
  addtree2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult4_p_net,
    b => mult5_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree2_s_net
  );
  addtree3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult6_p_net,
    b => mult7_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree3_s_net
  );
  addtree4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree0_s_net,
    b => addtree1_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree4_s_net
  );
  addtree5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree2_s_net,
    b => addtree3_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree5_s_net
  );
  addtree6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree4_s_net,
    b => addtree5_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree6_s_net
  );
  constant_x0 : entity xil_defaultlib.sysgen_constant_1c20f10c72 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  mult0 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub0_s_net,
    b => constant0_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult0_p_net
  );
  mult1 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub1_s_net,
    b => constant1_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult1_p_net
  );
  mult2 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub2_s_net,
    b => constant2_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult2_p_net
  );
  mult3 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub3_s_net,
    b => constant3_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult3_p_net
  );
  mult4 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub4_s_net,
    b => constant4_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult4_p_net
  );
  mult5 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub5_s_net,
    b => constant5_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult5_p_net
  );
  mult6 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub6_s_net,
    b => constant6_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult6_p_net
  );
  mult7 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub7_s_net,
    b => constant7_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult7_p_net
  );
  tap1 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => reinterpret_output_port_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap1_q_net
  );
  tap10 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap9_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap10_q_net
  );
  tap11 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap10_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap11_q_net
  );
  tap12 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap11_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap12_q_net
  );
  tap13 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap12_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap13_q_net
  );
  tap14 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap13_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap14_q_net
  );
  tap2 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap4_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap2_q_net
  );
  tap3 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap2_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap3_q_net
  );
  tap4 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap1_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap4_q_net
  );
  tap5 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap3_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap5_q_net
  );
  tap6 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap5_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap6_q_net
  );
  tap7 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap6_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap7_q_net
  );
  tap8 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap7_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap8_q_net
  );
  tap9 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap8_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap9_q_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even2_FP
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even2_fp is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    datain : in std_logic_vector( 17-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_even2_fp;
architecture structural of resampler_fir_even2_fp is 
  signal ce_net : std_logic;
  signal clk_net : std_logic;
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
begin
  dataout <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= datain;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fir_even2_coeff : entity xil_defaultlib.resampler_fir_even2_coeff 
  port map (
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net
  );
  fir_even_core_fullpipe : entity xil_defaultlib.resampler_fir_even_core_fullpipe_x0 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    data => reinterpret_output_port_net,
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out_x0 => addtree6_s_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even4_FP/FIR_Even4_Coeff
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even4_coeff is
  port (
    coeff0 : out std_logic_vector( 27-1 downto 0 );
    coeff1 : out std_logic_vector( 27-1 downto 0 );
    coeff2 : out std_logic_vector( 27-1 downto 0 );
    coeff3 : out std_logic_vector( 27-1 downto 0 );
    coeff4 : out std_logic_vector( 27-1 downto 0 );
    coeff5 : out std_logic_vector( 27-1 downto 0 );
    coeff6 : out std_logic_vector( 27-1 downto 0 );
    coeff7 : out std_logic_vector( 27-1 downto 0 )
  );
end resampler_fir_even4_coeff;
architecture structural of resampler_fir_even4_coeff is 
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
begin
  coeff0 <= constant0_op_net;
  coeff1 <= constant1_op_net;
  coeff2 <= constant2_op_net;
  coeff3 <= constant3_op_net;
  coeff4 <= constant4_op_net;
  coeff5 <= constant5_op_net;
  coeff6 <= constant6_op_net;
  coeff7 <= constant7_op_net;
  constant0 : entity xil_defaultlib.sysgen_constant_b28213583e 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_46f38e8fa5 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant2 : entity xil_defaultlib.sysgen_constant_27c7013ab2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant2_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_de70bacc42 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  constant4 : entity xil_defaultlib.sysgen_constant_1f7dbeddc3 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant4_op_net
  );
  constant5 : entity xil_defaultlib.sysgen_constant_35f261146a 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant5_op_net
  );
  constant6 : entity xil_defaultlib.sysgen_constant_805f03b38e 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant6_op_net
  );
  constant7 : entity xil_defaultlib.sysgen_constant_a454c59521 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even4_FP/FIR_Even_Core_FullPipe
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even_core_fullpipe_x1 is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    data : in std_logic_vector( 17-1 downto 0 );
    coeff0 : in std_logic_vector( 27-1 downto 0 );
    coeff1 : in std_logic_vector( 27-1 downto 0 );
    coeff2 : in std_logic_vector( 27-1 downto 0 );
    coeff3 : in std_logic_vector( 27-1 downto 0 );
    coeff4 : in std_logic_vector( 27-1 downto 0 );
    coeff5 : in std_logic_vector( 27-1 downto 0 );
    coeff6 : in std_logic_vector( 27-1 downto 0 );
    coeff7 : in std_logic_vector( 27-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_even_core_fullpipe_x1;
architecture structural of resampler_fir_even_core_fullpipe_x1 is 
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal tap10_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap12_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub5_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap13_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap11_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub6_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap6_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap8_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap2_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap14_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap9_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap3_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap5_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap4_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap1_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub2_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub0_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub3_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub4_s_net : std_logic_vector( 17-1 downto 0 );
  signal mult2_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree2_s_net : std_logic_vector( 44-1 downto 0 );
  signal tap7_q_net : std_logic_vector( 17-1 downto 0 );
  signal mult6_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult5_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree1_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant_op_net : std_logic_vector( 17-1 downto 0 );
  signal mult3_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree3_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult4_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult7_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub7_s_net : std_logic_vector( 17-1 downto 0 );
  signal addtree4_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree5_s_net : std_logic_vector( 44-1 downto 0 );
begin
  out_x0 <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= data;
  constant0_op_net <= coeff0;
  constant1_op_net <= coeff1;
  constant2_op_net <= coeff2;
  constant3_op_net <= coeff3;
  constant4_op_net <= coeff4;
  constant5_op_net <= coeff5;
  constant6_op_net <= coeff6;
  constant7_op_net <= coeff7;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addsub0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => reinterpret_output_port_net,
    b => tap14_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub0_s_net
  );
  addsub1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap1_q_net,
    b => tap13_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  addsub2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap4_q_net,
    b => tap12_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub2_s_net
  );
  addsub3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap2_q_net,
    b => tap11_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub3_s_net
  );
  addsub4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap3_q_net,
    b => tap10_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub4_s_net
  );
  addsub5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap5_q_net,
    b => tap9_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub5_s_net
  );
  addsub6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap6_q_net,
    b => tap8_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub6_s_net
  );
  addsub7 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i2",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap7_q_net,
    b => constant_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub7_s_net
  );
  addtree0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult0_p_net,
    b => mult1_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree0_s_net
  );
  addtree1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult2_p_net,
    b => mult3_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree1_s_net
  );
  addtree2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult4_p_net,
    b => mult5_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree2_s_net
  );
  addtree3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult6_p_net,
    b => mult7_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree3_s_net
  );
  addtree4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree0_s_net,
    b => addtree1_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree4_s_net
  );
  addtree5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree2_s_net,
    b => addtree3_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree5_s_net
  );
  addtree6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree4_s_net,
    b => addtree5_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree6_s_net
  );
  constant_x0 : entity xil_defaultlib.sysgen_constant_1c20f10c72 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  mult0 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub0_s_net,
    b => constant0_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult0_p_net
  );
  mult1 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub1_s_net,
    b => constant1_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult1_p_net
  );
  mult2 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub2_s_net,
    b => constant2_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult2_p_net
  );
  mult3 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub3_s_net,
    b => constant3_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult3_p_net
  );
  mult4 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub4_s_net,
    b => constant4_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult4_p_net
  );
  mult5 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub5_s_net,
    b => constant5_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult5_p_net
  );
  mult6 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub6_s_net,
    b => constant6_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult6_p_net
  );
  mult7 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub7_s_net,
    b => constant7_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult7_p_net
  );
  tap1 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => reinterpret_output_port_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap1_q_net
  );
  tap10 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap9_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap10_q_net
  );
  tap11 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap10_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap11_q_net
  );
  tap12 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap11_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap12_q_net
  );
  tap13 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap12_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap13_q_net
  );
  tap14 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap13_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap14_q_net
  );
  tap2 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap4_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap2_q_net
  );
  tap3 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap2_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap3_q_net
  );
  tap4 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap1_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap4_q_net
  );
  tap5 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap3_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap5_q_net
  );
  tap6 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap5_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap6_q_net
  );
  tap7 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap6_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap7_q_net
  );
  tap8 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap7_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap8_q_net
  );
  tap9 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap8_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap9_q_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Even4_FP
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_even4_fp is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    datain : in std_logic_vector( 17-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_even4_fp;
architecture structural of resampler_fir_even4_fp is 
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal clk_net : std_logic;
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal ce_net : std_logic;
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
begin
  dataout <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= datain;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fir_even4_coeff : entity xil_defaultlib.resampler_fir_even4_coeff 
  port map (
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net
  );
  fir_even_core_fullpipe : entity xil_defaultlib.resampler_fir_even_core_fullpipe_x1 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    data => reinterpret_output_port_net,
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out_x0 => addtree6_s_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd1_FP/FIR_Odd1_Coeff
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd1_coeff is
  port (
    coeff0 : out std_logic_vector( 27-1 downto 0 );
    coeff1 : out std_logic_vector( 27-1 downto 0 );
    coeff2 : out std_logic_vector( 27-1 downto 0 );
    coeff3 : out std_logic_vector( 27-1 downto 0 );
    coeff4 : out std_logic_vector( 27-1 downto 0 );
    coeff5 : out std_logic_vector( 27-1 downto 0 );
    coeff6 : out std_logic_vector( 27-1 downto 0 );
    coeff7 : out std_logic_vector( 27-1 downto 0 )
  );
end resampler_fir_odd1_coeff;
architecture structural of resampler_fir_odd1_coeff is 
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant8_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
begin
  coeff0 <= constant0_op_net;
  coeff1 <= constant1_op_net;
  coeff2 <= constant2_op_net;
  coeff3 <= constant4_op_net;
  coeff4 <= constant5_op_net;
  coeff5 <= constant6_op_net;
  coeff6 <= constant7_op_net;
  coeff7 <= constant8_op_net;
  constant0 : entity xil_defaultlib.sysgen_constant_9fd5968d9b 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_b575f8b494 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant2 : entity xil_defaultlib.sysgen_constant_1feba3d02c 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant2_op_net
  );
  constant4 : entity xil_defaultlib.sysgen_constant_246104e666 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant4_op_net
  );
  constant5 : entity xil_defaultlib.sysgen_constant_504d725421 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant5_op_net
  );
  constant6 : entity xil_defaultlib.sysgen_constant_dad3dd0c31 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant6_op_net
  );
  constant7 : entity xil_defaultlib.sysgen_constant_eb2f31b720 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
  constant8 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant8_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd1_FP/FIR_Odd_Core_FullPipe
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd_core_fullpipe is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    data : in std_logic_vector( 17-1 downto 0 );
    coeff0 : in std_logic_vector( 27-1 downto 0 );
    coeff1 : in std_logic_vector( 27-1 downto 0 );
    coeff2 : in std_logic_vector( 27-1 downto 0 );
    coeff3 : in std_logic_vector( 27-1 downto 0 );
    coeff4 : in std_logic_vector( 27-1 downto 0 );
    coeff5 : in std_logic_vector( 27-1 downto 0 );
    coeff6 : in std_logic_vector( 27-1 downto 0 );
    coeff7 : in std_logic_vector( 27-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_odd_core_fullpipe;
architecture structural of resampler_fir_odd_core_fullpipe is 
  signal clk_net : std_logic;
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant8_op_net : std_logic_vector( 27-1 downto 0 );
  signal addsub3_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap10_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub0_s_net : std_logic_vector( 17-1 downto 0 );
  signal ce_net : std_logic;
  signal tap2_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap14_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap3_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap12_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap11_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub4_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub2_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap4_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap1_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap13_q_net : std_logic_vector( 17-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub7_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap7_q_net : std_logic_vector( 17-1 downto 0 );
  signal mult5_p_net : std_logic_vector( 44-1 downto 0 );
  signal tap5_q_net : std_logic_vector( 17-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree1_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult3_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree2_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult2_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult6_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult7_p_net : std_logic_vector( 44-1 downto 0 );
  signal tap8_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap6_q_net : std_logic_vector( 17-1 downto 0 );
  signal constant_op_net : std_logic_vector( 17-1 downto 0 );
  signal addsub5_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap9_q_net : std_logic_vector( 17-1 downto 0 );
  signal mult4_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub6_s_net : std_logic_vector( 17-1 downto 0 );
  signal addtree3_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree5_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree4_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
begin
  out_x0 <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= data;
  constant0_op_net <= coeff0;
  constant1_op_net <= coeff1;
  constant2_op_net <= coeff2;
  constant4_op_net <= coeff3;
  constant5_op_net <= coeff4;
  constant6_op_net <= coeff5;
  constant7_op_net <= coeff6;
  constant8_op_net <= coeff7;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addsub0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => reinterpret_output_port_net,
    b => tap14_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub0_s_net
  );
  addsub1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap1_q_net,
    b => tap13_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  addsub2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap4_q_net,
    b => tap12_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub2_s_net
  );
  addsub3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap2_q_net,
    b => tap11_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub3_s_net
  );
  addsub4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap3_q_net,
    b => tap10_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub4_s_net
  );
  addsub5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap5_q_net,
    b => tap9_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub5_s_net
  );
  addsub6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap6_q_net,
    b => tap8_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub6_s_net
  );
  addsub7 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap7_q_net,
    b => constant_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub7_s_net
  );
  addtree0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult0_p_net,
    b => mult1_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree0_s_net
  );
  addtree1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult2_p_net,
    b => mult3_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree1_s_net
  );
  addtree2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult4_p_net,
    b => mult5_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree2_s_net
  );
  addtree3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult6_p_net,
    b => mult7_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree3_s_net
  );
  addtree4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree0_s_net,
    b => addtree1_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree4_s_net
  );
  addtree5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree2_s_net,
    b => addtree3_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree5_s_net
  );
  addtree6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree4_s_net,
    b => addtree5_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree6_s_net
  );
  constant_x0 : entity xil_defaultlib.sysgen_constant_1c20f10c72 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  mult0 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub0_s_net,
    b => constant0_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult0_p_net
  );
  mult1 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub1_s_net,
    b => constant1_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult1_p_net
  );
  mult2 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub2_s_net,
    b => constant2_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult2_p_net
  );
  mult3 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub3_s_net,
    b => constant4_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult3_p_net
  );
  mult4 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub4_s_net,
    b => constant5_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult4_p_net
  );
  mult5 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub5_s_net,
    b => constant6_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult5_p_net
  );
  mult6 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub6_s_net,
    b => constant7_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult6_p_net
  );
  mult7 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub7_s_net,
    b => constant8_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult7_p_net
  );
  tap1 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => reinterpret_output_port_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap1_q_net
  );
  tap10 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap9_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap10_q_net
  );
  tap11 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap10_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap11_q_net
  );
  tap12 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap11_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap12_q_net
  );
  tap13 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap12_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap13_q_net
  );
  tap14 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap13_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap14_q_net
  );
  tap2 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap4_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap2_q_net
  );
  tap3 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap2_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap3_q_net
  );
  tap4 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap1_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap4_q_net
  );
  tap5 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap3_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap5_q_net
  );
  tap6 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap5_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap6_q_net
  );
  tap7 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap6_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap7_q_net
  );
  tap8 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap7_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap8_q_net
  );
  tap9 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap8_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap9_q_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd1_FP
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd1_fp is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    datain : in std_logic_vector( 17-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_odd1_fp;
architecture structural of resampler_fir_odd1_fp is 
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal constant8_op_net : std_logic_vector( 27-1 downto 0 );
  signal ce_net : std_logic;
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
begin
  dataout <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= datain;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fir_odd1_coeff : entity xil_defaultlib.resampler_fir_odd1_coeff 
  port map (
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant4_op_net,
    coeff4 => constant5_op_net,
    coeff5 => constant6_op_net,
    coeff6 => constant7_op_net,
    coeff7 => constant8_op_net
  );
  fir_odd_core_fullpipe : entity xil_defaultlib.resampler_fir_odd_core_fullpipe 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    data => reinterpret_output_port_net,
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant4_op_net,
    coeff4 => constant5_op_net,
    coeff5 => constant6_op_net,
    coeff6 => constant7_op_net,
    coeff7 => constant8_op_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out_x0 => addtree6_s_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd3_FP/FIR_Odd3_Coeff
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd3_coeff is
  port (
    coeff0 : out std_logic_vector( 27-1 downto 0 );
    coeff1 : out std_logic_vector( 27-1 downto 0 );
    coeff2 : out std_logic_vector( 27-1 downto 0 );
    coeff3 : out std_logic_vector( 27-1 downto 0 );
    coeff4 : out std_logic_vector( 27-1 downto 0 );
    coeff5 : out std_logic_vector( 27-1 downto 0 );
    coeff6 : out std_logic_vector( 27-1 downto 0 );
    coeff7 : out std_logic_vector( 27-1 downto 0 )
  );
end resampler_fir_odd3_coeff;
architecture structural of resampler_fir_odd3_coeff is 
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
begin
  coeff0 <= constant0_op_net;
  coeff1 <= constant1_op_net;
  coeff2 <= constant2_op_net;
  coeff3 <= constant3_op_net;
  coeff4 <= constant4_op_net;
  coeff5 <= constant5_op_net;
  coeff6 <= constant6_op_net;
  coeff7 <= constant7_op_net;
  constant0 : entity xil_defaultlib.sysgen_constant_d9348b126b 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_8be66ec32e 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant2 : entity xil_defaultlib.sysgen_constant_4f2b0c986b 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant2_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_45a7d6f74a 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  constant4 : entity xil_defaultlib.sysgen_constant_4a2055cf9f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant4_op_net
  );
  constant5 : entity xil_defaultlib.sysgen_constant_a5322a5e1d 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant5_op_net
  );
  constant6 : entity xil_defaultlib.sysgen_constant_933c0ddd93 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant6_op_net
  );
  constant7 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd3_FP/FIR_Odd_Core_FullPipe
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd_core_fullpipe_x0 is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    data : in std_logic_vector( 17-1 downto 0 );
    coeff0 : in std_logic_vector( 27-1 downto 0 );
    coeff1 : in std_logic_vector( 27-1 downto 0 );
    coeff2 : in std_logic_vector( 27-1 downto 0 );
    coeff3 : in std_logic_vector( 27-1 downto 0 );
    coeff4 : in std_logic_vector( 27-1 downto 0 );
    coeff5 : in std_logic_vector( 27-1 downto 0 );
    coeff6 : in std_logic_vector( 27-1 downto 0 );
    coeff7 : in std_logic_vector( 27-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_odd_core_fullpipe_x0;
architecture structural of resampler_fir_odd_core_fullpipe_x0 is 
  signal addsub3_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap2_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap10_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap12_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap14_q_net : std_logic_vector( 17-1 downto 0 );
  signal ce_net : std_logic;
  signal tap11_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap4_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap13_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub5_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap5_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap9_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap1_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub0_s_net : std_logic_vector( 17-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal addsub4_s_net : std_logic_vector( 17-1 downto 0 );
  signal addsub2_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap3_q_net : std_logic_vector( 17-1 downto 0 );
  signal clk_net : std_logic;
  signal tap7_q_net : std_logic_vector( 17-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult2_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub6_s_net : std_logic_vector( 17-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult5_p_net : std_logic_vector( 44-1 downto 0 );
  signal constant_op_net : std_logic_vector( 17-1 downto 0 );
  signal addtree3_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult6_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult3_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree2_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult4_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult7_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree1_s_net : std_logic_vector( 44-1 downto 0 );
  signal tap6_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap8_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub7_s_net : std_logic_vector( 17-1 downto 0 );
  signal addtree5_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree4_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
begin
  out_x0 <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= data;
  constant0_op_net <= coeff0;
  constant1_op_net <= coeff1;
  constant2_op_net <= coeff2;
  constant3_op_net <= coeff3;
  constant4_op_net <= coeff4;
  constant5_op_net <= coeff5;
  constant6_op_net <= coeff6;
  constant7_op_net <= coeff7;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addsub0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => reinterpret_output_port_net,
    b => tap14_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub0_s_net
  );
  addsub1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap1_q_net,
    b => tap13_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  addsub2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap4_q_net,
    b => tap12_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub2_s_net
  );
  addsub3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap2_q_net,
    b => tap11_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub3_s_net
  );
  addsub4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap3_q_net,
    b => tap10_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub4_s_net
  );
  addsub5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap5_q_net,
    b => tap9_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub5_s_net
  );
  addsub6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap6_q_net,
    b => tap8_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub6_s_net
  );
  addsub7 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap7_q_net,
    b => constant_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub7_s_net
  );
  addtree0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult0_p_net,
    b => mult1_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree0_s_net
  );
  addtree1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult2_p_net,
    b => mult3_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree1_s_net
  );
  addtree2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult4_p_net,
    b => mult5_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree2_s_net
  );
  addtree3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult6_p_net,
    b => mult7_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree3_s_net
  );
  addtree4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree0_s_net,
    b => addtree1_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree4_s_net
  );
  addtree5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree2_s_net,
    b => addtree3_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree5_s_net
  );
  addtree6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree4_s_net,
    b => addtree5_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree6_s_net
  );
  constant_x0 : entity xil_defaultlib.sysgen_constant_1c20f10c72 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  mult0 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub0_s_net,
    b => constant0_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult0_p_net
  );
  mult1 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub1_s_net,
    b => constant1_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult1_p_net
  );
  mult2 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub2_s_net,
    b => constant2_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult2_p_net
  );
  mult3 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub3_s_net,
    b => constant3_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult3_p_net
  );
  mult4 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub4_s_net,
    b => constant4_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult4_p_net
  );
  mult5 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub5_s_net,
    b => constant5_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult5_p_net
  );
  mult6 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub6_s_net,
    b => constant6_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult6_p_net
  );
  mult7 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub7_s_net,
    b => constant7_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult7_p_net
  );
  tap1 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => reinterpret_output_port_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap1_q_net
  );
  tap10 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap9_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap10_q_net
  );
  tap11 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap10_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap11_q_net
  );
  tap12 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap11_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap12_q_net
  );
  tap13 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap12_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap13_q_net
  );
  tap14 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap13_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap14_q_net
  );
  tap2 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap4_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap2_q_net
  );
  tap3 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap2_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap3_q_net
  );
  tap4 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap1_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap4_q_net
  );
  tap5 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap3_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap5_q_net
  );
  tap6 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap5_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap6_q_net
  );
  tap7 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap6_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap7_q_net
  );
  tap8 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap7_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap8_q_net
  );
  tap9 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap8_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap9_q_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd3_FP
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd3_fp is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    datain : in std_logic_vector( 17-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_odd3_fp;
architecture structural of resampler_fir_odd3_fp is 
  signal ce_net : std_logic;
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal clk_net : std_logic;
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
begin
  dataout <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= datain;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fir_odd3_coeff : entity xil_defaultlib.resampler_fir_odd3_coeff 
  port map (
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net
  );
  fir_odd_core_fullpipe : entity xil_defaultlib.resampler_fir_odd_core_fullpipe_x0 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    data => reinterpret_output_port_net,
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out_x0 => addtree6_s_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd5_FP/FIR_Odd5_Coeff
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd5_coeff is
  port (
    coeff0 : out std_logic_vector( 27-1 downto 0 );
    coeff1 : out std_logic_vector( 27-1 downto 0 );
    coeff2 : out std_logic_vector( 27-1 downto 0 );
    coeff3 : out std_logic_vector( 27-1 downto 0 );
    coeff4 : out std_logic_vector( 27-1 downto 0 );
    coeff5 : out std_logic_vector( 27-1 downto 0 );
    coeff6 : out std_logic_vector( 27-1 downto 0 );
    coeff7 : out std_logic_vector( 27-1 downto 0 )
  );
end resampler_fir_odd5_coeff;
architecture structural of resampler_fir_odd5_coeff is 
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
begin
  coeff0 <= constant0_op_net;
  coeff1 <= constant1_op_net;
  coeff2 <= constant2_op_net;
  coeff3 <= constant3_op_net;
  coeff4 <= constant4_op_net;
  coeff5 <= constant5_op_net;
  coeff6 <= constant6_op_net;
  coeff7 <= constant7_op_net;
  constant0 : entity xil_defaultlib.sysgen_constant_10f6429a43 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_6089bb8102 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant2 : entity xil_defaultlib.sysgen_constant_66f5b97168 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant2_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_3a459fcf31 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  constant4 : entity xil_defaultlib.sysgen_constant_8bf26556ab 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant4_op_net
  );
  constant5 : entity xil_defaultlib.sysgen_constant_19d465b51c 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant5_op_net
  );
  constant6 : entity xil_defaultlib.sysgen_constant_1d1a433e47 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant6_op_net
  );
  constant7 : entity xil_defaultlib.sysgen_constant_fd0b169856 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd5_FP/FIR_Odd_Core_FullPipe
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd_core_fullpipe_x1 is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    data : in std_logic_vector( 17-1 downto 0 );
    coeff0 : in std_logic_vector( 27-1 downto 0 );
    coeff1 : in std_logic_vector( 27-1 downto 0 );
    coeff2 : in std_logic_vector( 27-1 downto 0 );
    coeff3 : in std_logic_vector( 27-1 downto 0 );
    coeff4 : in std_logic_vector( 27-1 downto 0 );
    coeff5 : in std_logic_vector( 27-1 downto 0 );
    coeff6 : in std_logic_vector( 27-1 downto 0 );
    coeff7 : in std_logic_vector( 27-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_odd_core_fullpipe_x1;
architecture structural of resampler_fir_odd_core_fullpipe_x1 is 
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal tap14_q_net : std_logic_vector( 17-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal addsub0_s_net : std_logic_vector( 17-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal tap10_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub5_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap6_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub3_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap5_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap2_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub6_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap4_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub2_s_net : std_logic_vector( 17-1 downto 0 );
  signal tap13_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap9_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap8_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap3_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap1_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap12_q_net : std_logic_vector( 17-1 downto 0 );
  signal tap11_q_net : std_logic_vector( 17-1 downto 0 );
  signal addsub4_s_net : std_logic_vector( 17-1 downto 0 );
  signal mult3_p_net : std_logic_vector( 44-1 downto 0 );
  signal addsub7_s_net : std_logic_vector( 17-1 downto 0 );
  signal mult6_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult5_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 44-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 44-1 downto 0 );
  signal tap7_q_net : std_logic_vector( 17-1 downto 0 );
  signal addtree4_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree5_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree2_s_net : std_logic_vector( 44-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult2_p_net : std_logic_vector( 44-1 downto 0 );
  signal addtree3_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult7_p_net : std_logic_vector( 44-1 downto 0 );
  signal constant_op_net : std_logic_vector( 17-1 downto 0 );
  signal addtree1_s_net : std_logic_vector( 44-1 downto 0 );
  signal mult4_p_net : std_logic_vector( 44-1 downto 0 );
begin
  out_x0 <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= data;
  constant0_op_net <= coeff0;
  constant1_op_net <= coeff1;
  constant2_op_net <= coeff2;
  constant3_op_net <= coeff3;
  constant4_op_net <= coeff4;
  constant5_op_net <= coeff5;
  constant6_op_net <= coeff6;
  constant7_op_net <= coeff7;
  clk_net <= clk_1;
  ce_net <= ce_1;
  addsub0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => reinterpret_output_port_net,
    b => tap14_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub0_s_net
  );
  addsub1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap1_q_net,
    b => tap13_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  addsub2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap4_q_net,
    b => tap12_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub2_s_net
  );
  addsub3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap2_q_net,
    b => tap11_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub3_s_net
  );
  addsub4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap3_q_net,
    b => tap10_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub4_s_net
  );
  addsub5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap5_q_net,
    b => tap9_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub5_s_net
  );
  addsub6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap6_q_net,
    b => tap8_q_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub6_s_net
  );
  addsub7 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 16,
    b_width => 17,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 18,
    core_name0 => "resampler_c_addsub_v12_0_i4",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 18,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 16,
    s_width => 17
  )
  port map (
    clr => '0',
    a => tap7_q_net,
    b => constant_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub7_s_net
  );
  addtree0 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult0_p_net,
    b => mult1_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree0_s_net
  );
  addtree1 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult2_p_net,
    b => mult3_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree1_s_net
  );
  addtree2 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult4_p_net,
    b => mult5_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree2_s_net
  );
  addtree3 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => mult6_p_net,
    b => mult7_p_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree3_s_net
  );
  addtree4 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree0_s_net,
    b => addtree1_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree4_s_net
  );
  addtree5 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree2_s_net,
    b => addtree3_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree5_s_net
  );
  addtree6 : entity xil_defaultlib.resampler_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 43,
    b_width => 44,
    c_has_c_out => 0,
    c_latency => 1,
    c_output_width => 45,
    core_name0 => "resampler_c_addsub_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 45,
    latency => 1,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 43,
    s_width => 44
  )
  port map (
    clr => '0',
    a => addtree4_s_net,
    b => addtree5_s_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    s => addtree6_s_net
  );
  constant_x0 : entity xil_defaultlib.sysgen_constant_1c20f10c72 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  mult0 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub0_s_net,
    b => constant0_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult0_p_net
  );
  mult1 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub1_s_net,
    b => constant1_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult1_p_net
  );
  mult2 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub2_s_net,
    b => constant2_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult2_p_net
  );
  mult3 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub3_s_net,
    b => constant3_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult3_p_net
  );
  mult4 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub4_s_net,
    b => constant4_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult4_p_net
  );
  mult5 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub5_s_net,
    b => constant5_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult5_p_net
  );
  mult6 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub6_s_net,
    b => constant6_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult6_p_net
  );
  mult7 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 16,
    a_width => 17,
    b_arith => xlSigned,
    b_bin_pt => 26,
    b_width => 27,
    c_a_type => 0,
    c_a_width => 17,
    c_b_type => 0,
    c_b_width => 27,
    c_baat => 17,
    c_output_width => 44,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i3",
    en_arith => xlUnsigned,
    en_bin_pt => 0,
    en_width => 1,
    extra_registers => 0,
    multsign => 2,
    overflow => 1,
    p_arith => xlSigned,
    p_bin_pt => 43,
    p_width => 44,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    rst => "0",
    a => addsub7_s_net,
    b => constant7_op_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult7_p_net
  );
  tap1 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => reinterpret_output_port_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap1_q_net
  );
  tap10 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap9_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap10_q_net
  );
  tap11 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap10_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap11_q_net
  );
  tap12 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap11_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap12_q_net
  );
  tap13 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap12_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap13_q_net
  );
  tap14 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap13_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap14_q_net
  );
  tap2 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap4_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap2_q_net
  );
  tap3 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap2_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap3_q_net
  );
  tap4 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap1_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap4_q_net
  );
  tap5 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap3_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap5_q_net
  );
  tap6 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap5_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap6_q_net
  );
  tap7 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap6_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap7_q_net
  );
  tap8 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap7_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap8_q_net
  );
  tap9 : entity xil_defaultlib.resampler_xlregister 
  generic map (
    d_width => 17,
    init_value => b"00000000000000000"
  )
  port map (
    d => tap8_q_net,
    rst => rst_net,
    en => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => tap9_q_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay/FIR_Odd5_FP
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_fir_odd5_fp is
  port (
    rst : in std_logic_vector( 1-1 downto 0 );
    enb : in std_logic_vector( 1-1 downto 0 );
    datain : in std_logic_vector( 17-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 )
  );
end resampler_fir_odd5_fp;
architecture structural of resampler_fir_odd5_fp is 
  signal constant0_op_net : std_logic_vector( 27-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal constant4_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant6_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 27-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 27-1 downto 0 );
begin
  dataout <= addtree6_s_net;
  rst_net <= rst;
  delaypipesl5_q_net <= enb;
  reinterpret_output_port_net <= datain;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fir_odd5_coeff : entity xil_defaultlib.resampler_fir_odd5_coeff 
  port map (
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net
  );
  fir_odd_core_fullpipe : entity xil_defaultlib.resampler_fir_odd_core_fullpipe_x1 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    data => reinterpret_output_port_net,
    coeff0 => constant0_op_net,
    coeff1 => constant1_op_net,
    coeff2 => constant2_op_net,
    coeff3 => constant3_op_net,
    coeff4 => constant4_op_net,
    coeff5 => constant5_op_net,
    coeff6 => constant6_op_net,
    coeff7 => constant7_op_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out_x0 => addtree6_s_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl/VFD_FunctionalModel_NoDelay
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_vfd_functionalmodel_nodelay is
  port (
    datain : in std_logic_vector( 16-1 downto 0 );
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    fir0 : out std_logic_vector( 44-1 downto 0 );
    fir1 : out std_logic_vector( 44-1 downto 0 );
    fir2 : out std_logic_vector( 44-1 downto 0 );
    fir3 : out std_logic_vector( 44-1 downto 0 );
    fir4 : out std_logic_vector( 44-1 downto 0 );
    fir5 : out std_logic_vector( 44-1 downto 0 );
    gateway_outi1 : out std_logic_vector( 17-1 downto 0 );
    gateway_outi2 : out std_logic_vector( 17-1 downto 0 )
  );
end resampler_vfd_functionalmodel_nodelay;
architecture structural of resampler_vfd_functionalmodel_nodelay is 
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal addtree6_s_net_x2 : std_logic_vector( 44-1 downto 0 );
  signal convert_dout_net : std_logic_vector( 17-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal addtree6_s_net_x4 : std_logic_vector( 44-1 downto 0 );
  signal addtree6_s_net_x3 : std_logic_vector( 44-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 4-1 downto 0 );
  signal delaypipesl4_q_net : std_logic_vector( 16-1 downto 0 );
  signal ce_net : std_logic;
  signal addtree6_s_net_x0 : std_logic_vector( 44-1 downto 0 );
  signal clk_net : std_logic;
  signal addtree6_s_net_x1 : std_logic_vector( 44-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 17-1 downto 0 );
begin
  fir0 <= addtree6_s_net_x3;
  fir1 <= addtree6_s_net_x0;
  fir2 <= addtree6_s_net_x2;
  fir3 <= addtree6_s_net;
  fir4 <= addtree6_s_net_x1;
  fir5 <= addtree6_s_net_x4;
  delaypipesl4_q_net <= datain;
  delaypipesl5_q_net <= new_x0;
  rst_net <= rst;
  gateway_outi1 <= mult0_p_net;
  gateway_outi2 <= reinterpret_output_port_net;
  clk_net <= clk_1;
  ce_net <= ce_1;
  fir_even0_fp : entity xil_defaultlib.resampler_fir_even0_fp 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    datain => reinterpret_output_port_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree6_s_net_x3
  );
  fir_even2_fp : entity xil_defaultlib.resampler_fir_even2_fp 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    datain => reinterpret_output_port_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree6_s_net_x2
  );
  fir_even4_fp : entity xil_defaultlib.resampler_fir_even4_fp 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    datain => reinterpret_output_port_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree6_s_net_x1
  );
  fir_odd1_fp : entity xil_defaultlib.resampler_fir_odd1_fp 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    datain => reinterpret_output_port_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree6_s_net_x0
  );
  fir_odd3_fp : entity xil_defaultlib.resampler_fir_odd3_fp 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    datain => reinterpret_output_port_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree6_s_net
  );
  fir_odd5_fp : entity xil_defaultlib.resampler_fir_odd5_fp 
  port map (
    rst => rst_net,
    enb => delaypipesl5_q_net,
    datain => reinterpret_output_port_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree6_s_net_x4
  );
  constant0 : entity xil_defaultlib.sysgen_constant_3809e7e7a5 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  convert : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 15,
    din_width => 16,
    dout_arith => 2,
    dout_bin_pt => 15,
    dout_width => 17,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => delaypipesl4_q_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert_dout_net
  );
  mult0 : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 3,
    a_width => 4,
    b_arith => xlSigned,
    b_bin_pt => 15,
    b_width => 16,
    c_a_type => 0,
    c_a_width => 4,
    c_b_type => 0,
    c_b_width => 16,
    c_baat => 4,
    c_output_width => 20,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i2",
    extra_registers => 0,
    multsign => 2,
    overflow => 2,
    p_arith => xlSigned,
    p_bin_pt => 16,
    p_width => 17,
    quantization => 2
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => constant0_op_net,
    b => delaypipesl4_q_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => mult0_p_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_703c620e80 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => convert_dout_net,
    output_port => reinterpret_output_port_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/FIRCtrl
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_firctrl is
  port (
    datain : in std_logic_vector( 16-1 downto 0 );
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    dlyin : in std_logic_vector( 27-1 downto 0 );
    push : in std_logic_vector( 1-1 downto 0 );
    pop : in std_logic_vector( 1-1 downto 0 );
    valid : in std_logic_vector( 1-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 44-1 downto 0 );
    validout : out std_logic_vector( 1-1 downto 0 );
    num : out std_logic_vector( 8-1 downto 0 );
    dbgfirinmem : out std_logic_vector( 44-1 downto 0 );
    dbgfiroutmem : out std_logic_vector( 44-1 downto 0 );
    dbgdly : out std_logic_vector( 27-1 downto 0 );
    dbgop : out std_logic_vector( 1-1 downto 0 );
    gateway_outi1 : out std_logic_vector( 17-1 downto 0 );
    gateway_outi2 : out std_logic_vector( 17-1 downto 0 )
  );
end resampler_firctrl;
architecture structural of resampler_firctrl is 
  signal fifo1_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal fifo5_dout_net : std_logic_vector( 44-1 downto 0 );
  signal delayvalid9_q_net : std_logic_vector( 1-1 downto 0 );
  signal dlycast3_dout_net : std_logic_vector( 44-1 downto 0 );
  signal dlycast4_dout_net : std_logic_vector( 44-1 downto 0 );
  signal delaypipesl2_q_net : std_logic_vector( 1-1 downto 0 );
  signal fifo3_dout_net : std_logic_vector( 44-1 downto 0 );
  signal dlycast5_dout_net : std_logic_vector( 44-1 downto 0 );
  signal addtree6_s_net_x2 : std_logic_vector( 44-1 downto 0 );
  signal addtree6_s_net_x3 : std_logic_vector( 44-1 downto 0 );
  signal addtree6_s_net_x4 : std_logic_vector( 44-1 downto 0 );
  signal dlycast_dout_net : std_logic_vector( 44-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal fifo1_dout_net : std_logic_vector( 44-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal fifo4_dout_net : std_logic_vector( 44-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 27-1 downto 0 );
  signal dlycast2_dout_net : std_logic_vector( 44-1 downto 0 );
  signal addtree6_s_net_x1 : std_logic_vector( 44-1 downto 0 );
  signal delaypipesl1_q_net : std_logic_vector( 27-1 downto 0 );
  signal addtree6_s_net_x0 : std_logic_vector( 44-1 downto 0 );
  signal delaypipesl4_q_net : std_logic_vector( 16-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal dlycast1_dout_net : std_logic_vector( 44-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 17-1 downto 0 );
  signal ce_net : std_logic;
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal fifo_dout_net : std_logic_vector( 44-1 downto 0 );
  signal clk_net : std_logic;
  signal fifo2_dout_net : std_logic_vector( 44-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
begin
  dataout <= addtree0_s_net;
  validout <= delayvalid9_q_net;
  num <= fifo1_dcount_net;
  dbgfirinmem <= addtree6_s_net;
  dbgfiroutmem <= fifo_dout_net;
  dbgdly <= delaypipesl1_q_net;
  dbgop <= delaypipesl2_q_net;
  delaypipesl4_q_net <= datain;
  delaypipesl5_q_net <= new_x0;
  reinterpret4_output_port_net <= dlyin;
  logical1_y_net <= push;
  logical2_y_net <= pop;
  logical3_y_net <= valid;
  rst_net <= rst;
  gateway_outi1 <= mult0_p_net;
  gateway_outi2 <= reinterpret_output_port_net;
  clk_net <= clk_1;
  ce_net <= ce_1;
  horner : entity xil_defaultlib.resampler_horner 
  port map (
    fir0 => fifo_dout_net,
    fir1 => fifo1_dout_net,
    fir2 => fifo2_dout_net,
    fir3 => fifo3_dout_net,
    fir4 => fifo4_dout_net,
    fir5 => fifo5_dout_net,
    dly => delaypipesl1_q_net,
    valid => delaypipesl2_q_net,
    rst => rst_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree0_s_net,
    validout => delayvalid9_q_net
  );
  synchmem : entity xil_defaultlib.resampler_synchmem 
  port map (
    firin0 => dlycast_dout_net,
    firin1 => dlycast1_dout_net,
    firin2 => dlycast2_dout_net,
    firin3 => dlycast3_dout_net,
    firin4 => dlycast4_dout_net,
    firin5 => dlycast5_dout_net,
    push => logical1_y_net,
    pop => logical2_y_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    firout0 => fifo_dout_net,
    firout1 => fifo1_dout_net,
    firout2 => fifo2_dout_net,
    firout3 => fifo3_dout_net,
    firout4 => fifo4_dout_net,
    firout5 => fifo5_dout_net,
    num => fifo1_dcount_net
  );
  vfd_functionalmodel_nodelay : entity xil_defaultlib.resampler_vfd_functionalmodel_nodelay 
  port map (
    datain => delaypipesl4_q_net,
    new_x0 => delaypipesl5_q_net,
    rst => rst_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    fir0 => addtree6_s_net,
    fir1 => addtree6_s_net_x1,
    fir2 => addtree6_s_net_x4,
    fir3 => addtree6_s_net_x2,
    fir4 => addtree6_s_net_x0,
    fir5 => addtree6_s_net_x3,
    gateway_outi1 => mult0_p_net,
    gateway_outi2 => reinterpret_output_port_net
  );
  delaypipesl1 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 27
  )
  port map (
    en => '1',
    rst => '1',
    d => reinterpret4_output_port_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl1_q_net
  );
  delaypipesl2 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '1',
    d => logical3_y_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl2_q_net
  );
  dlycast : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 43,
    din_width => 44,
    dout_arith => 2,
    dout_bin_pt => 43,
    dout_width => 44,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => addtree6_s_net,
    clk => clk_net,
    ce => ce_net,
    dout => dlycast_dout_net
  );
  dlycast1 : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 43,
    din_width => 44,
    dout_arith => 2,
    dout_bin_pt => 43,
    dout_width => 44,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => addtree6_s_net_x1,
    clk => clk_net,
    ce => ce_net,
    dout => dlycast1_dout_net
  );
  dlycast2 : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 43,
    din_width => 44,
    dout_arith => 2,
    dout_bin_pt => 43,
    dout_width => 44,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => addtree6_s_net_x4,
    clk => clk_net,
    ce => ce_net,
    dout => dlycast2_dout_net
  );
  dlycast3 : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 43,
    din_width => 44,
    dout_arith => 2,
    dout_bin_pt => 43,
    dout_width => 44,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => addtree6_s_net_x2,
    clk => clk_net,
    ce => ce_net,
    dout => dlycast3_dout_net
  );
  dlycast4 : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 43,
    din_width => 44,
    dout_arith => 2,
    dout_bin_pt => 43,
    dout_width => 44,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => addtree6_s_net_x0,
    clk => clk_net,
    ce => ce_net,
    dout => dlycast4_dout_net
  );
  dlycast5 : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 43,
    din_width => 44,
    dout_arith => 2,
    dout_bin_pt => 43,
    dout_width => 44,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => addtree6_s_net_x3,
    clk => clk_net,
    ce => ce_net,
    dout => dlycast5_dout_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/StateCtrl/Controller
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_controller is
  port (
    newdata : in std_logic_vector( 1-1 downto 0 );
    opdata : in std_logic_vector( 1-1 downto 0 );
    delayindata : in std_logic_vector( 27-1 downto 0 );
    push : in std_logic_vector( 1-1 downto 0 );
    pop : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    newoutdata : out std_logic_vector( 1-1 downto 0 );
    opdataout : out std_logic_vector( 1-1 downto 0 );
    delayoutdata : out std_logic_vector( 27-1 downto 0 )
  );
end resampler_controller;
architecture structural of resampler_controller is 
  signal slicenew_y_net : std_logic_vector( 1-1 downto 0 );
  signal ce_net : std_logic;
  signal concat_y_net : std_logic_vector( 2-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl7_q_net : std_logic_vector( 27-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal relational0_op_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl6_q_net : std_logic_vector( 1-1 downto 0 );
  signal sliceop_y_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal reinterpret1_output_port_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 27-1 downto 0 );
  signal fifo1_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal fifo1_empty_net : std_logic;
  signal fifo1_full_net : std_logic;
  signal slicedly_y_net : std_logic_vector( 27-1 downto 0 );
  signal concat1_y_net : std_logic_vector( 29-1 downto 0 );
  signal fifo1_dout_net : std_logic_vector( 29-1 downto 0 );
begin
  newoutdata <= slicenew_y_net;
  opdataout <= sliceop_y_net;
  delayoutdata <= reinterpret4_output_port_net;
  delaypipesl5_q_net <= newdata;
  delaypipesl6_q_net <= opdata;
  delaypipesl7_q_net <= delayindata;
  logical1_y_net <= push;
  relational0_op_net <= pop;
  clk_net <= clk_1;
  ce_net <= ce_1;
  concat : entity xil_defaultlib.sysgen_concat_4105021c84 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret_output_port_net,
    in1 => reinterpret1_output_port_net,
    y => concat_y_net
  );
  concat1 : entity xil_defaultlib.sysgen_concat_6f677e0681 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => concat_y_net,
    in1 => reinterpret2_output_port_net,
    y => concat1_y_net
  );
  fifo1 : entity xil_defaultlib.resampler_xlfifogen_u 
  generic map (
    core_name0 => "resampler_fifo_generator_i2",
    data_count_width => 8,
    data_width => 29,
    extra_registers => 1,
    has_ae => 0,
    has_af => 0,
    has_rst => false,
    percent_full_width => 1
  )
  port map (
    en => '1',
    rst => '1',
    din => concat1_y_net,
    we => logical1_y_net(0),
    re => relational0_op_net(0),
    clk => clk_net,
    ce => ce_net,
    we_ce => ce_net,
    re_ce => ce_net,
    dout => fifo1_dout_net,
    empty => fifo1_empty_net,
    full => fifo1_full_net,
    dcount => fifo1_dcount_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_3e60f2b48c 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => delaypipesl5_q_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3e60f2b48c 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => delaypipesl6_q_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_712fcaed43 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => delaypipesl7_q_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_959796c013 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slicedly_y_net,
    output_port => reinterpret4_output_port_net
  );
  slicedly : entity xil_defaultlib.resampler_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 26,
    x_width => 29,
    y_width => 27
  )
  port map (
    x => fifo1_dout_net,
    y => slicedly_y_net
  );
  slicenew : entity xil_defaultlib.resampler_xlslice 
  generic map (
    new_lsb => 28,
    new_msb => 28,
    x_width => 29,
    y_width => 1
  )
  port map (
    x => fifo1_dout_net,
    y => slicenew_y_net
  );
  sliceop : entity xil_defaultlib.resampler_xlslice 
  generic map (
    new_lsb => 27,
    new_msb => 27,
    x_width => 29,
    y_width => 1
  )
  port map (
    x => fifo1_dout_net,
    y => sliceop_y_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/StateCtrl/Counter
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_counter is
  port (
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    countend : out std_logic_vector( 1-1 downto 0 )
  );
end resampler_counter;
architecture structural of resampler_counter is 
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl1_q_net : std_logic_vector( 1-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal ce_net : std_logic;
  signal constant1_op_net : std_logic_vector( 1-1 downto 0 );
  signal constant0_op_net : std_logic_vector( 4-1 downto 0 );
  signal relational0_op_net : std_logic;
  signal counter_op_net : std_logic_vector( 4-1 downto 0 );
  signal delaypipesl_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical0_y_net : std_logic_vector( 1-1 downto 0 );
begin
  countend <= logical1_y_net;
  delaypipesl1_q_net <= new_x0;
  rst_net <= rst;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant0 : entity xil_defaultlib.sysgen_constant_efc83d4a08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant0_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_58ce3fbb94 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  counter : entity xil_defaultlib.resampler_xlcounter_free 
  generic map (
    core_name0 => "resampler_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 4
  )
  port map (
    clr => '0',
    rst => logical0_y_net,
    en => delaypipesl1_q_net,
    clk => clk_net,
    ce => ce_net,
    op => counter_op_net
  );
  delaypipesl : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    rst => '1',
    d => constant1_op_net,
    en => relational0_op_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl_q_net
  );
  logical0 : entity xil_defaultlib.sysgen_logical_9da2671832 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => rst_net,
    d1 => logical1_y_net,
    y => logical0_y_net
  );
  logical1 : entity xil_defaultlib.sysgen_logical_9da2671832 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => delaypipesl_q_net,
    d1(0) => relational0_op_net,
    y => logical1_y_net
  );
  relational0 : entity xil_defaultlib.sysgen_relational_8c3b1d85bf 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => counter_op_net,
    b => constant0_op_net,
    op(0) => relational0_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/StateCtrl/Counter1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_counter1 is
  port (
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    countend : out std_logic_vector( 1-1 downto 0 )
  );
end resampler_counter1;
architecture structural of resampler_counter1 is 
  signal clk_net : std_logic;
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 1-1 downto 0 );
  signal ce_net : std_logic;
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl_q_net : std_logic_vector( 1-1 downto 0 );
begin
  countend <= logical1_y_net;
  delaypipesl5_q_net <= new_x0;
  rst_net <= rst;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant1 : entity xil_defaultlib.sysgen_constant_58ce3fbb94 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  delaypipesl : entity xil_defaultlib.sysgen_delay_c31cbaf7cc 
  port map (
    clr => '0',
    d => constant1_op_net,
    rst => rst_net,
    en => logical1_y_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl_q_net
  );
  logical1 : entity xil_defaultlib.sysgen_logical_9da2671832 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => delaypipesl_q_net,
    d1 => delaypipesl5_q_net,
    y => logical1_y_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I/StateCtrl
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_statectrl is
  port (
    datain : in std_logic_vector( 16-1 downto 0 );
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    dly : in std_logic_vector( 27-1 downto 0 );
    op : in std_logic_vector( 1-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    firnum : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    firdata : out std_logic_vector( 16-1 downto 0 );
    firnew : out std_logic_vector( 1-1 downto 0 );
    firdly : out std_logic_vector( 27-1 downto 0 );
    firpush : out std_logic_vector( 1-1 downto 0 );
    firpop : out std_logic_vector( 1-1 downto 0 );
    datavalid : out std_logic_vector( 1-1 downto 0 );
    synchpush : out std_logic_vector( 1-1 downto 0 );
    synchpop : out std_logic_vector( 1-1 downto 0 )
  );
end resampler_statectrl;
architecture structural of resampler_statectrl is 
  signal logical1_y_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal delaypipesl4_q_net : std_logic_vector( 27-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl3_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl6_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl7_q_net : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl1_q_net : std_logic_vector( 1-1 downto 0 );
  signal relational0_op_net : std_logic_vector( 1-1 downto 0 );
  signal ce_net : std_logic;
  signal sliceop_y_net : std_logic_vector( 1-1 downto 0 );
  signal fifo1_dcount_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal delaypipesl4_q_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl2_q_net : std_logic_vector( 16-1 downto 0 );
  signal slicenew_y_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl0_q_net : std_logic_vector( 16-1 downto 0 );
  signal logical1_y_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl3_q_net_x0 : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl8_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl2_q_net_x0 : std_logic_vector( 1-1 downto 0 );
begin
  firdata <= delaypipesl4_q_net_x0;
  firnew <= delaypipesl5_q_net_x0;
  firdly <= reinterpret4_output_port_net;
  firpush <= logical1_y_net_x0;
  firpop <= logical2_y_net;
  datavalid <= logical3_y_net;
  synchpush <= logical1_y_net_x1;
  synchpop <= relational0_op_net;
  delaypipesl2_q_net <= datain;
  delaypipesl3_q_net <= new_x0;
  delaypipesl4_q_net <= dly;
  delaypipesl5_q_net <= op;
  rst_net <= rst;
  fifo1_dcount_net_x0 <= firnum;
  clk_net <= clk_1;
  ce_net <= ce_1;
  controller : entity xil_defaultlib.resampler_controller 
  port map (
    newdata => delaypipesl5_q_net_x0,
    opdata => delaypipesl6_q_net,
    delayindata => delaypipesl7_q_net,
    push => logical1_y_net_x1,
    pop => relational0_op_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    newoutdata => slicenew_y_net,
    opdataout => sliceop_y_net,
    delayoutdata => reinterpret4_output_port_net
  );
  counter : entity xil_defaultlib.resampler_counter 
  port map (
    new_x0 => delaypipesl1_q_net,
    rst => rst_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    countend => logical1_y_net
  );
  counter1 : entity xil_defaultlib.resampler_counter1 
  port map (
    new_x0 => delaypipesl5_q_net_x0,
    rst => rst_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    countend => logical1_y_net_x1
  );
  constant1 : entity xil_defaultlib.sysgen_constant_d92945d066 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  delaypipesl0 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 16
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl2_q_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl0_q_net
  );
  delaypipesl1 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl3_q_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl1_q_net
  );
  delaypipesl2 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl5_q_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl2_q_net_x0
  );
  delaypipesl3 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 27
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl4_q_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl3_q_net_x0
  );
  delaypipesl4 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 16
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl0_q_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl4_q_net_x0
  );
  delaypipesl5 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl1_q_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl5_q_net_x0
  );
  delaypipesl6 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl2_q_net_x0,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl6_q_net
  );
  delaypipesl7 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 27
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl3_q_net_x0,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl7_q_net
  );
  delaypipesl8 : entity xil_defaultlib.resampler_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '1',
    d => delaypipesl5_q_net_x0,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl8_q_net
  );
  logical1 : entity xil_defaultlib.sysgen_logical_9b9a61fa28 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => logical1_y_net,
    d1 => delaypipesl8_q_net,
    y => logical1_y_net_x0
  );
  logical2 : entity xil_defaultlib.sysgen_logical_9b9a61fa28 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => slicenew_y_net,
    d1 => relational0_op_net,
    y => logical2_y_net
  );
  logical3 : entity xil_defaultlib.sysgen_logical_9b9a61fa28 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => sliceop_y_net,
    d1 => relational0_op_net,
    y => logical3_y_net
  );
  relational0 : entity xil_defaultlib.sysgen_relational_6710662b9f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => fifo1_dcount_net_x0,
    b => constant1_op_net,
    op => relational0_op_net
  );
end structural;
-- Generated from Simulink block Resampler/RSPUP_I
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_rspup_i is
  port (
    datain : in std_logic_vector( 16-1 downto 0 );
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    toutnup : in std_logic_vector( 29-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dataout : out std_logic_vector( 52-1 downto 0 );
    valid : out std_logic_vector( 1-1 downto 0 );
    gateway_outi : out std_logic_vector( 16-1 downto 0 );
    gateway_outi1 : out std_logic_vector( 17-1 downto 0 );
    gateway_outi2 : out std_logic_vector( 17-1 downto 0 )
  );
end resampler_rspup_i;
architecture structural of resampler_rspup_i is 
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal relational0_op_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl2_q_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal outregdata_q_net : std_logic_vector( 52-1 downto 0 );
  signal delayvalid9_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net : std_logic_vector( 1-1 downto 0 );
  signal addtree6_s_net : std_logic_vector( 44-1 downto 0 );
  signal fifo1_dcount_net : std_logic_vector( 8-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal data_in_net : std_logic_vector( 16-1 downto 0 );
  signal delaypipesl4_q_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal outputreg0_q_net : std_logic_vector( 27-1 downto 0 );
  signal outputreg1_q_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal addtree0_s_net : std_logic_vector( 44-1 downto 0 );
  signal tout_up_net : std_logic_vector( 29-1 downto 0 );
  signal fifo_dout_net : std_logic_vector( 44-1 downto 0 );
  signal delaypipesl2_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl5_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal clk_net : std_logic;
  signal logical1_y_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 17-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl3_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl4_q_net : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl1_q_net : std_logic_vector( 27-1 downto 0 );
  signal delaypipesl0_q_net_x0 : std_logic_vector( 16-1 downto 0 );
  signal new_net : std_logic_vector( 1-1 downto 0 );
  signal ce_net : std_logic;
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 27-1 downto 0 );
  signal outregvalid_q_net : std_logic_vector( 1-1 downto 0 );
  signal gaindly0_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaysyncsl0_q_net : std_logic_vector( 16-1 downto 0 );
  signal delaysyncsl3_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delaysyncsl1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delaysyncsl2_q_net : std_logic_vector( 16-1 downto 0 );
  signal gainvalue_op_net : std_logic_vector( 52-1 downto 0 );
  signal gainmult_p_net : std_logic_vector( 52-1 downto 0 );
begin
  dataout <= outregdata_q_net;
  valid <= outregvalid_q_net;
  data_in_net <= datain;
  new_net <= new_x0;
  tout_up_net <= toutnup;
  rst_net <= rst;
  gateway_outi <= delaypipesl4_q_net_x0;
  gateway_outi1 <= mult0_p_net;
  gateway_outi2 <= reinterpret_output_port_net;
  clk_net <= clk_1;
  ce_net <= ce_1;
  dly_comput_great_ic_functionalmodel_nodelay : entity xil_defaultlib.resampler_dly_comput_great_ic_functionalmodel_nodelay 
  port map (
    new_x0 => new_net,
    toutn => tout_up_net,
    rst => rst_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dly => outputreg0_q_net,
    op => outputreg1_q_net
  );
  firctrl : entity xil_defaultlib.resampler_firctrl 
  port map (
    datain => delaypipesl4_q_net_x0,
    new_x0 => delaypipesl5_q_net_x0,
    dlyin => reinterpret4_output_port_net,
    push => logical1_y_net_x0,
    pop => logical2_y_net,
    valid => logical3_y_net,
    rst => rst_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => addtree0_s_net,
    validout => delayvalid9_q_net,
    num => fifo1_dcount_net,
    dbgfirinmem => addtree6_s_net,
    dbgfiroutmem => fifo_dout_net,
    dbgdly => delaypipesl1_q_net,
    dbgop => delaypipesl2_q_net,
    gateway_outi1 => mult0_p_net,
    gateway_outi2 => reinterpret_output_port_net
  );
  statectrl : entity xil_defaultlib.resampler_statectrl 
  port map (
    datain => delaypipesl2_q_net_x0,
    new_x0 => delaypipesl3_q_net,
    dly => delaypipesl4_q_net,
    op => delaypipesl5_q_net,
    rst => rst_net,
    firnum => fifo1_dcount_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    firdata => delaypipesl4_q_net_x0,
    firnew => delaypipesl5_q_net_x0,
    firdly => reinterpret4_output_port_net,
    firpush => logical1_y_net_x0,
    firpop => logical2_y_net,
    datavalid => logical3_y_net,
    synchpush => logical1_y_net,
    synchpop => relational0_op_net
  );
  delaypipesl0 : entity xil_defaultlib.sysgen_delay_ee88b0f9a0 
  port map (
    clr => '0',
    d => data_in_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl0_q_net_x0
  );
  delaypipesl1 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => new_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl1_q_net_x0
  );
  delaypipesl2 : entity xil_defaultlib.sysgen_delay_ee88b0f9a0 
  port map (
    clr => '0',
    d => delaysyncsl2_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl2_q_net_x0
  );
  delaypipesl3 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delaysyncsl3_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl3_q_net
  );
  delaypipesl4 : entity xil_defaultlib.sysgen_delay_552786d6d7 
  port map (
    clr => '0',
    d => outputreg0_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl4_q_net
  );
  delaypipesl5 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => outputreg1_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaypipesl5_q_net
  );
  delaysyncsl0 : entity xil_defaultlib.sysgen_delay_ee88b0f9a0 
  port map (
    clr => '0',
    d => delaypipesl0_q_net_x0,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaysyncsl0_q_net
  );
  delaysyncsl1 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delaypipesl1_q_net_x0,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaysyncsl1_q_net
  );
  delaysyncsl2 : entity xil_defaultlib.sysgen_delay_ee88b0f9a0 
  port map (
    clr => '0',
    d => delaysyncsl0_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaysyncsl2_q_net
  );
  delaysyncsl3 : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => delaysyncsl1_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => delaysyncsl3_q_net
  );
  gaindly0 : entity xil_defaultlib.sysgen_delay_b08e6841ee 
  port map (
    clr => '0',
    d => delayvalid9_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => gaindly0_q_net
  );
  gainmult : entity xil_defaultlib.resampler_xlmult 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 43,
    a_width => 44,
    b_arith => xlSigned,
    b_bin_pt => 46,
    b_width => 52,
    c_a_type => 0,
    c_a_width => 44,
    c_b_type => 0,
    c_b_width => 52,
    c_baat => 44,
    c_output_width => 96,
    c_type => 0,
    core_name0 => "resampler_mult_gen_v12_0_i0",
    extra_registers => 1,
    multsign => 2,
    overflow => 2,
    p_arith => xlSigned,
    p_bin_pt => 51,
    p_width => 52,
    quantization => 2
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => addtree0_s_net,
    b => gainvalue_op_net,
    clk => clk_net,
    ce => ce_net,
    core_clk => clk_net,
    core_ce => ce_net,
    p => gainmult_p_net
  );
  gainvalue : entity xil_defaultlib.sysgen_constant_35fdb3d1fe 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => gainvalue_op_net
  );
  outregdata : entity xil_defaultlib.sysgen_delay_06d43d52c3 
  port map (
    clr => '0',
    d => gainmult_p_net,
    rst => rst_net,
    en => gaindly0_q_net,
    clk => clk_net,
    ce => ce_net,
    q => outregdata_q_net
  );
  outregvalid : entity xil_defaultlib.sysgen_delay_8fc3988b6c 
  port map (
    clr => '0',
    d => gaindly0_q_net,
    rst => rst_net,
    clk => clk_net,
    ce => ce_net,
    q => outregvalid_q_net
  );
end structural;
-- Generated from Simulink block Resampler_struct
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_struct is
  port (
    data_in : in std_logic_vector( 16-1 downto 0 );
    new_x0 : in std_logic_vector( 1-1 downto 0 );
    rst : in std_logic_vector( 1-1 downto 0 );
    tout_down : in std_logic_vector( 29-1 downto 0 );
    tout_up : in std_logic_vector( 29-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    data_out : out std_logic_vector( 16-1 downto 0 );
    valid : out std_logic_vector( 1-1 downto 0 );
    gateway_outi : out std_logic_vector( 16-1 downto 0 );
    gateway_outi1 : out std_logic_vector( 17-1 downto 0 );
    gateway_outi2 : out std_logic_vector( 17-1 downto 0 )
  );
end resampler_struct;
architecture structural of resampler_struct is 
  signal outregdata_q_net : std_logic_vector( 52-1 downto 0 );
  signal data_in_net : std_logic_vector( 16-1 downto 0 );
  signal new_net : std_logic_vector( 1-1 downto 0 );
  signal tout_down_net : std_logic_vector( 29-1 downto 0 );
  signal tout_up_net : std_logic_vector( 29-1 downto 0 );
  signal data_out_x0 : std_logic_vector( 16-1 downto 0 );
  signal mult0_p_net : std_logic_vector( 17-1 downto 0 );
  signal clk_net : std_logic;
  signal reinterpret_output_port_net : std_logic_vector( 17-1 downto 0 );
  signal outregvalid_q_net : std_logic_vector( 1-1 downto 0 );
  signal ce_net : std_logic;
  signal rst_net : std_logic_vector( 1-1 downto 0 );
  signal delaypipesl4_q_net : std_logic_vector( 16-1 downto 0 );
begin
  data_in_net <= data_in;
  data_out <= data_out_x0;
  new_net <= new_x0;
  rst_net <= rst;
  tout_down_net <= tout_down;
  tout_up_net <= tout_up;
  valid <= outregvalid_q_net;
  gateway_outi <= delaypipesl4_q_net;
  gateway_outi1 <= mult0_p_net;
  gateway_outi2 <= reinterpret_output_port_net;
  clk_net <= clk_1;
  ce_net <= ce_1;
  rspup_i : entity xil_defaultlib.resampler_rspup_i 
  port map (
    datain => data_in_net,
    new_x0 => new_net,
    toutnup => tout_up_net,
    rst => rst_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dataout => outregdata_q_net,
    valid => outregvalid_q_net,
    gateway_outi => delaypipesl4_q_net,
    gateway_outi1 => mult0_p_net,
    gateway_outi2 => reinterpret_output_port_net
  );
  dlycast1 : entity xil_defaultlib.resampler_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 51,
    din_width => 52,
    dout_arith => 2,
    dout_bin_pt => 15,
    dout_width => 16,
    latency => 0,
    overflow => xlSaturate,
    quantization => xlRound
  )
  port map (
    clr => '0',
    en => "1",
    din => outregdata_q_net,
    clk => clk_net,
    ce => ce_net,
    dout => data_out_x0
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler_default_clock_driver is
  port (
    resampler_sysclk : in std_logic;
    resampler_sysce : in std_logic;
    resampler_sysclr : in std_logic;
    resampler_clk1 : out std_logic;
    resampler_ce1 : out std_logic
  );
end resampler_default_clock_driver;
architecture structural of resampler_default_clock_driver is 
begin
  clockdriver : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => resampler_sysclk,
    sysce => resampler_sysce,
    sysclr => resampler_sysclr,
    clk => resampler_clk1,
    ce => resampler_ce1
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity resampler is
  port (
    data_in : in std_logic_vector( 16-1 downto 0 );
    new_x0 : in std_logic;
    rst : in std_logic;
    tout_down : in std_logic_vector( 29-1 downto 0 );
    tout_up : in std_logic_vector( 29-1 downto 0 );
    clk : in std_logic;
    data_out : out std_logic_vector( 16-1 downto 0 );
    valid : out std_logic;
    gateway_outi : out std_logic_vector( 16-1 downto 0 );
    gateway_outi1 : out std_logic_vector( 17-1 downto 0 );
    gateway_outi2 : out std_logic_vector( 17-1 downto 0 )
  );
end resampler;
architecture structural of resampler is 
  attribute core_generation_info : string;
  attribute core_generation_info of structural : architecture is "resampler,sysgen_core_2018_2,{,compilation=IP Catalog,block_icon_display=Default,family=zynquplus,part=xczu7ev,speed=-1-e,package=ffvf1517,synthesis_language=vhdl,hdl_library=xil_defaultlib,synthesis_strategy=Vivado Synthesis Defaults,implementation_strategy=Vivado Implementation Defaults,testbench=1,interface_doc=1,ce_clr=0,clock_period=4,system_simulink_period=4e-09,waveform_viewer=0,axilite_interface=0,ip_catalog_plugin=0,hwcosim_burst_mode=0,simulation_time=0.0001,abs=1,addsub=97,concat=3,constant=63,convert=9,counter=1,delay=67,fifo=8,inv=1,logical=6,mult=55,mux=1,register=86,reinterpret=5,relational=3,slice=3,}";
  signal ce_1_net : std_logic;
  signal clk_1_net : std_logic;
begin
  resampler_default_clock_driver : entity xil_defaultlib.resampler_default_clock_driver 
  port map (
    resampler_sysclk => clk,
    resampler_sysce => '1',
    resampler_sysclr => '0',
    resampler_clk1 => clk_1_net,
    resampler_ce1 => ce_1_net
  );
  resampler_struct : entity xil_defaultlib.resampler_struct 
  port map (
    data_in => data_in,
    new_x0(0) => new_x0,
    rst(0) => rst,
    tout_down => tout_down,
    tout_up => tout_up,
    clk_1 => clk_1_net,
    ce_1 => ce_1_net,
    data_out => data_out,
    valid(0) => valid,
    gateway_outi => gateway_outi,
    gateway_outi1 => gateway_outi1,
    gateway_outi2 => gateway_outi2
  );
end structural;

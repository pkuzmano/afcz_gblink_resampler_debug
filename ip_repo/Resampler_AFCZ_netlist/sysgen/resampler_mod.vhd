-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
entity resampler_stub is
  port (
    data_in : in std_logic_vector( 16-1 downto 0 );
    new_x0 : in std_logic;
    rst : in std_logic;
    tout_down : in std_logic_vector( 29-1 downto 0 );
    tout_up : in std_logic_vector( 29-1 downto 0 );
    clk : in std_logic;
    data_out : out std_logic_vector( 16-1 downto 0 );
    valid : out std_logic;
    gateway_outi : out std_logic_vector( 16-1 downto 0 );
    gateway_outi1 : out std_logic_vector( 17-1 downto 0 );
    gateway_outi2 : out std_logic_vector( 17-1 downto 0 )
  );
end resampler_stub;
architecture structural of resampler_stub is 
begin
  sysgen_dut : entity xil_defaultlib.resampler_0 
  port map (
    data_in => data_in,
    new_x0 => new_x0,
    rst => rst,
    tout_down => tout_down,
    tout_up => tout_up,
    clk => clk,
    data_out => data_out,
    valid => valid,
    gateway_outi => gateway_outi,
    gateway_outi1 => gateway_outi1,
    gateway_outi2 => gateway_outi2
  );
end structural;

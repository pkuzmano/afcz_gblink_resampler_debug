  sysgen_dut : entity xil_defaultlib.resampler 
  port map (
    data_in => data_in,
    new_x0 => new_x0,
    rst => rst,
    tout_down => tout_down,
    tout_up => tout_up,
    clk => clk,
    data_out => data_out,
    valid => valid,
    gateway_outi => gateway_outi,
    gateway_outi1 => gateway_outi1,
    gateway_outi2 => gateway_outi2
  );

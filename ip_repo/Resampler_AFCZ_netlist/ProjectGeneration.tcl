# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator. 
#


namespace eval ::xilinx::dsp::planaheaddriver {
	set Compilation {IP Catalog}
	set CompilationFlow {IP}
	set CreateInterfaceDocument {on}
	set DSPDevice {xczu7ev}
	set DSPFamily {zynquplus}
	set DSPPackage {ffvf1517}
	set DSPSpeed {-1-e}
	set FPGAClockPeriod 4
	set GenerateTestBench 1
	set HDLLanguage {vhdl}
	set IPOOCCacheRootPath {C:/Users/pkuzmano/AppData/Local/Xilinx/Sysgen/SysgenVivado/win64.o/ip}
	set IP_Auto_Infer {0}
	set IP_Categories_Text {System Generator for DSP}
	set IP_Common_Repos {0}
	set IP_Description {}
	set IP_Dir {}
	set IP_Library_Text {SysGen}
	set IP_LifeCycle_Menu {1}
	set IP_Logo {sysgen_icon_100.png}
	set IP_Name {Resampler_stripped_SIS}
	set IP_Revision {201549660}
	set IP_Socket_IP {0}
	set IP_Socket_IP_Proj_Path {}
	set IP_Vendor_Text {CERN}
	set IP_Version_Text {1.0}
	set ImplStrategyName {Vivado Implementation Defaults}
	set PostProjectCreationProc {dsp_package_for_vivado_ip_integrator}
	set Project {resampler}
	set ProjectFiles {
		{{conv_pkg.vhd} -lib {xil_defaultlib}}
		{{synth_reg.vhd} -lib {xil_defaultlib}}
		{{synth_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{srl17e.vhd} -lib {xil_defaultlib}}
		{{srl33e.vhd} -lib {xil_defaultlib}}
		{{synth_reg_reg.vhd} -lib {xil_defaultlib}}
		{{single_reg_w_init.vhd} -lib {xil_defaultlib}}
		{{xlclockdriver_rd.vhd} -lib {xil_defaultlib}}
		{{vivado_ip.tcl}}
		{{resampler_entity_declarations.vhd} -lib {xil_defaultlib}}
		{{resampler.vhd} -lib {xil_defaultlib}}
		{{resampler_tb.vhd}}
		{{resampler_clock.xdc}}
		{{resampler.xdc}}
		{{resampler.htm}}
	}
	set SimPeriod 4e-09
	set SimTime 0.0001
	set SimulationTime {100204.00000000 ns}
	set SynthStrategyName {Vivado Synthesis Defaults}
	set SynthesisTool {Vivado}
	set TargetDir {C:/PLDesign/Resampler/Rsp_sysgen_AFCZ/Resampler_AFCZ_netlist}
	set TestBenchModule {resampler_tb}
	set TopLevelModule {resampler}
	set TopLevelSimulinkHandle 2.00012
	set VHDLLib {xil_defaultlib}
	set TopLevelPortInterface {}
	dict set TopLevelPortInterface tout_up Name {tout_up}
	dict set TopLevelPortInterface tout_up Type Fix_29_26
	dict set TopLevelPortInterface tout_up ArithmeticType xlSigned
	dict set TopLevelPortInterface tout_up BinaryPoint 26
	dict set TopLevelPortInterface tout_up Width 29
	dict set TopLevelPortInterface tout_up DatFile {resampler_tout_up.dat}
	dict set TopLevelPortInterface tout_up IconText {tout_up}
	dict set TopLevelPortInterface tout_up Direction in
	dict set TopLevelPortInterface tout_up Period 1
	dict set TopLevelPortInterface tout_up Interface 0
	dict set TopLevelPortInterface tout_up InterfaceName {}
	dict set TopLevelPortInterface tout_up InterfaceString {DATA}
	dict set TopLevelPortInterface tout_up ClockDomain {resampler}
	dict set TopLevelPortInterface tout_up Locs {}
	dict set TopLevelPortInterface tout_up IOStandard {}
	dict set TopLevelPortInterface tout_down Name {tout_down}
	dict set TopLevelPortInterface tout_down Type Fix_29_26
	dict set TopLevelPortInterface tout_down ArithmeticType xlSigned
	dict set TopLevelPortInterface tout_down BinaryPoint 26
	dict set TopLevelPortInterface tout_down Width 29
	dict set TopLevelPortInterface tout_down DatFile {resampler_tout_down.dat}
	dict set TopLevelPortInterface tout_down IconText {tout_down}
	dict set TopLevelPortInterface tout_down Direction in
	dict set TopLevelPortInterface tout_down Period 1
	dict set TopLevelPortInterface tout_down Interface 0
	dict set TopLevelPortInterface tout_down InterfaceName {}
	dict set TopLevelPortInterface tout_down InterfaceString {DATA}
	dict set TopLevelPortInterface tout_down ClockDomain {resampler}
	dict set TopLevelPortInterface tout_down Locs {}
	dict set TopLevelPortInterface tout_down IOStandard {}
	dict set TopLevelPortInterface rst Name {rst}
	dict set TopLevelPortInterface rst Type Bool
	dict set TopLevelPortInterface rst ArithmeticType xlUnsigned
	dict set TopLevelPortInterface rst BinaryPoint 0
	dict set TopLevelPortInterface rst Width 1
	dict set TopLevelPortInterface rst DatFile {resampler_rst.dat}
	dict set TopLevelPortInterface rst IconText {rst}
	dict set TopLevelPortInterface rst Direction in
	dict set TopLevelPortInterface rst Period 1
	dict set TopLevelPortInterface rst Interface 0
	dict set TopLevelPortInterface rst InterfaceName {}
	dict set TopLevelPortInterface rst InterfaceString {DATA}
	dict set TopLevelPortInterface rst ClockDomain {resampler}
	dict set TopLevelPortInterface rst Locs {}
	dict set TopLevelPortInterface rst IOStandard {}
	dict set TopLevelPortInterface new_x0 Name {new_x0}
	dict set TopLevelPortInterface new_x0 Type Bool
	dict set TopLevelPortInterface new_x0 ArithmeticType xlUnsigned
	dict set TopLevelPortInterface new_x0 BinaryPoint 0
	dict set TopLevelPortInterface new_x0 Width 1
	dict set TopLevelPortInterface new_x0 DatFile {resampler_new.dat}
	dict set TopLevelPortInterface new_x0 IconText {new}
	dict set TopLevelPortInterface new_x0 Direction in
	dict set TopLevelPortInterface new_x0 Period 1
	dict set TopLevelPortInterface new_x0 Interface 0
	dict set TopLevelPortInterface new_x0 InterfaceName {}
	dict set TopLevelPortInterface new_x0 InterfaceString {DATA}
	dict set TopLevelPortInterface new_x0 ClockDomain {resampler}
	dict set TopLevelPortInterface new_x0 Locs {}
	dict set TopLevelPortInterface new_x0 IOStandard {}
	dict set TopLevelPortInterface data_in Name {data_in}
	dict set TopLevelPortInterface data_in Type Fix_16_15
	dict set TopLevelPortInterface data_in ArithmeticType xlSigned
	dict set TopLevelPortInterface data_in BinaryPoint 15
	dict set TopLevelPortInterface data_in Width 16
	dict set TopLevelPortInterface data_in DatFile {resampler_data_in.dat}
	dict set TopLevelPortInterface data_in IconText {data_in}
	dict set TopLevelPortInterface data_in Direction in
	dict set TopLevelPortInterface data_in Period 1
	dict set TopLevelPortInterface data_in Interface 0
	dict set TopLevelPortInterface data_in InterfaceName {}
	dict set TopLevelPortInterface data_in InterfaceString {DATA}
	dict set TopLevelPortInterface data_in ClockDomain {resampler}
	dict set TopLevelPortInterface data_in Locs {}
	dict set TopLevelPortInterface data_in IOStandard {}
	dict set TopLevelPortInterface data_out Name {data_out}
	dict set TopLevelPortInterface data_out Type Fix_16_15
	dict set TopLevelPortInterface data_out ArithmeticType xlSigned
	dict set TopLevelPortInterface data_out BinaryPoint 15
	dict set TopLevelPortInterface data_out Width 16
	dict set TopLevelPortInterface data_out DatFile {resampler_data_out.dat}
	dict set TopLevelPortInterface data_out IconText {data_out}
	dict set TopLevelPortInterface data_out Direction out
	dict set TopLevelPortInterface data_out Period 1
	dict set TopLevelPortInterface data_out Interface 0
	dict set TopLevelPortInterface data_out InterfaceName {}
	dict set TopLevelPortInterface data_out InterfaceString {DATA}
	dict set TopLevelPortInterface data_out ClockDomain {resampler}
	dict set TopLevelPortInterface data_out Locs {}
	dict set TopLevelPortInterface data_out IOStandard {}
	dict set TopLevelPortInterface valid Name {valid}
	dict set TopLevelPortInterface valid Type Bool
	dict set TopLevelPortInterface valid ArithmeticType xlUnsigned
	dict set TopLevelPortInterface valid BinaryPoint 0
	dict set TopLevelPortInterface valid Width 1
	dict set TopLevelPortInterface valid DatFile {resampler_valid.dat}
	dict set TopLevelPortInterface valid IconText {valid}
	dict set TopLevelPortInterface valid Direction out
	dict set TopLevelPortInterface valid Period 1
	dict set TopLevelPortInterface valid Interface 0
	dict set TopLevelPortInterface valid InterfaceName {}
	dict set TopLevelPortInterface valid InterfaceString {DATA}
	dict set TopLevelPortInterface valid ClockDomain {resampler}
	dict set TopLevelPortInterface valid Locs {}
	dict set TopLevelPortInterface valid IOStandard {}
	dict set TopLevelPortInterface gateway_outi Name {gateway_outi}
	dict set TopLevelPortInterface gateway_outi Type Fix_16_15
	dict set TopLevelPortInterface gateway_outi ArithmeticType xlSigned
	dict set TopLevelPortInterface gateway_outi BinaryPoint 15
	dict set TopLevelPortInterface gateway_outi Width 16
	dict set TopLevelPortInterface gateway_outi DatFile {resampler_rspup_i_firctrl_vfd_functionalmodel_nodelay_gateway_outi.dat}
	dict set TopLevelPortInterface gateway_outi IconText {Gateway OutI}
	dict set TopLevelPortInterface gateway_outi Direction out
	dict set TopLevelPortInterface gateway_outi Period 1
	dict set TopLevelPortInterface gateway_outi Interface 0
	dict set TopLevelPortInterface gateway_outi InterfaceName {}
	dict set TopLevelPortInterface gateway_outi InterfaceString {DATA}
	dict set TopLevelPortInterface gateway_outi ClockDomain {resampler}
	dict set TopLevelPortInterface gateway_outi Locs {}
	dict set TopLevelPortInterface gateway_outi IOStandard {}
	dict set TopLevelPortInterface gateway_outi1 Name {gateway_outi1}
	dict set TopLevelPortInterface gateway_outi1 Type Fix_17_16
	dict set TopLevelPortInterface gateway_outi1 ArithmeticType xlSigned
	dict set TopLevelPortInterface gateway_outi1 BinaryPoint 16
	dict set TopLevelPortInterface gateway_outi1 Width 17
	dict set TopLevelPortInterface gateway_outi1 DatFile {resampler_rspup_i_firctrl_vfd_functionalmodel_nodelay_gateway_outi1.dat}
	dict set TopLevelPortInterface gateway_outi1 IconText {Gateway OutI1}
	dict set TopLevelPortInterface gateway_outi1 Direction out
	dict set TopLevelPortInterface gateway_outi1 Period 1
	dict set TopLevelPortInterface gateway_outi1 Interface 0
	dict set TopLevelPortInterface gateway_outi1 InterfaceName {}
	dict set TopLevelPortInterface gateway_outi1 InterfaceString {DATA}
	dict set TopLevelPortInterface gateway_outi1 ClockDomain {resampler}
	dict set TopLevelPortInterface gateway_outi1 Locs {}
	dict set TopLevelPortInterface gateway_outi1 IOStandard {}
	dict set TopLevelPortInterface gateway_outi2 Name {gateway_outi2}
	dict set TopLevelPortInterface gateway_outi2 Type Fix_17_16
	dict set TopLevelPortInterface gateway_outi2 ArithmeticType xlSigned
	dict set TopLevelPortInterface gateway_outi2 BinaryPoint 16
	dict set TopLevelPortInterface gateway_outi2 Width 17
	dict set TopLevelPortInterface gateway_outi2 DatFile {resampler_rspup_i_firctrl_vfd_functionalmodel_nodelay_gateway_outi2.dat}
	dict set TopLevelPortInterface gateway_outi2 IconText {Gateway OutI2}
	dict set TopLevelPortInterface gateway_outi2 Direction out
	dict set TopLevelPortInterface gateway_outi2 Period 1
	dict set TopLevelPortInterface gateway_outi2 Interface 0
	dict set TopLevelPortInterface gateway_outi2 InterfaceName {}
	dict set TopLevelPortInterface gateway_outi2 InterfaceString {DATA}
	dict set TopLevelPortInterface gateway_outi2 ClockDomain {resampler}
	dict set TopLevelPortInterface gateway_outi2 Locs {}
	dict set TopLevelPortInterface gateway_outi2 IOStandard {}
	dict set TopLevelPortInterface clk Name {clk}
	dict set TopLevelPortInterface clk Type -
	dict set TopLevelPortInterface clk ArithmeticType xlUnsigned
	dict set TopLevelPortInterface clk BinaryPoint 0
	dict set TopLevelPortInterface clk Width 1
	dict set TopLevelPortInterface clk DatFile {}
	dict set TopLevelPortInterface clk Direction in
	dict set TopLevelPortInterface clk Period 1
	dict set TopLevelPortInterface clk Interface 6
	dict set TopLevelPortInterface clk InterfaceName {}
	dict set TopLevelPortInterface clk InterfaceString {CLOCK}
	dict set TopLevelPortInterface clk ClockDomain {resampler}
	dict set TopLevelPortInterface clk Locs {}
	dict set TopLevelPortInterface clk IOStandard {}
	dict set TopLevelPortInterface clk AssociatedInterfaces {}
	dict set TopLevelPortInterface clk AssociatedResets {}
	set MemoryMappedPort {}
}

source SgPaProject.tcl
::xilinx::dsp::planaheadworker::dsp_create_project
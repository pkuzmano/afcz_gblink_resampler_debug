library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity GBLink_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S_AXI
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 12;

		-- Parameters of Axi Master Bus Interface M_AXIS_DATA
		C_M_AXIS_DATA_TDATA_WIDTH	: integer	:= 64;
		C_M_AXIS_DATA_START_COUNT	: integer	:= 32;

		-- Parameters of Axi Master Bus Interface M_AXIS_USER_K
		C_M_AXIS_USER_K_TDATA_WIDTH	: integer	:= 64;
		C_M_AXIS_USER_K_START_COUNT	: integer	:= 32;

		-- Parameters of Axi Slave Bus Interface S_AXIS_DATA
		C_S_AXIS_DATA_TDATA_WIDTH	: integer	:= 64;

		-- Parameters of Axi Slave Bus Interface S_AXIS_USER_K
		C_S_AXIS_USER_K_TDATA_WIDTH	: integer	:= 64
	);
	port (
		-- Users to add ports here

		-- Ports for interfacing with the Aurora core
		aurora_reset_pb : out std_logic;
		aurora_pma_init : out std_logic;
		aurora_link_reset_out : in std_logic;
		aurora_sys_reset_out : in std_logic;
		aurora_gt_pll_lock : in std_logic;
		aurora_lane_up : in std_logic;
		aurora_channel_up : in std_logic;
		aurora_hard_err : in std_logic;
		aurora_soft_err : in std_logic;

		-- Clock/reset ports
		user_clk_slower : in std_logic; -- 78.125 (125)
		user_rst_slower : in std_logic;
		user_clk_faster : in std_logic; -- 156.25 (250)
		user_rst_faster : in std_logic;

		-- TX logic interface
		-- DSP, ch1
		s_tx_dsp_ch1_data : in std_logic_vector(31 downto 0);
		s_tx_dsp_ch1_valid : in std_logic;
		s_tx_dsp_ch1_frev : in std_logic;
		s_tx_dsp_ch1_turn_number : in std_logic_vector(31 downto 0); -- 29 should suffice for coast cycles
		-- DSP, ch2
		s_tx_dsp_ch2_data : in std_logic_vector(31 downto 0);
		s_tx_dsp_ch2_valid : in std_logic;
		s_tx_dsp_ch2_frev : in std_logic;
		s_tx_dsp_ch2_turn_number : in std_logic_vector(31 downto 0); -- 29 should suffice for coast cycles
		-- NCO
		s_tx_nco_id : in std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction

		-- White Rabbit: synchronisation for fixed-latency mode
		sync_tx : in std_logic;
		sync_rx : in std_logic;

		-- RX logic interface
		-- DSP, ch1
		s_rx_dsp_ch1_read : in std_logic;
		s_rx_dsp_ch1_valid : out std_logic;
		s_rx_dsp_ch1_data : out std_logic_vector(31 downto 0);
		s_rx_dsp_ch1_frev_flag : out std_logic;
		s_rx_dsp_ch1_bucket_index : out std_logic_vector(15 downto 0);
		s_rx_dsp_ch1_full_error : out std_logic;
		s_rx_dsp_ch1_turn_error : out std_logic;
		s_rx_ch1_id : out std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction
		-- DSP, ch2
		s_rx_dsp_ch2_read : in std_logic;
		s_rx_dsp_ch2_valid : out std_logic;
		s_rx_dsp_ch2_data : out std_logic_vector(31 downto 0);
		s_rx_dsp_ch2_frev_flag : out std_logic;
		s_rx_dsp_ch2_bucket_index : out std_logic_vector(15 downto 0);
		s_rx_dsp_ch2_full_error : out std_logic;
		s_rx_dsp_ch2_turn_error : out std_logic;
		s_rx_ch2_id : out std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S_AXI
		s_axi_aclk	: in std_logic;
		s_axi_aresetn	: in std_logic;
		s_axi_awaddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awprot	: in std_logic_vector(2 downto 0);
		s_axi_awvalid	: in std_logic;
		s_axi_awready	: out std_logic;
		s_axi_wdata	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_wstrb	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		s_axi_wvalid	: in std_logic;
		s_axi_wready	: out std_logic;
		s_axi_bresp	: out std_logic_vector(1 downto 0);
		s_axi_bvalid	: out std_logic;
		s_axi_bready	: in std_logic;
		s_axi_araddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arprot	: in std_logic_vector(2 downto 0);
		s_axi_arvalid	: in std_logic;
		s_axi_arready	: out std_logic;
		s_axi_rdata	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_rresp	: out std_logic_vector(1 downto 0);
		s_axi_rvalid	: out std_logic;
		s_axi_rready	: in std_logic;

		-- Ports of Axi Master Bus Interface M_AXIS_DATA
		m_axis_data_aclk	: in std_logic;
		m_axis_data_aresetn	: in std_logic;
		m_axis_data_tvalid	: out std_logic;
		m_axis_data_tdata	: out std_logic_vector(C_M_AXIS_DATA_TDATA_WIDTH-1 downto 0);
		m_axis_data_tstrb	: out std_logic_vector((C_M_AXIS_DATA_TDATA_WIDTH/8)-1 downto 0);
		m_axis_data_tlast	: out std_logic;
		m_axis_data_tready	: in std_logic;

		-- Ports of Axi Master Bus Interface M_AXIS_USER_K
		m_axis_user_k_aclk	: in std_logic;
		m_axis_user_k_aresetn	: in std_logic;
		m_axis_user_k_tvalid	: out std_logic;
		m_axis_user_k_tdata	: out std_logic_vector(C_M_AXIS_USER_K_TDATA_WIDTH-1 downto 0);
		m_axis_user_k_tstrb	: out std_logic_vector((C_M_AXIS_USER_K_TDATA_WIDTH/8)-1 downto 0);
		m_axis_user_k_tlast	: out std_logic;
		m_axis_user_k_tready	: in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXIS_DATA
		s_axis_data_aclk	: in std_logic;
		s_axis_data_aresetn	: in std_logic;
		s_axis_data_tready	: out std_logic;
		s_axis_data_tdata	: in std_logic_vector(C_S_AXIS_DATA_TDATA_WIDTH-1 downto 0);
		s_axis_data_tstrb	: in std_logic_vector((C_S_AXIS_DATA_TDATA_WIDTH/8)-1 downto 0);
		s_axis_data_tlast	: in std_logic;
		s_axis_data_tvalid	: in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXIS_USER_K
		s_axis_user_k_aclk	: in std_logic;
		s_axis_user_k_aresetn	: in std_logic;
		s_axis_user_k_tready	: out std_logic;
		s_axis_user_k_tdata	: in std_logic_vector(C_S_AXIS_USER_K_TDATA_WIDTH-1 downto 0);
		s_axis_user_k_tstrb	: in std_logic_vector((C_S_AXIS_USER_K_TDATA_WIDTH/8)-1 downto 0);
		s_axis_user_k_tlast	: in std_logic;
		s_axis_user_k_tvalid	: in std_logic
	);
end GBLink_v1_0;

architecture arch_imp of GBLink_v1_0 is

	component axi4lite_cheburashka_bridge is
		port (
		ACLK    : in std_logic;
		ARESETN : in std_logic;
		ARVALID : in  std_logic;
		AWVALID : in  std_logic;
		BREADY  : in  std_logic;
		RREADY  : in  std_logic;
		WLAST   : in  std_logic := '1';
		WVALID  : in  std_logic;
		ARADDR  : in  std_logic_vector (31 downto 0);
		AWADDR  : in  std_logic_vector (31 downto 0);
		WDATA   : in  std_logic_vector (31 downto 0);
		WSTRB   : in  std_logic_vector (3 downto 0);
		ARREADY : out std_logic;
		AWREADY : out std_logic;
		BVALID  : out std_logic;
		RLAST   : out std_logic;
		RVALID  : out std_logic;
		WREADY  : out std_logic;
		BRESP   : out std_logic_vector (1 downto 0);
		RRESP   : out std_logic_vector (1 downto 0);
		RDATA   : out std_logic_vector (31 downto 0);
		ch_addr_o    : out std_logic_vector(31 downto 0);
		ch_rd_data_i : in  std_logic_vector(31 downto 0);
		ch_wr_data_o : out std_logic_vector(31 downto 0);
		ch_rd_mem_o  : out std_logic;
		ch_wr_mem_o  : out std_logic;
		ch_rd_done_i : in  std_logic;
		ch_wr_done_i : in  std_logic
		);
	end component axi4lite_cheburashka_bridge;

	component RegCtrl_gbLink is
		port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(11 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		firmwareID : in std_logic_vector(31 downto 0);
		firmwareVersion : in std_logic_vector(31 downto 0);
		designerID : in std_logic_vector(31 downto 0);
		auroraStatus_linkResetOut : in std_logic;
		auroraStatus_sysResetOut : in std_logic;
		auroraStatus_gtPllLock : in std_logic;
		auroraStatus_laneUp : in std_logic;
		auroraStatus_channelUp : in std_logic;
		auroraStatus_hardErr : in std_logic;
		auroraStatus_softErr : in std_logic;
		auroraStatus_initOk : in std_logic;
		auroraStatus_gtPllLockLatched : in std_logic;
		auroraStatus_laneUpLatched : in std_logic;
		auroraStatus_channelUpLatched : in std_logic;
		auroraStatus_hardErrLatched : in std_logic;
		auroraStatus_softErrLatched : in std_logic;
		auroraControl_reset : out std_logic;
		auroraControl_gtPllLockClear : out std_logic;
		auroraControl_laneUpClear : out std_logic;
		auroraControl_channelUpClear : out std_logic;
		auroraControl_hardErrClear : out std_logic;
		auroraControl_softErrClear : out std_logic;
		txStatus_fifoCh1NumData : in std_logic_vector(31 downto 0);
		txStatus_fifoCh1NumDataTurn : in std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumData : in std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumDataTurn : in std_logic_vector(31 downto 0);
		txControls_general_enable : out std_logic;
		txControls_general_reset : out std_logic;
		txControls_numBuckets : out std_logic_vector(15 downto 0);
		txControls_interleaver_enable : out std_logic;
		txControls_interleaver_rateRatioCh1 : out std_logic_vector(5 downto 4);
		txControls_interleaver_rateRatioCh2 : out std_logic_vector(9 downto 8);
		txControls_fixedLatency_enable : out std_logic;
		rxControls_general_enable : out std_logic;
		rxControls_general_reset : out std_logic;
		rxControls_numBuckets : out std_logic_vector(15 downto 0);
		rxControls_interleaver_enable : out std_logic;
		rxControls_interleaver_rateRatioCh1 : out std_logic_vector(5 downto 4);
		rxControls_interleaver_rateRatioCh2 : out std_logic_vector(9 downto 8);
		rxControls_fixedLatency_enable : out std_logic;
		rxControls_latency : out std_logic_vector(15 downto 0)
		);
	end component RegCtrl_gbLink;

	component aurora_reset_logic is
		generic (
		C_RESET_PB_ASSERT_COUNTER_WIDTH	: integer := 8;
		C_PMA_INIT_ASSERT_COUNTER_WIDTH	: integer := 27
		);
		port (
		clk		: in std_logic;
		resetn	: in std_logic;

		txrxReset : in std_logic;

		aurora_user_clk		: in std_logic;
		aurora_user_resetn	: in std_logic;

		aurora_reset_pb	: out std_logic;
		aurora_pma_init	: out std_logic
		);
	end component aurora_reset_logic;

	component status_reg_latched is
		generic (
		C_STATUS_REG_WIDTH	: integer := 1;
		C_STATUS_DEFAULT	: std_logic := '0'
		);
		port (
		clk		: in std_logic;
		resetn	: in std_logic;

		status_reg_in		: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_reset	: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_out		: out std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0)
		);
	end component status_reg_latched;

	component gblink_tx_logic is
		generic (
		-- Parameters of Axi Master Bus Interface M_AXIS_DATA
		C_M_AXIS_DATA_TDATA_WIDTH	: integer	:= 64;
		--C_M_AXIS_DATA_START_COUNT	: integer	:= 32;

		-- Parameters of Axi Master Bus Interface M_AXIS_USER_K
		C_M_AXIS_USER_K_TDATA_WIDTH	: integer	:= 64
		--C_M_AXIS_USER_K_START_COUNT	: integer	:= 32;
		);
		port (
		-- Clock/reset ports
		user_clk_slower : in std_logic; -- 78.125 (125)
		user_rst_slower : in std_logic;
		user_clk_faster : in std_logic; -- 156.25 (250)
		user_rst_faster : in std_logic;

		-- TX logic interface
		-- DSP, ch1
		s_tx_dsp_ch1_data : in std_logic_vector(31 downto 0);
		s_tx_dsp_ch1_valid : in std_logic;
		s_tx_dsp_ch1_frev : in std_logic;
		s_tx_dsp_ch1_turn_number : in std_logic_vector(31 downto 0); -- 29 should suffice for coast cycles
		-- DSP, ch2
		s_tx_dsp_ch2_data : in std_logic_vector(31 downto 0);
		s_tx_dsp_ch2_valid : in std_logic;
		s_tx_dsp_ch2_frev : in std_logic;
		s_tx_dsp_ch2_turn_number : in std_logic_vector(31 downto 0); -- 29 should suffice for coast cycles
		-- NCO
		s_tx_nco_id : in std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction

		-- White Rabbit: synchronisation for fixed-latency mode
		sync_tx : in std_logic;

		-- Ports of Axi Master Bus Interface M_AXIS_DATA
		m_axis_data_aclk	: in std_logic;
		m_axis_data_aresetn	: in std_logic;
		m_axis_data_tvalid	: out std_logic;
		m_axis_data_tdata	: out std_logic_vector(C_M_AXIS_DATA_TDATA_WIDTH-1 downto 0);
		m_axis_data_tstrb	: out std_logic_vector((C_M_AXIS_DATA_TDATA_WIDTH/8)-1 downto 0);
		m_axis_data_tlast	: out std_logic;
		m_axis_data_tready	: in std_logic;

		-- Ports of Axi Master Bus Interface M_AXIS_USER_K
		m_axis_user_k_aclk	: in std_logic;
		m_axis_user_k_aresetn	: in std_logic;
		m_axis_user_k_tvalid	: out std_logic;
		m_axis_user_k_tdata	: out std_logic_vector(C_M_AXIS_USER_K_TDATA_WIDTH-1 downto 0);
		m_axis_user_k_tstrb	: out std_logic_vector((C_M_AXIS_USER_K_TDATA_WIDTH/8)-1 downto 0);
		m_axis_user_k_tlast	: out std_logic;
		m_axis_user_k_tready	: in std_logic;

		txControls_general_enable : in std_logic;
		txControls_general_reset : in std_logic;
		txControls_numBuckets : in std_logic_vector(15 downto 0);
		txControls_interleaver_enable : in std_logic;
		txControls_interleaver_rateRatioCh1 : in std_logic_vector(5 downto 4);
		txControls_interleaver_rateRatioCh2 : in std_logic_vector(9 downto 8);
		txControls_fixedLatency_enable : in std_logic;

		txStatus_fifoCh1NumData : out std_logic_vector(31 downto 0);
		txStatus_fifoCh1NumDataTurn : out std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumData : out std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumDataTurn : out std_logic_vector(31 downto 0)
		);
	end component gblink_tx_logic;

	component gblink_rx_logic is
		generic (
		-- Parameters of Axi Slave Bus Interface S_AXIS_DATA
		C_S_AXIS_DATA_TDATA_WIDTH	: integer	:= 64;
		--C_S_AXIS_DATA_START_COUNT	: integer	:= 32;

		-- Parameters of Axi Slave Bus Interface S_AXIS_USER_K
		C_S_AXIS_USER_K_TDATA_WIDTH	: integer	:= 64
		--C_S_AXIS_USER_K_START_COUNT	: integer	:= 32;
		);
		port (
		-- Clock/reset ports
		user_clk_slower : in std_logic; -- 78.125 (125)
		user_rst_slower : in std_logic;
		user_clk_faster : in std_logic; -- 156.25 (250)
		user_rst_faster : in std_logic;

		-- RX logic interface
		-- DSP, ch1
		s_rx_dsp_ch1_read : in std_logic;
		s_rx_dsp_ch1_valid : out std_logic;
		s_rx_dsp_ch1_data : out std_logic_vector(31 downto 0);
		s_rx_dsp_ch1_frev_flag : out std_logic;
		s_rx_dsp_ch1_bucket_index : out std_logic_vector(15 downto 0);
		s_rx_dsp_ch1_full_error : out std_logic;
		s_rx_dsp_ch1_turn_error : out std_logic;
		s_rx_ch1_id : out std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction
		-- DSP, ch2
		s_rx_dsp_ch2_read : in std_logic;
		s_rx_dsp_ch2_valid : out std_logic;
		s_rx_dsp_ch2_data : out std_logic_vector(31 downto 0);
		s_rx_dsp_ch2_frev_flag : out std_logic;
		s_rx_dsp_ch2_bucket_index : out std_logic_vector(15 downto 0);
		s_rx_dsp_ch2_full_error : out std_logic;
		s_rx_dsp_ch2_turn_error : out std_logic;
		s_rx_ch2_id : out std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction

		-- White Rabbit: synchronisation for fixed-latency mode
		sync_rx : in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXIS_DATA
		s_axis_data_aclk	: in std_logic;
		s_axis_data_aresetn	: in std_logic;
		s_axis_data_tready	: out std_logic;
		s_axis_data_tdata	: in std_logic_vector(C_S_AXIS_DATA_TDATA_WIDTH-1 downto 0);
		s_axis_data_tstrb	: in std_logic_vector((C_S_AXIS_DATA_TDATA_WIDTH/8)-1 downto 0);
		s_axis_data_tlast	: in std_logic;
		s_axis_data_tvalid	: in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXIS_USER_K
		s_axis_user_k_aclk	: in std_logic;
		s_axis_user_k_aresetn	: in std_logic;
		s_axis_user_k_tready	: out std_logic;
		s_axis_user_k_tdata	: in std_logic_vector(C_S_AXIS_USER_K_TDATA_WIDTH-1 downto 0);
		s_axis_user_k_tstrb	: in std_logic_vector((C_S_AXIS_USER_K_TDATA_WIDTH/8)-1 downto 0);
		s_axis_user_k_tlast	: in std_logic;
		s_axis_user_k_tvalid	: in std_logic;

		rxControls_general_enable : in std_logic;
		rxControls_general_reset : in std_logic;
		rxControls_numBuckets : in std_logic_vector(15 downto 0);
		rxControls_interleaver_enable : in std_logic;
		rxControls_interleaver_rateRatioCh1 : in std_logic_vector(5 downto 4);
		rxControls_interleaver_rateRatioCh2 : in std_logic_vector(9 downto 8);
		rxControls_fixedLatency_enable : in std_logic;
		rxControls_latency : in std_logic_vector(15 downto 0)

		-- fifo_tx_wr_rst_busy : out std_logic_vector(2 downto 1);
		-- fifo_tx_rd_rst_busy : out std_logic_vector(2 downto 1)
		);
	end component gblink_rx_logic;

	component GBLink_v1_0_S_AXIS_DATA is
		generic (
		C_S_AXIS_TDATA_WIDTH	: integer	:= 32
		);
		port (
		S_AXIS_ACLK	: in std_logic;
		S_AXIS_ARESETN	: in std_logic;
		S_AXIS_TREADY	: out std_logic;
		S_AXIS_TDATA	: in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
		S_AXIS_TSTRB	: in std_logic_vector((C_S_AXIS_TDATA_WIDTH/8)-1 downto 0);
		S_AXIS_TLAST	: in std_logic;
		S_AXIS_TVALID	: in std_logic
		);
	end component GBLink_v1_0_S_AXIS_DATA;

	component GBLink_v1_0_S_AXIS_USER_K is
		generic (
		C_S_AXIS_TDATA_WIDTH	: integer	:= 32
		);
		port (
		S_AXIS_ACLK	: in std_logic;
		S_AXIS_ARESETN	: in std_logic;
		S_AXIS_TREADY	: out std_logic;
		S_AXIS_TDATA	: in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
		S_AXIS_TSTRB	: in std_logic_vector((C_S_AXIS_TDATA_WIDTH/8)-1 downto 0);
		S_AXIS_TLAST	: in std_logic;
		S_AXIS_TVALID	: in std_logic
		);
	end component GBLink_v1_0_S_AXIS_USER_K;

	type gblink_rx_fifo_state_t is (RX_IDLE);

	signal gblink_rx_fifo_state			: gblink_rx_fifo_state_t;
	signal gblink_rx_fifo_state_next	: gblink_rx_fifo_state_t;

	signal ch_addr_o	: std_logic_vector(31 downto 0);
	signal ch_rd_data_i	: std_logic_vector(31 downto 0);
	signal ch_wr_data_o	: std_logic_vector(31 downto 0);
	signal ch_rd_mem_o	: std_logic;
	signal ch_wr_mem_o	: std_logic;
	signal ch_rd_done_i	: std_logic;
	signal ch_wr_done_i	: std_logic;

	signal RegCtrl_gbLink_Rst					: std_logic;
	signal axi4lite_cheburashka_bridge_ARESETN	: std_logic;

	constant firmwareID			: std_logic_vector(31 downto 0) := x"AAAAAAAA";
	constant firmwareVersion	: std_logic_vector(31 downto 0) := x"BBBBBBBB";
	constant designerID			: std_logic_vector(31 downto 0) := x"CCCCCCCC";

	signal auroraStatus_initOk				: std_logic;

	signal auroraStatus_gtPllLockLatched	: std_logic;
	signal auroraStatus_laneUpLatched		: std_logic;
	signal auroraStatus_channelUpLatched	: std_logic;
	signal auroraStatus_hardErrLatched		: std_logic;
	signal auroraStatus_softErrLatched		: std_logic;

	signal auroraControl_reset				: std_logic;

	signal auroraControl_gtPllLockClear		: std_logic;
	signal auroraControl_laneUpClear		: std_logic;
	signal auroraControl_channelUpClear		: std_logic;
	signal auroraControl_hardErrClear		: std_logic;
	signal auroraControl_softErrClear		: std_logic;

	signal aurora_status_latched_default0	: std_logic_vector(1 downto 0);
	signal aurora_status_latched_default1	: std_logic_vector(2 downto 0);

	signal txStatus : std_logic_vector(31 downto 0);
	signal txControls_general_enable : std_logic;
	signal txControls_general_reset : std_logic;
	signal txControls_numBuckets : std_logic_vector(15 downto 0);
	signal txControls_interleaver_enable : std_logic;
	signal txControls_interleaver_rateRatioCh1 : std_logic_vector(5 downto 4);
	signal txControls_interleaver_rateRatioCh2 : std_logic_vector(9 downto 8);
	signal txControls_fixedLatency_enable : std_logic;
	signal rxControls_general_enable : std_logic;
	signal rxControls_general_reset : std_logic;
	signal rxControls_numBuckets : std_logic_vector(15 downto 0);
	signal rxControls_interleaver_enable : std_logic;
	signal rxControls_interleaver_rateRatioCh1 : std_logic_vector(5 downto 4);
	signal rxControls_interleaver_rateRatioCh2 : std_logic_vector(9 downto 8);
	signal rxControls_fixedLatency_enable : std_logic;
	signal rxControls_latency : std_logic_vector(15 downto 0);

	signal txStatus_fifoCh1NumData : std_logic_vector(31 downto 0);
	signal txStatus_fifoCh1NumDataTurn : std_logic_vector(31 downto 0);
	signal txStatus_fifoCh2NumData : std_logic_vector(31 downto 0);
	signal txStatus_fifoCh2NumDataTurn : std_logic_vector(31 downto 0);

	signal fifo_tx_wr_rst_busy : std_logic_vector(2 downto 1);
	signal fifo_tx_rd_rst_busy : std_logic_vector(2 downto 1);

begin

-- Instantiation of AXI4Lite to Cheburashka bridge
axi4lite_cheburashka_bridge_inst : axi4lite_cheburashka_bridge
	port map (
		ACLK									=> s_axi_aclk,
		ARESETN									=> s_axi_aresetn,
		ARVALID									=> s_axi_arvalid,
		AWVALID 								=> s_axi_awvalid,
		BREADY									=> s_axi_bready,
		RREADY									=> s_axi_rready,
		WLAST									=> '1',
		WVALID									=> s_axi_wvalid,
		ARADDR(31 downto C_S_AXI_ADDR_WIDTH)	=> (others => '0'),
		ARADDR(C_S_AXI_ADDR_WIDTH-1 downto 0)	=> s_axi_araddr,
		AWADDR(31 downto C_S_AXI_ADDR_WIDTH)	=> (others => '0'),
		AWADDR(C_S_AXI_ADDR_WIDTH-1 downto 0)	=> s_axi_awaddr,
		WDATA									=> s_axi_wdata,
		WSTRB									=> s_axi_wstrb,
		ARREADY									=> s_axi_arready,
		AWREADY									=> s_axi_awready,
		BVALID									=> s_axi_bvalid,
		RLAST									=> open,
		RVALID									=> s_axi_rvalid,
		WREADY									=> s_axi_wready,
		BRESP									=> s_axi_bresp,
		RRESP									=> s_axi_rresp,
		RDATA									=> s_axi_rdata,
		ch_addr_o		=> ch_addr_o,
		ch_rd_data_i	=> ch_rd_data_i,
		ch_wr_data_o	=> ch_wr_data_o,
		ch_rd_mem_o		=> ch_rd_mem_o,
		ch_wr_mem_o		=> ch_wr_mem_o,
		ch_rd_done_i	=> ch_rd_done_i,
		ch_wr_done_i	=> ch_wr_done_i
	);

	axi4lite_cheburashka_bridge_ARESETN <= not RegCtrl_gbLink_Rst;

-- Instantiation of GBLink registers
RegCtrl_gbLink_inst : RegCtrl_gbLink
	port map (
		Clk									=> s_axi_aclk,
		Rst									=> RegCtrl_gbLink_Rst,
		VMEAddr								=> ch_addr_o(C_S_AXI_ADDR_WIDTH-1 downto 2),
		VMERdData							=> ch_rd_data_i,
		VMEWrData							=> ch_wr_data_o,
		VMERdMem							=> ch_rd_mem_o,
		VMEWrMem							=> ch_wr_mem_o,
		VMERdDone							=> ch_rd_done_i,
		VMEWrDone							=> ch_wr_done_i,
		VMERdError							=> open,
		VMEWrError							=> open,
		firmwareID							=> firmwareID,
		firmwareVersion						=> firmwareVersion,
		designerID							=> designerID,
		auroraStatus_linkResetOut			=> aurora_link_reset_out,
		auroraStatus_sysResetOut			=> aurora_sys_reset_out,
		auroraStatus_gtPllLock				=> aurora_gt_pll_lock,
		auroraStatus_laneUp					=> aurora_lane_up,
		auroraStatus_channelUp				=> aurora_channel_up,
		auroraStatus_hardErr				=> aurora_hard_err,
		auroraStatus_softErr				=> aurora_soft_err,
		auroraStatus_initOk					=> auroraStatus_initOk,
		auroraStatus_gtPllLockLatched		=> auroraStatus_gtPllLockLatched,
		auroraStatus_laneUpLatched			=> auroraStatus_laneUpLatched,
		auroraStatus_channelUpLatched		=> auroraStatus_channelUpLatched,
		auroraStatus_hardErrLatched			=> auroraStatus_hardErrLatched,
		auroraStatus_softErrLatched			=> auroraStatus_softErrLatched,
		auroraControl_reset					=> auroraControl_reset,
		auroraControl_gtPllLockClear		=> auroraControl_gtPllLockClear,
		auroraControl_laneUpClear			=> auroraControl_laneUpClear,
		auroraControl_channelUpClear		=> auroraControl_channelUpClear,
		auroraControl_hardErrClear			=> auroraControl_hardErrClear,
		auroraControl_softErrClear			=> auroraControl_softErrClear,
		txStatus_fifoCh1NumData				=> txStatus_fifoCh1NumData,
		txStatus_fifoCh1NumDataTurn			=> txStatus_fifoCh1NumDataTurn,
		txStatus_fifoCh2NumData				=> txStatus_fifoCh2NumData,
		txStatus_fifoCh2NumDataTurn			=> txStatus_fifoCh2NumDataTurn,
		txControls_general_enable			=> txControls_general_enable,
		txControls_general_reset			=> txControls_general_reset,
		txControls_numBuckets				=> txControls_numBuckets,
		txControls_interleaver_enable		=> txControls_interleaver_enable,
		txControls_interleaver_rateRatioCh1	=> txControls_interleaver_rateRatioCh1,
		txControls_interleaver_rateRatioCh2	=> txControls_interleaver_rateRatioCh2,
		txControls_fixedLatency_enable		=> txControls_fixedLatency_enable,
		rxControls_general_enable			=> rxControls_general_enable,
		rxControls_general_reset			=> rxControls_general_reset,
		rxControls_numBuckets				=> rxControls_numBuckets,
		rxControls_interleaver_enable		=> rxControls_interleaver_enable,
		rxControls_interleaver_rateRatioCh1	=> rxControls_interleaver_rateRatioCh1,
		rxControls_interleaver_rateRatioCh2	=> rxControls_interleaver_rateRatioCh2,
		rxControls_fixedLatency_enable		=> rxControls_fixedLatency_enable,
		rxControls_latency					=> rxControls_latency
	);

	txStatus <= (others => '0');

	RegCtrl_gbLink_Rst <= not s_axi_aresetn;

	auroraStatus_initOk <= (aurora_channel_up and aurora_lane_up and aurora_gt_pll_lock);

aurora_reset_logic_inst : aurora_reset_logic
	generic map (
		C_RESET_PB_ASSERT_COUNTER_WIDTH	=> 8,
		C_PMA_INIT_ASSERT_COUNTER_WIDTH	=> 27
	)
	port map (
		clk		=> s_axi_aclk,
		resetn	=> s_axi_aresetn,

		txrxReset => auroraControl_reset,

		aurora_user_clk		=> m_axis_data_aclk,
		aurora_user_resetn	=> m_axis_data_aresetn,

		aurora_reset_pb	=> aurora_reset_pb,
		aurora_pma_init	=> aurora_pma_init
	);

status_reg_latched_inst_default0 : status_reg_latched
	generic map (
		C_STATUS_REG_WIDTH	=> 2,
		C_STATUS_DEFAULT	=> '0'
	)
	port map (
		clk		=> s_axi_aclk,
		resetn	=> s_axi_aresetn,

		status_reg_in		=> (aurora_soft_err & aurora_hard_err),
		status_reg_reset	=> (auroraControl_softErrClear & auroraControl_hardErrClear),
		status_reg_out		=> aurora_status_latched_default0
	);
	auroraStatus_hardErrLatched <= aurora_status_latched_default0(0);
	auroraStatus_softErrLatched <= aurora_status_latched_default0(1);

status_reg_latched_inst_default1 : status_reg_latched
	generic map (
		C_STATUS_REG_WIDTH	=> 3,
		C_STATUS_DEFAULT	=> '1'
	)
	port map (
		clk		=> s_axi_aclk,
		resetn	=> s_axi_aresetn,

		status_reg_in		=> (aurora_channel_up & aurora_lane_up & aurora_gt_pll_lock),
		status_reg_reset	=> (auroraControl_channelUpClear & auroraControl_laneUpClear & auroraControl_gtPllLockClear),
		status_reg_out		=> aurora_status_latched_default1
	);
	auroraStatus_gtPllLockLatched	<= aurora_status_latched_default1(0);
	auroraStatus_laneUpLatched		<= aurora_status_latched_default1(1);
	auroraStatus_channelUpLatched	<= aurora_status_latched_default1(2);

-- Instantiation of GBLink TX logic
gblink_tx_logic_inst : gblink_tx_logic
	generic map (
		C_M_AXIS_DATA_TDATA_WIDTH => C_M_AXIS_DATA_TDATA_WIDTH,
		C_M_AXIS_USER_K_TDATA_WIDTH	=> C_M_AXIS_USER_K_TDATA_WIDTH
	)
	port map (
		user_clk_slower => user_clk_slower,
		user_rst_slower => user_rst_slower,
		user_clk_faster => user_clk_faster,
		user_rst_faster => user_rst_faster,

		s_tx_dsp_ch1_data => s_tx_dsp_ch1_data,
		s_tx_dsp_ch1_valid => s_tx_dsp_ch1_valid,
		s_tx_dsp_ch1_frev => s_tx_dsp_ch1_frev,
		s_tx_dsp_ch1_turn_number => s_tx_dsp_ch1_turn_number,

		s_tx_dsp_ch2_data => s_tx_dsp_ch2_data,
		s_tx_dsp_ch2_valid => s_tx_dsp_ch2_valid,
		s_tx_dsp_ch2_frev => s_tx_dsp_ch2_frev,
		s_tx_dsp_ch2_turn_number => s_tx_dsp_ch2_turn_number,

		s_tx_nco_id => s_tx_nco_id,

		sync_tx => sync_tx,

		m_axis_data_aclk	=> m_axis_data_aclk,
		m_axis_data_aresetn	=> m_axis_data_aresetn,
		m_axis_data_tvalid	=> m_axis_data_tvalid,
		m_axis_data_tdata	=> m_axis_data_tdata,
		m_axis_data_tstrb	=> m_axis_data_tstrb,
		m_axis_data_tlast	=> m_axis_data_tlast,
		m_axis_data_tready	=> m_axis_data_tready,

		m_axis_user_k_aclk	=> m_axis_user_k_aclk,
		m_axis_user_k_aresetn	=> m_axis_user_k_aresetn,
		m_axis_user_k_tvalid	=> m_axis_user_k_tvalid,
		m_axis_user_k_tdata	=> m_axis_user_k_tdata,
		m_axis_user_k_tstrb	=> m_axis_user_k_tstrb,
		m_axis_user_k_tlast	=> m_axis_user_k_tlast,
		m_axis_user_k_tready	=> m_axis_user_k_tready,

		txControls_general_enable => txControls_general_enable,
		txControls_general_reset => txControls_general_reset,
		txControls_numBuckets => txControls_numBuckets,
		txControls_interleaver_enable => txControls_interleaver_enable,
		txControls_interleaver_rateRatioCh1 => txControls_interleaver_rateRatioCh1,
		txControls_interleaver_rateRatioCh2 => txControls_interleaver_rateRatioCh2,
		txControls_fixedLatency_enable => txControls_fixedLatency_enable,

		txStatus_fifoCh1NumData => txStatus_fifoCh1NumData,
		txStatus_fifoCh1NumDataTurn => txStatus_fifoCh1NumDataTurn,
		txStatus_fifoCh2NumData => txStatus_fifoCh2NumData,
		txStatus_fifoCh2NumDataTurn => txStatus_fifoCh2NumDataTurn
	);

-- Instantiation of GBLink RX logic
gblink_rx_logic_inst : gblink_rx_logic
	generic map (
		C_S_AXIS_DATA_TDATA_WIDTH => C_S_AXIS_DATA_TDATA_WIDTH,
		C_S_AXIS_USER_K_TDATA_WIDTH	=> C_S_AXIS_USER_K_TDATA_WIDTH
	)
	port map (
		user_clk_slower => user_clk_slower,
		user_rst_slower => user_rst_slower,
		user_clk_faster => user_clk_faster,
		user_rst_faster => user_rst_faster,

		s_rx_dsp_ch1_read => s_rx_dsp_ch1_read,
		s_rx_dsp_ch1_valid => s_rx_dsp_ch1_valid,
		s_rx_dsp_ch1_data => s_rx_dsp_ch1_data,
		s_rx_dsp_ch1_frev_flag => s_rx_dsp_ch1_frev_flag,
		s_rx_dsp_ch1_bucket_index => s_rx_dsp_ch1_bucket_index,
		s_rx_dsp_ch1_full_error => s_rx_dsp_ch1_full_error,
		s_rx_dsp_ch1_turn_error => s_rx_dsp_ch1_turn_error,
		s_rx_ch1_id => s_rx_ch1_id,

		s_rx_dsp_ch2_read => s_rx_dsp_ch2_read,
		s_rx_dsp_ch2_valid => s_rx_dsp_ch2_valid,
		s_rx_dsp_ch2_data => s_rx_dsp_ch2_data,
		s_rx_dsp_ch2_frev_flag => s_rx_dsp_ch2_frev_flag,
		s_rx_dsp_ch2_bucket_index => s_rx_dsp_ch2_bucket_index,
		s_rx_dsp_ch2_full_error => s_rx_dsp_ch2_full_error,
		s_rx_dsp_ch2_turn_error => s_rx_dsp_ch2_turn_error,
		s_rx_ch2_id => s_rx_ch2_id,

		sync_rx => sync_rx,

		s_axis_data_aclk	=> s_axis_data_aclk,
		s_axis_data_aresetn	=> s_axis_data_aresetn,
		s_axis_data_tready	=> s_axis_data_tready,
		s_axis_data_tdata	=> s_axis_data_tdata,
		s_axis_data_tstrb	=> s_axis_data_tstrb,
		s_axis_data_tlast	=> s_axis_data_tlast,
		s_axis_data_tvalid	=> s_axis_data_tvalid,

		s_axis_user_k_aclk	=> s_axis_user_k_aclk,
		s_axis_user_k_aresetn	=> s_axis_user_k_aresetn,
		s_axis_user_k_tready	=> s_axis_user_k_tready,
		s_axis_user_k_tdata	=> s_axis_user_k_tdata,
		s_axis_user_k_tstrb	=> s_axis_user_k_tstrb,
		s_axis_user_k_tlast	=> s_axis_user_k_tlast,
		s_axis_user_k_tvalid	=> s_axis_user_k_tvalid,

		rxControls_general_enable => rxControls_general_enable,
		rxControls_general_reset => rxControls_general_reset,
		rxControls_numBuckets => rxControls_numBuckets,
		rxControls_interleaver_enable => rxControls_interleaver_enable,
		rxControls_interleaver_rateRatioCh1 => rxControls_interleaver_rateRatioCh1,
		rxControls_interleaver_rateRatioCh2 => rxControls_interleaver_rateRatioCh2,
		rxControls_fixedLatency_enable => rxControls_fixedLatency_enable,
		rxControls_latency => rxControls_latency
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;

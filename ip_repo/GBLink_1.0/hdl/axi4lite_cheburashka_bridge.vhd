library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi4_pkg.all;

entity axi4lite_cheburashka_bridge is
  port (
    ACLK    : in std_logic;
    ARESETN : in std_logic;

    ARVALID : in  std_logic;
    AWVALID : in  std_logic;
    BREADY  : in  std_logic;
    RREADY  : in  std_logic;
    WLAST   : in  std_logic := '1';
    WVALID  : in  std_logic;
    ARADDR  : in  std_logic_vector (31 downto 0);
    AWADDR  : in  std_logic_vector (31 downto 0);
    WDATA   : in  std_logic_vector (31 downto 0);
    WSTRB   : in  std_logic_vector (3 downto 0);
    ARREADY : out std_logic;
    AWREADY : out std_logic;
    BVALID  : out std_logic;
    RLAST   : out std_logic;
    RVALID  : out std_logic;
    WREADY  : out std_logic;
    BRESP   : out std_logic_vector (1 downto 0);
    RRESP   : out std_logic_vector (1 downto 0);
    RDATA   : out std_logic_vector (31 downto 0);

    ch_addr_o    : out std_logic_vector(31 downto 0);
    ch_rd_data_i : in  std_logic_vector(31 downto 0);
    ch_wr_data_o : out std_logic_vector(31 downto 0);
    ch_rd_mem_o  : out std_logic;
    ch_wr_mem_o  : out std_logic;
    ch_rd_done_i : in  std_logic;
    ch_wr_done_i : in  std_logic

    );

end axi4lite_cheburashka_bridge;

architecture rtl of axi4lite_cheburashka_bridge is

  constant c_timeout : integer := 256;

  type t_state is
    (IDLE, ISSUE_WRITE, ISSUE_READ, COMPLETE_WRITE, COMPLETE_READ, RESPONSE_READ, RESPONSE_WRITE);

  signal state : t_state;

  signal count : unsigned(10 downto 0);

begin

  process(ACLK)
  begin
    if rising_edge(ACLK) then
      if ARESETN = '0' then
        ARREADY <= '0';
        AWREADY <= '0';
        BVALID <= '0';
        RVALID <= '0';
        RLAST <= '0';
        WREADY <= '0';

        ch_rd_mem_o  <= '0';
        ch_wr_mem_o  <= '0';
        ch_addr_o    <= (others => '0');
        ch_wr_data_o <= (others => '0');

        state <= IDLE;
      else
        case state is
          when IDLE =>
            ch_rd_mem_o          <= '0';
            ch_wr_mem_o          <= '0';
            ARREADY <= '1';
            AWREADY <= '1';
            WREADY  <= '0';
            BVALID  <= '0';
            BRESP   <= (others => 'X');
            RDATA   <= (others => 'X');
            RRESP   <= (others => 'X');
            RVALID  <= '0';
            RLAST   <= '0';

            if(AWVALID = '1') then
              state     <= ISSUE_WRITE;
              ch_addr_o <= AWADDR;
            elsif (ARVALID = '1') then
              state     <= ISSUE_READ;
              ch_addr_o <= ARADDR;
            end if;

          when ISSUE_WRITE =>
            WREADY <= '1';


            if(WVALID = '1') then
              ch_wr_mem_o  <= '1';
              ch_wr_data_o <= WDATA;
              state        <= COMPLETE_WRITE;
            end if;

          when ISSUE_READ =>
            ch_rd_mem_o         <= '1';
            RVALID <= '0';
            RLAST  <= '0';
            state               <= COMPLETE_READ;

          when COMPLETE_READ =>
            ch_rd_mem_o         <= '0';

            if ch_rd_done_i = '1' then
              state               <= RESPONSE_READ;
--              RRESP  <= c_AXI4_RESP_EXOKAY;
              RRESP  <= c_AXI4_RESP_OKAY;
              RDATA  <= ch_rd_data_i;
              RVALID <= '1';
              RLAST  <= '1';
            end if;

          when COMPLETE_WRITE =>
            ch_wr_mem_o         <= '0';

            if ch_wr_done_i = '1' then
              state               <= RESPONSE_WRITE;
              BVALID <= '1';
--              BRESP  <= c_AXI4_RESP_EXOKAY;
              BRESP  <= c_AXI4_RESP_OKAY;
            end if;

          when RESPONSE_WRITE =>
            if (BREADY = '1') then
              BVALID <= '0';
              state               <= IDLE;
            end if;

          when RESPONSE_READ =>
            if (RREADY = '1') then
              RVALID <= '0';
              state               <= IDLE;
            end if;
        end case;
      end if;
    end if;
  end process;

end rtl;


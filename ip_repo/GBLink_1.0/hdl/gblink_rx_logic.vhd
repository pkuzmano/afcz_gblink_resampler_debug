library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gblink_rx_logic is
	generic (
		-- Parameters of Axi Slave Bus Interface S_AXIS_DATA
		C_S_AXIS_DATA_TDATA_WIDTH	: integer	:= 64;
		--C_S_AXIS_DATA_START_COUNT	: integer	:= 32;

		-- Parameters of Axi Slave Bus Interface S_AXIS_USER_K
		C_S_AXIS_USER_K_TDATA_WIDTH	: integer	:= 64
		--C_S_AXIS_USER_K_START_COUNT	: integer	:= 32;
	);
	port (
		-- Clock/reset ports
		user_clk_slower : in std_logic; -- 78.125 (125)
		user_rst_slower : in std_logic;
		user_clk_faster : in std_logic; -- 156.25 (250)
		user_rst_faster : in std_logic;

		-- RX logic interface
		-- DSP, ch1
		s_rx_dsp_ch1_read : in std_logic;
		s_rx_dsp_ch1_valid : out std_logic;
		s_rx_dsp_ch1_data : out std_logic_vector(31 downto 0);
		s_rx_dsp_ch1_frev_flag : out std_logic;
		s_rx_dsp_ch1_bucket_index : out std_logic_vector(15 downto 0);
		s_rx_dsp_ch1_full_error : out std_logic;
		s_rx_dsp_ch1_turn_error : out std_logic;
		s_rx_ch1_id : out std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction
		-- DSP, ch2
		s_rx_dsp_ch2_read : in std_logic;
		s_rx_dsp_ch2_valid : out std_logic;
		s_rx_dsp_ch2_data : out std_logic_vector(31 downto 0);
		s_rx_dsp_ch2_frev_flag : out std_logic;
		s_rx_dsp_ch2_bucket_index : out std_logic_vector(15 downto 0);
		s_rx_dsp_ch2_full_error : out std_logic;
		s_rx_dsp_ch2_turn_error : out std_logic;
		s_rx_ch2_id : out std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction

		-- White Rabbit: synchronisation for fixed-latency mode
		sync_rx : in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXIS_DATA
		s_axis_data_aclk	: in std_logic;
		s_axis_data_aresetn	: in std_logic;
		s_axis_data_tready	: out std_logic; -- Aurora doesn't have "m_axi_rx_tready", so not needed
		s_axis_data_tdata	: in std_logic_vector(C_S_AXIS_DATA_TDATA_WIDTH-1 downto 0);
		s_axis_data_tstrb	: in std_logic_vector((C_S_AXIS_DATA_TDATA_WIDTH/8)-1 downto 0);
		s_axis_data_tlast	: in std_logic;
		s_axis_data_tvalid	: in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXIS_USER_K
		s_axis_user_k_aclk	: in std_logic;
		s_axis_user_k_aresetn	: in std_logic;
		s_axis_user_k_tready	: out std_logic; -- Aurora doesn't have "m_axi_rx_user_k_tready", so not needed
		s_axis_user_k_tdata	: in std_logic_vector(C_S_AXIS_USER_K_TDATA_WIDTH-1 downto 0);
		s_axis_user_k_tstrb	: in std_logic_vector((C_S_AXIS_USER_K_TDATA_WIDTH/8)-1 downto 0);
		s_axis_user_k_tlast	: in std_logic;
		s_axis_user_k_tvalid	: in std_logic;

		rxControls_general_enable : in std_logic;
		rxControls_general_reset : in std_logic;
		rxControls_numBuckets : in std_logic_vector(15 downto 0);
		rxControls_interleaver_enable : in std_logic;
		rxControls_interleaver_rateRatioCh1 : in std_logic_vector(5 downto 4);
		rxControls_interleaver_rateRatioCh2 : in std_logic_vector(9 downto 8);
		rxControls_fixedLatency_enable : in std_logic;
		rxControls_latency : in std_logic_vector(15 downto 0)
	);
end entity gblink_rx_logic;

architecture arch_behav of gblink_rx_logic is

	-- Shift right with ceiling rounding
	-- x : input data
	-- n : number of bits to shift right, minus 1 (0, 1, 2, or 3)
	function ceil_shift_right(x : integer; n : integer) return integer is
	  variable x_vector : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(x, 16));
	  variable n_shift : integer := n + 1;
	begin
	  if (n_shift = 1) then
	    if (x_vector(0) = '1') then -- if any residue
		  return x/2 + 1;
		else
		  return x/2;
		end if;
	  elsif (n_shift = 2) then
	    if ((x_vector(0) = '1') or (x_vector(1) = '1')) then -- if any residue
		  return x/4 + 1;
		else
		  return x/4;
		end if;
	  elsif (n_shift = 3) then
	    if ((x_vector(0) = '1') or (x_vector(1) = '1') or (x_vector(2) = '1')) then -- if any residue
		  return x/8 + 1;
		else
		  return x/8;
		end if;
	  elsif (n_shift = 4) then
	    if ((x_vector(0) = '1') or (x_vector(1) = '1') or (x_vector(2) = '1') or (x_vector(3) = '1')) then -- if any residue
		  return x/16 + 1;
		else
		  return x/16;
		end if;
	  else
	    return 0;
	  end if;
	end;

	component status_reg_latched is
		generic (
		C_STATUS_REG_WIDTH	: integer := 1;
		C_STATUS_DEFAULT	: std_logic := '0'
		);
		port (
		clk		: in std_logic;
		resetn	: in std_logic;

		status_reg_in		: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_reset	: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_out		: out std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0)
		);
	end component status_reg_latched;

	component fifo_generator_gblink_rx_buffer is
		port (
		clk : in std_logic;
		srst : in std_logic;
		din : in std_logic_vector(63 downto 0);
		wr_en : in std_logic;
		rd_en : in std_logic;
		dout : out std_logic_vector(63 downto 0);
		full : out std_logic;
		wr_ack : out std_logic;
		overflow : out std_logic;
		empty : out std_logic;
		valid : out std_logic;
		underflow : out std_logic;
		wr_rst_busy : out std_logic;
		rd_rst_busy : out std_logic
		);
	end component fifo_generator_gblink_rx_buffer;

	component fifo_generator_gblink_rx is
		port (
		clk : in std_logic;
		srst : in std_logic;
		din : in std_logic_vector(35 downto 0);
		wr_en : in std_logic;
		rd_en : in std_logic;
		dout : out std_logic_vector(35 downto 0);
		full : out std_logic;
		wr_ack : out std_logic;
		overflow : out std_logic;
		empty : out std_logic;
		valid : out std_logic;
		underflow : out std_logic;
		wr_rst_busy : out std_logic;
		rd_rst_busy : out std_logic
		);
	end component fifo_generator_gblink_rx;

	type gblink_rx_fifo_state_t is (FIFO_UNINIT, FIFO_DISABLED, FIFO_USER_K_WAIT, FIFO_DATA_WAIT);
	signal gblink_rx_fifo_state : gblink_rx_fifo_state_t;
	signal gblink_rx_fifo_state_next : gblink_rx_fifo_state_t;

	type gblink_rx_fsm_state_t is (FSM_DISABLED, FSM_FREV_WAIT, FSM_FREV_STORE, FSM_FREV_STORE_WAIT, FSM_SYNC_WAIT, FSM_DATA_WAIT, FSM_DATA_STORE, FSM_DATA_STORE_WAIT);
	signal gblink_rx_fsm_state : gblink_rx_fsm_state_t;
	signal gblink_rx_fsm_state_next : gblink_rx_fsm_state_t;

	type gblink_rx_latency_state_t is (LAT_DISABLED, LAT_SYNC_WAIT, LAT_WAIT, LAT_STREAMING);
	signal gblink_rx_latency_state : gblink_rx_latency_state_t;
	signal gblink_rx_latency_state_next : gblink_rx_latency_state_t;

	type fifo_rx_din_t is array (2 downto 1) of std_logic_vector(35 downto 0);
	type fifo_rx_dout_t is array (2 downto 1) of std_logic_vector(35 downto 0);

	type fifo_rx_level_t is array (2 downto 1) of std_logic_vector(10 downto 0); -- 1024 (actual 1025) FIFO depth
	type fifo_rx_num_data_t is array (2 downto 1) of std_logic_vector(15 downto 0); -- 4620 buckets (but, 16b width for easier comparison with expected number of data)

	signal fifo_rx_buffer_srst : std_logic;
	signal fifo_rx_buffer_din : std_logic_vector(63 downto 0);
	signal fifo_rx_buffer_wr_en : std_logic;
	signal fifo_rx_buffer_rd_en : std_logic;
	signal fifo_rx_buffer_dout : std_logic_vector(63 downto 0);
	signal fifo_rx_buffer_full : std_logic;
	signal fifo_rx_buffer_wr_ack : std_logic;
	signal fifo_rx_buffer_overflow : std_logic;
	signal fifo_rx_buffer_empty : std_logic;
	signal fifo_rx_buffer_valid : std_logic;
	signal fifo_rx_buffer_underflow : std_logic;
	signal fifo_rx_buffer_wr_rst_busy : std_logic;
	signal fifo_rx_buffer_rd_rst_busy : std_logic;

	signal fifo_rx_buffer_wr_en_del : std_logic;
	signal rxStatus_fifo_buffer_wr_error : std_logic;
	signal rxStatus_fifoBufferWrErr : std_logic; -- should be output (MM)
	signal rxStatus_fifoBufferWrErrLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferWrErrClear : std_logic := '0'; -- should be input (MM)
	signal rxStatus_fifoBufferFull : std_logic; -- should be output (MM)
	signal rxStatus_fifoBufferFullLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferFullClear : std_logic := '0'; -- should be input (MM)
	signal rxStatus_fifoBufferEmpty : std_logic; -- should be output (MM)
	signal rxStatus_fifoBufferOverflow : std_logic; -- should be output (MM)... do we need non-latched version in the MM?
	signal rxStatus_fifoBufferOverflowLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferOverflowClear : std_logic := '0'; -- should be input (MM)
	signal rxStatus_fifoBufferUnderflow : std_logic; -- should be output (MM)... do we need non-latched version in the MM?
	signal rxStatus_fifoBufferUnderflowLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferUnderflowClear : std_logic := '0'; -- should be input (MM)
	signal fifo_rx_buffer_level : std_logic_vector(10 downto 0); -- 1024 (actual 1025) FIFO depth
	signal rxStatus_fifoBufferLevel : std_logic_vector (10 downto 0); -- should be output (MM)
	signal fifo_rx_buffer_num_data : std_logic_vector(15 downto 0); -- 4620 buckets (but, 16b width for easier comparison with expected number of data)
	signal rxStatus_fifoBufferNumData : std_logic_vector(15 downto 0); -- should be output (MM)
	signal fifo_rx_buffer_num_data_low_error : std_logic;
	signal rxStatus_fifoBufferNumDataLowErr : std_logic; -- should be output (MM)
	signal rxStatus_fifoBufferNumDataErrLowLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferNumDataErrLowClear : std_logic := '0'; -- should be input (MM)
	signal fifo_rx_buffer_num_data_high_error : std_logic;
	signal rxStatus_fifoBufferNumDataHighErr : std_logic; -- should be output (MM)
	signal rxStatus_fifoBufferNumDataErrHighLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferNumDataErrHighClear : std_logic := '0'; -- should be input (MM)
	signal fifo_rx_buffer_frev_error : std_logic;
	signal rxStatus_fifoBufferFrevErr : std_logic; -- should be output (MM)
	signal rxStatus_fifoBufferFrevErrLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferFrevErrClear : std_logic := '0'; -- should be input (MM)
	signal fifo_rx_buffer_sync_error : std_logic;
	signal rxStatus_fifoBufferSyncErr : std_logic; -- should be output (MM)
	signal rxStatus_fifoBufferSyncErrLatched : std_logic; -- should be output (MM)
	signal rxControls_fifoBufferSyncErrClear : std_logic := '0'; -- should be input (MM)
	signal fifo_rx_buffer_wr_rst_done : std_logic;
	signal rxStatus_fifoBufferWrRstDone : std_logic; -- should be output (MM)
	signal fifo_rx_buffer_rd_rst_done : std_logic;
	signal rxStatus_fifoBufferRdRstDone : std_logic; -- should be output (MM)

	signal fifo_rx_srst : std_logic_vector(2 downto 1);
	signal fifo_rx_din : fifo_rx_din_t;
	signal fifo_rx_wr_en : std_logic_vector(2 downto 1);
	signal fifo_rx_rd_en : std_logic_vector(2 downto 1);
	signal fifo_rx_dout : fifo_rx_dout_t;
	signal fifo_rx_full : std_logic_vector(2 downto 1);
	signal fifo_rx_wr_ack : std_logic_vector(2 downto 1);
	signal fifo_rx_overflow : std_logic_vector(2 downto 1);
	signal fifo_rx_empty : std_logic_vector(2 downto 1);
	signal fifo_rx_valid : std_logic_vector(2 downto 1);
	signal fifo_rx_underflow : std_logic_vector(2 downto 1);
	signal fifo_rx_wr_rst_busy : std_logic_vector(2 downto 1);
	signal fifo_rx_rd_rst_busy : std_logic_vector(2 downto 1);

	signal fifo_rx_wr_en_del : std_logic_vector(2 downto 1);
	signal rxStatus_fifo_wr_error : std_logic_vector(2 downto 1);
	signal rxStatus_fifoWrErr : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxStatus_fifoWrErrLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxControls_fifoWrErrClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal rxStatus_fifoFull : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxStatus_fifoFullLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxControls_fifoFullClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal rxStatus_fifoEmpty : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxStatus_fifoOverflow : std_logic_vector(2 downto 1); -- should be output (MM)... do we need non-latched version in the MM?
	signal rxStatus_fifoOverflowLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxControls_fifoOverflowClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal rxStatus_fifoUnderflow : std_logic_vector(2 downto 1); -- should be output (MM)... do we need non-latched version in the MM?
	signal rxStatus_fifoUnderflowLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxControls_fifoUnderflowClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal fifo_rx_level : fifo_rx_level_t;
	signal rxStatus_fifoLevel : fifo_rx_level_t; -- should be output (MM)
	signal fifo_rx_num_data : fifo_rx_num_data_t;
	signal rxStatus_fifoNumData : fifo_rx_num_data_t; -- should be output (MM)
	signal fifo_rx_wr_rst_done : std_logic_vector(2 downto 1);
	signal rxStatus_fifoWrRstDone : std_logic_vector(2 downto 1); -- should be output (MM)
	signal fifo_rx_rd_rst_done : std_logic_vector(2 downto 1);
	signal rxStatus_fifoRdRstDone : std_logic_vector(2 downto 1); -- should be output (MM)
	signal fifo_rx_rst_done : std_logic_vector(2 downto 1);

	signal rxControls_general_enable_del : std_logic;
	signal rxControls_general_reset_del : std_logic;
	--signal fifo_rx_buffer_rst_busy : std_logic;
	signal fifo_rx_buffer_rst_done : std_logic;

	signal rxControls_numBuckets_latch : std_logic_vector(15 downto 0);
	signal rxControls_interleaver_enable_latch : std_logic;
	signal rxControls_interleaver_rateRatioCh1_latch : std_logic_vector(5 downto 4);
	signal rxControls_interleaver_rateRatioCh2_latch : std_logic_vector(9 downto 8);
	signal expectedNumData_ch1 : std_logic_vector(15 downto 0);
	signal expectedNumData_ch2 : std_logic_vector(15 downto 0);
	signal expectedNumDataPackets : std_logic_vector(15 downto 0);
	signal rxControls_fixedLatency_enable_latch : std_logic;
	signal rxControls_latency_latch : std_logic_vector(15 downto 0);

	signal rx_logic_status_default0 : std_logic_vector(15 downto 0);
	signal rx_logic_status_clear_default0 : std_logic_vector(15 downto 0);
	signal rx_logic_status_latched_default0 : std_logic_vector(15 downto 0);

	signal rx_logic_status_default1_slow : std_logic_vector(1 downto 0);
	signal rx_logic_status_clear_default1_slow : std_logic_vector(1 downto 0);
	signal rx_logic_status_latched_default1_slow : std_logic_vector(1 downto 0);

	signal rx_logic_status_default1_fast : std_logic_vector(5 downto 0);
	signal rx_logic_status_clear_default1_fast : std_logic_vector(5 downto 0);
	signal rx_logic_status_latched_default1_fast : std_logic_vector(5 downto 0);

	signal data_present : std_logic_vector(2 downto 1);
	signal data_present_counter : std_logic_vector(2 downto 0); -- 3-bit counter enough for ratios 0, 1, 2 and 3
	signal data_store_done : std_logic_vector(2 downto 1);
	signal data_store_done_msw_ch1 : std_logic;
	signal data_read_counter : fifo_rx_num_data_t;

	signal rx_data_buffer : std_logic_vector(63 downto 0);
	signal rx_data_buffer_msw : std_logic_vector(31 downto 0);
	signal rx_data_buffer_lsw : std_logic_vector(31 downto 0);

	signal latency_counter : std_logic_vector(15 downto 0);
	signal streaming_counter : std_logic_vector(1 downto 0);
	signal fifo_rx_valid_latched : std_logic_vector(2 downto 1);
	signal fifo_rx_dout_latched : fifo_rx_dout_t;
	signal stream_valid : std_logic_vector(2 downto 1);
	signal stream_cut_error : std_logic_vector(2 downto 1);
	signal rxStatus_streamCutErr : std_logic_vector(2 downto 1); -- should be output (MM)
	signal rxControls_streamCutErrClear : std_logic_vector(2 downto 1); -- should be input (MM)

	signal s_rx_dsp_ch1_valid_int : std_logic;
	signal s_rx_dsp_ch2_valid_int : std_logic;

	constant C_FREV_FIFO_ENTRY : std_logic_vector(35 downto 0) := x"F00000000";
	constant C_FREV_USER_K_DATA : std_logic_vector(63 downto 0) := x"FFFFFFFFFFFFFFFF";
	constant C_SYNC_USER_K_DATA : std_logic_vector(63 downto 0) := x"1111111111111111";

	constant C_SLOW_CLK_PERIOD_NS : integer := 8; -- 125MHz
	constant C_FAST_CLK_PERIOD_NS : integer := 4; -- 250MHz
	--constant C_FAST_CLK_CYCLES_IN_US : integer := 1000/C_FAST_CLK_PERIOD_NS;

begin

	-- FIFO state transition (slower clock)
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  gblink_rx_fifo_state <= FIFO_UNINIT;
		else
		  gblink_rx_fifo_state <= gblink_rx_fifo_state_next;
		end if;
	  end if;
	end process;

	-- FSM state transition (faster clock)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  gblink_rx_fsm_state <= FSM_DISABLED;
		else
		  gblink_rx_fsm_state <= gblink_rx_fsm_state_next;
		end if;
	  end if;
	end process;

	-- LAT state transition (faster clock)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  gblink_rx_latency_state <= LAT_DISABLED;
		else
		  gblink_rx_latency_state <= gblink_rx_latency_state_next;
		end if;
	  end if;
	end process;

	-- FIFO next state logic
	process (gblink_rx_fifo_state, fifo_rx_buffer_rst_done, fifo_rx_rst_done, rxControls_general_reset_del, rxControls_general_enable, fifo_rx_buffer_wr_ack, rxControls_fixedLatency_enable_latch, fifo_rx_buffer_num_data, expectedNumDataPackets) begin
	  case gblink_rx_fifo_state is
	    when FIFO_UNINIT =>
		  if ((fifo_rx_buffer_rst_done = '1') and (fifo_rx_rst_done = "11")) then
		    gblink_rx_fifo_state_next <= FIFO_DISABLED; -- wait for the FIFO reset sequence to complete
		  else
		    gblink_rx_fifo_state_next <= FIFO_UNINIT;
		  end if;
		when FIFO_DISABLED =>
		  if (rxControls_general_reset_del = '1') then
		    gblink_rx_fifo_state_next <= FIFO_UNINIT; -- another reset issued before the enable
		  elsif (rxControls_general_enable = '1') then
		    gblink_rx_fifo_state_next <= FIFO_USER_K_WAIT; -- wait for the enable through the MM
		  else
		    gblink_rx_fifo_state_next <= FIFO_DISABLED;
		  end if;
		when FIFO_USER_K_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fifo_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (fifo_rx_buffer_wr_ack = '1') then
		    gblink_rx_fifo_state_next <= FIFO_DATA_WAIT; -- wait for the USER_K data to start the initial turn
		  else
		    gblink_rx_fifo_state_next <= FIFO_USER_K_WAIT;
		  end if;
		when FIFO_DATA_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fifo_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (rxControls_fixedLatency_enable_latch = '0') then -- beam-synchronous mode
		    if (fifo_rx_buffer_num_data = expectedNumDataPackets) then
			  gblink_rx_fifo_state_next <= FIFO_USER_K_WAIT; -- expected number of data read, discard any excess
			else
			  gblink_rx_fifo_state_next <= FIFO_DATA_WAIT;
			end if;
		  else -- fixed-latency mode
		    gblink_rx_fifo_state_next <= FIFO_DATA_WAIT; -- no notion of turn
		  end if;
		when others =>
		  null;
	  end case;
	end process;

	-- FSM next state logic
	process (gblink_rx_fsm_state, rxControls_general_enable, rxControls_fixedLatency_enable, fifo_rx_buffer_valid, fifo_rx_buffer_dout, fifo_rx_wr_ack, rxControls_interleaver_enable_latch, rxControls_fixedLatency_enable_latch, data_store_done, data_store_done_msw_ch1, fifo_rx_num_data, expectedNumData_ch1, expectedNumData_ch2) begin
	  case gblink_rx_fsm_state is
	    when FSM_DISABLED =>
		  if (rxControls_general_enable = '1') then -- wait for the enable through the MM
		    if (rxControls_fixedLatency_enable = '0') then
			  gblink_rx_fsm_state_next <= FSM_FREV_WAIT; -- beam-synchronous mode
			else
			  gblink_rx_fsm_state_next <= FSM_SYNC_WAIT; -- fixed-latency mode
			end if;
		  else
		    gblink_rx_fsm_state_next <= FSM_DISABLED;
		  end if;
		when FSM_FREV_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_rx_buffer_valid = '1') and (fifo_rx_buffer_dout = C_FREV_USER_K_DATA)) then
		    gblink_rx_fsm_state_next <= FSM_FREV_STORE; -- USER_K data read from the buffer FIFO
		  else
		    gblink_rx_fsm_state_next <= FSM_FREV_WAIT;
		  end if;
		when FSM_FREV_STORE =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  else
		    gblink_rx_fsm_state_next <= FSM_FREV_STORE_WAIT; -- go to the next state right away because "fifo_rx_wr_en" should be active for only 1 clock cycle, and not 2
		  end if;
		when FSM_FREV_STORE_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_rx_wr_ack(1) = '1') and (fifo_rx_wr_ack(2) = rxControls_interleaver_enable_latch)) then
		    gblink_rx_fsm_state_next <= FSM_DATA_WAIT; -- FREV entry stored in the ch1/ch2 FIFOs
		  else
		    gblink_rx_fsm_state_next <= FSM_FREV_STORE_WAIT;
		  end if;
		when FSM_SYNC_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_rx_buffer_valid = '1') and (fifo_rx_buffer_dout = C_SYNC_USER_K_DATA)) then
		    gblink_rx_fsm_state_next <= FSM_DATA_WAIT; -- no need to store USER_K data in the ch1/ch2 FIFOs
		  else
		    gblink_rx_fsm_state_next <= FSM_SYNC_WAIT;
		  end if;
		when FSM_DATA_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_rx_buffer_valid = '1') and (fifo_rx_buffer_dout /= C_FREV_USER_K_DATA) and (fifo_rx_buffer_dout /= C_SYNC_USER_K_DATA)) then
		    -- if (data_present /= "00") then
			  -- gblink_rx_fsm_state_next <= FSM_DATA_STORE; -- valid data read from the buffer FIFO
			-- else
			  -- gblink_rx_fsm_state_next <= FSM_DATA_WAIT; -- don't save dummy data
			-- end if;
			gblink_rx_fsm_state_next <= FSM_DATA_STORE; -- valid data read from the buffer FIFO
		  else
		    gblink_rx_fsm_state_next <= FSM_DATA_WAIT;
		  end if;
		when FSM_DATA_STORE =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  else
		    gblink_rx_fsm_state_next <= FSM_DATA_STORE_WAIT; -- go to the next state right away because "fifo_rx_wr_en" should be active for only 1 clock cycle, and not 2
		  end if;
		when FSM_DATA_STORE_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif (rxControls_interleaver_enable_latch = '0') then
			if (fifo_rx_wr_ack(1) = '1') then
			  if ((rxControls_fixedLatency_enable_latch = '0') and (fifo_rx_num_data(1) = std_logic_vector(unsigned(expectedNumData_ch1) - 1))) then
			    gblink_rx_fsm_state_next <= FSM_FREV_WAIT; -- last data of the turn is being written (beam-synchronous mode)
			  elsif (data_store_done_msw_ch1 = '0') then
			    gblink_rx_fsm_state_next <= FSM_DATA_STORE; -- no interleaving, MSW written to ch1 FIFO
			  else
			    gblink_rx_fsm_state_next <= FSM_DATA_WAIT; -- LSW written to ch1 FIFO, more data expected
			  end if;
			else
			  gblink_rx_fsm_state_next <= FSM_DATA_STORE_WAIT;
			end if;
		  elsif (data_store_done = "11") then
		    if ((rxControls_fixedLatency_enable_latch = '0') and (fifo_rx_num_data(1) = expectedNumData_ch1) and (fifo_rx_num_data(2) = expectedNumData_ch2)) then
			  gblink_rx_fsm_state_next <= FSM_FREV_WAIT; -- expected number of data has already been read in the current turn (beam-synchronous mode)
			else
			  gblink_rx_fsm_state_next <= FSM_DATA_WAIT; -- more data expected in the current turn
			end if;
		  else
		    gblink_rx_fsm_state_next <= FSM_DATA_STORE_WAIT;
		  end if;
		when others =>
		  null;
	  end case;
	end process;

	-- LAT next state logic
	process (gblink_rx_latency_state, rxControls_general_enable, rxControls_fixedLatency_enable_latch, sync_rx, latency_counter, rxControls_latency_latch) begin
	  case gblink_rx_latency_state is
		when LAT_DISABLED =>
		  if (rxControls_general_enable = '1') then
		    gblink_rx_latency_state_next <= LAT_SYNC_WAIT; -- wait for the enable through the MM
		  else
		    gblink_rx_latency_state_next <= LAT_DISABLED;
		  end if;
		when LAT_SYNC_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_latency_state_next <= LAT_DISABLED; -- suddenly disabled
		  elsif ((rxControls_fixedLatency_enable_latch = '1') and (sync_rx = '1')) then
		    gblink_rx_latency_state_next <= LAT_WAIT; -- SYNC pulse detected (fixed-latency mode)
		  else
		    gblink_rx_latency_state_next <= LAT_SYNC_WAIT;
		  end if;
		when LAT_WAIT =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_latency_state_next <= LAT_DISABLED; -- suddenly disabled
		  --elsif (latency_counter = std_logic_vector(to_unsigned(C_FAST_CLK_CYCLES_IN_US*to_integer(unsigned(rxControls_latency_latch)), latency_counter'length) - 1)) then
		  elsif (latency_counter = std_logic_vector(to_unsigned(2*to_integer(unsigned(rxControls_latency_latch)), latency_counter'length) - 1)) then -- factor 2 is due to faster/slower clock ratio
		    gblink_rx_latency_state_next <= LAT_STREAMING; -- count until desired latency is reached
		  else
		    gblink_rx_latency_state_next <= LAT_WAIT;
		  end if;
		when LAT_STREAMING =>
		  if (rxControls_general_enable = '0') then
		    gblink_rx_latency_state_next <= LAT_DISABLED; -- suddenly disabled
		  else
		    gblink_rx_latency_state_next <= LAT_STREAMING;
		  end if;
		when others =>
		  null;
	  end case;
	end process;

	s_axis_data_tready <= '1'; -- not used by Aurora, incoming data needs to be buffered
	--s_axis_user_k_tready <= '1'; -- not used by Aurora, incoming data needs to be buffered
	s_axis_user_k_tready <= s_axis_user_k_tvalid; -- fix: DATA/USER_K ready signals begave differently

	-- Synchronous RX buffer FIFO reset: through MM
	-- Also: register stage for the general reset/enable
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  fifo_rx_buffer_srst <= '0';
		  rxControls_general_enable_del <= '0';
		  rxControls_general_reset_del <= '0';
		else
		  fifo_rx_buffer_srst <= rxControls_general_reset;
		  rxControls_general_enable_del <= rxControls_general_enable;
		  rxControls_general_reset_del <= rxControls_general_reset;
		end if;
	  end if;
	end process;

	-- Synchronous RX ch1/ch2 FIFOs reset: through MM
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_rx_srst(1) <= '0';
		  fifo_rx_srst(2) <= '0';
		else
		  fifo_rx_srst(1) <= rxControls_general_reset;
		  fifo_rx_srst(2) <= rxControls_general_reset;
		end if;
	  end if;
	end process;

	-- Latch RX controls upon enable
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  rxControls_numBuckets_latch <= (others => '0');
		  rxControls_interleaver_enable_latch <= '0';
		  rxControls_interleaver_rateRatioCh1_latch <= (others => '0');
		  rxControls_interleaver_rateRatioCh2_latch <= (others => '0');
		  expectedNumData_ch1 <= (others => '0');
		  expectedNumData_ch2 <= (others => '0');
		  expectedNumDataPackets <= (others => '0');
		  rxControls_fixedLatency_enable_latch <= '0';
		  rxControls_latency_latch <= (others => '0');
		else
		  if ((rxControls_general_enable = '1') and (rxControls_general_enable_del = '0')) then -- save the parameters on the rising edge of enable signal
		    rxControls_numBuckets_latch <= rxControls_numBuckets;
			rxControls_interleaver_enable_latch <= rxControls_interleaver_enable;
			rxControls_interleaver_rateRatioCh1_latch <= rxControls_interleaver_rateRatioCh1;
			rxControls_interleaver_rateRatioCh2_latch <= rxControls_interleaver_rateRatioCh2;
			if (rxControls_interleaver_enable = '1') then
			  expectedNumData_ch1 <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(rxControls_numBuckets)), to_integer(unsigned(rxControls_interleaver_rateRatioCh1))), expectedNumData_ch1'length));
			  expectedNumData_ch2 <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(rxControls_numBuckets)), to_integer(unsigned(rxControls_interleaver_rateRatioCh2))), expectedNumData_ch2'length));
			  if (to_integer(unsigned(rxControls_interleaver_rateRatioCh1)) < to_integer(unsigned(rxControls_interleaver_rateRatioCh2))) then
			    expectedNumDataPackets <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(rxControls_numBuckets)), to_integer(unsigned(rxControls_interleaver_rateRatioCh1))), expectedNumDataPackets'length));
			  else
			    expectedNumDataPackets <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(rxControls_numBuckets)), to_integer(unsigned(rxControls_interleaver_rateRatioCh2))), expectedNumDataPackets'length));
			  end if;
			else
			  expectedNumData_ch1 <= rxControls_numBuckets;
			  expectedNumData_ch2 <= (others => '0');
			  expectedNumDataPackets <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(rxControls_numBuckets)), 0), expectedNumDataPackets'length));
			end if;
			rxControls_fixedLatency_enable_latch <= rxControls_fixedLatency_enable;
			rxControls_latency_latch <= rxControls_latency;
		  else
		    null;
		  end if;
		end if;
	  end if;
	end process;

-- Reset complete flags for the buffer FIFO (slow clock)
fifo_buffer_reset_inst_default1 : status_reg_latched
	generic map (
		C_STATUS_REG_WIDTH	=> 2,
		C_STATUS_DEFAULT	=> '1'
	)
	port map (
		clk		=> user_clk_slower,
		resetn	=> user_rst_slower,

		status_reg_in		=> rx_logic_status_default1_slow,
		status_reg_reset	=> rx_logic_status_clear_default1_slow,
		status_reg_out		=> rx_logic_status_latched_default1_slow
	);
	rx_logic_status_default1_slow <= fifo_rx_buffer_rd_rst_busy &
	                                 fifo_rx_buffer_wr_rst_busy;
	rx_logic_status_clear_default1_slow <= fifo_rx_buffer_srst &
	                                       fifo_rx_buffer_srst;
	fifo_rx_buffer_wr_rst_done <= not rx_logic_status_latched_default1_slow(0);
	rxStatus_fifoBufferWrRstDone <= fifo_rx_buffer_wr_rst_done;
	fifo_rx_buffer_rd_rst_done <= not rx_logic_status_latched_default1_slow(1);
	rxStatus_fifoBufferRdRstDone <= fifo_rx_buffer_rd_rst_done;

-- Reset complete flags for the ch1/ch2 FIFOs and read engine statuses (fast clock)
fifo_reset_inst_default1 : status_reg_latched
	generic map (
		C_STATUS_REG_WIDTH	=> 6,
		C_STATUS_DEFAULT	=> '1'
	)
	port map (
		clk		=> user_clk_faster,
		resetn	=> user_rst_faster,

		status_reg_in		=> rx_logic_status_default1_fast,
		status_reg_reset	=> rx_logic_status_clear_default1_fast,
		status_reg_out		=> rx_logic_status_latched_default1_fast
	);
	rx_logic_status_default1_fast <= stream_valid(2) &
	                                 stream_valid(1) &
	                                 fifo_rx_rd_rst_busy(2) &
	                                 fifo_rx_wr_rst_busy(2) &
								     fifo_rx_rd_rst_busy(1) &
								     fifo_rx_wr_rst_busy(1);
	stream_valid(1) <= s_rx_dsp_ch1_valid_int when (gblink_rx_latency_state = LAT_STREAMING) else
	                   '0';
	stream_valid(2) <= s_rx_dsp_ch2_valid_int when (gblink_rx_latency_state = LAT_STREAMING) else
	                   '0';
	rx_logic_status_clear_default1_fast <= rxControls_streamCutErrClear(2) &
	                                       rxControls_streamCutErrClear(1) &
	                                       fifo_rx_srst(2) &
	                                       fifo_rx_srst(2) &
									       fifo_rx_srst(1) &
									       fifo_rx_srst(1);
	fifo_rx_wr_rst_done(1) <= not rx_logic_status_latched_default1_fast(0);
	rxStatus_fifoWrRstDone(1) <= fifo_rx_wr_rst_done(1);
	fifo_rx_rd_rst_done(1) <= not rx_logic_status_latched_default1_fast(1);
	rxStatus_fifoRdRstDone(1) <= fifo_rx_rd_rst_done(1);
	fifo_rx_wr_rst_done(2) <= not rx_logic_status_latched_default1_fast(2);
	rxStatus_fifoWrRstDone(2) <= fifo_rx_wr_rst_done(2);
	fifo_rx_rd_rst_done(2) <= not rx_logic_status_latched_default1_fast(3);
	rxStatus_fifoRdRstDone(2) <= fifo_rx_rd_rst_done(2);
	stream_cut_error(1) <= rx_logic_status_latched_default1_fast(4);
	rxStatus_streamCutErr(1) <= not stream_cut_error(1);
	stream_cut_error(2) <= rx_logic_status_latched_default1_fast(5);
	rxStatus_streamCutErr(2) <= not stream_cut_error(2);

	--fifo_rx_buffer_rst_busy <= fifo_rx_buffer_wr_rst_busy and fifo_rx_buffer_rd_rst_busy;
	fifo_rx_buffer_rst_done <= fifo_rx_buffer_wr_rst_done and fifo_rx_buffer_rd_rst_done;
	fifo_rx_rst_done <= fifo_rx_wr_rst_done and fifo_rx_rd_rst_done;

	-- Catching errors on the buffer FIFO, WR port
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  fifo_rx_buffer_wr_en_del <= '0';
		  rxStatus_fifo_buffer_wr_error <= '0';
		else
		  fifo_rx_buffer_wr_en_del <= fifo_rx_buffer_wr_en;
		  rxStatus_fifo_buffer_wr_error <= fifo_rx_buffer_wr_ack xor fifo_rx_buffer_wr_en_del; -- error if "wr_ack" is not just "wr_en" delayed by 1 clock cycle
		end if;
	  end if;
	end process;

	-- Catching errors on the ch1/ch2 FIFOs, WR port
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_rx_wr_en_del <= (others => '0');
		  rxStatus_fifo_wr_error <= (others => '0');
		else
		  fifo_rx_wr_en_del <= fifo_rx_wr_en;
		  rxStatus_fifo_wr_error <= fifo_rx_wr_ack xor fifo_rx_wr_en_del; -- error if "wr_ack" is not just "wr_en" delayed by 1 clock cycle
		end if;
	  end if;
	end process;

	-- Counting the number of data in the RX buffer FIFO (instantaneous)
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  fifo_rx_buffer_level <= (others => '0');
		else
		  if (fifo_rx_buffer_wr_ack = '1' and fifo_rx_buffer_valid = '0') then
		    fifo_rx_buffer_level <= std_logic_vector(unsigned(fifo_rx_buffer_level) + 1); -- data written
		  elsif (fifo_rx_buffer_wr_ack = '0' and fifo_rx_buffer_valid = '1') then
		    fifo_rx_buffer_level <= std_logic_vector(unsigned(fifo_rx_buffer_level) - 1); -- data read
		  else
		    null; -- nothing, or both written and read
		  end if;
		end if;
	  end if;
	end process;
	rxStatus_fifoBufferLevel <= fifo_rx_buffer_level;

	-- Counting the number of data in the RX buffer FIFO (current turn)
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  fifo_rx_buffer_num_data <= (others => '0');
		else
		  if (gblink_rx_fifo_state = FIFO_DATA_WAIT) then
		    if (fifo_rx_buffer_wr_ack = '1') then
			  fifo_rx_buffer_num_data <= std_logic_vector(unsigned(fifo_rx_buffer_num_data) + 1); -- data written
			else
			  null;
			end if;
		  else
		    fifo_rx_buffer_num_data <= (others => '0'); -- reset on each turn
		  end if;
		end if;
	  end if;
	end process;
	rxStatus_fifoBufferNumData <= fifo_rx_buffer_num_data;
	fifo_rx_buffer_num_data_low_error <= s_axis_user_k_tvalid when ((rxControls_fixedLatency_enable_latch = '0') and (gblink_rx_fifo_state = FIFO_DATA_WAIT) and (fifo_rx_buffer_num_data < expectedNumDataPackets)) else -- too few data in the current turn (only for beam-synchronous mode)
	                                 '0';
	rxStatus_fifoBufferNumDataLowErr <= fifo_rx_buffer_num_data_low_error;
	fifo_rx_buffer_num_data_high_error <= s_axis_data_tvalid when ((gblink_rx_fifo_state = FIFO_USER_K_WAIT) and (fifo_rx_buffer_num_data >= expectedNumDataPackets)) else -- too much data in the current turn ((only for beam-synchronous mode))
	                                      '0';
	rxStatus_fifoBufferNumDataHighErr <= fifo_rx_buffer_num_data_high_error;

	-- Counting the number of data in the RX ch1/ch2 FIFOs (instantaneous)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_rx_level(1) <= (others => '0');
		  fifo_rx_level(2) <= (others => '0');
		else
		  if (fifo_rx_wr_ack(1) = '1' and fifo_rx_valid(1) = '0') then
		    fifo_rx_level(1) <= std_logic_vector(unsigned(fifo_rx_level(1)) + 1); -- data written
		  elsif (fifo_rx_wr_ack(1) = '0' and fifo_rx_valid(1) = '1') then
		    fifo_rx_level(1) <= std_logic_vector(unsigned(fifo_rx_level(1)) - 1); -- data read
		  else
		    null; -- nothing, or both written and read
		  end if;
		  if (fifo_rx_wr_ack(2) = '1' and fifo_rx_valid(2) = '0') then
		    fifo_rx_level(2) <= std_logic_vector(unsigned(fifo_rx_level(2)) + 1); -- data written
		  elsif (fifo_rx_wr_ack(2) = '0' and fifo_rx_valid(2) = '1') then
		    fifo_rx_level(2) <= std_logic_vector(unsigned(fifo_rx_level(2)) - 1); -- data read
		  else
		    null; -- nothing, or both written and read
		  end if;
		end if;
	  end if;
	end process;
	rxStatus_fifoLevel(1) <= fifo_rx_level(1);
	rxStatus_fifoLevel(2) <= fifo_rx_level(2);

	-- Counting the number of data in the RX ch1/ch2 FIFOs (current turn)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_rx_num_data(1) <= (others => '0');
		  fifo_rx_num_data(2) <= (others => '0');
		else
		  if (gblink_rx_fsm_state = FSM_DATA_STORE_WAIT) then
		    if (fifo_rx_wr_ack(1) = '1') then
			  fifo_rx_num_data(1) <= std_logic_vector(unsigned(fifo_rx_num_data(1)) + 1); -- data written
			else
			  null;
			end if;
		    if (fifo_rx_wr_ack(2) = '1') then
			  fifo_rx_num_data(2) <= std_logic_vector(unsigned(fifo_rx_num_data(2)) + 1); -- data written
			else
			  null;
			end if;
		  elsif (gblink_rx_fsm_state = FSM_FREV_WAIT) then
		    fifo_rx_num_data(1) <= (others => '0'); -- reset on each turn
		    fifo_rx_num_data(2) <= (others => '0'); -- reset on each turn
		  else
			null;
		  end if;
		end if;
	  end if;
	end process;
	rxStatus_fifoNumData(1) <= fifo_rx_num_data(1);
	rxStatus_fifoNumData(2) <= fifo_rx_num_data(2);

	-- Fetching the valid data from the RX buffer FIFO
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  rx_data_buffer <= (others => '0');
		  rx_data_buffer_msw <= (others => '0');
		  rx_data_buffer_lsw <= (others => '0');
		elsif (fifo_rx_buffer_valid = '1') then
		  rx_data_buffer <= fifo_rx_buffer_dout;
		  rx_data_buffer_msw <= fifo_rx_buffer_dout(63 downto 32);
		  rx_data_buffer_lsw <= fifo_rx_buffer_dout(31 downto 0);
		else
		  null;
		end if;
	  end if;
	end process;

-- Buffer for RX DATA/USER_K coming from the Aurora core
fifo_generator_gblink_rx_buffer_inst : fifo_generator_gblink_rx_buffer
	port map (
		clk => user_clk_slower,
		srst => fifo_rx_buffer_srst,
		din => fifo_rx_buffer_din,
		wr_en => fifo_rx_buffer_wr_en,
		rd_en => fifo_rx_buffer_rd_en,
		dout => fifo_rx_buffer_dout,
		full => fifo_rx_buffer_full, -- status in MM (+ sticky)
		wr_ack => fifo_rx_buffer_wr_ack, -- should be compared with "wr_en" delayed by 1 clock cycle, to produce status bit (write failed etc.)
		overflow => fifo_rx_buffer_overflow, -- status in MM
		empty => fifo_rx_buffer_empty, -- status in MM
		valid => fifo_rx_buffer_valid,
		underflow => fifo_rx_buffer_underflow, -- status in MM
		wr_rst_busy => fifo_rx_buffer_wr_rst_busy, -- status in MM (reset complete etc.)
		rd_rst_busy => fifo_rx_buffer_rd_rst_busy -- status in MM (reset complete etc.)
	);

	-- Both channels are written to the same buffer FIFO, USER_K has priority over DATA
	fifo_rx_buffer_din <= s_axis_user_k_tdata when (s_axis_user_k_tvalid = '1') else
	                      s_axis_data_tdata   when (s_axis_data_tvalid = '1')   else
						  (others => 'X');
	-- store the data in the buffer only when the RX logic is enabled
	fifo_rx_buffer_wr_en <= (s_axis_user_k_tvalid and rxControls_general_enable) when (gblink_rx_fifo_state = FIFO_USER_K_WAIT) else
	                        --((s_axis_data_tvalid or s_axis_user_k_tvalid) and rxControls_general_enable) when (gblink_rx_fifo_state = FIFO_DATA_WAIT) else
	                        (s_axis_data_tvalid and rxControls_general_enable) when (gblink_rx_fifo_state = FIFO_DATA_WAIT) else
	                        '0';
	fifo_rx_buffer_rd_en <= '1' when (gblink_rx_fsm_state = FSM_FREV_WAIT) else
	                        '1' when (gblink_rx_fsm_state = FSM_SYNC_WAIT) else
	                        '1' when ((gblink_rx_fsm_state = FSM_DATA_WAIT) and (rxControls_interleaver_enable_latch = '0')) else
	                        '1' when ((gblink_rx_fsm_state = FSM_DATA_WAIT) and (rxControls_interleaver_enable_latch = '1') and (data_present /= "00")) else
	                        '0';
	rxStatus_fifoBufferWrErr <= rxStatus_fifo_buffer_wr_error;
	rxStatus_fifoBufferFull <= fifo_rx_buffer_full;
	rxStatus_fifoBufferEmpty <= fifo_rx_buffer_empty;
	rxStatus_fifoBufferOverflow <= fifo_rx_buffer_overflow;
	rxStatus_fifoBufferUnderflow <= fifo_rx_buffer_underflow;

	fifo_rx_buffer_frev_error <= fifo_rx_buffer_valid when ((gblink_rx_fsm_state = FSM_FREV_WAIT) and not (fifo_rx_buffer_dout = C_FREV_USER_K_DATA)) else -- FREV entry expected, but got different data
	                             '0';
	rxStatus_fifoBufferFrevErr <= fifo_rx_buffer_frev_error;

	fifo_rx_buffer_sync_error <= fifo_rx_buffer_valid when ((gblink_rx_fsm_state = FSM_SYNC_WAIT) and not (fifo_rx_buffer_dout = C_SYNC_USER_K_DATA)) else -- SYNC entry expected, but got different data
	                             '0';
	rxStatus_fifoBufferSyncErr <= fifo_rx_buffer_sync_error;

-- Sticky status bits
status_reg_latched_inst_default0 : status_reg_latched
	generic map (
		C_STATUS_REG_WIDTH	=> 16,
		C_STATUS_DEFAULT	=> '0'
	)
	port map (
		clk		=> user_clk_slower,
		resetn	=> user_rst_slower,

		status_reg_in		=> rx_logic_status_default0,
		status_reg_reset	=> rx_logic_status_clear_default0,
		status_reg_out		=> rx_logic_status_latched_default0
	);
	rx_logic_status_default0 <= fifo_rx_underflow(2) &
	                            fifo_rx_underflow(1) &
								fifo_rx_overflow(2) &
								fifo_rx_overflow(1) &
								fifo_rx_full(2) &
								fifo_rx_full(1) &
								rxStatus_fifo_wr_error(2) &
	                            rxStatus_fifo_wr_error(1) &
								fifo_rx_buffer_sync_error &
								fifo_rx_buffer_frev_error &
	                            fifo_rx_buffer_num_data_high_error &
	                            fifo_rx_buffer_num_data_low_error &
	                            fifo_rx_buffer_underflow &
	                            fifo_rx_buffer_overflow &
	                            rxStatus_fifo_buffer_wr_error &
	                            fifo_rx_buffer_full;
	rx_logic_status_clear_default0 <= rxControls_fifoUnderflowClear(2) &
	                                  rxControls_fifoUnderflowClear(1) &
									  rxControls_fifoOverflowClear(2) &
									  rxControls_fifoOverflowClear(1) &
									  rxControls_fifoFullClear(2) &
									  rxControls_fifoFullClear(1) &
									  rxControls_fifoWrErrClear(2) &
	                                  rxControls_fifoWrErrClear(1) &
									  rxControls_fifoBufferSyncErrClear &
									  rxControls_fifoBufferFrevErrClear &
	                                  rxControls_fifoBufferNumDataErrHighClear &
	                                  rxControls_fifoBufferNumDataErrLowClear &
	                                  rxControls_fifoBufferUnderflowClear &
	                                  rxControls_fifoBufferOverflowClear &
	                                  rxControls_fifoBufferWrErrClear &
	                                  rxControls_fifoBufferFullClear;
	rxStatus_fifoBufferFullLatched <= rx_logic_status_latched_default0(0);
	rxStatus_fifoBufferWrErrLatched <= rx_logic_status_latched_default0(1);
	rxStatus_fifoBufferOverflowLatched <= rx_logic_status_latched_default0(2);
	rxStatus_fifoBufferUnderflowLatched <= rx_logic_status_latched_default0(3);
	rxStatus_fifoBufferNumDataErrLowLatched <= rx_logic_status_latched_default0(4);
	rxStatus_fifoBufferNumDataErrHighLatched <= rx_logic_status_latched_default0(5);
	rxStatus_fifoBufferFrevErrLatched <= rx_logic_status_latched_default0(6);
	rxStatus_fifoBufferSyncErrLatched <= rx_logic_status_latched_default0(7);
	rxStatus_fifoWrErrLatched(1) <= rx_logic_status_latched_default0(8);
	rxStatus_fifoWrErrLatched(2) <= rx_logic_status_latched_default0(9);
	rxStatus_fifoFullLatched(1) <= rx_logic_status_latched_default0(10);
	rxStatus_fifoFullLatched(2) <= rx_logic_status_latched_default0(11);
	rxStatus_fifoOverflowLatched(1) <= rx_logic_status_latched_default0(12);
	rxStatus_fifoOverflowLatched(2) <= rx_logic_status_latched_default0(13);
	rxStatus_fifoUnderflowLatched(1) <= rx_logic_status_latched_default0(14);
	rxStatus_fifoUnderflowLatched(2) <= rx_logic_status_latched_default0(15);

	-- Data present counter (what packets consist ch1/ch2 data?)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_present_counter <= (others => '0');
		elsif (rxControls_interleaver_enable_latch = '1') then -- interleaving ON
		  if ((gblink_rx_fsm_state = FSM_DATA_WAIT) or (gblink_rx_fsm_state = FSM_DATA_STORE) or (gblink_rx_fsm_state = FSM_DATA_STORE_WAIT)) then
		    if (data_store_done = "11") then -- increase after data has been split and put into ch1/ch2 FIFOs
			  data_present_counter <= std_logic_vector(unsigned(data_present_counter) + 1);
			else
			  null;
			end if;
		  else
		    data_present_counter <= (others => '0'); -- reset on each turn
		  end if;
		else -- interleaving OFF
		  data_present_counter <= (others => '0'); -- doesn't matter
		end if;
	  end if;
	end process;

	-- ratio 0 ("00"): ch1/ch2 data present in every data packet
	-- ratio 1 ("01"): ch1/ch2 data present in every 2nd data packet
	-- ratio 2 ("10"): ch1/ch2 data present in every 4th data packet
	-- ratio 3 ("11"): ch1/ch2 data present in every 8th data packet
	process (rxControls_interleaver_enable_latch, rxControls_interleaver_rateRatioCh1_latch, rxControls_interleaver_rateRatioCh2_latch, data_present_counter) begin
	  if (rxControls_interleaver_enable_latch = '1') then -- interleaving ON
	    case rxControls_interleaver_rateRatioCh1_latch is
	      when "00" =>
		    data_present(1) <= '1';
		  when "01" =>
		    data_present(1) <= '0';
		    if (data_present_counter(0) = '0') then
		      data_present(1) <= '1';
		    end if;
		  when "10" =>
		    data_present(1) <= '0';
		    if (data_present_counter(1 downto 0) = "00") then
		      data_present(1) <= '1';
		    end if;
		  when "11" =>
		    data_present(1) <= '0';
		    if (data_present_counter(2 downto 0) = "000") then
		      data_present(1) <= '1';
		    end if;
		  when others =>
		    null;
	    end case;
	    case rxControls_interleaver_rateRatioCh2_latch is
	      when "00" =>
		    data_present(2) <= '1';
		  when "01" =>
		    data_present(2) <= '0';
		    if (data_present_counter(0) = '0') then
		      data_present(2) <= '1';
		    end if;
		  when "10" =>
		    data_present(2) <= '0';
		    if (data_present_counter(1 downto 0) = "00") then
		      data_present(2) <= '1';
		    end if;
		  when "11" =>
		    data_present(2) <= '0';
		    if (data_present_counter(2 downto 0) = "000") then
		      data_present(2) <= '1';
		    end if;
		  when others =>
		    null;
	    end case;
	  else -- interleaving OFF
	    data_present(1) <= '0'; -- doesn't matter
	    data_present(2) <= '0'; -- doesn't matter
	  end if;
	end process;

-- Instantiation of RX FIFO, channel 1
fifo_generator_gblink_rx_ch1_inst : fifo_generator_gblink_rx
	port map(
		clk => user_clk_faster,
		srst => fifo_rx_srst(1),
		din => fifo_rx_din(1),
		wr_en => fifo_rx_wr_en(1),
		rd_en => fifo_rx_rd_en(1),
		dout => fifo_rx_dout(1),
		full => fifo_rx_full(1),
		wr_ack => fifo_rx_wr_ack(1),
		overflow => fifo_rx_overflow(1),
		empty => fifo_rx_empty(1),
		valid => fifo_rx_valid(1),
		underflow => fifo_rx_underflow(1),
		wr_rst_busy => fifo_rx_wr_rst_busy(1),
		rd_rst_busy => fifo_rx_rd_rst_busy(1)
	);

-- Instantiation of RX FIFO, channel 2 (interleaving)
fifo_generator_gblink_rx_ch2_inst : fifo_generator_gblink_rx
	port map(
		clk => user_clk_faster,
		srst => fifo_rx_srst(2),
		din => fifo_rx_din(2),
		wr_en => fifo_rx_wr_en(2),
		rd_en => fifo_rx_rd_en(2),
		dout => fifo_rx_dout(2),
		full => fifo_rx_full(2),
		wr_ack => fifo_rx_wr_ack(2),
		overflow => fifo_rx_overflow(2),
		empty => fifo_rx_empty(2),
		valid => fifo_rx_valid(2),
		underflow => fifo_rx_underflow(2),
		wr_rst_busy => fifo_rx_wr_rst_busy(2),
		rd_rst_busy => fifo_rx_rd_rst_busy(2)
	);

	-- Data to be written in the ch1/ch2 FIFOs
	process (gblink_rx_fsm_state, rxControls_interleaver_enable_latch, data_store_done_msw_ch1, rx_data_buffer_msw, rx_data_buffer_lsw, data_present) begin
	  case (gblink_rx_fsm_state) is
	    when FSM_FREV_STORE =>
		  fifo_rx_din(1) <= C_FREV_FIFO_ENTRY;
		  if (rxControls_interleaver_enable_latch = '1') then
		    fifo_rx_din(2) <= C_FREV_FIFO_ENTRY;
		  else
		    fifo_rx_din(2) <= (others => 'X');
		  end if;
		when FSM_DATA_STORE =>
		  if (rxControls_interleaver_enable_latch = '0') then
		    if (data_store_done_msw_ch1 = '0') then
			  fifo_rx_din(1) <= (x"0" & rx_data_buffer_msw);
			else
			  fifo_rx_din(1) <= (x"0" & rx_data_buffer_lsw);
			end if;
		    fifo_rx_din(2) <= (others => 'X');
		  else
		    if (data_present(1) = '1') then
			  fifo_rx_din(1) <= (x"0" & rx_data_buffer_msw);
			else
			  fifo_rx_din(1) <= (others => 'X');
			end if;
		    if (data_present(2) = '1') then
			  fifo_rx_din(2) <= (x"0" & rx_data_buffer_lsw);
			else
			  fifo_rx_din(2) <= (others => 'X');
			end if;
		  end if;
		when others =>
		  fifo_rx_din(1) <= (others => 'X');
		  fifo_rx_din(2) <= (others => 'X');
	  end case;
	end process;
	-- fifo_rx_din(1) <= C_FREV_FIFO_ENTRY when (gblink_rx_fsm_state = FSM_FREV_STORE) else
	                  -- (x"0" & rx_data_buffer_msw) when ((gblink_rx_fsm_state = FSM_DATA_STORE) and (data_store_done_msw_ch1 = '0')) else
	                  -- (x"0" & rx_data_buffer_lsw) when ((gblink_rx_fsm_state = FSM_DATA_STORE) and (data_store_done_msw_ch1 = '1')) else
	                  -- (others => 'X');
	-- fifo_rx_din(2) <= C_FREV_FIFO_ENTRY when ((gblink_rx_fsm_state = FSM_FREV_STORE) and (rxControls_interleaver_enable_latch = '1')) else
	                  -- (x"0" & rx_data_buffer_lsw) when ((gblink_rx_fsm_state = FSM_DATA_STORE) and (rxControls_interleaver_enable_latch = '1')) else
	                  -- (others => 'X');

	fifo_rx_wr_en(1) <= '1' when (gblink_rx_fsm_state = FSM_FREV_STORE) else
	                    '1' when ((gblink_rx_fsm_state = FSM_DATA_STORE) and (rxControls_interleaver_enable_latch = '0')) else
	                    '1' when ((gblink_rx_fsm_state = FSM_DATA_STORE) and (rxControls_interleaver_enable_latch = '1') and (data_present(1) = '1')) else
	                    '0';
	fifo_rx_wr_en(2) <= '1' when ((gblink_rx_fsm_state = FSM_FREV_STORE) and (rxControls_interleaver_enable_latch = '1')) else
	                    '1' when ((gblink_rx_fsm_state = FSM_DATA_STORE) and (rxControls_interleaver_enable_latch = '1') and (data_present(2) = '1')) else
	                    '0';

	fifo_rx_rd_en(1) <= s_rx_dsp_ch1_read when (rxControls_fixedLatency_enable_latch = '0') else
	                    '1' when ((gblink_rx_latency_state = LAT_STREAMING) and (streaming_counter = "00")) else
						'0';
	fifo_rx_rd_en(2) <= (s_rx_dsp_ch2_read and rxControls_interleaver_enable_latch) when (rxControls_fixedLatency_enable_latch = '0') else
	                    rxControls_interleaver_enable_latch when ((gblink_rx_latency_state = LAT_STREAMING) and (streaming_counter = "00")) else
						'0';

	rxStatus_fifoWrErr <= rxStatus_fifo_wr_error;
	rxStatus_fifoFull <= fifo_rx_full;
	rxStatus_fifoEmpty <= fifo_rx_empty;
	rxStatus_fifoOverflow <= fifo_rx_overflow;
	rxStatus_fifoUnderflow <= fifo_rx_underflow;

	-- Signals telling that the current valid MSW/LSW data has been stored in the RX ch1/ch2 FIFOs
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_store_done(1) <= '0';
		  data_store_done(2) <= '0';
		elsif ((gblink_rx_fsm_state = FSM_DATA_WAIT) or (gblink_rx_fsm_state = FSM_DATA_STORE) or (gblink_rx_fsm_state = FSM_DATA_STORE_WAIT)) then
		  if (rxControls_interleaver_enable_latch = '0') then
		    data_store_done(1) <= data_store_done_msw_ch1 and fifo_rx_wr_ack(1); -- MSW and LSW stored in the ch1 FIFO (exception: last packet with odd number of buckets)
		  elsif (data_present(1) = '0') then
		    if (data_store_done(1) = '0') then
			  data_store_done(1) <= '1';
			else
			  data_store_done(1) <= '0';
			end if;
		  else
		    data_store_done(1) <= fifo_rx_wr_ack(1); -- MSW stored in the ch1 FIFO
		  end if;
		  if (rxControls_interleaver_enable_latch = '0') then
		    data_store_done(2) <= '1';
		  elsif (data_present(2) = '0') then
		    if (data_store_done(2) = '0') then
			  data_store_done(2) <= '1';
			else
			  data_store_done(2) <= '0';
			end if;
		  else
		    data_store_done(2) <= fifo_rx_wr_ack(2); -- LSW stored in the ch2 FIFO
		  end if;
		else
		  data_store_done(1) <= '0'; -- reset on each new packet
		  data_store_done(2) <= '0'; -- reset on each new packet
		end if;
	  end if;
	end process;

	-- Signal telling that the current valid MSW data has been stored in the RX ch1 FIFO
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_store_done_msw_ch1 <= '0';
		elsif ((gblink_rx_fsm_state = FSM_DATA_STORE) or (gblink_rx_fsm_state = FSM_DATA_STORE_WAIT)) then
		  if (fifo_rx_wr_ack(1) = '1') then
		    data_store_done_msw_ch1 <= '1';
		  else
		    null;
		  end if;
		else
		  data_store_done_msw_ch1 <= '0'; -- reset on each new packet
		end if;
	  end if;
	end process;

	-- Counting the number of valid data read from the ch1/ch2 FIFOs (current turn)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_read_counter(1) <= (others => '0');
		  data_read_counter(2) <= (others => '0');
		else
		  case gblink_rx_fsm_state is
	        when FSM_DATA_WAIT | FSM_DATA_STORE | FSM_DATA_STORE_WAIT | FSM_FREV_WAIT =>
			  if ((fifo_rx_valid(1) = '1') and (fifo_rx_dout(1) /= C_FREV_FIFO_ENTRY)) then
			    data_read_counter(1) <= std_logic_vector(unsigned(data_read_counter(1)) + 1);
			  else
			    null;
			  end if;
			  if ((fifo_rx_valid(2) = '1') and (fifo_rx_dout(2) /= C_FREV_FIFO_ENTRY)) then
			    data_read_counter(2) <= std_logic_vector(unsigned(data_read_counter(2)) + 1);
			  else
			    null;
			  end if;
		    when others =>
			  data_read_counter(1) <= (others => '0');
			  data_read_counter(2) <= (others => '0');
	      end case;
		end if;
	  end if;
	end process;

	s_rx_dsp_ch1_valid_int <= '1' when ((fifo_rx_valid(1) = '1') and (fifo_rx_dout(1) /= C_FREV_FIFO_ENTRY) and (rxControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                          fifo_rx_valid_latched(1) when (rxControls_fixedLatency_enable_latch = '1') else -- fixed-latency mode
							  '0';
	s_rx_dsp_ch1_valid <= s_rx_dsp_ch1_valid_int;
	s_rx_dsp_ch1_data <= fifo_rx_dout(1)(s_rx_dsp_ch1_data'range) when ((fifo_rx_valid(1) = '1') and (fifo_rx_dout(1) /= C_FREV_FIFO_ENTRY) and (rxControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                     fifo_rx_dout_latched(1)(s_rx_dsp_ch1_data'range) when ((fifo_rx_valid_latched(1) = '1') and (rxControls_fixedLatency_enable_latch = '1')) else -- fixed-latency mode
	                     (others => '0');
	s_rx_dsp_ch1_frev_flag <= '1' when ((s_rx_dsp_ch1_valid_int = '1') and (unsigned(data_read_counter(1)) = 0) and (rxControls_fixedLatency_enable_latch = '0')) else -- frev flag asserted with bucket0 data (beam-synchronous mode)
	                          '0';
	s_rx_dsp_ch1_bucket_index <= data_read_counter(1) when (rxControls_fixedLatency_enable_latch = '0') else -- beam-synchronous mode
	                             (others => 'X');

	s_rx_dsp_ch2_valid_int <= '1' when ((fifo_rx_valid(2) = '1') and (fifo_rx_dout(2) /= C_FREV_FIFO_ENTRY) and (rxControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                          fifo_rx_valid_latched(2) when (rxControls_fixedLatency_enable_latch = '1') else -- fixed-latency mode
							  '0';
	s_rx_dsp_ch2_valid <= s_rx_dsp_ch2_valid_int;
	s_rx_dsp_ch2_data <= fifo_rx_dout(2)(s_rx_dsp_ch2_data'range) when ((fifo_rx_valid(2) = '1') and (fifo_rx_dout(2) /= C_FREV_FIFO_ENTRY) and (rxControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                     fifo_rx_dout_latched(2)(s_rx_dsp_ch2_data'range) when ((fifo_rx_valid_latched(2) = '1') and (rxControls_fixedLatency_enable_latch = '1')) else -- fixed-latency mode
	                     (others => 'X');
	s_rx_dsp_ch2_frev_flag <= '1' when ((s_rx_dsp_ch2_valid_int = '1') and (unsigned(data_read_counter(2)) = 0) and (rxControls_fixedLatency_enable_latch = '0')) else -- frev flag asserted with bucket0 data (beam-synchronous mode)
	                          '0';
	s_rx_dsp_ch2_bucket_index <= data_read_counter(2) when (rxControls_fixedLatency_enable_latch = '0') else -- beam-synchronous mode
	                             (others => '0');

	-- Latency counter
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  latency_counter <= (others => '0');
		elsif (gblink_rx_latency_state = LAT_WAIT) then
		  latency_counter <= std_logic_vector(unsigned(latency_counter) + 1);
		else
		  latency_counter <= (others => '0');
		end if;
	  end if;
	end process;

	-- Streaming counter
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  streaming_counter <= (others => '0');
		elsif (gblink_rx_latency_state = LAT_STREAMING) then
		  if (streaming_counter = "01") then -- read engine working at 125MSps instead of 62.5MSps (original idea), so 1-bit counter is enough (instead of 2-bit counter) 
		    streaming_counter <= (others => '0');
		  else
		    streaming_counter <= std_logic_vector(unsigned(streaming_counter) + 1);
		  end if;
		else
		  streaming_counter <= (others => '0');
		end if;
	  end if;
	end process;


	-- Latching of the "fifo_rx_valid" and "fifo_rx_dout" for fixed-latency mode
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_rx_valid_latched <= (others => '0');
		  fifo_rx_dout_latched(1) <= (others => '0');
		  fifo_rx_dout_latched(2) <= (others => '0');
		elsif ((gblink_rx_latency_state = LAT_STREAMING) and (streaming_counter = "01")) then -- "fifo_rx_rd_en" is asserted when "streaming_counter" equals 0
		  fifo_rx_valid_latched <= fifo_rx_valid;
		  fifo_rx_dout_latched(1) <= fifo_rx_dout(1);
		  fifo_rx_dout_latched(2) <= fifo_rx_dout(2);
		else
		  null;
		end if;
	  end if;
	end process;


-- C_FAST_CLK_CYCLES_IN_US

	-- (LAT_DISABLED, LAT_SYNC_WAIT, LAT_WAIT, LAT_STREAMING);


		-- s_rx_dsp_ch1_read : in std_logic;
		-- s_rx_dsp_ch1_valid : out std_logic;
		-- s_rx_dsp_ch1_data : out std_logic_vector(31 downto 0);
		-- s_rx_dsp_ch1_frev_flag : out std_logic;
		-- s_rx_dsp_ch1_full_error : out std_logic;
		-- s_rx_dsp_ch1_turn_error : out std_logic;
		-- s_rx_ch1_id : out std_logic_vector(7 downto 0); -- check what range is necessary

end arch_behav;

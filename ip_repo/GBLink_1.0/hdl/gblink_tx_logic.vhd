library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gblink_tx_logic is
	generic (
		-- Parameters of Axi Master Bus Interface M_AXIS_DATA
		C_M_AXIS_DATA_TDATA_WIDTH	: integer	:= 64;
		--C_M_AXIS_DATA_START_COUNT	: integer	:= 32;

		-- Parameters of Axi Master Bus Interface M_AXIS_USER_K
		C_M_AXIS_USER_K_TDATA_WIDTH	: integer	:= 64
		--C_M_AXIS_USER_K_START_COUNT	: integer	:= 32;
	);
	port (
		-- Clock/reset ports
		user_clk_slower : in std_logic; -- 78.125 (125)
		user_rst_slower : in std_logic;
		user_clk_faster : in std_logic; -- 156.25 (250)
		user_rst_faster : in std_logic;

		-- TX logic interface
		-- DSP, ch1
		s_tx_dsp_ch1_data : in std_logic_vector(31 downto 0);
		s_tx_dsp_ch1_valid : in std_logic;
		s_tx_dsp_ch1_frev : in std_logic;
		s_tx_dsp_ch1_turn_number : in std_logic_vector(31 downto 0); -- 29 should suffice for coast cycles
		-- DSP, ch2
		s_tx_dsp_ch2_data : in std_logic_vector(31 downto 0);
		s_tx_dsp_ch2_valid : in std_logic;
		s_tx_dsp_ch2_frev : in std_logic;
		s_tx_dsp_ch2_turn_number : in std_logic_vector(31 downto 0); -- 29 should suffice for coast cycles
		-- NCO
		s_tx_nco_id : in std_logic_vector(7 downto 0); -- check what range is necessary
		-- phi_correction

		-- White Rabbit: synchronisation for fixed-latency mode
		sync_tx : in std_logic;

		-- Ports of Axi Master Bus Interface M_AXIS_DATA
		m_axis_data_aclk	: in std_logic;
		m_axis_data_aresetn	: in std_logic;
		m_axis_data_tvalid	: out std_logic;
		m_axis_data_tdata	: out std_logic_vector(C_M_AXIS_DATA_TDATA_WIDTH-1 downto 0);
		m_axis_data_tstrb	: out std_logic_vector((C_M_AXIS_DATA_TDATA_WIDTH/8)-1 downto 0);
		m_axis_data_tlast	: out std_logic;
		m_axis_data_tready	: in std_logic;

		-- Ports of Axi Master Bus Interface M_AXIS_USER_K
		m_axis_user_k_aclk	: in std_logic;
		m_axis_user_k_aresetn	: in std_logic;
		m_axis_user_k_tvalid	: out std_logic;
		m_axis_user_k_tdata	: out std_logic_vector(C_M_AXIS_USER_K_TDATA_WIDTH-1 downto 0);
		m_axis_user_k_tstrb	: out std_logic_vector((C_M_AXIS_USER_K_TDATA_WIDTH/8)-1 downto 0);
		m_axis_user_k_tlast	: out std_logic;
		m_axis_user_k_tready	: in std_logic;

		txControls_general_enable : in std_logic;
		txControls_general_reset : in std_logic;
		txControls_numBuckets : in std_logic_vector(15 downto 0);
		txControls_interleaver_enable : in std_logic;
		txControls_interleaver_rateRatioCh1 : in std_logic_vector(5 downto 4);
		txControls_interleaver_rateRatioCh2 : in std_logic_vector(9 downto 8);
		txControls_fixedLatency_enable : in std_logic;

		txStatus_fifoCh1NumData : out std_logic_vector(31 downto 0);
		txStatus_fifoCh1NumDataTurn : out std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumData : out std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumDataTurn : out std_logic_vector(31 downto 0)
	);
end entity gblink_tx_logic;

architecture arch_behav of gblink_tx_logic is

	-- Shift right with ceiling rounding
	-- x : input data
	-- n : number of bits to shift right, minus 1 (0, 1, 2, or 3)
	function ceil_shift_right(x : integer; n : integer) return integer is
	  variable x_vector : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(x, 16));
	  variable n_shift : integer := n + 1;
	begin
	  if (n_shift = 1) then
	    if (x_vector(0) = '1') then -- if any residue
		  return x/2 + 1;
		else
		  return x/2;
		end if;
	  elsif (n_shift = 2) then
	    if ((x_vector(0) = '1') or (x_vector(1) = '1')) then -- if any residue
		  return x/4 + 1;
		else
		  return x/4;
		end if;
	  elsif (n_shift = 3) then
	    if ((x_vector(0) = '1') or (x_vector(1) = '1') or (x_vector(2) = '1')) then -- if any residue
		  return x/8 + 1;
		else
		  return x/8;
		end if;
	  elsif (n_shift = 4) then
	    if ((x_vector(0) = '1') or (x_vector(1) = '1') or (x_vector(2) = '1') or (x_vector(3) = '1')) then -- if any residue
		  return x/16 + 1;
		else
		  return x/16;
		end if;
	  else
	    return 0;
	  end if;
	end;

	component status_reg_latched is
		generic (
		C_STATUS_REG_WIDTH	: integer := 1;
		C_STATUS_DEFAULT	: std_logic := '0'
		);
		port (
		clk		: in std_logic;
		resetn	: in std_logic;

		status_reg_in		: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_reset	: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_out		: out std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0)
		);
	end component status_reg_latched;

	component fifo_generator_gblink_tx is
		port (
		clk : in std_logic;
		srst : in std_logic;
		din : in std_logic_vector(35 downto 0);
		wr_en : in std_logic;
		rd_en : in std_logic;
		dout : out std_logic_vector(35 downto 0);
		full : out std_logic;
		wr_ack : out std_logic;
		overflow : out std_logic;
		empty : out std_logic;
		valid : out std_logic;
		underflow : out std_logic;
		wr_rst_busy : out std_logic;
		rd_rst_busy : out std_logic
		);
	end component fifo_generator_gblink_tx;

	type gblink_tx_fifo_state_t is (FIFO_UNINIT, FIFO_DISABLED, FIFO_FREV_WAIT, FIFO_FREV_STORE, FIFO_FREV_STORE_WAIT, FIFO_SYNC_WAIT, FIFO_SYNC_STORE, FIFO_SYNC_STORE_WAIT, FIFO_BUCKETS);
	signal gblink_tx_fifo_ch1_state : gblink_tx_fifo_state_t;
	signal gblink_tx_fifo_ch1_state_next : gblink_tx_fifo_state_t;
	signal gblink_tx_fifo_ch2_state : gblink_tx_fifo_state_t;
	signal gblink_tx_fifo_ch2_state_next : gblink_tx_fifo_state_t;

	type gblink_tx_fsm_state_t is (FSM_DISABLED, FSM_FREV_WAIT, FSM_FREV_WAIT_CH1, FSM_FREV_WAIT_CH2, FSM_SYNC_WAIT, FSM_SYNC_WAIT_CH1, FSM_SYNC_WAIT_CH2, FSM_USER_K_SEND, FSM_BUFFER_WAIT, FSM_DATA_SEND);
	signal gblink_tx_fsm_state : gblink_tx_fsm_state_t;
	--signal gblink_tx_fsm_state_slow : gblink_tx_fsm_state_t;
	signal gblink_tx_fsm_state_next : gblink_tx_fsm_state_t;

	type gblink_tx_fixed_rate_state_t is (FRT_DISABLED, FRT_SYNC_WAIT, FRT_FIXED_VALID);
	signal gblink_tx_fixed_rate_state : gblink_tx_fixed_rate_state_t;
	signal gblink_tx_fixed_rate_state_next : gblink_tx_fixed_rate_state_t;

	--signal s_tx_nco_frev_del : std_logic;
	signal s_tx_dsp_ch1_data_del : std_logic_vector(31 downto 0);
	signal s_tx_dsp_ch1_data_del2 : std_logic_vector(31 downto 0);
	signal s_tx_dsp_ch1_data_del3 : std_logic_vector(31 downto 0);
	signal s_tx_dsp_ch1_valid_del : std_logic;
	signal s_tx_dsp_ch1_valid_del2 : std_logic;
	signal s_tx_dsp_ch1_valid_del3 : std_logic;
	signal s_tx_dsp_ch2_data_del : std_logic_vector(31 downto 0);
	signal s_tx_dsp_ch2_data_del2 : std_logic_vector(31 downto 0);
	signal s_tx_dsp_ch2_data_del3 : std_logic_vector(31 downto 0);
	signal s_tx_dsp_ch2_valid_del : std_logic;
	signal s_tx_dsp_ch2_valid_del2 : std_logic;
	signal s_tx_dsp_ch2_valid_del3 : std_logic;

	signal m_axis_data_tvalid_int : std_logic;
	signal m_axis_data_tvalid_sync_ff_control : std_logic;
	signal m_axis_data_tvalid_sync_ff : std_logic_vector(1 downto 0); -- 2FF synchroniser
	signal m_axis_data_tdata_sync_ff_enable : std_logic;
	signal m_axis_data_tdata_sync_ff_data : std_logic_vector(C_M_AXIS_DATA_TDATA_WIDTH-1 downto 0);
	signal m_axis_data_tdata_sync_ff : std_logic_vector(1 downto 0); -- 2FF synchroniser
	signal m_axis_user_k_tvalid_int : std_logic;
	signal m_axis_user_k_tvalid_sync_ff_control : std_logic;
	signal m_axis_user_k_tvalid_sync_ff : std_logic_vector(1 downto 0); -- 2FF synchroniser
	signal m_axis_user_k_tdata_sync_ff_enable : std_logic;
	signal m_axis_user_k_tdata_sync_ff_data : std_logic_vector(C_M_AXIS_USER_K_TDATA_WIDTH-1 downto 0);
	signal m_axis_user_k_tdata_sync_ff : std_logic_vector(1 downto 0); -- 2FF synchroniser

	attribute ASYNC_REG : string;
	attribute ASYNC_REG of m_axis_data_tvalid_sync_ff : signal is "TRUE";
	attribute ASYNC_REG of m_axis_data_tdata_sync_ff : signal is "TRUE";
	attribute ASYNC_REG of m_axis_user_k_tvalid_sync_ff : signal is "TRUE";
	attribute ASYNC_REG of m_axis_user_k_tdata_sync_ff : signal is "TRUE";

	type fifo_tx_din_t is array (2 downto 1) of std_logic_vector(35 downto 0);
	type fifo_tx_dout_t is array (2 downto 1) of std_logic_vector(35 downto 0);

	type fifo_tx_level_t is array (2 downto 1) of std_logic_vector(10 downto 0); -- 1024 (actual 1025) FIFO depth
	type fifo_tx_num_data_t is array (2 downto 1) of std_logic_vector(15 downto 0); -- 4620 buckets (but, 16b width for easier comparison with expected number of data)

	signal fifo_tx_srst : std_logic_vector(2 downto 1);
	signal fifo_tx_din : fifo_tx_din_t;
	signal fifo_tx_wr_en : std_logic_vector(2 downto 1);
	signal fifo_tx_rd_en : std_logic_vector(2 downto 1);
	signal fifo_tx_dout : fifo_tx_dout_t;
	signal fifo_tx_full : std_logic_vector(2 downto 1);
	signal fifo_tx_wr_ack : std_logic_vector(2 downto 1);
	signal fifo_tx_overflow : std_logic_vector(2 downto 1);
	signal fifo_tx_empty : std_logic_vector(2 downto 1);
	signal fifo_tx_valid : std_logic_vector(2 downto 1);
	signal fifo_tx_underflow : std_logic_vector(2 downto 1);
	signal fifo_tx_wr_rst_busy : std_logic_vector(2 downto 1);
	signal fifo_tx_rd_rst_busy : std_logic_vector(2 downto 1);

	signal fifo_tx_wr_en_del : std_logic_vector(2 downto 1);
	signal txStatus_fifo_wr_error : std_logic_vector(2 downto 1);
	signal txStatus_fifoWrErr : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txStatus_fifoWrErrLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoWrErrClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal txStatus_fifoFull : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txStatus_fifoFullLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoFullClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal txStatus_fifoEmpty : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txStatus_fifoOverflow : std_logic_vector(2 downto 1); -- should be output (MM)... do we need non-latched version in the MM?
	signal txStatus_fifoOverflowLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoOverflowClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal txStatus_fifoUnderflow : std_logic_vector(2 downto 1); -- should be output (MM)... do we need non-latched version in the MM?
	signal txStatus_fifoUnderflowLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoUnderflowClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal fifo_tx_level : fifo_tx_level_t;
	signal txStatus_fifoLevel : fifo_tx_level_t; -- should be output (MM)
	signal fifo_tx_num_data : fifo_tx_num_data_t;
	signal fifo_tx_num_data_del : fifo_tx_num_data_t;
	signal txStatus_fifoNumData : fifo_tx_num_data_t; -- should be output (MM)
	signal fifo_tx_num_data_turn : fifo_tx_num_data_t;
	signal txStatus_fifoNumDataTurn : fifo_tx_num_data_t; -- should be output (MM)
	signal fifo_tx_num_data_low_error : std_logic_vector(2 downto 1);
	signal txStatus_fifoNumDataLowErr : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txStatus_fifoNumDataErrLowLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoNumDataErrLowClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal fifo_tx_num_data_high_error : std_logic_vector(2 downto 1);
	signal txStatus_fifoNumDataHighErr : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txStatus_fifoNumDataErrHighLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoNumDataErrHighClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal fifo_tx_frev_error : std_logic_vector(2 downto 1);
	signal txStatus_fifoFrevErr : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txStatus_fifoFrevErrLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoFrevErrClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal fifo_tx_sync_error : std_logic_vector(2 downto 1);
	signal txStatus_fifoSyncErr : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txStatus_fifoSyncErrLatched : std_logic_vector(2 downto 1); -- should be output (MM)
	signal txControls_fifoSyncErrClear : std_logic_vector(2 downto 1) := (others => '0'); -- should be input (MM)
	signal fifo_tx_wr_rst_done : std_logic_vector(2 downto 1);
	signal txStatus_fifoWrRstDone : std_logic_vector(2 downto 1); -- should be output (MM)
	signal fifo_tx_rd_rst_done : std_logic_vector(2 downto 1);
	signal txStatus_fifoRdRstDone : std_logic_vector(2 downto 1); -- should be output (MM)
	signal fifo_tx_rst_done : std_logic_vector(2 downto 1);

	signal txControls_general_enable_del : std_logic;
	signal txControls_general_reset_del : std_logic;

	signal txControls_numBuckets_latch : std_logic_vector(15 downto 0);
	signal txControls_interleaver_enable_latch : std_logic;
	signal txControls_interleaver_rateRatioCh1_latch : std_logic_vector(5 downto 4);
	signal txControls_interleaver_rateRatioCh2_latch : std_logic_vector(9 downto 8);
	signal expectedNumData_ch1 : std_logic_vector(15 downto 0);
	signal expectedNumData_ch2 : std_logic_vector(15 downto 0);
	signal expectedNumDataPackets : std_logic_vector(15 downto 0);
	signal txControls_fixedLatency_enable_latch : std_logic;

	signal tx_logic_status_default0 : std_logic_vector(15 downto 0);
	signal tx_logic_status_clear_default0 : std_logic_vector(15 downto 0);
	signal tx_logic_status_latched_default0 : std_logic_vector(15 downto 0);

	signal tx_logic_status_default1 : std_logic_vector(3 downto 0);
	signal tx_logic_status_clear_default1 : std_logic_vector(3 downto 0);
	signal tx_logic_status_latched_default1 : std_logic_vector(3 downto 0);

	signal data_present : std_logic_vector(2 downto 1);
	signal data_present_counter : std_logic_vector(2 downto 0); -- 3-bit counter enough for ratios 0, 1, 2 and 3
	signal data_read_done : std_logic_vector(2 downto 1);
	signal data_read_done_msw_ch1 : std_logic;
	signal data_read_counter : fifo_tx_num_data_t;
	signal packets_sent_counter : std_logic_vector(15 downto 0);

	signal tx_data_buffer : std_logic_vector(63 downto 0);
	signal tx_data_buffer_msw : std_logic_vector(31 downto 0);
	signal tx_data_buffer_lsw : std_logic_vector(31 downto 0);

	signal fixed_valid : std_logic_vector(2 downto 1);
	signal fixed_valid_del : std_logic_vector(2 downto 1);
	signal fixed_valid_del2 : std_logic_vector(2 downto 1);
	--signal fixed_valid_del3 : std_logic_vector(2 downto 1);
	signal valid_counter : std_logic_vector(3 downto 0); -- 4-bit counter enough for ratios 0, 1, 2 and 3

	constant C_FREV_FIFO_ENTRY : std_logic_vector(35 downto 0) := x"F00000000";
	constant C_SYNC_FIFO_ENTRY : std_logic_vector(35 downto 0) := x"100000000";
	constant C_FREV_USER_K_DATA : std_logic_vector(63 downto 0) := x"FFFFFFFFFFFFFFFF";
	constant C_SYNC_USER_K_DATA : std_logic_vector(63 downto 0) := x"1111111111111111";

begin

	-- Delayed signals from the TX DSP interfaces and internal logic
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  s_tx_dsp_ch1_data_del <= (others => '0');
		  s_tx_dsp_ch1_data_del2 <= (others => '0');
		  s_tx_dsp_ch1_data_del3 <= (others => '0');
		  s_tx_dsp_ch1_valid_del <= '0';
		  s_tx_dsp_ch1_valid_del2 <= '0';
		  s_tx_dsp_ch1_valid_del3 <= '0';
		  s_tx_dsp_ch2_data_del <= (others => '0');
		  s_tx_dsp_ch2_data_del2 <= (others => '0');
		  s_tx_dsp_ch2_data_del3 <= (others => '0');
		  s_tx_dsp_ch2_valid_del <= '0';
		  s_tx_dsp_ch2_valid_del2 <= '0';
		  s_tx_dsp_ch2_valid_del3 <= '0';
		  fixed_valid_del <= (others => '0');
		  fixed_valid_del2 <= (others => '0');
		  --fixed_valid_del3 <= (others => '0');
		  fifo_tx_num_data_del(1) <= (others => '0');
		  fifo_tx_num_data_del(2) <= (others => '0');
		else
		  s_tx_dsp_ch1_data_del <= s_tx_dsp_ch1_data;
		  s_tx_dsp_ch1_data_del2 <= s_tx_dsp_ch1_data_del;
		  s_tx_dsp_ch1_data_del3 <= s_tx_dsp_ch1_data_del2;
		  s_tx_dsp_ch1_valid_del <= s_tx_dsp_ch1_valid;
		  s_tx_dsp_ch1_valid_del2 <= s_tx_dsp_ch1_valid_del;
		  s_tx_dsp_ch1_valid_del3 <= s_tx_dsp_ch1_valid_del2;
		  s_tx_dsp_ch2_data_del <= s_tx_dsp_ch2_data;
		  s_tx_dsp_ch2_data_del2 <= s_tx_dsp_ch2_data_del;
		  s_tx_dsp_ch2_data_del3 <= s_tx_dsp_ch2_data_del2;
		  s_tx_dsp_ch2_valid_del <= s_tx_dsp_ch2_valid;
		  s_tx_dsp_ch2_valid_del2 <= s_tx_dsp_ch2_valid_del;
		  s_tx_dsp_ch2_valid_del3 <= s_tx_dsp_ch2_valid_del2;
		  fixed_valid_del <= fixed_valid;
		  fixed_valid_del2 <= fixed_valid_del;
		  --fixed_valid_del3 <= fixed_valid_del2;
		  fifo_tx_num_data_del(1) <= fifo_tx_num_data(1);
		  fifo_tx_num_data_del(2) <= fifo_tx_num_data(2);
		end if;
	  end if;
	end process;

	-- FIFO state transition (faster clock)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  gblink_tx_fifo_ch1_state <= FIFO_UNINIT;
		  gblink_tx_fifo_ch2_state <= FIFO_UNINIT;
		else
		  gblink_tx_fifo_ch1_state <= gblink_tx_fifo_ch1_state_next;
		  gblink_tx_fifo_ch2_state <= gblink_tx_fifo_ch2_state_next;
		end if;
	  end if;
	end process;

	-- FSM state transition (faster clock)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  gblink_tx_fsm_state <= FSM_DISABLED;
		else
		  gblink_tx_fsm_state <= gblink_tx_fsm_state_next;
		end if;
	  end if;
	end process;

	-- -- FSM state transition (slower clock)
	-- process (user_clk_slower) begin
	  -- if rising_edge(user_clk_slower) then
	    -- if (user_rst_slower = '0') then
		  -- gblink_tx_fsm_state_slow <= FSM_DISABLED;
		-- else
		  -- gblink_tx_fsm_state_slow <= gblink_tx_fsm_state_next;
		-- end if;
	  -- end if;
	-- end process;

	-- FRT state transition (faster clock)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  gblink_tx_fixed_rate_state <= FRT_DISABLED;
		else
		  gblink_tx_fixed_rate_state <= gblink_tx_fixed_rate_state_next;
		end if;
	  end if;
	end process;

	-- FIFO ch1 next state logic
	process (gblink_tx_fifo_ch1_state, fifo_tx_rst_done(1), txControls_general_reset_del, txControls_general_enable, txControls_fixedLatency_enable, txControls_fixedLatency_enable_latch, s_tx_dsp_ch1_frev, fifo_tx_wr_ack(1), sync_tx, fifo_tx_wr_en(1), fifo_tx_num_data(1), expectedNumData_ch1) begin
	  case gblink_tx_fifo_ch1_state is
	    when FIFO_UNINIT =>
		  if (fifo_tx_rst_done(1) = '1') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_DISABLED; -- wait for the FIFO reset sequence to complete
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT;
		  end if;
		when FIFO_DISABLED =>
		  if (txControls_general_reset_del = '1') then -- or "txControls_general_reset"?
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- another reset issued before the enable
		  elsif (txControls_general_enable = '1') then -- wait for the enable through the MM
		    if (txControls_fixedLatency_enable = '0') then
			  gblink_tx_fifo_ch1_state_next <= FIFO_FREV_WAIT; -- beam-synchronous mode
			else
			  gblink_tx_fifo_ch1_state_next <= FIFO_SYNC_WAIT; -- fixed-latency mode
			end if;
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_DISABLED;
		  end if;
		when FIFO_FREV_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
	      elsif (s_tx_dsp_ch1_frev = '1') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_FREV_STORE; -- wait for the FREV1 pulse
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_FREV_WAIT;
	      end if;
		when FIFO_FREV_STORE =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_FREV_STORE_WAIT; -- go to the next state right away because "fifo_tx_wr_en" should be active for only 1 clock cycle, and not 2
	      end if;
		when FIFO_FREV_STORE_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (fifo_tx_wr_ack(1) = '1') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_BUCKETS; -- wait for the FREV entry to be stored
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_FREV_STORE_WAIT;
		  end if;
		when FIFO_SYNC_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
	      elsif (sync_tx = '1') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_SYNC_STORE; -- wait for the SYNC pulse
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_SYNC_WAIT;
	      end if;
		when FIFO_SYNC_STORE =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_SYNC_STORE_WAIT; -- go to the next state right away because "fifo_tx_wr_en" should be active for only 1 clock cycle, and not 2
	      end if;
		when FIFO_SYNC_STORE_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (fifo_tx_wr_ack(1) = '1') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_BUCKETS; -- wait for the SYNC entry to be stored
		  else
		    gblink_tx_fifo_ch1_state_next <= FIFO_SYNC_STORE_WAIT;
		  end if;
		when FIFO_BUCKETS =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch1_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (txControls_fixedLatency_enable_latch = '0') then -- beam-synchronous mode
		    if (s_tx_dsp_ch1_frev = '1') then
			  gblink_tx_fifo_ch1_state_next <= FIFO_FREV_STORE;
		    elsif ((fifo_tx_wr_en(1) = '1') and (fifo_tx_num_data(1) = std_logic_vector(unsigned(expectedNumData_ch1) - 1))) then
			  gblink_tx_fifo_ch1_state_next <= FIFO_FREV_WAIT;
			else
			  gblink_tx_fifo_ch1_state_next <= FIFO_BUCKETS;
			end if;
		  else -- fixed-latency mode
		    gblink_tx_fifo_ch1_state_next <= FIFO_BUCKETS; -- no notion of turn, stay in BUCKETS state
	      end if;
		when others =>
		  null;
	  end case;
	end process;

	-- FIFO ch2 next state logic
	process (gblink_tx_fifo_ch2_state, fifo_tx_rst_done(2), txControls_general_reset_del, txControls_general_enable, txControls_fixedLatency_enable, txControls_fixedLatency_enable_latch, txControls_interleaver_enable_latch, s_tx_dsp_ch2_frev, fifo_tx_wr_ack(2), sync_tx, fifo_tx_wr_en(2), fifo_tx_num_data(2), expectedNumData_ch2) begin
	  case gblink_tx_fifo_ch2_state is
	    when FIFO_UNINIT =>
		  if (fifo_tx_rst_done(2) = '1') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_DISABLED; -- wait for the FIFO reset sequence to complete
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT;
		  end if;
		when FIFO_DISABLED =>
		  if (txControls_general_reset_del = '1') then -- or "txControls_general_reset"?
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- another reset issued before the enable
		  elsif (txControls_general_enable = '1') then -- wait for the enable through the MM
		    if (txControls_fixedLatency_enable = '0') then
			  gblink_tx_fifo_ch2_state_next <= FIFO_FREV_WAIT; -- beam-synchronous mode
			else
			  gblink_tx_fifo_ch2_state_next <= FIFO_SYNC_WAIT; -- fixed-latency mode
			end if;
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_DISABLED;
		  end if;
		when FIFO_FREV_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  -- ********** only difference between ch1 and ch2 FIFO state transtions ********** --
		  elsif (txControls_interleaver_enable_latch = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_FREV_WAIT; -- stay in the idle state if interleaving is not used
		  -- ********** only difference between ch1 and ch2 FIFO state transtions ********** --
	      elsif (s_tx_dsp_ch2_frev = '1') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_FREV_STORE; -- wait for the FREV1 pulse
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_FREV_WAIT;
	      end if;
		when FIFO_FREV_STORE =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_FREV_STORE_WAIT; -- go to the next state right away because "fifo_tx_wr_en" should be active for only 1 clock cycle, and not 2
	      end if;
		when FIFO_FREV_STORE_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (fifo_tx_wr_ack(2) = '1') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_BUCKETS; -- wait for the FREV entry to be stored
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_FREV_STORE_WAIT;
		  end if;
		when FIFO_SYNC_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  -- ********** only difference between ch1 and ch2 FIFO state transtions ********** --
		  elsif (txControls_interleaver_enable_latch = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_SYNC_WAIT; -- stay in the idle state if interleaving is not used
		  -- ********** only difference between ch1 and ch2 FIFO state transtions ********** --
	      elsif (sync_tx = '1') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_SYNC_STORE; -- wait for the SYNC pulse
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_SYNC_WAIT;
	      end if;
		when FIFO_SYNC_STORE =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_SYNC_STORE_WAIT; -- go to the next state right away because "fifo_tx_wr_en" should be active for only 1 clock cycle, and not 2
	      end if;
		when FIFO_SYNC_STORE_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (fifo_tx_wr_ack(2) = '1') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_BUCKETS; -- wait for the SYNC entry to be stored
		  else
		    gblink_tx_fifo_ch2_state_next <= FIFO_SYNC_STORE_WAIT;
		  end if;
		when FIFO_BUCKETS =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fifo_ch2_state_next <= FIFO_UNINIT; -- suddenly disabled, FIFO data needs to be flushed
		  elsif (txControls_fixedLatency_enable_latch = '0') then -- beam-synchronous mode
		    if (s_tx_dsp_ch2_frev = '1') then
			  gblink_tx_fifo_ch2_state_next <= FIFO_FREV_STORE;
		    elsif ((fifo_tx_wr_en(2) = '1') and (fifo_tx_num_data(2) = std_logic_vector(unsigned(expectedNumData_ch2) - 1))) then
			  gblink_tx_fifo_ch2_state_next <= FIFO_FREV_WAIT;
			else
			  gblink_tx_fifo_ch2_state_next <= FIFO_BUCKETS;
			end if;
		  else -- fixed-latency mode
		    gblink_tx_fifo_ch2_state_next <= FIFO_BUCKETS; -- no notion of turn, stay in BUCKETS state
	      end if;
		when others =>
		  null;
	  end case;
	end process;

	-- FSM next state logic
	process (gblink_tx_fsm_state, txControls_general_enable, txControls_fixedLatency_enable, txControls_fixedLatency_enable_latch, txControls_interleaver_enable_latch, fifo_tx_valid, fifo_tx_dout, m_axis_user_k_tvalid_int, m_axis_user_k_tready, data_read_done, data_present, m_axis_data_tvalid_int, m_axis_data_tready, packets_sent_counter, expectedNumDataPackets) begin
	  case gblink_tx_fsm_state is
	    when FSM_DISABLED =>
		  if (txControls_general_enable = '1') then -- wait for the enable through the MM
		    if (txControls_fixedLatency_enable = '0') then
			  gblink_tx_fsm_state_next <= FSM_FREV_WAIT; -- beam-synchronous mode
			else
			  gblink_tx_fsm_state_next <= FSM_SYNC_WAIT; -- fixed-latency mode
			end if;
		  else
		    gblink_tx_fsm_state_next <= FSM_DISABLED;
		  end if;
		when FSM_FREV_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif (txControls_interleaver_enable_latch = '0') then
		    if ((fifo_tx_valid(1) = '1') and (fifo_tx_dout(1) = C_FREV_FIFO_ENTRY)) then
			  gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- FREV entry read from the ch1 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_FREV_WAIT;
			end if;
		  elsif (fifo_tx_valid = "11") then
		    if ((fifo_tx_dout(1) = C_FREV_FIFO_ENTRY) and (fifo_tx_dout(2) = C_FREV_FIFO_ENTRY)) then
			  gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- FREV entries read simultaneously from both ch1 and ch2 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_FREV_WAIT;
			end if;
		  elsif (fifo_tx_valid(1) = '1') then
		    if (fifo_tx_dout(1) = C_FREV_FIFO_ENTRY) then
			  gblink_tx_fsm_state_next <= FSM_FREV_WAIT_CH2; -- FREV entry read from ch1 FIFO, wait for it on ch2 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_FREV_WAIT;
			end if;
		  elsif (fifo_tx_valid(2) = '1') then
		    if (fifo_tx_dout(2) = C_FREV_FIFO_ENTRY) then
			  gblink_tx_fsm_state_next <= FSM_FREV_WAIT_CH1; -- FREV entry read from ch2 FIFO, wait for it on ch1 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_FREV_WAIT;
			end if;
		  else
		    gblink_tx_fsm_state_next <= FSM_FREV_WAIT;
		  end if;
		when FSM_FREV_WAIT_CH1 =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_tx_valid(1) = '1') and (fifo_tx_dout(1) = C_FREV_FIFO_ENTRY)) then
		    gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- both FREV entries read (ch2, then ch1)
		  else
		    gblink_tx_fsm_state_next <= FSM_FREV_WAIT_CH1;
		  end if;
		when FSM_FREV_WAIT_CH2 =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_tx_valid(2) = '1') and (fifo_tx_dout(2) = C_FREV_FIFO_ENTRY)) then
		    gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- both FREV entries read (ch1, then ch2)
		  else
		    gblink_tx_fsm_state_next <= FSM_FREV_WAIT_CH2;
		  end if;
		when FSM_SYNC_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif (txControls_interleaver_enable_latch = '0') then
		    if ((fifo_tx_valid(1) = '1') and (fifo_tx_dout(1) = C_SYNC_FIFO_ENTRY)) then
			  gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- SYNC entry read from the ch1 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_SYNC_WAIT;
			end if;
		  elsif (fifo_tx_valid = "11") then
		    if ((fifo_tx_dout(1) = C_SYNC_FIFO_ENTRY) and (fifo_tx_dout(2) = C_SYNC_FIFO_ENTRY)) then
			  gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- SYNC entries read simultaneously from both ch1 and ch2 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_SYNC_WAIT;
			end if;
		  elsif (fifo_tx_valid(1) = '1') then
		    if (fifo_tx_dout(1) = C_SYNC_FIFO_ENTRY) then
			  gblink_tx_fsm_state_next <= FSM_SYNC_WAIT_CH2; -- SYNC entry read from ch1 FIFO, wait for it on ch2 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_SYNC_WAIT;
			end if;
		  elsif (fifo_tx_valid(2) = '1') then
		    if (fifo_tx_dout(2) = C_SYNC_FIFO_ENTRY) then
			  gblink_tx_fsm_state_next <= FSM_SYNC_WAIT_CH1; -- SYNC entry read from ch2 FIFO, wait for it on ch1 FIFO
			else
			  gblink_tx_fsm_state_next <= FSM_SYNC_WAIT;
			end if;
		  else
		    gblink_tx_fsm_state_next <= FSM_SYNC_WAIT;
		  end if;
		when FSM_SYNC_WAIT_CH1 =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_tx_valid(1) = '1') and (fifo_tx_dout(1) = C_SYNC_FIFO_ENTRY)) then
		    gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- both SYNC entries read (ch2, then ch1)
		  else
		    gblink_tx_fsm_state_next <= FSM_SYNC_WAIT_CH1;
		  end if;
		when FSM_SYNC_WAIT_CH2 =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((fifo_tx_valid(2) = '1') and (fifo_tx_dout(2) = C_SYNC_FIFO_ENTRY)) then
		    gblink_tx_fsm_state_next <= FSM_USER_K_SEND; -- both SYNC entries read (ch1, then ch2)
		  else
		    gblink_tx_fsm_state_next <= FSM_SYNC_WAIT_CH2;
		  end if;
		when FSM_USER_K_SEND =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((m_axis_user_k_tvalid_int = '1') and (m_axis_user_k_tready = '1')) then
		    gblink_tx_fsm_state_next <= FSM_BUFFER_WAIT; -- USER_K packet properly transmitted to the Aurora core
		  else
		    gblink_tx_fsm_state_next <= FSM_USER_K_SEND;
		  end if;
		when FSM_BUFFER_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif (txControls_interleaver_enable_latch = '0')  then
		    if (data_read_done = "11") then
			  gblink_tx_fsm_state_next <= FSM_DATA_SEND;
			else
			  gblink_tx_fsm_state_next <= FSM_BUFFER_WAIT;
			end if;
		  elsif ((data_read_done = "11") and (data_present /= "00")) then -- interleaving case, no dummy packets sent
		    gblink_tx_fsm_state_next <= FSM_DATA_SEND;
		  else
		    gblink_tx_fsm_state_next <= FSM_BUFFER_WAIT;
		  end if;
		when FSM_DATA_SEND =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fsm_state_next <= FSM_DISABLED; -- suddenly disabled
		  elsif ((m_axis_data_tvalid_int = '1') and (m_axis_data_tready = '1')) then -- DATA packet properly transmitted to the Aurora core
		    if (txControls_fixedLatency_enable_latch = '0') then -- beam-synchronous mode
			  if (packets_sent_counter = std_logic_vector(unsigned(expectedNumDataPackets) - 1)) then
			    gblink_tx_fsm_state_next <= FSM_FREV_WAIT; -- all the packets in the current turn have been transmitted
			  else
			    gblink_tx_fsm_state_next <= FSM_BUFFER_WAIT; -- more packets to be transmitted in the current turn
			  end if;
			else -- fixed-latency mode
			  gblink_tx_fsm_state_next <= FSM_BUFFER_WAIT; -- no notion of turn
			end if;
		  else
		    gblink_tx_fsm_state_next <= FSM_DATA_SEND;
		  end if;
		when others =>
		  null;
	  end case;
	end process;

	-- FRT next state logic
	process (gblink_tx_fixed_rate_state, txControls_general_enable, txControls_fixedLatency_enable_latch, sync_tx) begin
	  case gblink_tx_fixed_rate_state is
		when FRT_DISABLED =>
		  if (txControls_general_enable = '1') then
		    gblink_tx_fixed_rate_state_next <= FRT_SYNC_WAIT; -- wait for the enable through the MM
		  else
		    gblink_tx_fixed_rate_state_next <= FRT_DISABLED;
		  end if;
		when FRT_SYNC_WAIT =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fixed_rate_state_next <= FRT_DISABLED; -- suddenly disabled
		  elsif ((txControls_fixedLatency_enable_latch = '1') and (sync_tx = '1')) then
		    gblink_tx_fixed_rate_state_next <= FRT_FIXED_VALID; -- SYNC pulse detected (fixed-latency mode)
		  else
		    gblink_tx_fixed_rate_state_next <= FRT_SYNC_WAIT;
		  end if;
		when FRT_FIXED_VALID =>
		  if (txControls_general_enable = '0') then
		    gblink_tx_fixed_rate_state_next <= FRT_DISABLED; -- suddenly disabled
		  else
		    gblink_tx_fixed_rate_state_next <= FRT_FIXED_VALID;
		  end if;

		-- when LAT_WAIT =>
		  -- if (rxControls_general_enable = '0') then
		    -- gblink_rx_latency_state_next <= LAT_DISABLED; -- suddenly disabled
		  -- --elsif (latency_counter = std_logic_vector(to_unsigned(C_FAST_CLK_CYCLES_IN_US*to_integer(unsigned(rxControls_latency_latch)), latency_counter'length) - 1)) then
		  -- elsif (latency_counter = std_logic_vector(to_unsigned(2*to_integer(unsigned(rxControls_latency_latch)), latency_counter'length) - 1)) then -- factor 2 is due to faster/slower clock ratio
		    -- gblink_rx_latency_state_next <= LAT_STREAMING; -- count until desired latency is reached
		  -- else
		    -- gblink_rx_latency_state_next <= LAT_WAIT;
		  -- end if;
		-- when LAT_STREAMING =>
		  -- if (rxControls_general_enable = '0') then
		    -- gblink_rx_latency_state_next <= LAT_DISABLED; -- suddenly disabled
		  -- else
		    -- gblink_rx_latency_state_next <= LAT_STREAMING;
		  -- end if;
		when others =>
		  null;
	  end case;
	end process;

	-- Valid counter, i.e. when should fixed-rate input be written in the ch1/ch2 FIFOs?
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  valid_counter <= (others => '0');
		elsif (gblink_tx_fixed_rate_state = FRT_FIXED_VALID) then
		  valid_counter <= std_logic_vector(unsigned(valid_counter) + 1);
		else
		  valid_counter <= (others => '0');
		end if;
	  end if;
	end process;

	-- ratio 0 ("00"): ch1/ch2 fixed-rate input at 125 MSps
	-- ratio 1 ("01"): ch1/ch2 fixed-rate input at 62.5 MSps
	-- ratio 2 ("10"): ch1/ch2 fixed-rate input at 31.25 MSps
	-- ratio 3 ("11"): ch1/ch2 fixed-rate input at 15.625 MSps
	process (gblink_tx_fixed_rate_state, txControls_interleaver_rateRatioCh1_latch, txControls_interleaver_rateRatioCh2_latch, valid_counter) begin
	  if (gblink_tx_fixed_rate_state = FRT_FIXED_VALID) then
	    case txControls_interleaver_rateRatioCh1_latch is
		  when "00" =>
		    fixed_valid(1) <= '0';
			if (valid_counter(0) = '0') then -- every 2nd 250MHz clock cycle
			  fixed_valid(1) <= '1';
			end if;
		  when "01" =>
		    fixed_valid(1) <= '0';
			if (valid_counter(1 downto 0) = "00") then -- every 4th 250MHz clock cycle
			  fixed_valid(1) <= '1';
			end if;
		  when "10" =>
		    fixed_valid(1) <= '0';
			if (valid_counter(2 downto 0) = "000") then -- every 8th 250MHz clock cycle
			  fixed_valid(1) <= '1';
			end if;
		  when "11" =>
		    fixed_valid(1) <= '0';
			if (valid_counter(3 downto 0) = "0000") then -- every 16th 250MHz clock cycle
			  fixed_valid(1) <= '1';
			end if;
		  when others =>
		    null;
		end case;
	    case txControls_interleaver_rateRatioCh2_latch is
		  when "00" =>
		    fixed_valid(2) <= '0';
			if (valid_counter(0) = '0') then -- every 2nd 250MHz clock cycle
			  fixed_valid(2) <= '1';
			end if;
		  when "01" =>
		    fixed_valid(2) <= '0';
			if (valid_counter(1 downto 0) = "00") then -- every 4th 250MHz clock cycle
			  fixed_valid(2) <= '1';
			end if;
		  when "10" =>
		    fixed_valid(2) <= '0';
			if (valid_counter(2 downto 0) = "000") then -- every 8th 250MHz clock cycle
			  fixed_valid(2) <= '1';
			end if;
		  when "11" =>
		    fixed_valid(2) <= '0';
			if (valid_counter(3 downto 0) = "0000") then -- every 16th 250MHz clock cycle
			  fixed_valid(2) <= '1';
			end if;
		  when others =>
		    null;
		end case;
	  else
	    fixed_valid(1) <= '0';
		fixed_valid(2) <= '0';
	  end if;
	end process;

	m_axis_data_tstrb <= (others => '1'); -- not used by Aurora
	m_axis_data_tlast <= '1'; -- not used by Aurora

	m_axis_user_k_tstrb <= (others => '1'); -- not used by Aurora
	m_axis_user_k_tlast <= '1'; -- not used by Aurora

	-- Register stage for the general reset/enable
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  txControls_general_enable_del <= '0';
		  txControls_general_reset_del <= '0';
		else
		  txControls_general_enable_del <= txControls_general_enable;
		  txControls_general_reset_del <= txControls_general_reset;
		end if;
	  end if;
	end process;

	-- Synchronous TX ch1/ch2 FIFOs reset: through MM
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_tx_srst(1) <= '0';
		  fifo_tx_srst(2) <= '0';
		else
		  if (txControls_general_reset = '1') then
		    if (fifo_tx_srst(1) = '0') then
			  fifo_tx_srst(1) <= '1';
			else
			  fifo_tx_srst(1) <= '0';
			end if;
		  else
		    fifo_tx_srst(1) <= '0';
		  end if;
		  if (txControls_general_reset = '1') then
		    if (fifo_tx_srst(2) = '0') then
			  fifo_tx_srst(2) <= '1';
			else
			  fifo_tx_srst(2) <= '0';
			end if;
		  else
		    fifo_tx_srst(2) <= '0';
		  end if;
		  --fifo_tx_srst(1) <= txControls_general_reset;
		  --fifo_tx_srst(2) <= txControls_general_reset;
		end if;
	  end if;
	end process;

	-- Latch TX controls upon enable
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  txControls_numBuckets_latch <= (others => '0');
		  txControls_interleaver_enable_latch <= '0';
		  txControls_interleaver_rateRatioCh1_latch <= (others => '0');
		  txControls_interleaver_rateRatioCh2_latch <= (others => '0');
		  expectedNumData_ch1 <= (others => '0');
		  expectedNumData_ch2 <= (others => '0');
		  expectedNumDataPackets <= (others => '0');
		  txControls_fixedLatency_enable_latch <= '0';
		else
		  if ((txControls_general_enable = '1') and (txControls_general_enable_del = '0')) then -- save the parameters on the rising edge of enable signal
		    txControls_numBuckets_latch <= txControls_numBuckets;
			txControls_interleaver_enable_latch <= txControls_interleaver_enable;
			txControls_interleaver_rateRatioCh1_latch <= txControls_interleaver_rateRatioCh1;
			txControls_interleaver_rateRatioCh2_latch <= txControls_interleaver_rateRatioCh2;
			if (txControls_interleaver_enable = '1') then
			  expectedNumData_ch1 <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(txControls_numBuckets)), to_integer(unsigned(txControls_interleaver_rateRatioCh1))), expectedNumData_ch1'length));
			  expectedNumData_ch2 <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(txControls_numBuckets)), to_integer(unsigned(txControls_interleaver_rateRatioCh2))), expectedNumData_ch2'length));
			  if (to_integer(unsigned(txControls_interleaver_rateRatioCh1)) < to_integer(unsigned(txControls_interleaver_rateRatioCh2))) then
			    expectedNumDataPackets <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(txControls_numBuckets)), to_integer(unsigned(txControls_interleaver_rateRatioCh1))), expectedNumDataPackets'length));
			  else
			    expectedNumDataPackets <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(txControls_numBuckets)), to_integer(unsigned(txControls_interleaver_rateRatioCh2))), expectedNumDataPackets'length));
			  end if;
			else
			  expectedNumData_ch1 <= txControls_numBuckets;
			  expectedNumData_ch2 <= (others => '0');
			  expectedNumDataPackets <= std_logic_vector(to_unsigned(ceil_shift_right(to_integer(unsigned(txControls_numBuckets)), 0), expectedNumDataPackets'length));
			end if;
			txControls_fixedLatency_enable_latch <= txControls_fixedLatency_enable;
		  else
		    null;
		  end if;
		end if;
	  end if;
	end process;

-- Reset complete flags for the ch1/ch2 FIFOs (fast clock)
fifo_reset_inst_default1_fast : status_reg_latched
	generic map (
		C_STATUS_REG_WIDTH	=> 4,
		C_STATUS_DEFAULT	=> '1'
	)
	port map (
		clk		=> user_clk_faster,
		resetn	=> user_rst_faster,

		status_reg_in		=> tx_logic_status_default1,
		status_reg_reset	=> tx_logic_status_clear_default1,
		status_reg_out		=> tx_logic_status_latched_default1
	);
	tx_logic_status_default1 <= fifo_tx_rd_rst_busy(2) &
	                            fifo_tx_wr_rst_busy(2) &
								fifo_tx_rd_rst_busy(1) &
								fifo_tx_wr_rst_busy(1);
	tx_logic_status_clear_default1 <= fifo_tx_srst(2) &
	                                  fifo_tx_srst(2) &
									  fifo_tx_srst(1) &
									  fifo_tx_srst(1);
	fifo_tx_wr_rst_done(1) <= not tx_logic_status_latched_default1(0);
	txStatus_fifoWrRstDone(1) <= fifo_tx_wr_rst_done(1);
	fifo_tx_rd_rst_done(1) <= not tx_logic_status_latched_default1(1);
	txStatus_fifoRdRstDone(1) <= fifo_tx_rd_rst_done(1);
	fifo_tx_wr_rst_done(2) <= not tx_logic_status_latched_default1(2);
	txStatus_fifoWrRstDone(2) <= fifo_tx_wr_rst_done(2);
	fifo_tx_rd_rst_done(2) <= not tx_logic_status_latched_default1(3);
	txStatus_fifoRdRstDone(2) <= fifo_tx_rd_rst_done(2);

	fifo_tx_rst_done <= fifo_tx_wr_rst_done and fifo_tx_rd_rst_done;

	-- Catching errors on the ch1/ch2 FIFOs, WR port
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_tx_wr_en_del <= (others => '0');
		  txStatus_fifo_wr_error <= (others => '0');
		else
		  fifo_tx_wr_en_del <= fifo_tx_wr_en;
		  txStatus_fifo_wr_error <= fifo_tx_wr_ack xor fifo_tx_wr_en_del; -- error if "wr_ack" is not just "wr_en" delayed by 1 clock cycle
		end if;
	  end if;
	end process;

	-- Counting the number of data in the TX ch1/ch2 FIFOs (instantaneous)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_tx_level(1) <= (others => '0');
		  fifo_tx_level(2) <= (others => '0');
		else
		  if (fifo_tx_wr_ack(1) = '1' and fifo_tx_valid(1) = '0') then
		    fifo_tx_level(1) <= std_logic_vector(unsigned(fifo_tx_level(1)) + 1); -- data written
		  elsif (fifo_tx_wr_ack(1) = '0' and fifo_tx_valid(1) = '1') then
		    fifo_tx_level(1) <= std_logic_vector(unsigned(fifo_tx_level(1)) - 1); -- data read
		  else
		    null; -- nothing, or both written and read
		  end if;
		  if (fifo_tx_wr_ack(2) = '1' and fifo_tx_valid(2) = '0') then
		    fifo_tx_level(2) <= std_logic_vector(unsigned(fifo_tx_level(2)) + 1); -- data written
		  elsif (fifo_tx_wr_ack(2) = '0' and fifo_tx_valid(2) = '1') then
		    fifo_tx_level(2) <= std_logic_vector(unsigned(fifo_tx_level(2)) - 1); -- data read
		  else
		    null; -- nothing, or both written and read
		  end if;
		end if;
	  end if;
	end process;
	txStatus_fifoLevel(1) <= fifo_tx_level(1);
	txStatus_fifoLevel(2) <= fifo_tx_level(2);

	-- Counting the number of data in the TX ch1/ch2 FIFOs (current turn)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_tx_num_data(1) <= (others => '0');
		  fifo_tx_num_data(2) <= (others => '0');
		  --fifo_tx_num_data_turn(1) <= (others => '0');
		  --fifo_tx_num_data_turn(2) <= (others => '0');
		else
		  --fifo_tx_num_data_turn(1) <= fifo_tx_num_data_turn(1); -- default
		  if (gblink_tx_fifo_ch1_state = FIFO_BUCKETS) then
			if (s_tx_dsp_ch1_frev = '1') then
			  fifo_tx_num_data(1) <= (others => '0');
		    elsif (fifo_tx_wr_en(1) = '1') then
			  fifo_tx_num_data(1) <= std_logic_vector(unsigned(fifo_tx_num_data(1)) + 1); -- data written
			else
			  null;
			end if;
		  --elsif ((gblink_tx_fifo_ch1_state = FIFO_FREV_WAIT) or (gblink_tx_fifo_ch1_state = FIFO_FREV_STORE)) then
		  elsif (gblink_tx_fifo_ch1_state = FIFO_FREV_WAIT) then
		    fifo_tx_num_data(1) <= (others => '0'); -- reset on each turn
			--fifo_tx_num_data_turn(1) <= fifo_tx_num_data(1); -- save info for previous turn
		  else
			null;
		  end if;
		  --fifo_tx_num_data_turn(2) <= fifo_tx_num_data_turn(2); -- default
		  if (gblink_tx_fifo_ch2_state = FIFO_BUCKETS) then
			if (s_tx_dsp_ch2_frev = '1') then
			  fifo_tx_num_data(2) <= (others => '0');
		    elsif (fifo_tx_wr_en(2) = '1') then
			  fifo_tx_num_data(2) <= std_logic_vector(unsigned(fifo_tx_num_data(2)) + 1); -- data written
			else
			  null;
			end if;
		  --elsif ((gblink_tx_fifo_ch2_state = FIFO_FREV_WAIT) or (gblink_tx_fifo_ch2_state = FIFO_FREV_STORE)) then
		  elsif (gblink_tx_fifo_ch2_state = FIFO_FREV_WAIT) then
		    fifo_tx_num_data(2) <= (others => '0'); -- reset on each turn
			--fifo_tx_num_data_turn(2) <= fifo_tx_num_data(2); -- save info for previous turn
		  else
			null;
		  end if;
		end if;
	  end if;
	end process;
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  fifo_tx_num_data_turn(1) <= (others => '0');
		  fifo_tx_num_data_turn(2) <= (others => '0');
		else
		  if ((unsigned(fifo_tx_num_data(1)) = 0) and (unsigned(fifo_tx_num_data_del(1)) > 0)) then
		    fifo_tx_num_data_turn(1) <= fifo_tx_num_data_del(1); -- save info for previous turn
		  else
		    null;
		  end if;
		  if ((unsigned(fifo_tx_num_data(2)) = 0) and (unsigned(fifo_tx_num_data_del(2)) > 0)) then
		    fifo_tx_num_data_turn(2) <= fifo_tx_num_data_del(2); -- save info for previous turn
		  else
		    null;
		  end if;
		end if;
	  end if;
	end process;
	txStatus_fifoNumData(1) <= fifo_tx_num_data(1);
	txStatus_fifoNumData(2) <= fifo_tx_num_data(2);
	txStatus_fifoNumDataTurn(1) <= fifo_tx_num_data_turn(1);
	txStatus_fifoNumDataTurn(2) <= fifo_tx_num_data_turn(2);
	fifo_tx_num_data_low_error(1) <= s_tx_dsp_ch1_frev when ((txControls_fixedLatency_enable_latch = '0') and (gblink_tx_fifo_ch1_state = FIFO_BUCKETS) and (fifo_tx_num_data(1) < expectedNumData_ch1)) else -- too few data in the current turn (only for beam-synchronous mode)
	                                 '0';
	fifo_tx_num_data_low_error(2) <= s_tx_dsp_ch2_frev when ((txControls_fixedLatency_enable_latch = '0') and (gblink_tx_fifo_ch2_state = FIFO_BUCKETS) and (fifo_tx_num_data(2) < expectedNumData_ch1)) else -- too few data in the current turn (only for beam-synchronous mode)
	                                 '0';
	txStatus_fifoNumDataLowErr <= fifo_tx_num_data_low_error;
	fifo_tx_num_data_high_error(1) <= s_tx_dsp_ch1_valid_del3 when ((gblink_tx_fifo_ch1_state = FIFO_FREV_WAIT) and (unsigned(expectedNumData_ch1) > 0) and (fifo_tx_num_data(1) >= expectedNumData_ch1)) else -- too much data in the current turn (only for beam-synchronous mode)
	                                 '0';
	fifo_tx_num_data_high_error(2) <= s_tx_dsp_ch2_valid_del3 when ((gblink_tx_fifo_ch2_state = FIFO_FREV_WAIT) and (unsigned(expectedNumData_ch2) > 0) and (fifo_tx_num_data(2) >= expectedNumData_ch2)) else -- too much data in the current turn (only for beam-synchronous mode)
	                                 '0';
	txStatus_fifoNumDataHighErr <= fifo_tx_num_data_high_error;

	-- Fetching the valid data from the ch1/ch2 FIFOs
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  tx_data_buffer <= (others => '0');
		elsif (fifo_tx_valid = "11") then
		  tx_data_buffer(63 downto 32) <= fifo_tx_dout(1)(31 downto 0); -- ch1 data, goes to the MSW of the data buffer in case of interleaving
		  tx_data_buffer(31 downto 0) <= fifo_tx_dout(2)(31 downto 0); -- ch2 data, always goes to the LSW of the data buffer
		elsif (fifo_tx_valid(1) = '1') then
		  if (txControls_interleaver_enable_latch = '0') then
		    if (data_read_done_msw_ch1 = '0') then
			  tx_data_buffer(63 downto 32) <= fifo_tx_dout(1)(31 downto 0); -- no interleaving, ch1 data goes to the MSW of the data buffer
			  tx_data_buffer(31 downto 0) <= tx_data_buffer(31 downto 0);
			else
			  tx_data_buffer(63 downto 32) <= tx_data_buffer(63 downto 32);
			  tx_data_buffer(31 downto 0) <= fifo_tx_dout(1)(31 downto 0); -- no interleaving, ch1 data goes to the LSW of the data buffer
			end if;
		  else
		    tx_data_buffer(63 downto 32) <= fifo_tx_dout(1)(31 downto 0); -- ch1 data, goes to the MSW of the data buffer in case of interleaving
			tx_data_buffer(31 downto 0) <= tx_data_buffer(31 downto 0);
		  end if;
		elsif (fifo_tx_valid(2) = '1') then
		  tx_data_buffer(63 downto 32) <= tx_data_buffer(63 downto 32);
		  tx_data_buffer(31 downto 0) <= fifo_tx_dout(2)(31 downto 0); -- ch2 data, always goes to the LSW of the data buffer
		else
		  null;
		end if;
	  end if;
	end process;
	tx_data_buffer_msw <= tx_data_buffer(63 downto 32);
	tx_data_buffer_lsw <= tx_data_buffer(31 downto 0);

	-- Counting the number of valid data read from the ch1/ch2 FIFOs (current turn)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_read_counter(1) <= (others => '0');
		  data_read_counter(2) <= (others => '0');
		else
		  case gblink_tx_fsm_state is
	        when FSM_BUFFER_WAIT | FSM_DATA_SEND =>
			  if (fifo_tx_valid(1) = '1') then
			    data_read_counter(1) <= std_logic_vector(unsigned(data_read_counter(1)) + 1);
			  else
			    null;
			  end if;
			  if (fifo_tx_valid(2) = '1') then
			    data_read_counter(2) <= std_logic_vector(unsigned(data_read_counter(2)) + 1);
			  else
			    null;
			  end if;
		    when others =>
			  data_read_counter(1) <= (others => '0');
			  data_read_counter(2) <= (others => '0');
	      end case;
		end if;
	  end if;
	end process;

	-- Counting the number of valid data packets sent to the Aurora core (current turn)
	process (user_clk_slower) begin
	  if rising_edge(user_clk_slower) then
	    if (user_rst_slower = '0') then
		  packets_sent_counter <= (others => '0');
		else
		  case gblink_tx_fsm_state is
	        when FSM_BUFFER_WAIT | FSM_DATA_SEND =>
			  if ((m_axis_data_tvalid_int = '1') and (m_axis_data_tready = '1')) then
			    packets_sent_counter <= std_logic_vector(unsigned(packets_sent_counter) + 1);
			  else
			    null;
			  end if;
		    when others =>
			  packets_sent_counter <= (others => '0');
	      end case;
		end if;
	  end if;
	end process;

-- Instantiation of TX FIFO, channel 1
fifo_generator_gblink_tx_ch1_inst : fifo_generator_gblink_tx
	port map(
		clk => user_clk_faster,
		srst => fifo_tx_srst(1),
		din => fifo_tx_din(1),
		wr_en => fifo_tx_wr_en(1),
		rd_en => fifo_tx_rd_en(1),
		dout => fifo_tx_dout(1),
		full => fifo_tx_full(1),
		wr_ack => fifo_tx_wr_ack(1),
		overflow => fifo_tx_overflow(1),
		empty => fifo_tx_empty(1),
		valid => fifo_tx_valid(1),
		underflow => fifo_tx_underflow(1),
		wr_rst_busy => fifo_tx_wr_rst_busy(1),
		rd_rst_busy => fifo_tx_rd_rst_busy(1)
	);

-- Instantiation of TX FIFO, channel 2 (interleaving)
fifo_generator_gblink_tx_ch2_inst : fifo_generator_gblink_tx
	port map(
		clk => user_clk_faster,
		srst => fifo_tx_srst(2),
		din => fifo_tx_din(2),
		wr_en => fifo_tx_wr_en(2),
		rd_en => fifo_tx_rd_en(2),
		dout => fifo_tx_dout(2),
		full => fifo_tx_full(2),
		wr_ack => fifo_tx_wr_ack(2),
		overflow => fifo_tx_overflow(2),
		empty => fifo_tx_empty(2),
		valid => fifo_tx_valid(2),
		underflow => fifo_tx_underflow(2),
		wr_rst_busy => fifo_tx_wr_rst_busy(2),
		rd_rst_busy => fifo_tx_rd_rst_busy(2)
	);

	fifo_tx_din(1) <= C_FREV_FIFO_ENTRY when (gblink_tx_fifo_ch1_state = FIFO_FREV_STORE) else
	                  C_SYNC_FIFO_ENTRY when (gblink_tx_fifo_ch1_state = FIFO_SYNC_STORE) else
	                  (x"0" & s_tx_dsp_ch1_data_del3) when ((gblink_tx_fifo_ch1_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                  (x"0" & s_tx_dsp_ch1_data_del3) when ((gblink_tx_fifo_ch1_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '1')) else -- fixed-latency mode
	                  (others => 'X');
	fifo_tx_din(2) <= C_FREV_FIFO_ENTRY when (gblink_tx_fifo_ch2_state = FIFO_FREV_STORE) else
	                  C_SYNC_FIFO_ENTRY when (gblink_tx_fifo_ch2_state = FIFO_SYNC_STORE) else
	                  (x"0" & s_tx_dsp_ch2_data_del3) when ((gblink_tx_fifo_ch2_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                  (x"0" & s_tx_dsp_ch2_data_del3) when ((gblink_tx_fifo_ch2_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '1')) else -- fixed-latency mode
	                  (others => 'X');

	fifo_tx_wr_en(1) <= '1' when (gblink_tx_fifo_ch1_state = FIFO_FREV_STORE) else
	                    '1' when (gblink_tx_fifo_ch1_state = FIFO_SYNC_STORE) else
	                    s_tx_dsp_ch1_valid_del3 when ((gblink_tx_fifo_ch1_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                    fixed_valid_del2(1) when ((gblink_tx_fifo_ch1_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '1')) else -- fixed-latency mode
	                    '0';
	fifo_tx_wr_en(2) <= '1' when (gblink_tx_fifo_ch2_state = FIFO_FREV_STORE) else
	                    '1' when (gblink_tx_fifo_ch2_state = FIFO_SYNC_STORE) else
	                    s_tx_dsp_ch2_valid_del3 when ((gblink_tx_fifo_ch2_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                    fixed_valid_del2(2) when ((gblink_tx_fifo_ch2_state = FIFO_BUCKETS) and (txControls_fixedLatency_enable_latch = '1')) else -- fixed-latency mode
	                    '0';

	fifo_tx_rd_en(1) <= '1' when (gblink_tx_fsm_state = FSM_FREV_WAIT) else
	                    '1' when (gblink_tx_fsm_state = FSM_FREV_WAIT_CH1) else
						'1' when (gblink_tx_fsm_state = FSM_SYNC_WAIT) else
						'1' when (gblink_tx_fsm_state = FSM_SYNC_WAIT_CH1) else
						((not fifo_tx_valid(1) or not data_read_done_msw_ch1) and not data_read_done(1)) when ((gblink_tx_fsm_state = FSM_BUFFER_WAIT) and (txControls_interleaver_enable_latch = '0')) else
						(data_present(1) and not fifo_tx_valid(1) and not data_read_done(1)) when ((gblink_tx_fsm_state = FSM_BUFFER_WAIT) and (txControls_interleaver_enable_latch = '1')) else
	                    '0';
	fifo_tx_rd_en(2) <= '1' when ((gblink_tx_fsm_state = FSM_FREV_WAIT) and (txControls_interleaver_enable_latch = '1')) else
	                    '1' when (gblink_tx_fsm_state = FSM_FREV_WAIT_CH2) else
						'1' when ((gblink_tx_fsm_state = FSM_SYNC_WAIT) and (txControls_interleaver_enable_latch = '1')) else
						'1' when (gblink_tx_fsm_state = FSM_SYNC_WAIT_CH2) else
						(data_present(2) and not fifo_tx_valid(2) and not data_read_done(2)) when ((gblink_tx_fsm_state = FSM_BUFFER_WAIT) and (txControls_interleaver_enable_latch = '1')) else
	                    '0';

	txStatus_fifoWrErr <= txStatus_fifo_wr_error;
	txStatus_fifoFull <= fifo_tx_full;
	txStatus_fifoEmpty <= fifo_tx_empty;
	txStatus_fifoOverflow <= fifo_tx_overflow;
	txStatus_fifoUnderflow <= fifo_tx_underflow;

	fifo_tx_frev_error(1) <= fifo_tx_valid(1) when ((gblink_tx_fsm_state = FSM_FREV_WAIT) and not (fifo_tx_dout(1) = C_FREV_FIFO_ENTRY)) else -- FREV entry expected, but got different data
	                         fifo_tx_valid(1) when ((gblink_tx_fsm_state = FSM_FREV_WAIT_CH1) and not (fifo_tx_dout(1) = C_FREV_FIFO_ENTRY)) else -- FREV entry expected, but got different data
	                         '0';
	txStatus_fifoFrevErr(1) <= fifo_tx_frev_error(1);
	fifo_tx_frev_error(2) <= fifo_tx_valid(2) when ((gblink_tx_fsm_state = FSM_FREV_WAIT) and not (fifo_tx_dout(2) = C_FREV_FIFO_ENTRY)) else -- FREV entry expected, but got different data
	                         fifo_tx_valid(2) when ((gblink_tx_fsm_state = FSM_FREV_WAIT_CH2) and not (fifo_tx_dout(2) = C_FREV_FIFO_ENTRY)) else -- FREV entry expected, but got different data
	                         '0';
	txStatus_fifoFrevErr(2) <= fifo_tx_frev_error(2);

	fifo_tx_sync_error(1) <= fifo_tx_valid(1) when ((gblink_tx_fsm_state = FSM_SYNC_WAIT) and not (fifo_tx_dout(1) = C_SYNC_FIFO_ENTRY)) else -- SYNC entry expected, but got different data
	                         fifo_tx_valid(1) when ((gblink_tx_fsm_state = FSM_SYNC_WAIT_CH1) and not (fifo_tx_dout(1) = C_SYNC_FIFO_ENTRY)) else -- SYNC entry expected, but got different data
	                         '0';
	txStatus_fifoSyncErr(1) <= fifo_tx_sync_error(1);
	fifo_tx_sync_error(2) <= fifo_tx_valid(2) when ((gblink_tx_fsm_state = FSM_SYNC_WAIT) and not (fifo_tx_dout(2) = C_SYNC_FIFO_ENTRY)) else -- SYNC entry expected, but got different data
	                         fifo_tx_valid(2) when ((gblink_tx_fsm_state = FSM_SYNC_WAIT_CH2) and not (fifo_tx_dout(2) = C_SYNC_FIFO_ENTRY)) else -- SYNC entry expected, but got different data
	                         '0';
	txStatus_fifoSyncErr(2) <= fifo_tx_sync_error(2);

-- Sticky status bits
status_reg_latched_inst_default0 : status_reg_latched
	generic map (
		C_STATUS_REG_WIDTH	=> 16,
		C_STATUS_DEFAULT	=> '0'
	)
	port map (
		clk		=> user_clk_slower,
		resetn	=> user_rst_slower,

		status_reg_in		=> tx_logic_status_default0,
		status_reg_reset	=> tx_logic_status_clear_default0,
		status_reg_out		=> tx_logic_status_latched_default0
	);
	tx_logic_status_default0 <= fifo_tx_sync_error(2) &
	                            fifo_tx_sync_error(1) &
	                            fifo_tx_frev_error(2) &
	                            fifo_tx_frev_error(1) &
								fifo_tx_num_data_high_error(2) &
	                            fifo_tx_num_data_high_error(1) &
								fifo_tx_num_data_low_error(2) &
								fifo_tx_num_data_low_error(1) &
								fifo_tx_underflow(2) &
	                            fifo_tx_underflow(1) &
								fifo_tx_overflow(2) &
								fifo_tx_overflow(1) &
								fifo_tx_full(2) &
								fifo_tx_full(1) &
								txStatus_fifo_wr_error(2) &
	                            txStatus_fifo_wr_error(1);
	tx_logic_status_clear_default0 <= txControls_fifoSyncErrClear(2) &
	                                  txControls_fifoSyncErrClear(1) &
	                                  txControls_fifoFrevErrClear(2) &
	                                  txControls_fifoFrevErrClear(1) &
									  txControls_fifoNumDataErrHighClear(2) &
	                                  txControls_fifoNumDataErrHighClear(1) &
									  txControls_fifoNumDataErrLowClear(2) &
									  txControls_fifoNumDataErrLowClear(1) &
									  txControls_fifoUnderflowClear(2) &
	                                  txControls_fifoUnderflowClear(1) &
									  txControls_fifoOverflowClear(2) &
									  txControls_fifoOverflowClear(1) &
									  txControls_fifoFullClear(2) &
									  txControls_fifoFullClear(1) &
									  txControls_fifoWrErrClear(2) &
	                                  txControls_fifoWrErrClear(1);
	txStatus_fifoWrErrLatched(1) <= tx_logic_status_latched_default0(0);
	txStatus_fifoWrErrLatched(2) <= tx_logic_status_latched_default0(1);
	txStatus_fifoFullLatched(1) <= tx_logic_status_latched_default0(2);
	txStatus_fifoFullLatched(2) <= tx_logic_status_latched_default0(3);
	txStatus_fifoOverflowLatched(1) <= tx_logic_status_latched_default0(4);
	txStatus_fifoOverflowLatched(2) <= tx_logic_status_latched_default0(5);
	txStatus_fifoUnderflowLatched(1) <= tx_logic_status_latched_default0(6);
	txStatus_fifoUnderflowLatched(2) <= tx_logic_status_latched_default0(7);
	txStatus_fifoNumDataErrLowLatched(1) <= tx_logic_status_latched_default0(8);
	txStatus_fifoNumDataErrLowLatched(2) <= tx_logic_status_latched_default0(9);
	txStatus_fifoNumDataErrHighLatched(1) <= tx_logic_status_latched_default0(10);
	txStatus_fifoNumDataErrHighLatched(2) <= tx_logic_status_latched_default0(11);
	txStatus_fifoFrevErrLatched(1) <= tx_logic_status_latched_default0(12);
	txStatus_fifoFrevErrLatched(2) <= tx_logic_status_latched_default0(13);
	txStatus_fifoSyncErrLatched(1) <= tx_logic_status_latched_default0(14);
	txStatus_fifoSyncErrLatched(2) <= tx_logic_status_latched_default0(15);

	-- Data present counter (what packets consist ch1/ch2 data?)
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_present_counter <= (others => '0');
		elsif (txControls_interleaver_enable_latch = '1') then -- interleaving ON
		  if ((gblink_tx_fsm_state = FSM_BUFFER_WAIT) or (gblink_tx_fsm_state = FSM_DATA_SEND)) then
		    if (data_read_done = "11") then -- increase after data has been read from the ch1/ch2 FIFOs and is ready to be sent to the Aurora core
			  data_present_counter <= std_logic_vector(unsigned(data_present_counter) + 1);
			else
			  null;
			end if;
		  else
		    data_present_counter <= (others => '0'); -- reset on each turn
		  end if;
		else -- interleaving OFF
		  data_present_counter <= (others => '0'); -- doesn't matter
		end if;
	  end if;
	end process;

	-- ratio 0 ("00"): ch1/ch2 data present in every data packet
	-- ratio 1 ("01"): ch1/ch2 data present in every 2nd data packet
	-- ratio 2 ("10"): ch1/ch2 data present in every 4th data packet
	-- ratio 3 ("11"): ch1/ch2 data present in every 8th data packet
	process (txControls_interleaver_enable_latch, txControls_interleaver_rateRatioCh1_latch, txControls_interleaver_rateRatioCh2_latch, data_present_counter) begin
	  if (txControls_interleaver_enable_latch = '1') then -- interleaving ON
	    case txControls_interleaver_rateRatioCh1_latch is
	      when "00" =>
		    data_present(1) <= '1';
		  when "01" =>
		    data_present(1) <= '0';
		    if (data_present_counter(0) = '0') then
		      data_present(1) <= '1';
		    end if;
		  when "10" =>
		    data_present(1) <= '0';
		    if (data_present_counter(1 downto 0) = "00") then
		      data_present(1) <= '1';
		    end if;
		  when "11" =>
		    data_present(1) <= '0';
		    if (data_present_counter(2 downto 0) = "000") then
		      data_present(1) <= '1';
		    end if;
		  when others =>
		    null;
	    end case;
	    case txControls_interleaver_rateRatioCh2_latch is
	      when "00" =>
		    data_present(2) <= '1';
		  when "01" =>
		    data_present(2) <= '0';
		    if (data_present_counter(0) = '0') then
		      data_present(2) <= '1';
		    end if;
		  when "10" =>
		    data_present(2) <= '0';
		    if (data_present_counter(1 downto 0) = "00") then
		      data_present(2) <= '1';
		    end if;
		  when "11" =>
		    data_present(2) <= '0';
		    if (data_present_counter(2 downto 0) = "000") then
		      data_present(2) <= '1';
		    end if;
		  when others =>
		    null;
	    end case;
	  else -- interleaving OFF
	    data_present(1) <= '0'; -- doesn't matter
	    data_present(2) <= '0'; -- doesn't matter
	  end if;
	end process;

	-- Signals telling that the current valid MSW/LSW data has been read from the RX ch1/ch2 FIFOs
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_read_done(1) <= '0';
		  data_read_done(2) <= '0';
		elsif ((gblink_tx_fsm_state = FSM_BUFFER_WAIT) or (gblink_tx_fsm_state = FSM_DATA_SEND)) then
		  if (txControls_interleaver_enable_latch = '0') then
		    if (txControls_fixedLatency_enable_latch = '0') then -- beam-synchronous mode
			  if (data_read_counter(1) < expectedNumData_ch1) then
			    data_read_done(1) <= data_read_done_msw_ch1 and fifo_tx_valid(1); -- ch1 FIFO data stored in both MSW and LSW of the data buffer
			  else
			    data_read_done(1) <= data_read_done_msw_ch1; -- exception: last packet, odd number of buckets
			  end if;
			else -- fixed-latency mode
			  data_read_done(1) <= data_read_done_msw_ch1 and fifo_tx_valid(1); -- there is no dummy data in the packet, both the MSW and the LSW always contain valid data (no notion of turn)
			end if;
		  elsif (data_present(1) = '0') then
		    if (data_read_done(1) = '0') then
			  data_read_done(1) <= '1';
			elsif (data_read_done(2) = '1') then
			  data_read_done(1) <= '0'; -- dummy packets
			else
			  null;
			end if;
			--data_read_done(1) <= '1';
		  else
		    data_read_done(1) <= fifo_tx_valid(1); -- ch1 FIFO data stored in the MSW of the data buffer
		  end if;
		  if (txControls_interleaver_enable_latch = '0') then
		    data_read_done(2) <= '1';
		  elsif (data_present(2) = '0') then
		    if (data_read_done(2) = '0') then
			  data_read_done(2) <= '1';
			elsif (data_read_done(1) = '1') then
			  data_read_done(2) <= '0'; -- dummy packets
			else
			  null;
			end if;
			--data_read_done(2) <= '1';
		  else
		    data_read_done(2) <= fifo_tx_valid(2); -- ch2 FIFO data stored in the LSW of the data buffer
		  end if;
		else
		  data_read_done(1) <= '0'; -- reset on each new packet
		  data_read_done(2) <= '0'; -- reset on each new packet
		end if;
	  end if;
	end process;

	-- Signal telling that the current valid MSW data has been read from the RX ch1 FIFO
	process (user_clk_faster) begin
	  if rising_edge(user_clk_faster) then
	    if (user_rst_faster = '0') then
		  data_read_done_msw_ch1 <= '0';
		elsif (gblink_tx_fsm_state = FSM_BUFFER_WAIT) then
		  if (fifo_tx_valid(1) = '1') then
		    data_read_done_msw_ch1 <= '1';
		  else
		    null;
		  end if;
		else
		  data_read_done_msw_ch1 <= '0'; -- reset on each new packet
		end if;
	  end if;
	end process;
	-- data_read_done_msw_ch1 <= fifo_tx_valid(1) when ((gblink_tx_fsm_state = FSM_BUFFER_WAIT) or (gblink_tx_fsm_state = FSM_DATA_SEND)) else
	                          -- '0';

	-- -- dummy
	-- --m_axis_data_tvalid <= '0';
	-- --m_axis_data_tdata <= (others => '0');

	-- -- dummy
	-- --m_axis_user_k_tvalid <= '0';
	-- --m_axis_user_k_tdata <= (others => '0');

	-- -- M_AXIS_DATA: tvalid
	-- -- 2FF synchroniser
	-- -- m_axis_data_tvalid_sync_ff_control <= '1' when (gblink_tx_fsm_state = FSM_DATA_SEND) else
	                                      -- -- '0';
	-- process (user_clk_faster) begin
	  -- if rising_edge(user_clk_faster) then
	    -- if (user_rst_faster = '0') then
		  -- m_axis_data_tvalid_sync_ff_control <= '0';
		-- elsif (gblink_tx_fsm_state = FSM_DATA_SEND) then
		  -- m_axis_data_tvalid_sync_ff_control <= '1';
		-- else
		  -- m_axis_data_tvalid_sync_ff_control <= '0';
		-- end if;
	  -- end if;
	-- end process;
	-- process (m_axis_data_aclk) begin
	  -- if rising_edge(m_axis_data_aclk) then
	    -- m_axis_data_tvalid_sync_ff(1) <= m_axis_data_tvalid_sync_ff(0);
		-- m_axis_data_tvalid_sync_ff(0) <= m_axis_data_tvalid_sync_ff_control;
	  -- end if;
	-- end process;
	-- process (m_axis_data_aclk) begin
	  -- if rising_edge(m_axis_data_aclk) then
	    -- if (m_axis_data_aresetn = '0') then
		  -- m_axis_data_tvalid_int <= '0';
		-- elsif ((m_axis_data_tvalid_int = '0') and (m_axis_data_tvalid_sync_ff(0) = '1')) then -- valid for 1 clock cycle
		  -- m_axis_data_tvalid_int <= m_axis_data_tvalid_sync_ff(1);
		-- else
		  -- m_axis_data_tvalid_int <= '0';
		-- end if;
	  -- end if;
	-- end process;
	-- m_axis_data_tvalid <= m_axis_data_tvalid_int;

	-- -- M_AXIS_DATA: tdata
	-- -- 2FF synchroniser + data synchroniser
	-- -- m_axis_data_tdata_sync_ff_enable <= '1' when (gblink_tx_fsm_state = FSM_DATA_SEND) else
	                                    -- -- '0';
	-- process (user_clk_faster) begin
	  -- if rising_edge(user_clk_faster) then
	    -- if (user_rst_faster = '0') then
		  -- m_axis_data_tdata_sync_ff_enable <= '0';
		-- elsif (gblink_tx_fsm_state = FSM_DATA_SEND) then
		  -- m_axis_data_tdata_sync_ff_enable <= '1';
		-- else
		  -- m_axis_data_tdata_sync_ff_enable <= '0';
		-- end if;
	  -- end if;
	-- end process;
	-- process (m_axis_data_aclk) begin
	  -- if rising_edge(m_axis_data_aclk) then
	    -- if (m_axis_data_aresetn = '0') then
		  -- m_axis_data_tdata_sync_ff_data <= (others => '0');
		-- elsif (gblink_tx_fsm_state = FSM_DATA_SEND) then
		  -- m_axis_data_tdata_sync_ff_data <= tx_data_buffer;
		-- else
		  -- m_axis_data_tdata_sync_ff_data <= (others => 'X');
		-- end if;
	  -- end if;
	-- end process;
	-- process (m_axis_data_aclk) begin
	  -- if rising_edge(m_axis_data_aclk) then
	    -- m_axis_data_tdata_sync_ff(1) <= m_axis_data_tdata_sync_ff(0);
		-- m_axis_data_tdata_sync_ff(1) <= m_axis_data_tdata_sync_ff_enable;
	  -- end if;
	-- end process;
	-- process (m_axis_data_aclk) begin
	  -- if rising_edge(m_axis_data_aclk) then
	    -- if (m_axis_data_aresetn = '0') then
		  -- m_axis_data_tdata <= (others => '0');
		-- elsif (m_axis_data_tdata_sync_ff(1) = '1') then
		  -- m_axis_data_tdata <= m_axis_data_tdata_sync_ff_data;
		-- else
		  -- null;
		-- end if;
	  -- end if;
	-- end process;

	-- -- M_AXIS_USER_K: tvalid
	-- -- 2FF synchroniser
	-- -- m_axis_user_k_tvalid_sync_ff_control <= '1' when (gblink_tx_fsm_state = FSM_USER_K_SEND) else
	                                        -- -- '0';
	-- process (user_clk_faster) begin
	  -- if rising_edge(user_clk_faster) then
	    -- if (user_rst_faster = '0') then
		  -- m_axis_user_k_tvalid_sync_ff_control <= '0';
		-- elsif (gblink_tx_fsm_state = FSM_USER_K_SEND) then
		  -- m_axis_user_k_tvalid_sync_ff_control <= '1';
		-- else
		  -- m_axis_user_k_tvalid_sync_ff_control <= '0';
		-- end if;
	  -- end if;
	-- end process;
	-- process (m_axis_user_k_aclk) begin
	  -- if rising_edge(m_axis_user_k_aclk) then
	    -- m_axis_user_k_tvalid_sync_ff(1) <= m_axis_user_k_tvalid_sync_ff(0);
		-- m_axis_user_k_tvalid_sync_ff(0) <= m_axis_user_k_tvalid_sync_ff_control;
	  -- end if;
	-- end process;
	-- process (m_axis_user_k_aclk) begin
	  -- if rising_edge(m_axis_user_k_aclk) then
	    -- if (m_axis_user_k_aresetn = '0') then
		  -- m_axis_user_k_tvalid_int <= '0';
		-- elsif ((m_axis_user_k_tvalid_int = '0') and (m_axis_user_k_tvalid_sync_ff(0) = '1')) then -- valid for 1 clock cycle
		  -- m_axis_user_k_tvalid_int <= m_axis_user_k_tvalid_sync_ff(1);
		-- else
		  -- m_axis_user_k_tvalid_int <= '0';
		-- end if;
	  -- end if;
	-- end process;
	-- m_axis_user_k_tvalid <= m_axis_user_k_tvalid_int;

	-- -- M_AXIS_USER_K: tdata
	-- -- 2FF synchroniser + data synchroniser
	-- -- m_axis_user_k_tdata_sync_ff_enable <= '1' when ((gblink_tx_fsm_state = FSM_USER_K_SEND) and (txControls_fixedLatency_enable_latch = '0')) else -- beam-synchronous mode
	                                      -- -- '1' when ((gblink_tx_fsm_state = FSM_USER_K_SEND) and (txControls_fixedLatency_enable_latch = '1')) else -- fixed-latency mode
										  -- -- '0';
	-- process (user_clk_faster) begin
	  -- if rising_edge(user_clk_faster) then
	    -- if (user_rst_faster = '0') then
		  -- m_axis_user_k_tdata_sync_ff_enable <= '0';
		-- elsif (gblink_tx_fsm_state = FSM_USER_K_SEND) then
		  -- if (txControls_fixedLatency_enable_latch = '0') then
		    -- m_axis_user_k_tdata_sync_ff_enable <= '1'; -- beam-synchronous mode
		  -- else
		    -- m_axis_user_k_tdata_sync_ff_enable <= '1'; -- fixed-latency mode
          -- end if;			
		-- else
		  -- m_axis_user_k_tdata_sync_ff_enable <= '0';
		-- end if;
	  -- end if;
	-- end process;
	-- process (m_axis_user_k_aclk) begin
	  -- if rising_edge(m_axis_user_k_aclk) then
	    -- if (m_axis_user_k_aresetn = '0') then
		  -- m_axis_user_k_tdata_sync_ff_data <= (others => '0');
		-- elsif (gblink_tx_fsm_state = FSM_USER_K_SEND) then
		  -- if (txControls_fixedLatency_enable_latch = '0') then
		    -- m_axis_user_k_tdata_sync_ff_data <= C_FREV_USER_K_DATA; -- beam-synchronous mode
		  -- else
		    -- m_axis_user_k_tdata_sync_ff_data <= C_SYNC_USER_K_DATA; -- fixed-latency mode
          -- end if;			
		-- else
		  -- m_axis_user_k_tdata_sync_ff_data <= (others => 'X');
		-- end if;
	  -- end if;
	-- end process;

	-- process (m_axis_user_k_aclk) begin
	  -- if rising_edge(m_axis_user_k_aclk) then
	    -- m_axis_user_k_tdata_sync_ff(1) <= m_axis_user_k_tdata_sync_ff(0);
		-- m_axis_user_k_tdata_sync_ff(0) <= m_axis_user_k_tdata_sync_ff_enable;
	  -- end if;
	-- end process;
	-- process (m_axis_user_k_aclk) begin
	  -- if rising_edge(m_axis_user_k_aclk) then
	    -- if (m_axis_user_k_aresetn = '0') then
		  -- m_axis_user_k_tdata <= (others => '0');
		-- elsif (m_axis_user_k_tdata_sync_ff(1) = '1') then
		  -- m_axis_user_k_tdata <= m_axis_user_k_tdata_sync_ff_data;
		-- end if;
	  -- end if;
	-- end process;

	-- dummy
	--m_axis_data_tvalid <= '0';
	--m_axis_data_tdata <= (others => '0');

	-- dummy
	--m_axis_user_k_tvalid <= '0';
	--m_axis_user_k_tdata <= (others => '0');

	-- M_AXIS_DATA: tvalid
	process (m_axis_data_aclk) begin
	  if rising_edge(m_axis_data_aclk) then
	    if (m_axis_data_aresetn = '0') then
		  m_axis_data_tvalid_int <= '0';
		elsif (gblink_tx_fsm_state = FSM_DATA_SEND) then
		  -- if (m_axis_data_tvalid_int = '0') then
		    -- m_axis_data_tvalid_int <= '1';
		  -- else
		    -- m_axis_data_tvalid_int <= '0';
		  -- end if;
		  m_axis_data_tvalid_int <= '1';
		else
		  m_axis_data_tvalid_int <= '0';
		end if;
	  end if;
	end process;
	m_axis_data_tvalid <= m_axis_data_tvalid_int;

	-- M_AXIS_DATA: tdata
	process (m_axis_data_aclk) begin
	  if rising_edge(m_axis_data_aclk) then
	    if (m_axis_data_aresetn = '0') then
		  m_axis_data_tdata <= (others => '0');
		elsif (gblink_tx_fsm_state = FSM_DATA_SEND) then
		  m_axis_data_tdata <= tx_data_buffer;
		else
		  m_axis_data_tdata <= (others => 'X');
		end if;
	  end if;
	end process;

	-- M_AXIS_USER_K: tvalid
	process (m_axis_user_k_aclk) begin
	  if rising_edge(m_axis_user_k_aclk) then
	    if (m_axis_user_k_aresetn = '0') then
		  m_axis_user_k_tvalid_int <= '0';
		elsif (gblink_tx_fsm_state = FSM_USER_K_SEND) then
		  m_axis_user_k_tvalid_int <= '1';
		else
		  m_axis_user_k_tvalid_int <= '0';
		end if;
	  end if;
	end process;
	m_axis_user_k_tvalid <= m_axis_user_k_tvalid_int; -- and not m_axis_user_k_tready;

	-- M_AXIS_USER_K: tdata
	process (m_axis_user_k_aclk) begin
	  if rising_edge(m_axis_user_k_aclk) then
	    if (m_axis_user_k_aresetn = '0') then
		  m_axis_user_k_tdata <= (others => '0');
		elsif (gblink_tx_fsm_state = FSM_USER_K_SEND) then
		  if (txControls_fixedLatency_enable_latch = '0') then
		    m_axis_user_k_tdata <= C_FREV_USER_K_DATA; -- beam-synchronous mode
		  else
		    m_axis_user_k_tdata <= C_SYNC_USER_K_DATA; -- fixed-latency mode
		  end if;
		else
		  m_axis_user_k_tdata <= (others => 'X');
		end if;
	  end if;
	end process;

	-- Cheburashka MM status registers
	txStatus_fifoCh1NumData <= (x"0000" & txStatus_fifoNumData(1));
	txStatus_fifoCh1NumDataTurn <= (x"0000" & txStatus_fifoNumDataTurn(1));
	txStatus_fifoCh2NumData <= (x"0000" & txStatus_fifoNumData(2));
	txStatus_fifoCh2NumDataTurn <= (x"0000" & txStatus_fifoNumDataTurn(2));

end arch_behav;

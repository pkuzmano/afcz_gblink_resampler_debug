library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aurora_reset_logic is
	generic (
		C_RESET_PB_ASSERT_COUNTER_WIDTH	: integer := 8;
		C_PMA_INIT_ASSERT_COUNTER_WIDTH	: integer := 27
	);
	port (
		clk		: in std_logic;
		resetn	: in std_logic;

		txrxReset : in std_logic;

		aurora_user_clk		: in std_logic;
		aurora_user_resetn	: in std_logic;

		aurora_reset_pb	: out std_logic;
		aurora_pma_init	: out std_logic
	);
end entity aurora_reset_logic;

architecture arch_behav of aurora_reset_logic is

	type aurora_reset_fsm_state_t is (IDLE, RESET_PB_ASSERT, PMA_INIT_ASSERT, PMA_INIT_DEASSERT, INTERRUPT);

	signal aurora_txrxReset_reg	: std_logic_vector(1 downto 0);

	signal aurora_reset_fsm_state		: aurora_reset_fsm_state_t;
	signal aurora_reset_fsm_state_next	: aurora_reset_fsm_state_t;

	signal reset_pb_assert_counter			: std_logic_vector(C_RESET_PB_ASSERT_COUNTER_WIDTH-1 downto 0);
	constant RESET_PB_ASSERT_COUNTER_MAX	: std_logic_vector(C_RESET_PB_ASSERT_COUNTER_WIDTH-1 downto 0) := (others => '1');
	signal pma_init_assert_counter			: std_logic_vector(C_PMA_INIT_ASSERT_COUNTER_WIDTH-1 downto 0);
	constant PMA_INIT_ASSERT_COUNTER_MAX	: std_logic_vector(C_PMA_INIT_ASSERT_COUNTER_WIDTH-1 downto 0) := (others => '1');

begin

	process (clk) begin
	  if rising_edge(clk) then
	    if (resetn = '0') then
		  aurora_txrxReset_reg <= (others => '0');
		else
	      aurora_txrxReset_reg(0) <= txrxReset;
		  aurora_txrxReset_reg(1) <= aurora_txrxReset_reg(0);
		end if;
	  end if;
	end process;

	process (aurora_reset_fsm_state, aurora_txrxReset_reg, reset_pb_assert_counter, pma_init_assert_counter, txrxReset) begin
	  case aurora_reset_fsm_state is
	    when IDLE =>
	      if ((aurora_txrxReset_reg(0) = '1') and (aurora_txrxReset_reg(1) = '0')) then
		    aurora_reset_fsm_state_next <= RESET_PB_ASSERT;
		  else
		    aurora_reset_fsm_state_next <= IDLE;
	      end if;
		when RESET_PB_ASSERT =>
		  if (reset_pb_assert_counter = RESET_PB_ASSERT_COUNTER_MAX) then
		    aurora_reset_fsm_state_next <= PMA_INIT_ASSERT;
		  elsif ((aurora_txrxReset_reg(0) = '1') and (aurora_txrxReset_reg(1) = '0')) then
		    aurora_reset_fsm_state_next <= INTERRUPT;
		  else
		    aurora_reset_fsm_state_next <= RESET_PB_ASSERT;
		  end if;
		when PMA_INIT_ASSERT =>
		  if (pma_init_assert_counter = PMA_INIT_ASSERT_COUNTER_MAX) then
		    aurora_reset_fsm_state_next <= PMA_INIT_DEASSERT;
		  elsif ((aurora_txrxReset_reg(0) = '1') and (aurora_txrxReset_reg(1) = '0')) then
		    aurora_reset_fsm_state_next <= INTERRUPT;
		  else
		    aurora_reset_fsm_state_next <= PMA_INIT_ASSERT;
		  end if;
		when PMA_INIT_DEASSERT =>
		  if ((aurora_txrxReset_reg(0) = '1') and (aurora_txrxReset_reg(1) = '0')) then
		    aurora_reset_fsm_state_next <= INTERRUPT;
		  else
		    aurora_reset_fsm_state_next <= IDLE;
		  end if;
		when INTERRUPT =>
		  if (txrxReset = '0') then
		    aurora_reset_fsm_state_next <= IDLE;
		  else
		    aurora_reset_fsm_state_next <= INTERRUPT;
		  end if;
		when others =>
		  null;
	  end case;
	end process;

	process (clk) begin
	  if rising_edge(clk) then
	    if (resetn = '0') then
		  aurora_reset_fsm_state <= IDLE;
		else
		  aurora_reset_fsm_state <= aurora_reset_fsm_state_next;
		end if;
	  end if;
	end process;

	process (aurora_reset_fsm_state) begin
	  case aurora_reset_fsm_state is
	    when IDLE =>
		  aurora_reset_pb <= '0';
		  aurora_pma_init <= '0';
		when RESET_PB_ASSERT =>
		  aurora_reset_pb <= '1';
		  aurora_pma_init <= '0';
		when PMA_INIT_ASSERT =>
		  aurora_reset_pb <= '1';
		  aurora_pma_init <= '1';
		when PMA_INIT_DEASSERT =>
		  aurora_reset_pb <= '1';
		  aurora_pma_init <= '0';
		when INTERRUPT =>
		  aurora_reset_pb <= '0';
		  aurora_pma_init <= '0';
		when others =>
		  aurora_reset_pb <= '0';
		  aurora_pma_init <= '0';
	  end case;
	end process;

	process (aurora_user_clk) begin
	  if rising_edge(aurora_user_clk) then
	    if (aurora_user_resetn = '0') then
		  reset_pb_assert_counter <= (others => '0');
		  pma_init_assert_counter <= (others => '0');
		else
		  case aurora_reset_fsm_state is
	        when IDLE =>
			  reset_pb_assert_counter <= (others => '0');
			  pma_init_assert_counter <= (others => '0');
		    when RESET_PB_ASSERT =>
			  reset_pb_assert_counter <= std_logic_vector(unsigned(reset_pb_assert_counter) + 1);
			  pma_init_assert_counter <= (others => '0');
		    when PMA_INIT_ASSERT =>
			  reset_pb_assert_counter <= (others => '0');
			  pma_init_assert_counter <= std_logic_vector(unsigned(pma_init_assert_counter) + 1);
		    when PMA_INIT_DEASSERT =>
			  reset_pb_assert_counter <= (others => '0');
			  pma_init_assert_counter <= (others => '0');
		    when INTERRUPT =>
			  reset_pb_assert_counter <= (others => '0');
			  pma_init_assert_counter <= (others => '0');
		    when others =>
			  reset_pb_assert_counter <= (others => '0');
			  pma_init_assert_counter <= (others => '0');
	      end case;
		end if;
	  end if;
	end process;

end architecture arch_behav;

library ieee;
use ieee.std_logic_1164.all;

entity status_reg_latched is
	generic (
		C_STATUS_REG_WIDTH	: integer := 1;
		C_STATUS_DEFAULT	: std_logic := '0'
	);
	port (
		clk		: in std_logic;
		resetn	: in std_logic;

		status_reg_in		: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_reset	: in std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
		status_reg_out		: out std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0)
	);
end entity status_reg_latched;

architecture arch_behav of status_reg_latched is

	signal status_reg_in_reg0 : std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);
	signal status_reg_in_reg1 : std_logic_vector(C_STATUS_REG_WIDTH-1 downto 0);

begin

	process (clk) begin
	  if rising_edge(clk) then
	    if (resetn = '0') then
		  status_reg_in_reg0 <= (others => '0');
		  status_reg_in_reg1 <= (others => '0');
		else
	      status_reg_in_reg0 <= status_reg_in;
		  status_reg_in_reg1 <= status_reg_in_reg0;
		end if;
	  end if;
	end process;

	process (clk) begin
	  if rising_edge(clk) then
	    if (resetn = '0') then
		  status_reg_out <= (others => C_STATUS_DEFAULT);
		else
		  for i in C_STATUS_REG_WIDTH-1 downto 0 loop
		    if (status_reg_reset(i) = '1') then
			  status_reg_out(i) <= C_STATUS_DEFAULT;
			else
			  if ((status_reg_in_reg0(i) = (not C_STATUS_DEFAULT)) and (status_reg_in_reg1(i) = C_STATUS_DEFAULT)) then
			    status_reg_out(i) <= (not C_STATUS_DEFAULT);
			  else
			    null;
			  end if;
			end if;
		  end loop;
		end if;
	  end if;
	end process;

end architecture arch_behav;

--- VME memory map resampler
--- Memory map version 20191010
--- Generated on 2019-10-11 by pkuzmano using VHDLMap (Gena component)

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package MemMap_resampler is

	-- Ident Code
	constant C_resampler_IdentCode : std_logic_vector(31 downto 0) := X"00000000";

	-- Memory Map Version
	constant C_resampler_MemMapVersion : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(20191010,32));

	-- Semantic Memory Map Version
	constant C_resampler_SemanticMemMapVersion : std_logic_vector(31 downto 0) := X"00010000";

	-- Register Addresses : Memory Map
	constant C_Reg_resampler_toutUpHigh16		:		std_logic_vector(9 downto 2) := "00000000";-- : Word address : "0" & X"00"; Byte Address : X"00"
	constant C_Reg_resampler_toutUpLow16		:		std_logic_vector(9 downto 2) := "00000001";-- : Word address : "0" & X"01"; Byte Address : X"02"
	constant C_Reg_resampler_numData		:		std_logic_vector(9 downto 2) := "00000010";-- : Word address : "0" & X"02"; Byte Address : X"04"
	constant C_Reg_resampler_numDataTurn		:		std_logic_vector(9 downto 2) := "00000011";-- : Word address : "0" & X"03"; Byte Address : X"06"
	constant C_Reg_resampler_control		:		std_logic_vector(9 downto 2) := "00000100";-- : Word address : "0" & X"04"; Byte Address : X"08"
	constant C_Reg_resampler_numBuckets		:		std_logic_vector(9 downto 2) := "00000101";-- : Word address : "0" & X"05"; Byte Address : X"0a"
	constant C_Reg_resampler_k		:		std_logic_vector(9 downto 2) := "00000110";-- : Word address : "0" & X"06"; Byte Address : X"0c"

	-- Register Auto Clear Masks : Memory Map
	constant C_ACM_resampler_toutUpHigh16		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_resampler_toutUpLow16		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_resampler_numData		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_resampler_numDataTurn		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_resampler_control		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_resampler_numBuckets		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_resampler_k		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- Register Preset Masks : Memory Map
	constant C_PSM_resampler_toutUpHigh16		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_resampler_toutUpLow16		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_resampler_numData		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_resampler_numDataTurn		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_resampler_control		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_resampler_numBuckets		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_resampler_k		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- CODE FIELDS
	-- Memory Data : Memory Map
	-- Submap Addresses : Memory Map
end;
-- EOF
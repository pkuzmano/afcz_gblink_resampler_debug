--- VME memory map gbLink
--- Memory map version 20190813
--- Generated on 2019-10-10 by pkuzmano using VHDLMap (Gena component)

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package MemMap_gbLink is

	-- Ident Code
	constant C_gbLink_IdentCode : std_logic_vector(31 downto 0) := X"00000000";

	-- Memory Map Version
	constant C_gbLink_MemMapVersion : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(20190813,32));

	-- Semantic Memory Map Version
	constant C_gbLink_SemanticMemMapVersion : std_logic_vector(31 downto 0) := X"00010000";

	constant C_Area_gbLink_txStatus		:		std_logic_vector(11 downto 10) := "01";
	constant C_Area_gbLink_txControls		:		std_logic_vector(11 downto 10) := "10";
	constant C_Area_gbLink_rxControls		:		std_logic_vector(11 downto 10) := "11";
	-- Register Addresses : Area txStatus
	constant C_Reg_gbLink_txStatus_fifoCh1NumData		:		std_logic_vector(9 downto 2) := "00000000";-- : Word address : "0" & X"00"; Byte Address : X"00"
	constant C_Reg_gbLink_txStatus_fifoCh1NumDataTurn		:		std_logic_vector(9 downto 2) := "00000001";-- : Word address : "0" & X"01"; Byte Address : X"02"
	constant C_Reg_gbLink_txStatus_fifoCh2NumData		:		std_logic_vector(9 downto 2) := "00000010";-- : Word address : "0" & X"02"; Byte Address : X"04"
	constant C_Reg_gbLink_txStatus_fifoCh2NumDataTurn		:		std_logic_vector(9 downto 2) := "00000011";-- : Word address : "0" & X"03"; Byte Address : X"06"

	-- Register Auto Clear Masks : Area txStatus
	constant C_ACM_gbLink_txStatus_fifoCh1NumData		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_gbLink_txStatus_fifoCh1NumDataTurn		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_gbLink_txStatus_fifoCh2NumData		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_gbLink_txStatus_fifoCh2NumDataTurn		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"

	-- Register Preset Masks : Area txStatus
	constant C_PSM_gbLink_txStatus_fifoCh1NumData		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_gbLink_txStatus_fifoCh1NumDataTurn		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_gbLink_txStatus_fifoCh2NumData		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_gbLink_txStatus_fifoCh2NumDataTurn		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"

	-- CODE FIELDS
	-- Memory Data : Area txStatus
	-- Submap Addresses : Area txStatus
	-- Register Addresses : Area txControls
	constant C_Reg_gbLink_txControls_general		:		std_logic_vector(9 downto 2) := "00000000";-- : Word address : "0" & X"00"; Byte Address : X"00"
	constant C_Reg_gbLink_txControls_numBuckets		:		std_logic_vector(9 downto 2) := "00000001";-- : Word address : "0" & X"01"; Byte Address : X"02"
	constant C_Reg_gbLink_txControls_interleaver		:		std_logic_vector(9 downto 2) := "00000010";-- : Word address : "0" & X"02"; Byte Address : X"04"
	constant C_Reg_gbLink_txControls_fixedLatency		:		std_logic_vector(9 downto 2) := "00000011";-- : Word address : "0" & X"03"; Byte Address : X"06"

	-- Register Auto Clear Masks : Area txControls
	constant C_ACM_gbLink_txControls_general		:		std_logic_vector(15 downto 0) := "0000000000000010";-- : Value : X"0002"
	constant C_ACM_gbLink_txControls_numBuckets		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_gbLink_txControls_interleaver		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_gbLink_txControls_fixedLatency		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- Register Preset Masks : Area txControls
	constant C_PSM_gbLink_txControls_general		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_gbLink_txControls_numBuckets		:		std_logic_vector(15 downto 0) := "0001001000001100";-- : Value : X"120c"
	constant C_PSM_gbLink_txControls_interleaver		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_gbLink_txControls_fixedLatency		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- CODE FIELDS
	-- Memory Data : Area txControls
	-- Submap Addresses : Area txControls
	-- Register Addresses : Area rxControls
	constant C_Reg_gbLink_rxControls_general		:		std_logic_vector(9 downto 2) := "00000000";-- : Word address : "0" & X"00"; Byte Address : X"00"
	constant C_Reg_gbLink_rxControls_numBuckets		:		std_logic_vector(9 downto 2) := "00000001";-- : Word address : "0" & X"01"; Byte Address : X"02"
	constant C_Reg_gbLink_rxControls_interleaver		:		std_logic_vector(9 downto 2) := "00000010";-- : Word address : "0" & X"02"; Byte Address : X"04"
	constant C_Reg_gbLink_rxControls_fixedLatency		:		std_logic_vector(9 downto 2) := "00000011";-- : Word address : "0" & X"03"; Byte Address : X"06"
	constant C_Reg_gbLink_rxControls_latency		:		std_logic_vector(9 downto 2) := "00000100";-- : Word address : "0" & X"04"; Byte Address : X"08"

	-- Register Auto Clear Masks : Area rxControls
	constant C_ACM_gbLink_rxControls_general		:		std_logic_vector(15 downto 0) := "0000000000000010";-- : Value : X"0002"
	constant C_ACM_gbLink_rxControls_numBuckets		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_gbLink_rxControls_interleaver		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_gbLink_rxControls_fixedLatency		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_gbLink_rxControls_latency		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- Register Preset Masks : Area rxControls
	constant C_PSM_gbLink_rxControls_general		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_gbLink_rxControls_numBuckets		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_gbLink_rxControls_interleaver		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_gbLink_rxControls_fixedLatency		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_gbLink_rxControls_latency		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- CODE FIELDS
	-- Memory Data : Area rxControls
	-- Submap Addresses : Area rxControls
	-- Register Addresses : Memory Map
	constant C_Reg_gbLink_firmwareID		:		std_logic_vector(11 downto 2) := "0000000000";-- : Word address : "00" & "00" & X"00"; Byte Address : "00" & X"00"
	constant C_Reg_gbLink_firmwareVersion		:		std_logic_vector(11 downto 2) := "0000000001";-- : Word address : "00" & "00" & X"01"; Byte Address : "00" & X"02"
	constant C_Reg_gbLink_designerID		:		std_logic_vector(11 downto 2) := "0000000010";-- : Word address : "00" & "00" & X"02"; Byte Address : "00" & X"04"
	constant C_Reg_gbLink_auroraStatus		:		std_logic_vector(11 downto 2) := "0000000011";-- : Word address : "00" & "00" & X"03"; Byte Address : "00" & X"06"
	constant C_Reg_gbLink_auroraControl		:		std_logic_vector(11 downto 2) := "0000000100";-- : Word address : "00" & "00" & X"04"; Byte Address : "00" & X"08"

	-- Register Auto Clear Masks : Memory Map
	constant C_ACM_gbLink_firmwareID		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_gbLink_firmwareVersion		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_gbLink_designerID		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_gbLink_auroraStatus		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_gbLink_auroraControl		:		std_logic_vector(15 downto 0) := "0000000001111101";-- : Value : X"007d"

	-- Register Preset Masks : Memory Map
	constant C_PSM_gbLink_firmwareID		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_gbLink_firmwareVersion		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_gbLink_designerID		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_gbLink_auroraStatus		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_gbLink_auroraControl		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- CODE FIELDS
	-- Memory Data : Memory Map
	-- Submap Addresses : Memory Map
end;
-- EOF
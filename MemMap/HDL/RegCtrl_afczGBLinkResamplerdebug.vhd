-- VME registers for afczGBLinkResamplerDebug
-- Memory map version 20191011
-- Generated on 2019-10-11 10:46:13 by pkuzmano using Gena+ VHDL RegCtrl factory (2018-03-16)

library CommonVisual;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.MemMap_afczGBLinkResamplerDebug.all;
use work.MemMap_resampler.all;
use work.MemMap_ncoEmulator.all;

entity RegCtrl_afczGBLinkResamplerDebug is
	port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(11 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		resamplerCh1_Sel : out std_logic;
		resamplerCh1_Addr : out std_logic_vector(9 downto 2);
		resamplerCh1_RdData :  in std_logic_vector(31 downto 0);
		resamplerCh1_WrData : out std_logic_vector(31 downto 0);
		resamplerCh1_RdMem : out std_logic;
		resamplerCh1_WrMem : out std_logic;
		resamplerCh1_RdDone : in std_logic;
		resamplerCh1_WrDone : in std_logic;
		resamplerCh1_RdError : in std_logic := '0';
		resamplerCh1_WrError : in std_logic := '0';
		ncoEmulatorCh1_Sel : out std_logic;
		ncoEmulatorCh1_Addr : out std_logic_vector(9 downto 2);
		ncoEmulatorCh1_RdData :  in std_logic_vector(31 downto 0);
		ncoEmulatorCh1_WrData : out std_logic_vector(31 downto 0);
		ncoEmulatorCh1_RdMem : out std_logic;
		ncoEmulatorCh1_WrMem : out std_logic;
		ncoEmulatorCh1_RdDone : in std_logic;
		ncoEmulatorCh1_WrDone : in std_logic;
		ncoEmulatorCh1_RdError : in std_logic := '0';
		ncoEmulatorCh1_WrError : in std_logic := '0';
		resamplerCh2_Sel : out std_logic;
		resamplerCh2_Addr : out std_logic_vector(9 downto 2);
		resamplerCh2_RdData :  in std_logic_vector(31 downto 0);
		resamplerCh2_WrData : out std_logic_vector(31 downto 0);
		resamplerCh2_RdMem : out std_logic;
		resamplerCh2_WrMem : out std_logic;
		resamplerCh2_RdDone : in std_logic;
		resamplerCh2_WrDone : in std_logic;
		resamplerCh2_RdError : in std_logic := '0';
		resamplerCh2_WrError : in std_logic := '0';
		ncoEmulatorCh2_Sel : out std_logic;
		ncoEmulatorCh2_Addr : out std_logic_vector(9 downto 2);
		ncoEmulatorCh2_RdData :  in std_logic_vector(31 downto 0);
		ncoEmulatorCh2_WrData : out std_logic_vector(31 downto 0);
		ncoEmulatorCh2_RdMem : out std_logic;
		ncoEmulatorCh2_WrMem : out std_logic;
		ncoEmulatorCh2_RdDone : in std_logic;
		ncoEmulatorCh2_WrDone : in std_logic;
		ncoEmulatorCh2_RdError : in std_logic := '0';
		ncoEmulatorCh2_WrError : in std_logic := '0'
	);
end;

architecture Gena of RegCtrl_afczGBLinkResamplerDebug is
	signal Loc_VMERdMem : std_logic_vector(2 downto 0);
	signal Loc_VMEWrMem : std_logic_vector(1 downto 0);
	signal CRegRdData : std_logic_vector(31 downto 0);
	signal CRegRdOK : std_logic;
	signal CRegWrOK : std_logic;
	signal Loc_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_CRegRdOK : std_logic;
	signal Loc_CRegWrOK : std_logic;
	signal RegRdDone : std_logic;
	signal RegWrDone : std_logic;
	signal RegRdData : std_logic_vector(31 downto 0);
	signal RegRdOK : std_logic;
	signal Loc_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_RegRdOK : std_logic;
	signal MemRdData : std_logic_vector(31 downto 0);
	signal MemRdDone : std_logic;
	signal MemWrDone : std_logic;
	signal Loc_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_MemRdDone : std_logic;
	signal Loc_MemWrDone : std_logic;
	signal RdData : std_logic_vector(31 downto 0);
	signal RdDone : std_logic;
	signal WrDone : std_logic;
	signal RegRdError : std_logic;
	signal RegWrError : std_logic;
	signal MemRdError : std_logic;
	signal MemWrError : std_logic;
	signal Loc_MemRdError : std_logic;
	signal Loc_MemWrError : std_logic;
	signal RdError : std_logic;
	signal WrError : std_logic;
	signal Sel_resamplerCh1 : std_logic;
	signal Sel_ncoEmulatorCh1 : std_logic;
	signal Sel_resamplerCh2 : std_logic;
	signal Sel_ncoEmulatorCh2 : std_logic;
begin
	Loc_CRegRdData <= (others => '0');
	Loc_CRegRdOK <= '0';
	Loc_CRegWrOK <= '0';

	CRegRdData <= Loc_CRegRdData;
	CRegRdOK <= Loc_CRegRdOK;
	CRegWrOK <= Loc_CRegWrOk;

	Loc_RegRdData <= CRegRdData;
	Loc_RegRdOK <= CRegRdOK;

	RegRdData <= Loc_RegRdData;
	RegRdOK <= Loc_RegRdOK;

	RegRdDone <= Loc_VMERdMem(0) and RegRdOK;
	RegWrDone <= Loc_VMEWrMem(0) and CRegWrOK;

	RegRdError <= Loc_VMERdMem(0) and not RegRdOK;
	RegWrError <= Loc_VMEWrMem(0) and not CRegWrOK;

	Loc_MemRdData <= RegRdData;
	Loc_MemRdDone <= RegRdDone;
	Loc_MemRdError <= RegRdError;

	MemRdData <= Loc_MemRdData;
	MemRdDone <= Loc_MemRdDone;
	MemRdError <= Loc_MemRdError;

	Loc_MemWrDone <= RegWrDone;
	Loc_MemWrError <= RegWrError;

	MemWrDone <= Loc_MemWrDone;
	MemWrError <= Loc_MemWrError;

	AreaRdMux: process (VMEAddr, MemRdData, MemRdDone, resamplerCh1_RdData, resamplerCh1_RdDone, resamplerCh1_RdError, ncoEmulatorCh1_RdData, ncoEmulatorCh1_RdDone, ncoEmulatorCh1_RdError, resamplerCh2_RdData, resamplerCh2_RdDone, resamplerCh2_RdError, ncoEmulatorCh2_RdData, ncoEmulatorCh2_RdDone, ncoEmulatorCh2_RdError, MemRdError) begin
		Sel_resamplerCh1 <= '0';
		Sel_ncoEmulatorCh1 <= '0';
		Sel_resamplerCh2 <= '0';
		Sel_ncoEmulatorCh2 <= '0';
		if VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_resamplerCh1 then
			RdData <= resamplerCh1_RdData;
			RdDone <= resamplerCh1_RdDone;
			RdError <= resamplerCh1_RdError;
			Sel_resamplerCh1 <= '1';
		elsif VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_ncoEmulatorCh1 then
			RdData <= ncoEmulatorCh1_RdData;
			RdDone <= ncoEmulatorCh1_RdDone;
			RdError <= ncoEmulatorCh1_RdError;
			Sel_ncoEmulatorCh1 <= '1';
		elsif VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_resamplerCh2 then
			RdData <= resamplerCh2_RdData;
			RdDone <= resamplerCh2_RdDone;
			RdError <= resamplerCh2_RdError;
			Sel_resamplerCh2 <= '1';
		elsif VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_ncoEmulatorCh2 then
			RdData <= ncoEmulatorCh2_RdData;
			RdDone <= ncoEmulatorCh2_RdDone;
			RdError <= ncoEmulatorCh2_RdError;
			Sel_ncoEmulatorCh2 <= '1';
		else
			RdData <= MemRdData;
			RdDone <= MemRdDone;
			RdError <= MemRdError;
		end if;
	end process AreaRdMux;

	AreaWrMux: process (VMEAddr, MemWrDone, resamplerCh1_WrDone, resamplerCh1_WrError, ncoEmulatorCh1_WrDone, ncoEmulatorCh1_WrError, resamplerCh2_WrDone, resamplerCh2_WrError, ncoEmulatorCh2_WrDone, ncoEmulatorCh2_WrError, MemWrError) begin
		if VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_resamplerCh1 then
			WrDone <= resamplerCh1_WrDone;
			WrError <= resamplerCh1_WrError;
		elsif VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_ncoEmulatorCh1 then
			WrDone <= ncoEmulatorCh1_WrDone;
			WrError <= ncoEmulatorCh1_WrError;
		elsif VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_resamplerCh2 then
			WrDone <= resamplerCh2_WrDone;
			WrError <= resamplerCh2_WrError;
		elsif VMEAddr(11 downto 10) = C_Submap_afczGBLinkResamplerDebug_ncoEmulatorCh2 then
			WrDone <= ncoEmulatorCh2_WrDone;
			WrError <= ncoEmulatorCh2_WrError;
		else
			WrDone <= MemWrDone;
			WrError <= MemWrError;
		end if;
	end process AreaWrMux;

	resamplerCh1_Addr <= VMEAddr(9 downto 2);
	resamplerCh1_Sel <= Sel_resamplerCh1;
	resamplerCh1_RdMem <= Sel_resamplerCh1 and VMERdMem;
	resamplerCh1_WrMem <= Sel_resamplerCh1 and VMEWrMem;
	resamplerCh1_WrData <= VMEWrData;

	ncoEmulatorCh1_Addr <= VMEAddr(9 downto 2);
	ncoEmulatorCh1_Sel <= Sel_ncoEmulatorCh1;
	ncoEmulatorCh1_RdMem <= Sel_ncoEmulatorCh1 and VMERdMem;
	ncoEmulatorCh1_WrMem <= Sel_ncoEmulatorCh1 and VMEWrMem;
	ncoEmulatorCh1_WrData <= VMEWrData;

	resamplerCh2_Addr <= VMEAddr(9 downto 2);
	resamplerCh2_Sel <= Sel_resamplerCh2;
	resamplerCh2_RdMem <= Sel_resamplerCh2 and VMERdMem;
	resamplerCh2_WrMem <= Sel_resamplerCh2 and VMEWrMem;
	resamplerCh2_WrData <= VMEWrData;

	ncoEmulatorCh2_Addr <= VMEAddr(9 downto 2);
	ncoEmulatorCh2_Sel <= Sel_ncoEmulatorCh2;
	ncoEmulatorCh2_RdMem <= Sel_ncoEmulatorCh2 and VMERdMem;
	ncoEmulatorCh2_WrMem <= Sel_ncoEmulatorCh2 and VMEWrMem;
	ncoEmulatorCh2_WrData <= VMEWrData;

	StrobeSeq: process (Clk) begin
		if rising_edge(Clk) then
			Loc_VMERdMem(Loc_VMERdMem'high downto 0) <= Loc_VMERdMem(Loc_VMERdMem'high-1 downto 0) & VMERdMem;
			Loc_VMEWrMem(Loc_VMEWrMem'high downto 0) <= Loc_VMEWrMem(Loc_VMEWrMem'high-1 downto 0) & VMEWrMem;
		end if;
	end process StrobeSeq;

	VMERdData <= RdData;
	VMERdDone <= RdDone;
	VMEWrDone <= WrDone;
	VMERdError <= RdError;
	VMEWrError <= WrError;

end;

-- EOF

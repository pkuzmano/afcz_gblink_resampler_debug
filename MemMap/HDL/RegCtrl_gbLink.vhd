-- VME registers for gbLink
-- Memory map version 20190813
-- Generated on 2019-10-10 10:29:51 by pkuzmano using Gena+ VHDL RegCtrl factory (2018-03-16)

library CommonVisual;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.MemMap_gbLink.all;

entity RegCtrl_gbLink is
	port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(11 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		firmwareID : in std_logic_vector(31 downto 0);
		firmwareVersion : in std_logic_vector(31 downto 0);
		designerID : in std_logic_vector(31 downto 0);
		auroraStatus_linkResetOut : in std_logic;
		auroraStatus_sysResetOut : in std_logic;
		auroraStatus_gtPllLock : in std_logic;
		auroraStatus_laneUp : in std_logic;
		auroraStatus_channelUp : in std_logic;
		auroraStatus_hardErr : in std_logic;
		auroraStatus_softErr : in std_logic;
		auroraStatus_initOk : in std_logic;
		auroraStatus_gtPllLockLatched : in std_logic;
		auroraStatus_laneUpLatched : in std_logic;
		auroraStatus_channelUpLatched : in std_logic;
		auroraStatus_hardErrLatched : in std_logic;
		auroraStatus_softErrLatched : in std_logic;
		auroraControl_reset : out std_logic;
		auroraControl_gtPllLockClear : out std_logic;
		auroraControl_laneUpClear : out std_logic;
		auroraControl_channelUpClear : out std_logic;
		auroraControl_hardErrClear : out std_logic;
		auroraControl_softErrClear : out std_logic;
		txStatus_fifoCh1NumData : in std_logic_vector(31 downto 0);
		txStatus_fifoCh1NumDataTurn : in std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumData : in std_logic_vector(31 downto 0);
		txStatus_fifoCh2NumDataTurn : in std_logic_vector(31 downto 0);
		txControls_general_enable : out std_logic;
		txControls_general_reset : out std_logic;
		txControls_numBuckets : out std_logic_vector(15 downto 0);
		txControls_interleaver_enable : out std_logic;
		txControls_interleaver_rateRatioCh1 : out std_logic_vector(5 downto 4);
		txControls_interleaver_rateRatioCh2 : out std_logic_vector(9 downto 8);
		txControls_fixedLatency_enable : out std_logic;
		rxControls_general_enable : out std_logic;
		rxControls_general_reset : out std_logic;
		rxControls_numBuckets : out std_logic_vector(15 downto 0);
		rxControls_interleaver_enable : out std_logic;
		rxControls_interleaver_rateRatioCh1 : out std_logic_vector(5 downto 4);
		rxControls_interleaver_rateRatioCh2 : out std_logic_vector(9 downto 8);
		rxControls_fixedLatency_enable : out std_logic;
		rxControls_latency : out std_logic_vector(15 downto 0)
	);
end;

architecture Gena of RegCtrl_gbLink is
	component RMWReg
		generic (
			N : natural := 8
		);
		port (
			VMEWrData : in std_logic_vector(2*N-1 downto 0);
			Clk : in std_logic;
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			Rst : in std_logic;
			CRegSel : in std_logic;
			CReg : out std_logic_vector(N-1 downto 0);
			WriteMem : in std_logic;
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : RMWReg use entity CommonVisual.RMWReg(RMWReg);

	signal Loc_VMERdMem : std_logic_vector(2 downto 0);
	signal Loc_VMEWrMem : std_logic_vector(1 downto 0);
	signal CRegRdData : std_logic_vector(31 downto 0);
	signal CRegRdOK : std_logic;
	signal CRegWrOK : std_logic;
	signal Loc_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_CRegRdOK : std_logic;
	signal Loc_CRegWrOK : std_logic;
	signal RegRdDone : std_logic;
	signal RegWrDone : std_logic;
	signal RegRdData : std_logic_vector(31 downto 0);
	signal RegRdOK : std_logic;
	signal Loc_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_RegRdOK : std_logic;
	signal MemRdData : std_logic_vector(31 downto 0);
	signal MemRdDone : std_logic;
	signal MemWrDone : std_logic;
	signal Loc_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_MemRdDone : std_logic;
	signal Loc_MemWrDone : std_logic;
	signal RdData : std_logic_vector(31 downto 0);
	signal RdDone : std_logic;
	signal WrDone : std_logic;
	signal RegRdError : std_logic;
	signal RegWrError : std_logic;
	signal MemRdError : std_logic;
	signal MemWrError : std_logic;
	signal Loc_MemRdError : std_logic;
	signal Loc_MemWrError : std_logic;
	signal RdError : std_logic;
	signal WrError : std_logic;
	signal Loc_firmwareID : std_logic_vector(31 downto 0);
	signal Loc_firmwareVersion : std_logic_vector(31 downto 0);
	signal Loc_designerID : std_logic_vector(31 downto 0);
	signal Loc_auroraStatus : std_logic_vector(31 downto 0);
	signal Loc_auroraControl : std_logic_vector(15 downto 0);
	signal WrSel_auroraControl : std_logic;
	signal txStatus_CRegRdData : std_logic_vector(31 downto 0);
	signal txStatus_CRegRdOK : std_logic;
	signal txStatus_CRegWrOK : std_logic;
	signal Loc_txStatus_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_txStatus_CRegRdOK : std_logic;
	signal Loc_txStatus_CRegWrOK : std_logic;
	signal txStatus_RegRdDone : std_logic;
	signal txStatus_RegWrDone : std_logic;
	signal txStatus_RegRdData : std_logic_vector(31 downto 0);
	signal txStatus_RegRdOK : std_logic;
	signal Loc_txStatus_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_txStatus_RegRdOK : std_logic;
	signal txStatus_MemRdData : std_logic_vector(31 downto 0);
	signal txStatus_MemRdDone : std_logic;
	signal txStatus_MemWrDone : std_logic;
	signal Loc_txStatus_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_txStatus_MemRdDone : std_logic;
	signal Loc_txStatus_MemWrDone : std_logic;
	signal txStatus_RdData : std_logic_vector(31 downto 0);
	signal txStatus_RdDone : std_logic;
	signal txStatus_WrDone : std_logic;
	signal txStatus_RegRdError : std_logic;
	signal txStatus_RegWrError : std_logic;
	signal txStatus_MemRdError : std_logic;
	signal txStatus_MemWrError : std_logic;
	signal Loc_txStatus_MemRdError : std_logic;
	signal Loc_txStatus_MemWrError : std_logic;
	signal txStatus_RdError : std_logic;
	signal txStatus_WrError : std_logic;
	signal Loc_txStatus_fifoCh1NumData : std_logic_vector(31 downto 0);
	signal Loc_txStatus_fifoCh1NumDataTurn : std_logic_vector(31 downto 0);
	signal Loc_txStatus_fifoCh2NumData : std_logic_vector(31 downto 0);
	signal Loc_txStatus_fifoCh2NumDataTurn : std_logic_vector(31 downto 0);
	signal txControls_CRegRdData : std_logic_vector(31 downto 0);
	signal txControls_CRegRdOK : std_logic;
	signal txControls_CRegWrOK : std_logic;
	signal Loc_txControls_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_txControls_CRegRdOK : std_logic;
	signal Loc_txControls_CRegWrOK : std_logic;
	signal txControls_RegRdDone : std_logic;
	signal txControls_RegWrDone : std_logic;
	signal txControls_RegRdData : std_logic_vector(31 downto 0);
	signal txControls_RegRdOK : std_logic;
	signal Loc_txControls_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_txControls_RegRdOK : std_logic;
	signal txControls_MemRdData : std_logic_vector(31 downto 0);
	signal txControls_MemRdDone : std_logic;
	signal txControls_MemWrDone : std_logic;
	signal Loc_txControls_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_txControls_MemRdDone : std_logic;
	signal Loc_txControls_MemWrDone : std_logic;
	signal txControls_RdData : std_logic_vector(31 downto 0);
	signal txControls_RdDone : std_logic;
	signal txControls_WrDone : std_logic;
	signal txControls_RegRdError : std_logic;
	signal txControls_RegWrError : std_logic;
	signal txControls_MemRdError : std_logic;
	signal txControls_MemWrError : std_logic;
	signal Loc_txControls_MemRdError : std_logic;
	signal Loc_txControls_MemWrError : std_logic;
	signal txControls_RdError : std_logic;
	signal txControls_WrError : std_logic;
	signal Loc_txControls_general : std_logic_vector(15 downto 0);
	signal WrSel_txControls_general : std_logic;
	signal Loc_txControls_numBuckets : std_logic_vector(15 downto 0);
	signal WrSel_txControls_numBuckets : std_logic;
	signal Loc_txControls_interleaver : std_logic_vector(15 downto 0);
	signal WrSel_txControls_interleaver : std_logic;
	signal Loc_txControls_fixedLatency : std_logic_vector(15 downto 0);
	signal WrSel_txControls_fixedLatency : std_logic;
	signal rxControls_CRegRdData : std_logic_vector(31 downto 0);
	signal rxControls_CRegRdOK : std_logic;
	signal rxControls_CRegWrOK : std_logic;
	signal Loc_rxControls_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_rxControls_CRegRdOK : std_logic;
	signal Loc_rxControls_CRegWrOK : std_logic;
	signal rxControls_RegRdDone : std_logic;
	signal rxControls_RegWrDone : std_logic;
	signal rxControls_RegRdData : std_logic_vector(31 downto 0);
	signal rxControls_RegRdOK : std_logic;
	signal Loc_rxControls_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_rxControls_RegRdOK : std_logic;
	signal rxControls_MemRdData : std_logic_vector(31 downto 0);
	signal rxControls_MemRdDone : std_logic;
	signal rxControls_MemWrDone : std_logic;
	signal Loc_rxControls_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_rxControls_MemRdDone : std_logic;
	signal Loc_rxControls_MemWrDone : std_logic;
	signal rxControls_RdData : std_logic_vector(31 downto 0);
	signal rxControls_RdDone : std_logic;
	signal rxControls_WrDone : std_logic;
	signal rxControls_RegRdError : std_logic;
	signal rxControls_RegWrError : std_logic;
	signal rxControls_MemRdError : std_logic;
	signal rxControls_MemWrError : std_logic;
	signal Loc_rxControls_MemRdError : std_logic;
	signal Loc_rxControls_MemWrError : std_logic;
	signal rxControls_RdError : std_logic;
	signal rxControls_WrError : std_logic;
	signal Loc_rxControls_general : std_logic_vector(15 downto 0);
	signal WrSel_rxControls_general : std_logic;
	signal Loc_rxControls_numBuckets : std_logic_vector(15 downto 0);
	signal WrSel_rxControls_numBuckets : std_logic;
	signal Loc_rxControls_interleaver : std_logic_vector(15 downto 0);
	signal WrSel_rxControls_interleaver : std_logic;
	signal Loc_rxControls_fixedLatency : std_logic_vector(15 downto 0);
	signal WrSel_rxControls_fixedLatency : std_logic;
	signal Loc_rxControls_latency : std_logic_vector(15 downto 0);
	signal WrSel_rxControls_latency : std_logic;
begin
	Reg_auroraControl: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_auroraControl,
			AutoClrMsk => C_ACM_gbLink_auroraControl,
			Preset => C_PSM_gbLink_auroraControl,
			CReg => Loc_auroraControl(15 downto 0)
		);

	Loc_firmwareID <= firmwareID;

	Loc_firmwareVersion <= firmwareVersion;

	Loc_designerID <= designerID;

	Loc_auroraStatus(31 downto 15) <= C_PSM_gbLink_auroraStatus(31 downto 15);
	Loc_auroraStatus(14) <= auroraStatus_hardErrLatched;
	Loc_auroraStatus(13) <= auroraStatus_softErrLatched;
	Loc_auroraStatus(12) <= auroraStatus_channelUpLatched;
	Loc_auroraStatus(11) <= auroraStatus_laneUpLatched;
	Loc_auroraStatus(10) <= auroraStatus_gtPllLockLatched;
	Loc_auroraStatus(9 downto 8) <= C_PSM_gbLink_auroraStatus(9 downto 8);
	Loc_auroraStatus(7) <= auroraStatus_initOk;
	Loc_auroraStatus(6) <= auroraStatus_softErr;
	Loc_auroraStatus(5) <= auroraStatus_hardErr;
	Loc_auroraStatus(4) <= auroraStatus_channelUp;
	Loc_auroraStatus(3) <= auroraStatus_laneUp;
	Loc_auroraStatus(2) <= auroraStatus_gtPllLock;
	Loc_auroraStatus(1) <= auroraStatus_sysResetOut;
	Loc_auroraStatus(0) <= auroraStatus_linkResetOut;

	auroraControl_reset <= Loc_auroraControl(0);
	auroraControl_gtPllLockClear <= Loc_auroraControl(2);
	auroraControl_laneUpClear <= Loc_auroraControl(3);
	auroraControl_channelUpClear <= Loc_auroraControl(4);
	auroraControl_hardErrClear <= Loc_auroraControl(5);
	auroraControl_softErrClear <= Loc_auroraControl(6);

	WrSelDec: process (VMEAddr) begin
		WrSel_auroraControl <= '0';
		case VMEAddr(11 downto 2) is
			when C_Reg_gbLink_auroraControl =>
				WrSel_auroraControl <= '1';
				Loc_CRegWrOK <= '1';
			when others =>
				Loc_CRegWrOK <= '0';
		end case;
	end process WrSelDec;

	CRegRdMux: process (VMEAddr, Loc_auroraControl) begin
		case VMEAddr(11 downto 2) is
			when C_Reg_gbLink_auroraControl =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_auroraControl(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when others =>
				Loc_CRegRdData <= (others => '0');
				Loc_CRegRdOK <= '0';
		end case;
	end process CRegRdMux;

	CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			CRegRdData <= Loc_CRegRdData;
			CRegRdOK <= Loc_CRegRdOK;
			CRegWrOK <= Loc_CRegWrOk;
		end if;
	end process CRegRdMux_DFF;

	RegRdMux: process (VMEAddr, CRegRdData, CRegRdOK, Loc_firmwareID, Loc_firmwareVersion, Loc_designerID, Loc_auroraStatus) begin
		case VMEAddr(11 downto 2) is
			when C_Reg_gbLink_firmwareID =>
				Loc_RegRdData <= Loc_firmwareID(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_gbLink_firmwareVersion =>
				Loc_RegRdData <= Loc_firmwareVersion(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_gbLink_designerID =>
				Loc_RegRdData <= Loc_designerID(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_gbLink_auroraStatus =>
				Loc_RegRdData <= Loc_auroraStatus(31 downto 0);
				Loc_RegRdOK <= '1';
			when others =>
				Loc_RegRdData <= CRegRdData;
				Loc_RegRdOK <= CRegRdOK;
		end case;
	end process RegRdMux;

	RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			RegRdData <= Loc_RegRdData;
			RegRdOK <= Loc_RegRdOK;
		end if;
	end process RegRdMux_DFF;

	RegRdDone <= Loc_VMERdMem(2) and RegRdOK;
	RegWrDone <= Loc_VMEWrMem(1) and CRegWrOK;

	RegRdError <= Loc_VMERdMem(2) and not RegRdOK;
	RegWrError <= Loc_VMEWrMem(1) and not CRegWrOK;

	Loc_MemRdData <= RegRdData;
	Loc_MemRdDone <= RegRdDone;
	Loc_MemRdError <= RegRdError;

	MemRdData <= Loc_MemRdData;
	MemRdDone <= Loc_MemRdDone;
	MemRdError <= Loc_MemRdError;

	Loc_MemWrDone <= RegWrDone;
	Loc_MemWrError <= RegWrError;

	MemWrDone <= Loc_MemWrDone;
	MemWrError <= Loc_MemWrError;

	AreaRdMux: process (VMEAddr, MemRdData, MemRdDone, txStatus_RdData, txStatus_RdDone, txStatus_RdError, txControls_RdData, txControls_RdDone, txControls_RdError, rxControls_RdData, rxControls_RdDone, rxControls_RdError, MemRdError) begin
		if VMEAddr(11 downto 10) = C_Area_gbLink_txStatus then
			RdData <= txStatus_RdData;
			RdDone <= txStatus_RdDone;
			RdError <= txStatus_RdError;
		elsif VMEAddr(11 downto 10) = C_Area_gbLink_txControls then
			RdData <= txControls_RdData;
			RdDone <= txControls_RdDone;
			RdError <= txControls_RdError;
		elsif VMEAddr(11 downto 10) = C_Area_gbLink_rxControls then
			RdData <= rxControls_RdData;
			RdDone <= rxControls_RdDone;
			RdError <= rxControls_RdError;
		else
			RdData <= MemRdData;
			RdDone <= MemRdDone;
			RdError <= MemRdError;
		end if;
	end process AreaRdMux;

	AreaWrMux: process (VMEAddr, MemWrDone, txStatus_WrDone, txStatus_WrError, txControls_WrDone, txControls_WrError, rxControls_WrDone, rxControls_WrError, MemWrError) begin
		if VMEAddr(11 downto 10) = C_Area_gbLink_txStatus then
			WrDone <= txStatus_WrDone;
			WrError <= txStatus_WrError;
		elsif VMEAddr(11 downto 10) = C_Area_gbLink_txControls then
			WrDone <= txControls_WrDone;
			WrError <= txControls_WrError;
		elsif VMEAddr(11 downto 10) = C_Area_gbLink_rxControls then
			WrDone <= rxControls_WrDone;
			WrError <= rxControls_WrError;
		else
			WrDone <= MemWrDone;
			WrError <= MemWrError;
		end if;
	end process AreaWrMux;




	Reg_rxControls_general: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_rxControls_general,
			AutoClrMsk => C_ACM_gbLink_rxControls_general,
			Preset => C_PSM_gbLink_rxControls_general,
			CReg => Loc_rxControls_general(15 downto 0)
		);

	Reg_rxControls_numBuckets: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_rxControls_numBuckets,
			AutoClrMsk => C_ACM_gbLink_rxControls_numBuckets,
			Preset => C_PSM_gbLink_rxControls_numBuckets,
			CReg => Loc_rxControls_numBuckets(15 downto 0)
		);

	Reg_rxControls_interleaver: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_rxControls_interleaver,
			AutoClrMsk => C_ACM_gbLink_rxControls_interleaver,
			Preset => C_PSM_gbLink_rxControls_interleaver,
			CReg => Loc_rxControls_interleaver(15 downto 0)
		);

	Reg_rxControls_fixedLatency: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_rxControls_fixedLatency,
			AutoClrMsk => C_ACM_gbLink_rxControls_fixedLatency,
			Preset => C_PSM_gbLink_rxControls_fixedLatency,
			CReg => Loc_rxControls_fixedLatency(15 downto 0)
		);

	Reg_rxControls_latency: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_rxControls_latency,
			AutoClrMsk => C_ACM_gbLink_rxControls_latency,
			Preset => C_PSM_gbLink_rxControls_latency,
			CReg => Loc_rxControls_latency(15 downto 0)
		);

	rxControls_general_enable <= Loc_rxControls_general(0);
	rxControls_general_reset <= Loc_rxControls_general(1);

	rxControls_numBuckets <= Loc_rxControls_numBuckets;

	rxControls_interleaver_enable <= Loc_rxControls_interleaver(0);
	rxControls_interleaver_rateRatioCh1 <= Loc_rxControls_interleaver(5 downto 4);
	rxControls_interleaver_rateRatioCh2 <= Loc_rxControls_interleaver(9 downto 8);

	rxControls_fixedLatency_enable <= Loc_rxControls_fixedLatency(0);

	rxControls_latency <= Loc_rxControls_latency;

	rxControls_WrSelDec: process (VMEAddr) begin
		WrSel_rxControls_general <= '0';
		WrSel_rxControls_numBuckets <= '0';
		WrSel_rxControls_interleaver <= '0';
		WrSel_rxControls_fixedLatency <= '0';
		WrSel_rxControls_latency <= '0';
		if VMEAddr(11 downto 10) = C_Area_gbLink_rxControls then
			case VMEAddr(9 downto 2) is
				when C_Reg_gbLink_rxControls_general =>
					WrSel_rxControls_general <= '1';
					Loc_rxControls_CRegWrOK <= '1';
				when C_Reg_gbLink_rxControls_numBuckets =>
					WrSel_rxControls_numBuckets <= '1';
					Loc_rxControls_CRegWrOK <= '1';
				when C_Reg_gbLink_rxControls_interleaver =>
					WrSel_rxControls_interleaver <= '1';
					Loc_rxControls_CRegWrOK <= '1';
				when C_Reg_gbLink_rxControls_fixedLatency =>
					WrSel_rxControls_fixedLatency <= '1';
					Loc_rxControls_CRegWrOK <= '1';
				when C_Reg_gbLink_rxControls_latency =>
					WrSel_rxControls_latency <= '1';
					Loc_rxControls_CRegWrOK <= '1';
				when others =>
					Loc_rxControls_CRegWrOK <= '0';
			end case;
		else
			Loc_rxControls_CRegWrOK <= '0';
		end if;
	end process rxControls_WrSelDec;

	rxControls_CRegRdMux: process (VMEAddr, Loc_rxControls_general, Loc_rxControls_numBuckets, Loc_rxControls_interleaver, Loc_rxControls_fixedLatency, Loc_rxControls_latency) begin
		if VMEAddr(11 downto 10) = C_Area_gbLink_rxControls then
			case VMEAddr(9 downto 2) is
				when C_Reg_gbLink_rxControls_general =>
					Loc_rxControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_rxControls_general(15 downto 0)), 32));
					Loc_rxControls_CRegRdOK <= '1';
				when C_Reg_gbLink_rxControls_numBuckets =>
					Loc_rxControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_rxControls_numBuckets(15 downto 0)), 32));
					Loc_rxControls_CRegRdOK <= '1';
				when C_Reg_gbLink_rxControls_interleaver =>
					Loc_rxControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_rxControls_interleaver(15 downto 0)), 32));
					Loc_rxControls_CRegRdOK <= '1';
				when C_Reg_gbLink_rxControls_fixedLatency =>
					Loc_rxControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_rxControls_fixedLatency(15 downto 0)), 32));
					Loc_rxControls_CRegRdOK <= '1';
				when C_Reg_gbLink_rxControls_latency =>
					Loc_rxControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_rxControls_latency(15 downto 0)), 32));
					Loc_rxControls_CRegRdOK <= '1';
				when others =>
					Loc_rxControls_CRegRdData <= (others => '0');
					Loc_rxControls_CRegRdOK <= '0';
			end case;
		else
			Loc_rxControls_CRegRdData <= (others => '0');
			Loc_rxControls_CRegRdOK <= '0';
		end if;
	end process rxControls_CRegRdMux;

	rxControls_CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			rxControls_CRegRdData <= Loc_rxControls_CRegRdData;
			rxControls_CRegRdOK <= Loc_rxControls_CRegRdOK;
			rxControls_CRegWrOK <= Loc_rxControls_CRegWrOk;
		end if;
	end process rxControls_CRegRdMux_DFF;

	Loc_rxControls_RegRdData <= rxControls_CRegRdData;
	Loc_rxControls_RegRdOK <= rxControls_CRegRdOK;

	rxControls_RegRdData <= Loc_rxControls_RegRdData;
	rxControls_RegRdOK <= Loc_rxControls_RegRdOK;

	rxControls_RegRdDone <= Loc_VMERdMem(1) and rxControls_RegRdOK;
	rxControls_RegWrDone <= Loc_VMEWrMem(1) and rxControls_CRegWrOK;

	rxControls_RegRdError <= Loc_VMERdMem(1) and not rxControls_RegRdOK;
	rxControls_RegWrError <= Loc_VMEWrMem(1) and not rxControls_CRegWrOK;

	Loc_rxControls_MemRdData <= rxControls_RegRdData;
	Loc_rxControls_MemRdDone <= rxControls_RegRdDone;
	Loc_rxControls_MemRdError <= rxControls_RegRdError;

	rxControls_MemRdData <= Loc_rxControls_MemRdData;
	rxControls_MemRdDone <= Loc_rxControls_MemRdDone;
	rxControls_MemRdError <= Loc_rxControls_MemRdError;

	Loc_rxControls_MemWrDone <= rxControls_RegWrDone;
	Loc_rxControls_MemWrError <= rxControls_RegWrError;

	rxControls_MemWrDone <= Loc_rxControls_MemWrDone;
	rxControls_MemWrError <= Loc_rxControls_MemWrError;

	rxControls_RdData <= rxControls_MemRdData;
	rxControls_RdDone <= rxControls_MemRdDone;
	rxControls_WrDone <= rxControls_MemWrDone;
	rxControls_RdError <= rxControls_MemRdError;
	rxControls_WrError <= rxControls_MemWrError;

	Reg_txControls_general: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_txControls_general,
			AutoClrMsk => C_ACM_gbLink_txControls_general,
			Preset => C_PSM_gbLink_txControls_general,
			CReg => Loc_txControls_general(15 downto 0)
		);

	Reg_txControls_numBuckets: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_txControls_numBuckets,
			AutoClrMsk => C_ACM_gbLink_txControls_numBuckets,
			Preset => C_PSM_gbLink_txControls_numBuckets,
			CReg => Loc_txControls_numBuckets(15 downto 0)
		);

	Reg_txControls_interleaver: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_txControls_interleaver,
			AutoClrMsk => C_ACM_gbLink_txControls_interleaver,
			Preset => C_PSM_gbLink_txControls_interleaver,
			CReg => Loc_txControls_interleaver(15 downto 0)
		);

	Reg_txControls_fixedLatency: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_txControls_fixedLatency,
			AutoClrMsk => C_ACM_gbLink_txControls_fixedLatency,
			Preset => C_PSM_gbLink_txControls_fixedLatency,
			CReg => Loc_txControls_fixedLatency(15 downto 0)
		);

	txControls_general_enable <= Loc_txControls_general(0);
	txControls_general_reset <= Loc_txControls_general(1);

	txControls_numBuckets <= Loc_txControls_numBuckets;

	txControls_interleaver_enable <= Loc_txControls_interleaver(0);
	txControls_interleaver_rateRatioCh1 <= Loc_txControls_interleaver(5 downto 4);
	txControls_interleaver_rateRatioCh2 <= Loc_txControls_interleaver(9 downto 8);

	txControls_fixedLatency_enable <= Loc_txControls_fixedLatency(0);

	txControls_WrSelDec: process (VMEAddr) begin
		WrSel_txControls_general <= '0';
		WrSel_txControls_numBuckets <= '0';
		WrSel_txControls_interleaver <= '0';
		WrSel_txControls_fixedLatency <= '0';
		if VMEAddr(11 downto 10) = C_Area_gbLink_txControls then
			case VMEAddr(9 downto 2) is
				when C_Reg_gbLink_txControls_general =>
					WrSel_txControls_general <= '1';
					Loc_txControls_CRegWrOK <= '1';
				when C_Reg_gbLink_txControls_numBuckets =>
					WrSel_txControls_numBuckets <= '1';
					Loc_txControls_CRegWrOK <= '1';
				when C_Reg_gbLink_txControls_interleaver =>
					WrSel_txControls_interleaver <= '1';
					Loc_txControls_CRegWrOK <= '1';
				when C_Reg_gbLink_txControls_fixedLatency =>
					WrSel_txControls_fixedLatency <= '1';
					Loc_txControls_CRegWrOK <= '1';
				when others =>
					Loc_txControls_CRegWrOK <= '0';
			end case;
		else
			Loc_txControls_CRegWrOK <= '0';
		end if;
	end process txControls_WrSelDec;

	txControls_CRegRdMux: process (VMEAddr, Loc_txControls_general, Loc_txControls_numBuckets, Loc_txControls_interleaver, Loc_txControls_fixedLatency) begin
		if VMEAddr(11 downto 10) = C_Area_gbLink_txControls then
			case VMEAddr(9 downto 2) is
				when C_Reg_gbLink_txControls_general =>
					Loc_txControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_txControls_general(15 downto 0)), 32));
					Loc_txControls_CRegRdOK <= '1';
				when C_Reg_gbLink_txControls_numBuckets =>
					Loc_txControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_txControls_numBuckets(15 downto 0)), 32));
					Loc_txControls_CRegRdOK <= '1';
				when C_Reg_gbLink_txControls_interleaver =>
					Loc_txControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_txControls_interleaver(15 downto 0)), 32));
					Loc_txControls_CRegRdOK <= '1';
				when C_Reg_gbLink_txControls_fixedLatency =>
					Loc_txControls_CRegRdData <= std_logic_vector(resize(unsigned(Loc_txControls_fixedLatency(15 downto 0)), 32));
					Loc_txControls_CRegRdOK <= '1';
				when others =>
					Loc_txControls_CRegRdData <= (others => '0');
					Loc_txControls_CRegRdOK <= '0';
			end case;
		else
			Loc_txControls_CRegRdData <= (others => '0');
			Loc_txControls_CRegRdOK <= '0';
		end if;
	end process txControls_CRegRdMux;

	txControls_CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			txControls_CRegRdData <= Loc_txControls_CRegRdData;
			txControls_CRegRdOK <= Loc_txControls_CRegRdOK;
			txControls_CRegWrOK <= Loc_txControls_CRegWrOk;
		end if;
	end process txControls_CRegRdMux_DFF;

	Loc_txControls_RegRdData <= txControls_CRegRdData;
	Loc_txControls_RegRdOK <= txControls_CRegRdOK;

	txControls_RegRdData <= Loc_txControls_RegRdData;
	txControls_RegRdOK <= Loc_txControls_RegRdOK;

	txControls_RegRdDone <= Loc_VMERdMem(1) and txControls_RegRdOK;
	txControls_RegWrDone <= Loc_VMEWrMem(1) and txControls_CRegWrOK;

	txControls_RegRdError <= Loc_VMERdMem(1) and not txControls_RegRdOK;
	txControls_RegWrError <= Loc_VMEWrMem(1) and not txControls_CRegWrOK;

	Loc_txControls_MemRdData <= txControls_RegRdData;
	Loc_txControls_MemRdDone <= txControls_RegRdDone;
	Loc_txControls_MemRdError <= txControls_RegRdError;

	txControls_MemRdData <= Loc_txControls_MemRdData;
	txControls_MemRdDone <= Loc_txControls_MemRdDone;
	txControls_MemRdError <= Loc_txControls_MemRdError;

	Loc_txControls_MemWrDone <= txControls_RegWrDone;
	Loc_txControls_MemWrError <= txControls_RegWrError;

	txControls_MemWrDone <= Loc_txControls_MemWrDone;
	txControls_MemWrError <= Loc_txControls_MemWrError;

	txControls_RdData <= txControls_MemRdData;
	txControls_RdDone <= txControls_MemRdDone;
	txControls_WrDone <= txControls_MemWrDone;
	txControls_RdError <= txControls_MemRdError;
	txControls_WrError <= txControls_MemWrError;

	Loc_txStatus_fifoCh1NumData <= txStatus_fifoCh1NumData;

	Loc_txStatus_fifoCh1NumDataTurn <= txStatus_fifoCh1NumDataTurn;

	Loc_txStatus_fifoCh2NumData <= txStatus_fifoCh2NumData;

	Loc_txStatus_fifoCh2NumDataTurn <= txStatus_fifoCh2NumDataTurn;

	Loc_txStatus_CRegRdData <= (others => '0');
	Loc_txStatus_CRegRdOK <= '0';
	Loc_txStatus_CRegWrOK <= '0';

	txStatus_CRegRdData <= Loc_txStatus_CRegRdData;
	txStatus_CRegRdOK <= Loc_txStatus_CRegRdOK;
	txStatus_CRegWrOK <= Loc_txStatus_CRegWrOk;

	txStatus_RegRdMux: process (VMEAddr, txStatus_CRegRdData, txStatus_CRegRdOK, Loc_txStatus_fifoCh1NumData, Loc_txStatus_fifoCh1NumDataTurn, Loc_txStatus_fifoCh2NumData, Loc_txStatus_fifoCh2NumDataTurn) begin
		if VMEAddr(11 downto 10) = C_Area_gbLink_txStatus then
			case VMEAddr(9 downto 2) is
				when C_Reg_gbLink_txStatus_fifoCh1NumData =>
					Loc_txStatus_RegRdData <= Loc_txStatus_fifoCh1NumData(31 downto 0);
					Loc_txStatus_RegRdOK <= '1';
				when C_Reg_gbLink_txStatus_fifoCh1NumDataTurn =>
					Loc_txStatus_RegRdData <= Loc_txStatus_fifoCh1NumDataTurn(31 downto 0);
					Loc_txStatus_RegRdOK <= '1';
				when C_Reg_gbLink_txStatus_fifoCh2NumData =>
					Loc_txStatus_RegRdData <= Loc_txStatus_fifoCh2NumData(31 downto 0);
					Loc_txStatus_RegRdOK <= '1';
				when C_Reg_gbLink_txStatus_fifoCh2NumDataTurn =>
					Loc_txStatus_RegRdData <= Loc_txStatus_fifoCh2NumDataTurn(31 downto 0);
					Loc_txStatus_RegRdOK <= '1';
				when others =>
					Loc_txStatus_RegRdData <= txStatus_CRegRdData;
					Loc_txStatus_RegRdOK <= txStatus_CRegRdOK;
			end case;
		else
			Loc_txStatus_RegRdData <= txStatus_CRegRdData;
			Loc_txStatus_RegRdOK <= txStatus_CRegRdOK;
		end if;
	end process txStatus_RegRdMux;

	txStatus_RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			txStatus_RegRdData <= Loc_txStatus_RegRdData;
			txStatus_RegRdOK <= Loc_txStatus_RegRdOK;
		end if;
	end process txStatus_RegRdMux_DFF;

	txStatus_RegRdDone <= Loc_VMERdMem(1) and txStatus_RegRdOK;
	txStatus_RegWrDone <= Loc_VMEWrMem(0) and txStatus_CRegWrOK;

	txStatus_RegRdError <= Loc_VMERdMem(1) and not txStatus_RegRdOK;
	txStatus_RegWrError <= Loc_VMEWrMem(0) and not txStatus_CRegWrOK;

	Loc_txStatus_MemRdData <= txStatus_RegRdData;
	Loc_txStatus_MemRdDone <= txStatus_RegRdDone;
	Loc_txStatus_MemRdError <= txStatus_RegRdError;

	txStatus_MemRdData <= Loc_txStatus_MemRdData;
	txStatus_MemRdDone <= Loc_txStatus_MemRdDone;
	txStatus_MemRdError <= Loc_txStatus_MemRdError;

	Loc_txStatus_MemWrDone <= txStatus_RegWrDone;
	Loc_txStatus_MemWrError <= txStatus_RegWrError;

	txStatus_MemWrDone <= Loc_txStatus_MemWrDone;
	txStatus_MemWrError <= Loc_txStatus_MemWrError;

	txStatus_RdData <= txStatus_MemRdData;
	txStatus_RdDone <= txStatus_MemRdDone;
	txStatus_WrDone <= txStatus_MemWrDone;
	txStatus_RdError <= txStatus_MemRdError;
	txStatus_WrError <= txStatus_MemWrError;

	StrobeSeq: process (Clk) begin
		if rising_edge(Clk) then
			Loc_VMERdMem(Loc_VMERdMem'high downto 0) <= Loc_VMERdMem(Loc_VMERdMem'high-1 downto 0) & VMERdMem;
			Loc_VMEWrMem(Loc_VMEWrMem'high downto 0) <= Loc_VMEWrMem(Loc_VMEWrMem'high-1 downto 0) & VMEWrMem;
		end if;
	end process StrobeSeq;

	VMERdData <= RdData;
	VMERdDone <= RdDone;
	VMEWrDone <= WrDone;
	VMERdError <= RdError;
	VMEWrError <= WrError;

end;

-- EOF

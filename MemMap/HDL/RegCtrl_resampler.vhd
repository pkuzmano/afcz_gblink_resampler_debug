-- VME registers for resampler
-- Memory map version 20191010
-- Generated on 2019-10-11 10:46:13 by pkuzmano using Gena+ VHDL RegCtrl factory (2018-03-16)

library CommonVisual;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.MemMap_resampler.all;

entity RegCtrl_resampler is
	port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(9 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		toutUpHigh16 : out std_logic_vector(15 downto 0);
		toutUpLow16 : out std_logic_vector(15 downto 0);
		numData : in std_logic_vector(31 downto 0);
		numDataTurn : in std_logic_vector(31 downto 0);
		control_loopEnable : out std_logic;
		numBuckets : out std_logic_vector(15 downto 0);
		k : out std_logic_vector(15 downto 0)
	);
end;

architecture Gena of RegCtrl_resampler is
	component RMWReg
		generic (
			N : natural := 8
		);
		port (
			VMEWrData : in std_logic_vector(2*N-1 downto 0);
			Clk : in std_logic;
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			Rst : in std_logic;
			CRegSel : in std_logic;
			CReg : out std_logic_vector(N-1 downto 0);
			WriteMem : in std_logic;
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : RMWReg use entity CommonVisual.RMWReg(RMWReg);

	signal Loc_VMERdMem : std_logic_vector(2 downto 0);
	signal Loc_VMEWrMem : std_logic_vector(1 downto 0);
	signal CRegRdData : std_logic_vector(31 downto 0);
	signal CRegRdOK : std_logic;
	signal CRegWrOK : std_logic;
	signal Loc_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_CRegRdOK : std_logic;
	signal Loc_CRegWrOK : std_logic;
	signal RegRdDone : std_logic;
	signal RegWrDone : std_logic;
	signal RegRdData : std_logic_vector(31 downto 0);
	signal RegRdOK : std_logic;
	signal Loc_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_RegRdOK : std_logic;
	signal MemRdData : std_logic_vector(31 downto 0);
	signal MemRdDone : std_logic;
	signal MemWrDone : std_logic;
	signal Loc_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_MemRdDone : std_logic;
	signal Loc_MemWrDone : std_logic;
	signal RdData : std_logic_vector(31 downto 0);
	signal RdDone : std_logic;
	signal WrDone : std_logic;
	signal RegRdError : std_logic;
	signal RegWrError : std_logic;
	signal MemRdError : std_logic;
	signal MemWrError : std_logic;
	signal Loc_MemRdError : std_logic;
	signal Loc_MemWrError : std_logic;
	signal RdError : std_logic;
	signal WrError : std_logic;
	signal Loc_toutUpHigh16 : std_logic_vector(15 downto 0);
	signal WrSel_toutUpHigh16 : std_logic;
	signal Loc_toutUpLow16 : std_logic_vector(15 downto 0);
	signal WrSel_toutUpLow16 : std_logic;
	signal Loc_numData : std_logic_vector(31 downto 0);
	signal Loc_numDataTurn : std_logic_vector(31 downto 0);
	signal Loc_control : std_logic_vector(15 downto 0);
	signal WrSel_control : std_logic;
	signal Loc_numBuckets : std_logic_vector(15 downto 0);
	signal WrSel_numBuckets : std_logic;
	signal Loc_k : std_logic_vector(15 downto 0);
	signal WrSel_k : std_logic;
begin
	Reg_toutUpHigh16: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_toutUpHigh16,
			AutoClrMsk => C_ACM_resampler_toutUpHigh16,
			Preset => C_PSM_resampler_toutUpHigh16,
			CReg => Loc_toutUpHigh16(15 downto 0)
		);

	Reg_toutUpLow16: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_toutUpLow16,
			AutoClrMsk => C_ACM_resampler_toutUpLow16,
			Preset => C_PSM_resampler_toutUpLow16,
			CReg => Loc_toutUpLow16(15 downto 0)
		);

	Reg_control: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_control,
			AutoClrMsk => C_ACM_resampler_control,
			Preset => C_PSM_resampler_control,
			CReg => Loc_control(15 downto 0)
		);

	Reg_numBuckets: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_numBuckets,
			AutoClrMsk => C_ACM_resampler_numBuckets,
			Preset => C_PSM_resampler_numBuckets,
			CReg => Loc_numBuckets(15 downto 0)
		);

	Reg_k: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_k,
			AutoClrMsk => C_ACM_resampler_k,
			Preset => C_PSM_resampler_k,
			CReg => Loc_k(15 downto 0)
		);

	toutUpHigh16 <= Loc_toutUpHigh16;

	toutUpLow16 <= Loc_toutUpLow16;

	Loc_numData <= numData;

	Loc_numDataTurn <= numDataTurn;

	control_loopEnable <= Loc_control(0);

	numBuckets <= Loc_numBuckets;

	k <= Loc_k;

	WrSelDec: process (VMEAddr) begin
		WrSel_toutUpHigh16 <= '0';
		WrSel_toutUpLow16 <= '0';
		WrSel_control <= '0';
		WrSel_numBuckets <= '0';
		WrSel_k <= '0';
		case VMEAddr(9 downto 2) is
			when C_Reg_resampler_toutUpHigh16 =>
				WrSel_toutUpHigh16 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_resampler_toutUpLow16 =>
				WrSel_toutUpLow16 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_resampler_control =>
				WrSel_control <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_resampler_numBuckets =>
				WrSel_numBuckets <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_resampler_k =>
				WrSel_k <= '1';
				Loc_CRegWrOK <= '1';
			when others =>
				Loc_CRegWrOK <= '0';
		end case;
	end process WrSelDec;

	CRegRdMux: process (VMEAddr, Loc_toutUpHigh16, Loc_toutUpLow16, Loc_control, Loc_numBuckets, Loc_k) begin
		case VMEAddr(9 downto 2) is
			when C_Reg_resampler_toutUpHigh16 =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_toutUpHigh16(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when C_Reg_resampler_toutUpLow16 =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_toutUpLow16(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when C_Reg_resampler_control =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_control(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when C_Reg_resampler_numBuckets =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_numBuckets(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when C_Reg_resampler_k =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_k(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when others =>
				Loc_CRegRdData <= (others => '0');
				Loc_CRegRdOK <= '0';
		end case;
	end process CRegRdMux;

	CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			CRegRdData <= Loc_CRegRdData;
			CRegRdOK <= Loc_CRegRdOK;
			CRegWrOK <= Loc_CRegWrOk;
		end if;
	end process CRegRdMux_DFF;

	RegRdMux: process (VMEAddr, CRegRdData, CRegRdOK, Loc_numData, Loc_numDataTurn) begin
		case VMEAddr(9 downto 2) is
			when C_Reg_resampler_numData =>
				Loc_RegRdData <= Loc_numData(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_resampler_numDataTurn =>
				Loc_RegRdData <= Loc_numDataTurn(31 downto 0);
				Loc_RegRdOK <= '1';
			when others =>
				Loc_RegRdData <= CRegRdData;
				Loc_RegRdOK <= CRegRdOK;
		end case;
	end process RegRdMux;

	RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			RegRdData <= Loc_RegRdData;
			RegRdOK <= Loc_RegRdOK;
		end if;
	end process RegRdMux_DFF;

	RegRdDone <= Loc_VMERdMem(2) and RegRdOK;
	RegWrDone <= Loc_VMEWrMem(1) and CRegWrOK;

	RegRdError <= Loc_VMERdMem(2) and not RegRdOK;
	RegWrError <= Loc_VMEWrMem(1) and not CRegWrOK;

	Loc_MemRdData <= RegRdData;
	Loc_MemRdDone <= RegRdDone;
	Loc_MemRdError <= RegRdError;

	MemRdData <= Loc_MemRdData;
	MemRdDone <= Loc_MemRdDone;
	MemRdError <= Loc_MemRdError;

	Loc_MemWrDone <= RegWrDone;
	Loc_MemWrError <= RegWrError;

	MemWrDone <= Loc_MemWrDone;
	MemWrError <= Loc_MemWrError;

	RdData <= MemRdData;
	RdDone <= MemRdDone;
	WrDone <= MemWrDone;
	RdError <= MemRdError;
	WrError <= MemWrError;

	StrobeSeq: process (Clk) begin
		if rising_edge(Clk) then
			Loc_VMERdMem(Loc_VMERdMem'high downto 0) <= Loc_VMERdMem(Loc_VMERdMem'high-1 downto 0) & VMERdMem;
			Loc_VMEWrMem(Loc_VMEWrMem'high downto 0) <= Loc_VMEWrMem(Loc_VMEWrMem'high-1 downto 0) & VMEWrMem;
		end if;
	end process StrobeSeq;

	VMERdData <= RdData;
	VMERdDone <= RdDone;
	VMEWrDone <= WrDone;
	VMERdError <= RdError;
	VMEWrError <= WrError;

end;

-- EOF

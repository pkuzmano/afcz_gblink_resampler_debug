-- VME registers for ncoEmulator
-- Memory map version 20191010
-- Generated on 2019-10-11 10:46:13 by pkuzmano using Gena+ VHDL RegCtrl factory (2018-03-16)

library CommonVisual;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.MemMap_ncoEmulator.all;

entity RegCtrl_ncoEmulator is
	port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(9 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		frevCounterMax : out std_logic_vector(15 downto 0);
		frevOffset : out std_logic_vector(15 downto 0);
		syncOffset : out std_logic_vector(15 downto 0)
	);
end;

architecture Gena of RegCtrl_ncoEmulator is
	component RMWReg
		generic (
			N : natural := 8
		);
		port (
			VMEWrData : in std_logic_vector(2*N-1 downto 0);
			Clk : in std_logic;
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			Rst : in std_logic;
			CRegSel : in std_logic;
			CReg : out std_logic_vector(N-1 downto 0);
			WriteMem : in std_logic;
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : RMWReg use entity CommonVisual.RMWReg(RMWReg);

	signal Loc_VMERdMem : std_logic_vector(2 downto 0);
	signal Loc_VMEWrMem : std_logic_vector(1 downto 0);
	signal CRegRdData : std_logic_vector(31 downto 0);
	signal CRegRdOK : std_logic;
	signal CRegWrOK : std_logic;
	signal Loc_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_CRegRdOK : std_logic;
	signal Loc_CRegWrOK : std_logic;
	signal RegRdDone : std_logic;
	signal RegWrDone : std_logic;
	signal RegRdData : std_logic_vector(31 downto 0);
	signal RegRdOK : std_logic;
	signal Loc_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_RegRdOK : std_logic;
	signal MemRdData : std_logic_vector(31 downto 0);
	signal MemRdDone : std_logic;
	signal MemWrDone : std_logic;
	signal Loc_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_MemRdDone : std_logic;
	signal Loc_MemWrDone : std_logic;
	signal RdData : std_logic_vector(31 downto 0);
	signal RdDone : std_logic;
	signal WrDone : std_logic;
	signal RegRdError : std_logic;
	signal RegWrError : std_logic;
	signal MemRdError : std_logic;
	signal MemWrError : std_logic;
	signal Loc_MemRdError : std_logic;
	signal Loc_MemWrError : std_logic;
	signal RdError : std_logic;
	signal WrError : std_logic;
	signal Loc_frevCounterMax : std_logic_vector(15 downto 0);
	signal WrSel_frevCounterMax : std_logic;
	signal Loc_frevOffset : std_logic_vector(15 downto 0);
	signal WrSel_frevOffset : std_logic;
	signal Loc_syncOffset : std_logic_vector(15 downto 0);
	signal WrSel_syncOffset : std_logic;
begin
	Reg_frevCounterMax: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_frevCounterMax,
			AutoClrMsk => C_ACM_ncoEmulator_frevCounterMax,
			Preset => C_PSM_ncoEmulator_frevCounterMax,
			CReg => Loc_frevCounterMax(15 downto 0)
		);

	Reg_frevOffset: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_frevOffset,
			AutoClrMsk => C_ACM_ncoEmulator_frevOffset,
			Preset => C_PSM_ncoEmulator_frevOffset,
			CReg => Loc_frevOffset(15 downto 0)
		);

	Reg_syncOffset: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_syncOffset,
			AutoClrMsk => C_ACM_ncoEmulator_syncOffset,
			Preset => C_PSM_ncoEmulator_syncOffset,
			CReg => Loc_syncOffset(15 downto 0)
		);

	frevCounterMax <= Loc_frevCounterMax;

	frevOffset <= Loc_frevOffset;

	syncOffset <= Loc_syncOffset;

	WrSelDec: process (VMEAddr) begin
		WrSel_frevCounterMax <= '0';
		WrSel_frevOffset <= '0';
		WrSel_syncOffset <= '0';
		case VMEAddr(9 downto 2) is
			when C_Reg_ncoEmulator_frevCounterMax =>
				WrSel_frevCounterMax <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_ncoEmulator_frevOffset =>
				WrSel_frevOffset <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_ncoEmulator_syncOffset =>
				WrSel_syncOffset <= '1';
				Loc_CRegWrOK <= '1';
			when others =>
				Loc_CRegWrOK <= '0';
		end case;
	end process WrSelDec;

	CRegRdMux: process (VMEAddr, Loc_frevCounterMax, Loc_frevOffset, Loc_syncOffset) begin
		case VMEAddr(9 downto 2) is
			when C_Reg_ncoEmulator_frevCounterMax =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_frevCounterMax(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when C_Reg_ncoEmulator_frevOffset =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_frevOffset(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when C_Reg_ncoEmulator_syncOffset =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_syncOffset(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when others =>
				Loc_CRegRdData <= (others => '0');
				Loc_CRegRdOK <= '0';
		end case;
	end process CRegRdMux;

	CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			CRegRdData <= Loc_CRegRdData;
			CRegRdOK <= Loc_CRegRdOK;
			CRegWrOK <= Loc_CRegWrOk;
		end if;
	end process CRegRdMux_DFF;

	Loc_RegRdData <= CRegRdData;
	Loc_RegRdOK <= CRegRdOK;

	RegRdData <= Loc_RegRdData;
	RegRdOK <= Loc_RegRdOK;

	RegRdDone <= Loc_VMERdMem(1) and RegRdOK;
	RegWrDone <= Loc_VMEWrMem(1) and CRegWrOK;

	RegRdError <= Loc_VMERdMem(1) and not RegRdOK;
	RegWrError <= Loc_VMEWrMem(1) and not CRegWrOK;

	Loc_MemRdData <= RegRdData;
	Loc_MemRdDone <= RegRdDone;
	Loc_MemRdError <= RegRdError;

	MemRdData <= Loc_MemRdData;
	MemRdDone <= Loc_MemRdDone;
	MemRdError <= Loc_MemRdError;

	Loc_MemWrDone <= RegWrDone;
	Loc_MemWrError <= RegWrError;

	MemWrDone <= Loc_MemWrDone;
	MemWrError <= Loc_MemWrError;

	RdData <= MemRdData;
	RdDone <= MemRdDone;
	WrDone <= MemWrDone;
	RdError <= MemRdError;
	WrError <= MemWrError;

	StrobeSeq: process (Clk) begin
		if rising_edge(Clk) then
			Loc_VMERdMem(Loc_VMERdMem'high downto 0) <= Loc_VMERdMem(Loc_VMERdMem'high-1 downto 0) & VMERdMem;
			Loc_VMEWrMem(Loc_VMEWrMem'high downto 0) <= Loc_VMEWrMem(Loc_VMEWrMem'high-1 downto 0) & VMEWrMem;
		end if;
	end process StrobeSeq;

	VMERdData <= RdData;
	VMERdDone <= RdDone;
	VMEWrDone <= WrDone;
	VMERdError <= RdError;
	VMEWrError <= WrError;

end;

-- EOF

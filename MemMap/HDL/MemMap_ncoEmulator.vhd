--- VME memory map ncoEmulator
--- Memory map version 20191010
--- Generated on 2019-10-11 by pkuzmano using VHDLMap (Gena component)

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package MemMap_ncoEmulator is

	-- Ident Code
	constant C_ncoEmulator_IdentCode : std_logic_vector(31 downto 0) := X"00000000";

	-- Memory Map Version
	constant C_ncoEmulator_MemMapVersion : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(20191010,32));

	-- Semantic Memory Map Version
	constant C_ncoEmulator_SemanticMemMapVersion : std_logic_vector(31 downto 0) := X"00010000";

	-- Register Addresses : Memory Map
	constant C_Reg_ncoEmulator_frevCounterMax		:		std_logic_vector(9 downto 2) := "00000000";-- : Word address : "0" & X"00"; Byte Address : X"00"
	constant C_Reg_ncoEmulator_frevOffset		:		std_logic_vector(9 downto 2) := "00000001";-- : Word address : "0" & X"01"; Byte Address : X"02"
	constant C_Reg_ncoEmulator_syncOffset		:		std_logic_vector(9 downto 2) := "00000010";-- : Word address : "0" & X"02"; Byte Address : X"04"

	-- Register Auto Clear Masks : Memory Map
	constant C_ACM_ncoEmulator_frevCounterMax		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_ncoEmulator_frevOffset		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_ACM_ncoEmulator_syncOffset		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- Register Preset Masks : Memory Map
	constant C_PSM_ncoEmulator_frevCounterMax		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_ncoEmulator_frevOffset		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"
	constant C_PSM_ncoEmulator_syncOffset		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- CODE FIELDS
	-- Memory Data : Memory Map
	-- Submap Addresses : Memory Map
end;
-- EOF
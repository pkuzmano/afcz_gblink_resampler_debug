--- VME memory map afczGBLinkResamplerDebug
--- Memory map version 20191011
--- Generated on 2019-10-11 by pkuzmano using VHDLMap (Gena component)

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package MemMap_afczGBLinkResamplerDebug is

	-- Ident Code
	constant C_afczGBLinkResamplerDebug_IdentCode : std_logic_vector(31 downto 0) := X"00000053";

	-- Memory Map Version
	constant C_afczGBLinkResamplerDebug_MemMapVersion : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(20191011,32));

	-- Semantic Memory Map Version
	constant C_afczGBLinkResamplerDebug_SemanticMemMapVersion : std_logic_vector(31 downto 0) := X"00000002";

	-- Register Addresses : Memory Map

	-- Register Auto Clear Masks : Memory Map

	-- Register Preset Masks : Memory Map

	-- CODE FIELDS
	-- Memory Data : Memory Map
	-- Submap Addresses : Memory Map
	constant C_Submap_afczGBLinkResamplerDebug_resamplerCh1		:		std_logic_vector(11 downto 10) := "00";-- : Word address : "00" & "00" & X"0"; Byte Address : "00" & X"0"
	constant C_Submap_afczGBLinkResamplerDebug_ncoEmulatorCh1		:		std_logic_vector(11 downto 10) := "01";-- : Word address : "01" & "01" & X"0"; Byte Address : "10" & X"0"
	constant C_Submap_afczGBLinkResamplerDebug_resamplerCh2		:		std_logic_vector(11 downto 10) := "10";-- : Word address : "10" & "10" & X"0"; Byte Address : "00" & X"0"
	constant C_Submap_afczGBLinkResamplerDebug_ncoEmulatorCh2		:		std_logic_vector(11 downto 10) := "11";-- : Word address : "11" & "11" & X"0"; Byte Address : "10" & X"0"
end;
-- EOF
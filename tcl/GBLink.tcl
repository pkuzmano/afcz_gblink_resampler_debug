set_property name ../../MemMap/HDL/MemMap_gbLink.vhd [ipx::get_files c:/Users/pkuzmano/Work/GBlink_rtm_8sfp_debug5/MemMap/HDL/MemMap_gbLink.vhd -of_objects [ipx::get_file_groups xilinx_vhdlsynthesis -of_objects [ipx::current_core]]]
set_property name ../../MemMap/HDL/RegCtrl_gbLink.vhd [ipx::get_files c:/Users/pkuzmano/Work/GBlink_rtm_8sfp_debug5/MemMap/HDL/RegCtrl_gbLink.vhd -of_objects [ipx::get_file_groups xilinx_vhdlsynthesis -of_objects [ipx::current_core]]]
set_property name ../../MemMap/HDL/MemMap_gbLink.vhd [ipx::get_files c:/Users/pkuzmano/Work/GBlink_rtm_8sfp_debug5/MemMap/HDL/MemMap_gbLink.vhd -of_objects [ipx::get_file_groups xilinx_vhdlbehavioralsimulation -of_objects [ipx::current_core]]]
set_property name ../../MemMap/HDL/RegCtrl_gbLink.vhd [ipx::get_files c:/Users/pkuzmano/Work/GBlink_rtm_8sfp_debug5/MemMap/HDL/RegCtrl_gbLink.vhd -of_objects [ipx::get_file_groups xilinx_vhdlbehavioralsimulation -of_objects [ipx::current_core]]]
ipx::remove_all_port [ipx::current_core]
ipx::add_ports_from_hdl [ipx::current_core] -top_level_hdl_file c:/Users/pkuzmano/Work/GBlink_rtm_8sfp_debug5/ip_repo/GBLink_1.0/hdl/GBLink_v1_0.vhd -top_module_name GBLink_v1_0

ipx::infer_bus_interface {s_axi_awaddr s_axi_awprot s_axi_awvalid s_axi_awready s_axi_wdata s_axi_wstrb s_axi_wvalid s_axi_wready s_axi_bresp s_axi_bvalid s_axi_bready s_axi_araddr s_axi_arprot s_axi_arvalid s_axi_arready s_axi_rdata s_axi_rresp s_axi_rvalid s_axi_rready} xilinx.com:interface:aximm_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface {m_axis_data_tvalid m_axis_data_tdata m_axis_data_tstrb m_axis_data_tlast m_axis_data_tready} xilinx.com:interface:axis_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface {m_axis_user_k_tvalid m_axis_user_k_tdata m_axis_user_k_tstrb m_axis_user_k_tlast m_axis_user_k_tready} xilinx.com:interface:axis_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface {s_axis_data_tready s_axis_data_tdata s_axis_data_tstrb s_axis_data_tlast s_axis_data_tvalid} xilinx.com:interface:axis_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface {s_axis_user_k_tready s_axis_user_k_tdata s_axis_user_k_tstrb s_axis_user_k_tlast s_axis_user_k_tvalid} xilinx.com:interface:axis_rtl:1.0 [ipx::current_core]
ipx::add_port_map RST [ipx::get_bus_interfaces S_AXI_RST -of_objects [ipx::current_core]]
set_property physical_name s_axi_aresetn [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces S_AXI_RST -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces S_AXI_CLK -of_objects [ipx::current_core]]
set_property physical_name s_axi_aclk [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces S_AXI_CLK -of_objects [ipx::current_core]]]
ipx::add_port_map RST [ipx::get_bus_interfaces M_AXIS_USER_K_RST -of_objects [ipx::current_core]]
set_property physical_name m_axis_user_k_aresetn [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces M_AXIS_USER_K_RST -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces M_AXIS_USER_K_CLK -of_objects [ipx::current_core]]
set_property physical_name m_axis_user_k_aclk [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces M_AXIS_USER_K_CLK -of_objects [ipx::current_core]]]
ipx::add_port_map RST [ipx::get_bus_interfaces S_AXIS_DATA_RST -of_objects [ipx::current_core]]
set_property physical_name s_axis_data_aresetn [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces S_AXIS_DATA_RST -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces S_AXIS_DATA_CLK -of_objects [ipx::current_core]]
set_property physical_name s_axis_data_aclk [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces S_AXIS_DATA_CLK -of_objects [ipx::current_core]]]
ipx::add_port_map RST [ipx::get_bus_interfaces S_AXIS_USER_K_RST -of_objects [ipx::current_core]]
set_property physical_name s_axis_user_k_aresetn [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces S_AXIS_USER_K_RST -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces S_AXIS_USER_K_CLK -of_objects [ipx::current_core]]
set_property physical_name s_axis_user_k_aclk [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces S_AXIS_USER_K_CLK -of_objects [ipx::current_core]]]
ipx::add_port_map RST [ipx::get_bus_interfaces M_AXIS_DATA_RST -of_objects [ipx::current_core]]
set_property physical_name m_axis_data_aresetn [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces M_AXIS_DATA_RST -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces M_AXIS_DATA_CLK -of_objects [ipx::current_core]]
set_property physical_name m_axis_data_aclk [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces M_AXIS_DATA_CLK -of_objects [ipx::current_core]]]


ipx::infer_bus_interface fifo_clk_slower xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface fifo_clk_faster xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]


ipx::add_bus_interface S_TX_DSP_CH1 [ipx::current_core]
set_property abstraction_type_vlnv cern.ch:user:TX_DSP_rtl:1.0 [ipx::get_bus_interfaces S_TX_DSP_CH1 -of_objects [ipx::current_core]]
set_property bus_type_vlnv cern.ch:user:TX_DSP:1.0 [ipx::get_bus_interfaces S_TX_DSP_CH1 -of_objects [ipx::current_core]]
ipx::add_port_map tx_dsp_data [ipx::get_bus_interfaces S_TX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_tx_dsp_ch1_data [ipx::get_port_maps tx_dsp_data -of_objects [ipx::get_bus_interfaces S_TX_DSP_CH1 -of_objects [ipx::current_core]]]
ipx::add_port_map tx_dsp_valid [ipx::get_bus_interfaces S_TX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_tx_dsp_ch1_valid [ipx::get_port_maps tx_dsp_valid -of_objects [ipx::get_bus_interfaces S_TX_DSP_CH1 -of_objects [ipx::current_core]]]
set_property description {Slave interface for connecting GBLink TX logic with the DSP.} [ipx::get_bus_interfaces S_TX_DSP_CH1 -of_objects [ipx::current_core]]

ipx::add_bus_interface S_TX_DSP_CH2 [ipx::current_core]
set_property abstraction_type_vlnv cern.ch:user:TX_DSP_rtl:1.0 [ipx::get_bus_interfaces S_TX_DSP_CH2 -of_objects [ipx::current_core]]
set_property bus_type_vlnv cern.ch:user:TX_DSP:1.0 [ipx::get_bus_interfaces S_TX_DSP_CH2 -of_objects [ipx::current_core]]
ipx::add_port_map tx_dsp_data [ipx::get_bus_interfaces S_TX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_tx_dsp_ch2_data [ipx::get_port_maps tx_dsp_data -of_objects [ipx::get_bus_interfaces S_TX_DSP_CH2 -of_objects [ipx::current_core]]]
ipx::add_port_map tx_dsp_valid [ipx::get_bus_interfaces S_TX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_tx_dsp_ch2_valid [ipx::get_port_maps tx_dsp_valid -of_objects [ipx::get_bus_interfaces S_TX_DSP_CH2 -of_objects [ipx::current_core]]]
set_property description {Slave interface for connecting GBLink TX logic with the DSP.} [ipx::get_bus_interfaces S_TX_DSP_CH2 -of_objects [ipx::current_core]]

ipx::add_bus_interface S_TX_NCO [ipx::current_core]
set_property abstraction_type_vlnv cern.ch:user:TX_NCO_rtl:1.0 [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]
set_property bus_type_vlnv cern.ch:user:TX_NCO:1.0 [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]
set_property interface_mode master [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]
set_property interface_mode slave [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]
ipx::add_port_map tx_nco_id [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]
set_property physical_name s_tx_nco_id [ipx::get_port_maps tx_nco_id -of_objects [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]]
ipx::add_port_map tx_nco_turn_number [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]
set_property physical_name s_tx_nco_turn_number [ipx::get_port_maps tx_nco_turn_number -of_objects [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]]
ipx::add_port_map tx_nco_frev [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]
set_property physical_name s_tx_nco_frev [ipx::get_port_maps tx_nco_frev -of_objects [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]]
set_property description {Slave interface for connecting GBLink TX logic with the NCO.} [ipx::get_bus_interfaces S_TX_NCO -of_objects [ipx::current_core]]

ipx::add_bus_interface S_RX_DSP_CH1 [ipx::current_core]
set_property abstraction_type_vlnv cern.ch:user:RX_DSP_rtl:1.0 [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
set_property bus_type_vlnv cern.ch:user:RX_DSP:1.0 [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
ipx::add_port_map rx_dsp_valid [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch1_valid [ipx::get_port_maps rx_dsp_valid -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_frev_flag [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch1_frev_flag [ipx::get_port_maps rx_dsp_frev_flag -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_turn_error [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch1_turn_error [ipx::get_port_maps rx_dsp_turn_error -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_data [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch1_data [ipx::get_port_maps rx_dsp_data -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_full_error [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch1_full_error [ipx::get_port_maps rx_dsp_full_error -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_read [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch1_read [ipx::get_port_maps rx_dsp_read -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH1 -of_objects [ipx::current_core]]]

ipx::add_bus_interface S_RX_DSP_CH2 [ipx::current_core]
set_property abstraction_type_vlnv cern.ch:user:RX_DSP_rtl:1.0 [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
set_property bus_type_vlnv cern.ch:user:RX_DSP:1.0 [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
ipx::add_port_map rx_dsp_valid [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch2_valid [ipx::get_port_maps rx_dsp_valid -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_frev_flag [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch2_frev_flag [ipx::get_port_maps rx_dsp_frev_flag -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_turn_error [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch2_turn_error [ipx::get_port_maps rx_dsp_turn_error -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_data [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch2_data [ipx::get_port_maps rx_dsp_data -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_full_error [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch2_full_error [ipx::get_port_maps rx_dsp_full_error -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]]
ipx::add_port_map rx_dsp_read [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]
set_property physical_name s_rx_dsp_ch2_read [ipx::get_port_maps rx_dsp_read -of_objects [ipx::get_bus_interfaces S_RX_DSP_CH2 -of_objects [ipx::current_core]]]

current_project managed_ip_project
ipx::open_ipxact_file c:/Users/pkuzmano/Work/clean_git/GBlink_rtm_8sfp/ip_repo/RX_DSP.xml
ipx::unload_abstraction_definition c:/Users/pkuzmano/Work/clean_git/GBlink_rtm_8sfp/ip_repo/RX_DSP_rtl.xml
ipx::unload_bus_definition c:/Users/pkuzmano/Work/clean_git/GBlink_rtm_8sfp/ip_repo/RX_DSP.xml
current_project GBLink_v1_0_project
set_property description {Slave interface for connecting GBLink RX logic with the DSP.} [ipx::get_bus_interfaces S_RX_DSP -of_objects [ipx::current_core]]
set_property description {} [ipx::get_bus_interfaces S_RX_DSP -of_objects [ipx::current_core]]
ipx::add_port_map rx_id [ipx::get_bus_interfaces S_RX_DSP -of_objects [ipx::current_core]]
set_property physical_name s_rx_id [ipx::get_port_maps rx_id -of_objects [ipx::get_bus_interfaces S_RX_DSP -of_objects [ipx::current_core]]]

ipx::infer_bus_interface user_clk_faster xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface user_rst_faster xilinx.com:signal:reset_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface user_clk_slower xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface user_rst_slower xilinx.com:signal:reset_rtl:1.0 [ipx::current_core]

Slave interface for connecting GBLink RX logic with the DSP.

